package com.mashibing;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;


public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        Person bean = beanFactory.getBean(Person.class);
        System.out.println(bean);
        System.out.println("测试执行 BeanFactoryPostProcessor");
    }
}
