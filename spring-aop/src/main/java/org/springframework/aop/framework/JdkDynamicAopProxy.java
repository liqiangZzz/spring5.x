/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.framework;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.aop.AopInvocationException;
import org.springframework.aop.RawTargetAccess;
import org.springframework.aop.TargetSource;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.DecoratingProxy;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

/**
 * AopProxy的子类。使用JDK的方式创建代理对象。它持有Advised对象。
 *
 * JDK-based {@link AopProxy} implementation for the Spring AOP framework,
 * based on JDK {@link java.lang.reflect.Proxy dynamic proxies}.
 *
 * <p>Creates a dynamic proxy, implementing the interfaces exposed by
 * the AopProxy. Dynamic proxies <i>cannot</i> be used to proxy methods
 * defined in classes, rather than interfaces.
 *
 * <p>Objects of this type should be obtained through proxy factories,
 * configured by an {@link AdvisedSupport} class. This class is internal
 * to Spring's AOP framework and need not be used directly by client code.
 *
 * <p>Proxies created using this class will be thread-safe if the
 * underlying (target) class is thread-safe.
 *
 * <p>Proxies are serializable so long as all Advisors (including Advices
 * and Pointcuts) and the TargetSource are serializable.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Rob Harrop
 * @author Dave Syer
 * @see java.lang.reflect.Proxy
 * @see AdvisedSupport
 * @see ProxyFactory
 */
final class JdkDynamicAopProxy implements AopProxy, InvocationHandler, Serializable {

	/** use serialVersionUID from Spring 1.2 for interoperability. */
	private static final long serialVersionUID = 5531744639992436476L;


	/*
	 * NOTE: We could avoid the code duplication between this class and the CGLIB
	 * proxies by refactoring "invoke" into a template method. However, this approach
	 * adds at least 10% performance overhead versus a copy-paste solution, so we sacrifice
	 * elegance for performance. (We have a good test suite to ensure that the different
	 * proxies behave the same :-)
	 * This way, we can also more easily take advantage of minor optimizations in each class.
	 */

	/** We use a static Log to avoid serialization issues. */
	private static final Log logger = LogFactory.getLog(JdkDynamicAopProxy.class);

	/** AOP配置
	 * Config used to configure this proxy.
	 */
	private final AdvisedSupport advised;

	/**
	 * 是否在代理接口上定义了 equals方法
	 * Is the {@link #equals} method defined on the proxied interfaces?
	 */
	private boolean equalsDefined;

	/**
	 * 是否在代理接口上定义了 hashCode 方法
	 * Is the {@link #hashCode} method defined on the proxied interfaces?
	 */
	private boolean hashCodeDefined;


	/**
	 * 为给定的AOP配置构造一个新的JdkDynamicAopProxy
	 * Construct a new JdkDynamicAopProxy for the given AOP configuration.
	 * @param config the AOP configuration as AdvisedSupport object  AdvisedSupport对象的AOP配置
	 * @throws AopConfigException if the config is invalid. We try to throw an informative
	 * exception in this case, rather than let a mysterious failure happen later.
	 */
	public JdkDynamicAopProxy(AdvisedSupport config) throws AopConfigException {
		Assert.notNull(config, "AdvisedSupport must not be null");
		// 如果不存在Advisor或者目标对象为空的话,抛出异常
		if (config.getAdvisors().length == 0 && config.getTargetSource() == AdvisedSupport.EMPTY_TARGET_SOURCE) {
			throw new AopConfigException("No advisors and no TargetSource specified");
		}
		this.advised = config;
	}


	@Override
	public Object getProxy() {
		// 使用默认的类加载器
		return getProxy(ClassUtils.getDefaultClassLoader());
	}

	@Override
	public Object getProxy(@Nullable ClassLoader classLoader) {
		if (logger.isTraceEnabled()) {
			logger.trace("Creating JDK dynamic proxy: " + this.advised.getTargetSource());
		}
		// 获取AdvisedSupport类型对象的所有接口，主要目的是判断如果interfaces集合（evaluateProxyInterfaces方法中加入的接口集合）中没有
		// SpringProxy、Advised、DecoratingProxy这些接口，没有则尝试将SpringProxy、Advised、DecoratingProxy
		// 这三个接口添加到数组后三位，这表示新创建代理对象将实现这些接口
		Class<?>[] proxiedInterfaces = AopProxyUtils.completeProxiedInterfaces(this.advised, true);
		// 接口是否定义了equals和hashcode方法,正常是没有的
		findDefinedEqualsAndHashCodeMethods(proxiedInterfaces);
		// 创建代理对象 this是JdkDynamicAopProxy
		// JdkDynamicAopProxy同时实现了InvocationHandler接口
		// 当调用代理对象的方法时，将会方法调用被分派到调用处理程序的invoke方法，即JdkDynamicAopProxy对象的invoke方法
		// 返回的对象实现了proxiedInterfaces数组中的所有接口，因此这个代理对象可以被安全地转换为这些接口的任何实例
		return Proxy.newProxyInstance(classLoader, proxiedInterfaces, this);
	}

	/**
	 * 查找可能在提供的接口集上定义是否存在自定义的equals和hashCode方法
	 * Finds any {@link #equals} or {@link #hashCode} method that may be defined
	 * on the supplied set of interfaces.
	 * @param proxiedInterfaces the interfaces to introspect
	 */
	private void findDefinedEqualsAndHashCodeMethods(Class<?>[] proxiedInterfaces) {
		// 遍历接口
		for (Class<?> proxiedInterface : proxiedInterfaces) {
			Method[] methods = proxiedInterface.getDeclaredMethods();
			//获取当前接口自己的所有方法数组
			for (Method method : methods) {
				//如果存在equals方法，那么equalsDefined设置为true
				if (AopUtils.isEqualsMethod(method)) {
					this.equalsDefined = true;
				}
				//如果存在hashCode方法，那么hashCodeDefined设置为true
				if (AopUtils.isHashCodeMethod(method)) {
					this.hashCodeDefined = true;
				}
				if (this.equalsDefined && this.hashCodeDefined) {
					return;
				}
			}
		}
	}


	/**
	 * JDK动态代理的核心部分，主要功能是拦截并处理对代理对象方法的所有调用
	 * InvocationHandler.invoke 的实现<p> 调用程序将准确地看到目标抛出的异常，除非钩子方法抛出异常
	 * Implementation of {@code InvocationHandler.invoke}.
	 * <p>Callers will see exactly the exception thrown by the target,
	 * unless a hook method throws an exception.
	 */
	@Override
	@Nullable
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object oldProxy = null;
		//是否设置代理上下文
		boolean setProxyContext = false;

		// 获取到我们的目标对象
		TargetSource targetSource = this.advised.targetSource;
		Object target = null;

		try {
			// 如果代理接口集中不存在equals方法，并且当前拦截的方法是equals方法
			// 这说明目标类没有实现接口的equals方法本身
			if (!this.equalsDefined && AopUtils.isEqualsMethod(method)) {
				// The target does not implement the equals(Object) method itself.
				//目标本身未实现equals（Object）方法，调用JdkDynamicAopProxy自己的equals方法比较，因此可能会产生歧义
				return equals(args[0]);
			}
			// 如果代理接口集中不存在hashCode方法，并且当前拦截的方法是hashCode方法，
			// 这说明目标类没有实现接口的hashCode方法本身
			else if (!this.hashCodeDefined && AopUtils.isHashCodeMethod(method)) {
				// The target does not implement the hashCode() method itself.
				//目标本身未实现 hashCode（Object）方法，调用JdkDynamicAopProxy自己的 hashCode 方法比较，因此可能会产生歧义
				return hashCode();
			}
			// 若是DecoratingProxy也不要拦截器执行
			else if (method.getDeclaringClass() == DecoratingProxy.class) {
				// There is only getDecoratedClass() declared -> dispatch to proxy config.
				//直接调用AopProxyUtils.ultimateTargetClass方法返回代理的最终目标类型
				//并没有调用实现的getDecoratedClass方法
				return AopProxyUtils.ultimateTargetClass(this.advised);
			}
			//opaque标志通常指示代理是否对目标方法调用透明。如果为false，意味着我们可能需要特殊处理以保持调用的透明性。
			//method.getDeclaringClass().isInterface(): 确认当前被调用方法所属的类（即声明该方法的类）是一个接口。这与Java动态代理机制相关，因为JDK动态代理要求代理的类必须实现至少一个接口。
			// method.getDeclaringClass().isAssignableFrom(Advised.class): 检查当前方法声明的类是否可以被赋值给类型Advised或者就是Advised类本身。这意味着该接口或其超接口中包含了Advised接口，这在Spring框架中用于标识可进行AOP增强的对象。
			else if (!this.advised.opaque && method.getDeclaringClass().isInterface() &&
					method.getDeclaringClass().isAssignableFrom(Advised.class)) {
				// Service invocations on ProxyConfig with the proxy config...
				//直接反射调用原始方法
				return AopUtils.invokeJoinpointUsingReflection(this.advised, method, args);
			}

			Object retVal;

			/*
			 * 如果exposeProxy属性为true（默认false），表示需要暴露代理对象
			 * 可通过<aop:config/>标签的expose-proxy属性或者@EnableAspectJAutoProxy注解的exposeProxy属性控制
			 */
			if (this.advised.exposeProxy) {
				// Make invocation available if necessary.
				// 把我们的代理对象暴露到线程变量中
				oldProxy = AopContext.setCurrentProxy(proxy);
				setProxyContext = true;
			}

			// Get as late as possible to minimize the time we "own" the target,
			// in case it comes from a pool.
			// 获取我们的目标对象，被代理对象
			target = targetSource.getTarget();
			// 获取我们目标对象的class
			Class<?> targetClass = (target != null ? target.getClass() : null);

			// Get the interception chain for this method.
			// 从Advised中根据方法名和目标类获取AOP拦截器执行链
			List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);

			// Check whether we have any advice. If we don't, we can fallback on direct
			// reflective invocation of the target, and avoid creating a MethodInvocation.
			// 如果拦截器链为空
			if (chain.isEmpty()) {
				// We can skip creating a MethodInvocation: just invoke the target directly
				// Note that the final invoker must be an InvokerInterceptor so we know it does
				// nothing but a reflective operation on the target, and no hot swapping or fancy proxying.
				// 检查并调整args参数，以适应method方法的参数列表。在某些情况下，如代理与目标方法的参数类型不完全匹配时，此方法会进行必要的转换，确保调用的正确性。
				Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
				//使用反射来直接调用目标对象target的method方法，并传入经过适配的参数argsToUse。
				retVal = AopUtils.invokeJoinpointUsingReflection(target, method, argsToUse);
			}
			//如果针对当前方法的拦截器链不为空
			else {
				// We need to create a method invocation...
				// 将拦截器封装在ReflectiveMethodInvocation，以便于使用其proceed进行处理
				MethodInvocation invocation =
						new ReflectiveMethodInvocation(proxy, target, method, args, targetClass, chain);
				// Proceed to the joinpoint through the interceptor chain.
				// 执行拦截器链，调用proceed方法，进行方法的代理和增强，一般的AOP都是通过该方法执行的
				retVal = invocation.proceed();
			}

			// Massage return value if necessary.
			// 获取返回类型
			Class<?> returnType = method.getReturnType();
			//如果执行结果不为null && 返回值等于目标对象 && 返回值类型不是Object && 返回值类型和代理类型兼容
			//&& 方法所属的类的类型不是RawTargetAccess类型及其子类型
			if (retVal != null && retVal == target &&
					returnType != Object.class && returnType.isInstance(proxy) &&
					!RawTargetAccess.class.isAssignableFrom(method.getDeclaringClass())) {
				// Special case: it returned "this" and the return type of the method
				// is type-compatible. Note that we can't help if the target sets
				// a reference to itself in another returned object.
				//返回值retVal是一个指向自身（this）的引用，且返回类型与代理对象兼容。
				retVal = proxy;
			}
			//如果返回值为null，并且返回值类型不是void，并且返回值类型是基本类型，那么抛出异常
			else if (retVal == null && returnType != Void.TYPE && returnType.isPrimitive()) {
				throw new AopInvocationException(
						"Null return value from advice does not match primitive return type for: " + method);
			}
			return retVal;
		}
		finally {
			// 如果目标对象不为空且目标对象是可变的,如prototype类型
			// 通常我们的目标对象都是单例的,即targetSource.isStatic为true
			if (target != null && !targetSource.isStatic()) {
				// Must have come from TargetSource.
				// 释放目标对象
				targetSource.releaseTarget(target);
			}
			if (setProxyContext) {
				// Restore old proxy.
				// 线程上下文复位
				AopContext.setCurrentProxy(oldProxy);
			}
		}
	}


	/**
	 * Equality means interfaces, advisors and TargetSource are equal.
	 * <p>The compared object may be a JdkDynamicAopProxy instance itself
	 * or a dynamic proxy wrapping a JdkDynamicAopProxy instance.
	 */
	@Override
	public boolean equals(@Nullable Object other) {
		if (other == this) {
			return true;
		}
		if (other == null) {
			return false;
		}

		JdkDynamicAopProxy otherProxy;
		if (other instanceof JdkDynamicAopProxy) {
			otherProxy = (JdkDynamicAopProxy) other;
		}
		else if (Proxy.isProxyClass(other.getClass())) {
			InvocationHandler ih = Proxy.getInvocationHandler(other);
			if (!(ih instanceof JdkDynamicAopProxy)) {
				return false;
			}
			otherProxy = (JdkDynamicAopProxy) ih;
		}
		else {
			// Not a valid comparison...
			return false;
		}

		// If we get here, otherProxy is the other AopProxy.
		return AopProxyUtils.equalsInProxy(this.advised, otherProxy.advised);
	}

	/**
	 * Proxy uses the hash code of the TargetSource.
	 */
	@Override
	public int hashCode() {
		return JdkDynamicAopProxy.class.hashCode() * 13 + this.advised.getTargetSource().hashCode();
	}

}
