/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.aop.aspectj.autoproxy.AspectJAwareAdvisorAutoProxyCreator;
import org.springframework.aop.framework.autoproxy.InfrastructureAdvisorAutoProxyCreator;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.Ordered;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

/**
 * AOP配置工具类,主要是向Spring容器中注入可以生成Advisor和创建代理对象的bean
 *
 * 按升序顺序存储的自动代理创建者的类型集合
 * 可以看到，默认有三种类型，优先级就是比较索引顺序的大小，因此优先级为：
 * InfrastructureAdvisorAutoProxyCreator < AspectJAwareAdvisorAutoProxyCreator < AnnotationAwareAspectJAutoProxyCreator
 * <p>
 * 如果是解析<tx:annotation-driven />标签或者@EnableTransactionManagement事物注解，那么cls参数是InfrastructureAdvisorAutoProxyCreator.class
 * 如果是解析<aop:config />标签，那么cls参数是AspectJAwareAdvisorAutoProxyCreator.class
 * 如果是解析<aop:aspectj-autoproxy />标签或者@EnableAspectJAutoProxy注解，那么cls参数是AnnotationAwareAspectJAutoProxyCreator.class
 *
 * Utility class for handling registration of AOP auto-proxy creators.
 *
 * <p>Only a single auto-proxy creator should be registered yet multiple concrete
 * implementations are available. This class provides a simple escalation protocol,
 * allowing a caller to request a particular auto-proxy creator and know that creator,
 * <i>or a more capable variant thereof</i>, will be registered as a post-processor.
 *
 * @author Rob Harrop
 * @author Juergen Hoeller
 * @author Mark Fisher
 * @since 2.5
 * @see AopNamespaceUtils
 */
public abstract class AopConfigUtils {

	/**
	 * 内部管理的自动代理创建者的bean名称
	 * The bean name of the internally managed auto-proxy creator.
	 */
	public static final String AUTO_PROXY_CREATOR_BEAN_NAME =
			"org.springframework.aop.config.internalAutoProxyCreator";

	/**
	 * Stores the auto proxy creator classes in escalation order.
	 */
	private static final List<Class<?>> APC_PRIORITY_LIST = new ArrayList<>(3);

	/**
	 * APC_PRIORITY_LIST集合在AopConfigUtils类加载的时候就在静态块中按顺序初始化了一系列的自动代理创建者的类型，索引位置就是优先级。
	 * 因此优先级大小为：InfrastructureAdvisorAutoProxyCreator.class < AspectJAwareAdvisorAutoProxyCreator.class < AnnotationAwareAspectJAutoProxyCreator.class。
	 * 如果是解析< tx:annotation-driven />标签或者 @EnableTransactionManagement事物注解，那么cls参数是InfrastructureAdvisorAutoProxyCreator.class；
	 * 如果是解析< aop:config />标签，那么cls参数是AspectJAwareAdvisorAutoProxyCreator.class；
	 * 如果是解析< aop:aspectj-autoproxy />标签或者 @EnableAspectJAutoProxy注解，那么cls参数是AnnotationAwareAspectJAutoProxyCreator.class。
	 *
	 * 如果我们设置了< aop:config />和< aop:aspectj-autoproxy />两个标签，那么最终会注册AnnotationAwareAspectJAutoProxyCreator类型的自动代理创建者。
	 */
	static {
		// Set up the escalation list...
		APC_PRIORITY_LIST.add(InfrastructureAdvisorAutoProxyCreator.class);
		APC_PRIORITY_LIST.add(AspectJAwareAdvisorAutoProxyCreator.class);
		APC_PRIORITY_LIST.add(AnnotationAwareAspectJAutoProxyCreator.class);
	}


	@Nullable
	public static BeanDefinition registerAutoProxyCreatorIfNecessary(BeanDefinitionRegistry registry) {
		return registerAutoProxyCreatorIfNecessary(registry, null);
	}

	/**
	 * <tx:annotation-driven/>以及@EnableTransactionManagement使用InfrastructureAdvisorAutoProxyCreator 创建代理
	 * @param registry
	 * @param source
	 * @return
	 */
	@Nullable
	public static BeanDefinition registerAutoProxyCreatorIfNecessary(
			BeanDefinitionRegistry registry, @Nullable Object source) {

		return registerOrEscalateApcAsRequired(InfrastructureAdvisorAutoProxyCreator.class, registry, source);
	}

	@Nullable
	public static BeanDefinition registerAspectJAutoProxyCreatorIfNecessary(BeanDefinitionRegistry registry) {
		return registerAspectJAutoProxyCreatorIfNecessary(registry, null);
	}

	/**
	 * 尝试注册或者升级一个名为"org.springframework.aop.config.internalAutoProxyCreator"，
	 * 类型为AspectJAwareAdvisorAutoProxyCreator的自动代理创建者的bean定义
	 *  <aop:config/>标签使用AspectJAwareAdvisorAutoProxyCreator创建代理
	 * @param registry
	 * @param source
	 * @return
	 */
	@Nullable
	public static BeanDefinition registerAspectJAutoProxyCreatorIfNecessary(
			BeanDefinitionRegistry registry, @Nullable Object source) {

		return registerOrEscalateApcAsRequired(AspectJAwareAdvisorAutoProxyCreator.class, registry, source);
	}

	@Nullable
	public static BeanDefinition registerAspectJAnnotationAutoProxyCreatorIfNecessary(BeanDefinitionRegistry registry) {
		return registerAspectJAnnotationAutoProxyCreatorIfNecessary(registry, null);
	}

	/**
	 * <aop:aspectj-autoproxy/>以及@EnableAspectJAutoProxy使用AnnotationAwareAspectJAutoProxyCreator 创建代理
	 * @param registry
	 * @param source
	 * @return
	 */
	@Nullable
	public static BeanDefinition registerAspectJAnnotationAutoProxyCreatorIfNecessary(
			BeanDefinitionRegistry registry, @Nullable Object source) {

		return registerOrEscalateApcAsRequired(AnnotationAwareAspectJAutoProxyCreator.class, registry, source);
	}

	/**
	 *  强迫AutoProxyCreator使用基于类的代理，也就是CGLIB代理
	 * @param registry
	 */
	public static void forceAutoProxyCreatorToUseClassProxying(BeanDefinitionRegistry registry) {
		//如果包含名为"org.springframework.aop.config.internalAutoProxyCreator"的bean定义
		if (registry.containsBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME)) {
			//获取该bean定义
			BeanDefinition definition = registry.getBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME);
			//添加属性proxyTargetClass设置值为true，表示强制使用CGLIB代理
			definition.getPropertyValues().add("proxyTargetClass", Boolean.TRUE);
		}
	}

	/**
	 * 强迫AutoProxyCreator暴露代理对象
	 * @param registry
	 */
	public static void forceAutoProxyCreatorToExposeProxy(BeanDefinitionRegistry registry) {
		//如果包含名为"org.springframework.aop.config.internalAutoProxyCreator"的bean定义
		if (registry.containsBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME)) {
			//获取该bean定义
			BeanDefinition definition = registry.getBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME);
			//添加属性exposeProxy，设置值为true，表示强制暴露代理对象
			definition.getPropertyValues().add("exposeProxy", Boolean.TRUE);
		}
	}

	/**
	 *  尝试注册或者升级一个名为"org.springframework.aop.config.internalAutoProxyCreator"，
	 *  类型为AspectJAwareAdvisorAutoProxyCreator的自动代理创建者的bean定义
	 * @param cls 自动代理创建者的class，用于比较优先级或者创建bean定义
	 * @param registry
	 * @param source
	 * @return
	 */
	@Nullable
	private static BeanDefinition registerOrEscalateApcAsRequired(
			Class<?> cls, BeanDefinitionRegistry registry, @Nullable Object source) {

		Assert.notNull(registry, "BeanDefinitionRegistry must not be null");

		// 如果已经存在了自动代理创建器且存在的自动代理创建器与现在不一致，那么需要根据优先级来判断到底需要使用哪个
		if (registry.containsBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME)) {
			BeanDefinition apcDefinition = registry.getBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME);
			if (!cls.getName().equals(apcDefinition.getBeanClassName())) {
				//获取当前bean定义中的自动代理创建者的类型优先级，实际上就是存储在 APC_PRIORITY_LIST 集合的索引位置
				int currentPriority = findPriorityForClass(apcDefinition.getBeanClassName());
				//获取当前参数传递的自动代理创建者的类型优先级，实际上就是存储在 APC_PRIORITY_LIST 集合的索引位置
				int requiredPriority = findPriorityForClass(cls);
				//如果bean定义中的自动代理创建者的类型优先级 小于 当前参数传递的自动代理创建者的类型优先级
				if (currentPriority < requiredPriority) {
					// 改变bean所对应的className的属性
					apcDefinition.setBeanClassName(cls.getName());
				}
			}
			// 如果已经存在自动代理创建器并且与将要创建的一致，那么无须再次创建
			return null;
		}

		RootBeanDefinition beanDefinition = new RootBeanDefinition(cls);
		beanDefinition.setSource(source);
		// 设置order属性优先级最高，将会最先被应用并尝试创建代理对象
		beanDefinition.getPropertyValues().add("order", Ordered.HIGHEST_PRECEDENCE);
		beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		//调用registerBeanDefinition方法注册这个新的BeanDefinition到注册表的缓存中，名为"org.springframework.aop.config.internalAutoProxyCreator
		registry.registerBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME, beanDefinition);
		return beanDefinition;
	}

	private static int findPriorityForClass(Class<?> clazz) {
		return APC_PRIORITY_LIST.indexOf(clazz);
	}

	private static int findPriorityForClass(@Nullable String className) {
		for (int i = 0; i < APC_PRIORITY_LIST.size(); i++) {
			Class<?> clazz = APC_PRIORITY_LIST.get(i);
			if (clazz.getName().equals(className)) {
				return i;
			}
		}
		throw new IllegalArgumentException(
				"Class name [" + className + "] is not a known auto-proxy creator class");
	}

}
