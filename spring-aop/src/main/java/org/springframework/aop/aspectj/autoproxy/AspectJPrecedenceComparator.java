/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.aspectj.autoproxy;

import java.util.Comparator;

import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJAopUtils;
import org.springframework.aop.aspectj.AspectJPrecedenceInformation;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.util.Assert;

/**
 * Orders AspectJ advice/advisors by precedence (<i>not</i> invocation order).
 *
 * <p>Given two pieces of advice, {@code A} and {@code B}:
 * <ul>
 * <li>If {@code A} and {@code B} are defined in different aspects, then the advice
 * in the aspect with the lowest order value has the highest precedence.</li>
 * <li>If {@code A} and {@code B} are defined in the same aspect, if one of
 * {@code A} or {@code B} is a form of <em>after</em> advice, then the advice declared
 * last in the aspect has the highest precedence. If neither {@code A} nor {@code B}
 * is a form of <em>after</em> advice, then the advice declared first in the aspect
 * has the highest precedence.</li>
 * </ul>
 *
 * <p>Important: This comparator is used with AspectJ's
 * {@link org.aspectj.util.PartialOrder PartialOrder} sorting utility. Thus, unlike
 * a normal {@link Comparator}, a return value of {@code 0} from this comparator
 * means we don't care about the ordering, not that the two elements must be sorted
 * identically.
 *
 * @author Adrian Colyer
 * @author Juergen Hoeller
 * @since 2.0
 */
class AspectJPrecedenceComparator implements Comparator<Advisor> {

	/**
	 * 更高优先级
	 */
	private static final int HIGHER_PRECEDENCE = -1;

	/**
	 * 相同的优先级
	 */
	private static final int SAME_PRECEDENCE = 0;

	/**
	 * 较低优先级
	 */
	private static final int LOWER_PRECEDENCE = 1;


	private final Comparator<? super Advisor> advisorComparator;


	/**
	 * Create a default {@code AspectJPrecedenceComparator}.
	 */
	public AspectJPrecedenceComparator() {
		this.advisorComparator = AnnotationAwareOrderComparator.INSTANCE;
	}

	/**
	 * Create an {@code AspectJPrecedenceComparator}, using the given {@link Comparator}
	 * for comparing {@link org.springframework.aop.Advisor} instances.
	 * @param advisorComparator the {@code Comparator} to use for advisors
	 */
	public AspectJPrecedenceComparator(Comparator<? super Advisor> advisorComparator) {
		Assert.notNull(advisorComparator, "Advisor comparator must not be null");
		this.advisorComparator = advisorComparator;
	}

	/**
	 * 比较两个Advisor对象的优先级
	 */
	@Override
	public int compare(Advisor o1, Advisor o2) {
		int advisorPrecedence = this.advisorComparator.compare(o1, o2);
		// 如果它们的优先级相同，并且它们被声明在同一个切面中
		if (advisorPrecedence == SAME_PRECEDENCE && declaredInSameAspect(o1, o2)) {
			advisorPrecedence = comparePrecedenceWithinAspect(o1, o2);
		}
		return advisorPrecedence;
	}

	/**
	 * 比较两个切面顾问（Advisor）的优先级顺序
	 */
	private int comparePrecedenceWithinAspect(Advisor advisor1, Advisor advisor2) {
		//两个Advisor 中是否包含后置顾问（afterAdvice）
		boolean oneOrOtherIsAfterAdvice =
				(AspectJAopUtils.isAfterAdvice(advisor1) || AspectJAopUtils.isAfterAdvice(advisor2));
		/**
		 * 获取切面声明的顺序，该函数返回一个整数值，表示两个顾问的优先级关系。
		 * 1、如果其中一个顾问是后置顾问，则后声明的顾问具有更高优先级。
		 * 2、如果两个顾问都不是后置顾问，则先声明的顾问具有更高优先级。
		 * 3、返回值为负数表示第一个顾问优先级较低，
		 * 4、零表示两个顾问优先级相同，
		 * 5、正数表示第一个顾问优先级较高。
		 */
		int adviceDeclarationOrderDelta = getAspectDeclarationOrder(advisor1) - getAspectDeclarationOrder(advisor2);

		if (oneOrOtherIsAfterAdvice) {
			// the advice declared last has higher precedence
			if (adviceDeclarationOrderDelta < 0) {
				// advice1 was declared before advice2
				// so advice1 has lower precedence
				return LOWER_PRECEDENCE;
			}
			else if (adviceDeclarationOrderDelta == 0) {
				return SAME_PRECEDENCE;
			}
			else {
				return HIGHER_PRECEDENCE;
			}
		}
		else {
			// the advice declared first has higher precedence
			if (adviceDeclarationOrderDelta < 0) {
				// advice1 was declared before advice2
				// so advice1 has higher precedence
				return HIGHER_PRECEDENCE;
			}
			else if (adviceDeclarationOrderDelta == 0) {
				return SAME_PRECEDENCE;
			}
			else {
				return LOWER_PRECEDENCE;
			}
		}
	}

	/**
	 * 判断两个Advisor是否在同一个切面中声明
	 */
	private boolean declaredInSameAspect(Advisor advisor1, Advisor advisor2) {
		//判断advisor1和advisor2是否都具有切面名称
		return (hasAspectName(advisor1) && hasAspectName(advisor2) &&
				//如果都具有切面名称，则获取它们的切面名称（通过调用getAspectName方法）并进行比较；
				getAspectName(advisor1).equals(getAspectName(advisor2)));
	}

	/**
	 * 函数首先判断advisor是否是AspectJPrecedenceInformation类型，如果是则返回true。
	 * 如果不是，则通过advisor对象的getAdvice()方法获取其包含的切面信息对象，
	 * 并判断该对象是否是AspectJPrecedenceInformation类型，如果是则返回true，否则返回false。
	 */
	private boolean hasAspectName(Advisor advisor) {
		return (advisor instanceof AspectJPrecedenceInformation ||
				advisor.getAdvice() instanceof AspectJPrecedenceInformation);
	}

	/**
	 * 前提条件是hasAspectName返回true
	 * 用于获取给定Advisor对象所关联的AspectJ切面的名称。
	 * pre-condition is that hasAspectName returned true
	 */
	private String getAspectName(Advisor advisor) {
		AspectJPrecedenceInformation precedenceInfo = AspectJAopUtils.getAspectJPrecedenceInformationFor(advisor);
		Assert.state(precedenceInfo != null, () -> "Unresolvable AspectJPrecedenceInformation for " + advisor);
		return precedenceInfo.getAspectName();
	}

	/**
	 * 获取一个顾问（Advisor）的切面声明顺序
	 */
	private int getAspectDeclarationOrder(Advisor advisor) {
		AspectJPrecedenceInformation precedenceInfo = AspectJAopUtils.getAspectJPrecedenceInformationFor(advisor);
		return (precedenceInfo != null ? precedenceInfo.getDeclarationOrder() : 0);
	}

}
