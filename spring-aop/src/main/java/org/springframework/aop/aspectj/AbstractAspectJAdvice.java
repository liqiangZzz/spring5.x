/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.aspectj;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.weaver.tools.JoinPointMatch;
import org.aspectj.weaver.tools.PointcutParameter;

import org.springframework.aop.AopInvocationException;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.aop.interceptor.ExposeInvocationInterceptor;
import org.springframework.aop.support.ComposablePointcut;
import org.springframework.aop.support.MethodMatchers;
import org.springframework.aop.support.StaticMethodMatcher;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

/**
 * 使用AspectJ注解的通知类型顶级父类
 *
 * Base class for AOP Alliance {@link org.aopalliance.aop.Advice} classes
 * wrapping an AspectJ aspect or an AspectJ-annotated advice method.
 *
 * @author Rod Johnson
 * @author Adrian Colyer
 * @author Juergen Hoeller
 * @author Ramnivas Laddad
 * @since 2.0
 */
@SuppressWarnings("serial")
public abstract class AbstractAspectJAdvice implements Advice, AspectJPrecedenceInformation, Serializable {

	/**
	 * 在当前连接点的ReflectiveMethodInvocation userAttributes映射中使用的键。
	 * Key used in ReflectiveMethodInvocation userAttributes map for the current joinpoint.
	 */
	protected static final String JOIN_POINT_KEY = JoinPoint.class.getName();


	/**
	 * 惰性地实例化当前调用的连接点。要求MethodInvocation与ExposeInvocationInterceptor绑定
	 * <p>
	 * 如果可以访问当前的ReflectiveMethodInvocation（在周围的建议中），请不要使用。
	 * Lazily instantiate joinpoint for the current invocation.
	 * Requires MethodInvocation to be bound with ExposeInvocationInterceptor.
	 * <p>Do not use if access is available to the current ReflectiveMethodInvocation
	 * (in an around advice).
	 * @return current AspectJ joinpoint, or through an exception if we're not in a
	 * Spring AOP invocation.  当前的AspectJ连接点，如果不在Spring AOP调用中，则通过异常。
	 */
	public static JoinPoint currentJoinPoint() {
		// 获取当前的MethodInvocation 即ReflectiveMethodInvocation的实例
		MethodInvocation mi = ExposeInvocationInterceptor.currentInvocation();
		if (!(mi instanceof ProxyMethodInvocation)) {
			throw new IllegalStateException("MethodInvocation is not a Spring ProxyMethodInvocation: " + mi);
		}
		ProxyMethodInvocation pmi = (ProxyMethodInvocation) mi;
		// JOIN_POINT_KEY 的值 为 JoinPoint.class.getName()
		// 从ReflectiveMethodInvocation中获取JoinPoint 的值
		// 这里在第一次获取的时候 获取到的 JoinPoint是null
		// 然后把下面创建的MethodInvocationProceedingJoinPoint放入到ReflectiveMethodInvocation的userAttributes中
		// 这样在第二次获取的是 就会获取到这个 MethodInvocationProceedingJoinPoint
		JoinPoint jp = (JoinPoint) pmi.getUserAttribute(JOIN_POINT_KEY);
		if (jp == null) {
			jp = new MethodInvocationProceedingJoinPoint(pmi);
			pmi.setUserAttribute(JOIN_POINT_KEY, jp);
		}
		return jp;
	}


	private final Class<?> declaringClass;

	private final String methodName;

	/**
	 * 通知方法的参数类型数组
	 */
	private final Class<?>[] parameterTypes;

	/**
	 * 通知方法
	 */
	protected transient Method aspectJAdviceMethod;

	/**
	 * 表达式切入点
	 */
	private final AspectJExpressionPointcut pointcut;
	/**
	 * 切面实例工厂，可以获取切面类实例
	 */
	private final AspectInstanceFactory aspectInstanceFactory;

	/**
	 * The name of the aspect (ref bean) in which this advice was defined
	 * (used when determining advice precedence so that we can determine
	 * whether two pieces of advice come from the same aspect).
	 */
	private String aspectName = "";

	/**
	 * The order of declaration of this advice within the aspect.
	 */
	private int declarationOrder;

	/**
	 * 需要绑定的参数名数组
	 * 这个argumentNames就是解析通知标签的arg-names属性或者通知注解中的argNames属性得到的，我们传递的时候通过","分隔参数名
	 * 如果没有设置，那么Spring会自动获取
	 * This will be non-null if the creator of this advice object knows the argument names
	 * and sets them explicitly.
	 */
	@Nullable
	private String[] argumentNames;

	/**
	 * 异常通知的throwing属性的值
	 * Non-null if after throwing advice binds the thrown value.
	 */
	@Nullable
	private String throwingName;

	/**
	 *  后置通知的returning属性的值
	 * Non-null if after returning advice binds the return value.
	 * */
	@Nullable
	private String returningName;

	private Class<?> discoveredReturningType = Object.class;

	private Class<?> discoveredThrowingType = Object.class;

	/**
	 * JoinPoint参数在通知方法参数列表中的索引位置
	 *  当前仅支持索引 0（如果存在），也就是只能放在第一个参数位置
	 * Index for thisJoinPoint argument (currently only
	 * supported at index 0 if present at all).
	 */
	private int joinPointArgumentIndex = -1;

	/**
	 * 此JoinPoint内部的StaticPart 对象作为参数绑定时的索引位置
	 *  当前仅支持索引 0（如果存在），也就是只能放在第一个参数位置
	 * Index for thisJoinPointStaticPart argument (currently only
	 * supported at index 0 if present at all).
	 */
	private int joinPointStaticPartArgumentIndex = -1;

	/**
	 * 参数名 -> 通知方法参数索引的map，用于辅助参数绑定
	 *  不包括首位的JoinPoint参数名（如果存在JoinPoint参数
	 */
	@Nullable
	private Map<String, Integer> argumentBindings;
	/**
	 * 参数自省标志，默认为false
	 */
	private boolean argumentsIntrospected = false;

	@Nullable
	private Type discoveredReturningGenericType;
	// Note: Unlike return type, no such generic information is needed for the throwing type,
	// since Java doesn't allow exception types to be parameterized.


	/**
	 * Create a new AbstractAspectJAdvice for the given advice method.
	 * @param aspectJAdviceMethod the AspectJ-style advice method
	 * @param pointcut the AspectJ expression pointcut
	 * @param aspectInstanceFactory the factory for aspect instances
	 */
	public AbstractAspectJAdvice(
			Method aspectJAdviceMethod, AspectJExpressionPointcut pointcut, AspectInstanceFactory aspectInstanceFactory) {

		Assert.notNull(aspectJAdviceMethod, "Advice method must not be null");
		this.declaringClass = aspectJAdviceMethod.getDeclaringClass();
		this.methodName = aspectJAdviceMethod.getName();
		this.parameterTypes = aspectJAdviceMethod.getParameterTypes();
		this.aspectJAdviceMethod = aspectJAdviceMethod;
		this.pointcut = pointcut;
		this.aspectInstanceFactory = aspectInstanceFactory;
	}


	/**
	 * Return the AspectJ-style advice method.
	 */
	public final Method getAspectJAdviceMethod() {
		return this.aspectJAdviceMethod;
	}

	/**
	 * Return the AspectJ expression pointcut.
	 */
	public final AspectJExpressionPointcut getPointcut() {
		calculateArgumentBindings();
		return this.pointcut;
	}

	/**
	 * Build a 'safe' pointcut that excludes the AspectJ advice method itself.
	 * @return a composable pointcut that builds on the original AspectJ expression pointcut
	 * @see #getPointcut()
	 */
	public final Pointcut buildSafePointcut() {
		Pointcut pc = getPointcut();
		MethodMatcher safeMethodMatcher = MethodMatchers.intersection(
				new AdviceExcludingMethodMatcher(this.aspectJAdviceMethod), pc.getMethodMatcher());
		return new ComposablePointcut(pc.getClassFilter(), safeMethodMatcher);
	}

	/**
	 * Return the factory for aspect instances.
	 */
	public final AspectInstanceFactory getAspectInstanceFactory() {
		return this.aspectInstanceFactory;
	}

	/**
	 * Return the ClassLoader for aspect instances.
	 */
	@Nullable
	public final ClassLoader getAspectClassLoader() {
		return this.aspectInstanceFactory.getAspectClassLoader();
	}

	@Override
	public int getOrder() {
		return this.aspectInstanceFactory.getOrder();
	}


	/**
	 * Set the name of the aspect (bean) in which the advice was declared.
	 */
	public void setAspectName(String name) {
		this.aspectName = name;
	}

	@Override
	public String getAspectName() {
		return this.aspectName;
	}

	/**
	 * Set the declaration order of this advice within the aspect.
	 */
	public void setDeclarationOrder(int order) {
		this.declarationOrder = order;
	}

	@Override
	public int getDeclarationOrder() {
		return this.declarationOrder;
	}

	/**
	 * Set by creator of this advice object if the argument names are known.
	 * <p>This could be for example because they have been explicitly specified in XML,
	 * or in an advice annotation.
	 * @param argNames comma delimited list of arg names
	 */
	public void setArgumentNames(String argNames) {
		String[] tokens = StringUtils.commaDelimitedListToStringArray(argNames);
		setArgumentNamesFromStringArray(tokens);
	}

	public void setArgumentNamesFromStringArray(String... args) {
		this.argumentNames = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			this.argumentNames[i] = StringUtils.trimWhitespace(args[i]);
			if (!isVariableName(this.argumentNames[i])) {
				throw new IllegalArgumentException(
						"'argumentNames' property of AbstractAspectJAdvice contains an argument name '" +
						this.argumentNames[i] + "' that is not a valid Java identifier");
			}
		}
		if (this.argumentNames != null) {
			if (this.aspectJAdviceMethod.getParameterCount() == this.argumentNames.length + 1) {
				// May need to add implicit join point arg name...
				Class<?> firstArgType = this.aspectJAdviceMethod.getParameterTypes()[0];
				if (firstArgType == JoinPoint.class ||
						firstArgType == ProceedingJoinPoint.class ||
						firstArgType == JoinPoint.StaticPart.class) {
					String[] oldNames = this.argumentNames;
					this.argumentNames = new String[oldNames.length + 1];
					this.argumentNames[0] = "THIS_JOIN_POINT";
					System.arraycopy(oldNames, 0, this.argumentNames, 1, oldNames.length);
				}
			}
		}
	}

	public void setReturningName(String name) {
		throw new UnsupportedOperationException("Only afterReturning advice can be used to bind a return value");
	}

	/**
	 * We need to hold the returning name at this level for argument binding calculations,
	 * this method allows the afterReturning advice subclass to set the name.
	 */
	protected void setReturningNameNoCheck(String name) {
		// name could be a variable or a type...
		if (isVariableName(name)) {
			this.returningName = name;
		}
		else {
			// assume a type
			try {
				this.discoveredReturningType = ClassUtils.forName(name, getAspectClassLoader());
			}
			catch (Throwable ex) {
				throw new IllegalArgumentException("Returning name '" + name  +
						"' is neither a valid argument name nor the fully-qualified " +
						"name of a Java type on the classpath. Root cause: " + ex);
			}
		}
	}

	protected Class<?> getDiscoveredReturningType() {
		return this.discoveredReturningType;
	}

	@Nullable
	protected Type getDiscoveredReturningGenericType() {
		return this.discoveredReturningGenericType;
	}

	public void setThrowingName(String name) {
		throw new UnsupportedOperationException("Only afterThrowing advice can be used to bind a thrown exception");
	}

	/**
	 * We need to hold the throwing name at this level for argument binding calculations,
	 * this method allows the afterThrowing advice subclass to set the name.
	 */
	protected void setThrowingNameNoCheck(String name) {
		// name could be a variable or a type...
		if (isVariableName(name)) {
			this.throwingName = name;
		}
		else {
			// assume a type
			try {
				this.discoveredThrowingType = ClassUtils.forName(name, getAspectClassLoader());
			}
			catch (Throwable ex) {
				throw new IllegalArgumentException("Throwing name '" + name  +
						"' is neither a valid argument name nor the fully-qualified " +
						"name of a Java type on the classpath. Root cause: " + ex);
			}
		}
	}

	protected Class<?> getDiscoveredThrowingType() {
		return this.discoveredThrowingType;
	}

	private boolean isVariableName(String name) {
		char[] chars = name.toCharArray();
		if (!Character.isJavaIdentifierStart(chars[0])) {
			return false;
		}
		for (int i = 1; i < chars.length; i++) {
			if (!Character.isJavaIdentifierPart(chars[i])) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Do as much work as we can as part of the set-up so that argument binding
	 * on subsequent advice invocations can be as fast as possible.
	 * <p>If the first argument is of type JoinPoint or ProceedingJoinPoint then we
	 * pass a JoinPoint in that position (ProceedingJoinPoint for around advice).
	 * <p>If the first argument is of type {@code JoinPoint.StaticPart}
	 * then we pass a {@code JoinPoint.StaticPart} in that position.
	 * <p>Remaining arguments have to be bound by pointcut evaluation at
	 * a given join point. We will get back a map from argument name to
	 * value. We need to calculate which advice parameter needs to be bound
	 * to which argument name. There are multiple strategies for determining
	 * this binding, which are arranged in a ChainOfResponsibility.
	 */
	public final synchronized void calculateArgumentBindings() {
		// The simple case... nothing to bind.
		// 如果已经进行过参数绑定了或者通知方法中没有参数
		if (this.argumentsIntrospected || this.parameterTypes.length == 0) {
			return;
		}

		int numUnboundArgs = this.parameterTypes.length;
		// 通知方法参数类型
		Class<?>[] parameterTypes = this.aspectJAdviceMethod.getParameterTypes();
		/*
		 * 如果第一个参数类型为JoinPoint，则设置joinPointArgumentIndex属性值为0
		 * 或者第一个参数类型为ProceedingJoinPoint，同样设置joinPointArgumentIndex属性值为0
		 * 因为该类型的joinPoint可以调用proceed()方法，因此Spring固定该参数只有后置通知支持
		 * 其他通知方法如果设置则抛出异常："ProceedingJoinPoint is only supported for around advice"
		 * 或者第一个参数类型为JoinPoint.StaticPart，设置joinPointStaticPartArgumentIndex属性值为0
		 */
		// 如果第一个参数是JoinPoint或者ProceedingJoinPoint
		if (maybeBindJoinPoint(parameterTypes[0]) ||
				//这个方法中还有一个校验 即只有在环绕通知中第一个参数类型才能是ProceedingJoinPoint
				maybeBindProceedingJoinPoint(parameterTypes[0]) ||
				//如果第一个参数是JoinPoint.StaticPart
				maybeBindJoinPointStaticPart(parameterTypes[0])) {
			numUnboundArgs--;
		}
		//如果大于0，说明还有其他参数需要绑定
		if (numUnboundArgs > 0) {
			// need to bind arguments by name as returned from the pointcut match
			//需要按名称绑定参数从切入点返回的匹配项
			bindArgumentsByName(numUnboundArgs);
		}
		//参数自省，设置为true，表示当前连接点的参数已经通过calculateArgumentBindings优化了
		this.argumentsIntrospected = true;
	}

	/**
	 * 断传入的参数类型是否为JoinPoint类，如果是，则将joinPointArgumentIndex赋值为0，并返回true；否则，返回false。
	 * @param candidateParameterType
	 * @return
	 */
	private boolean maybeBindJoinPoint(Class<?> candidateParameterType) {
		if (JoinPoint.class == candidateParameterType) {
			this.joinPointArgumentIndex = 0;
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * 判断传入的candidateParameterType是否为ProceedingJoinPoint类，
	 * 如果是则判断当前是否支持ProceedingJoinPoint，如果不支持则抛出IllegalArgumentException异常，
	 * 同时设置joinPointArgumentIndex为0，并返回true；否则返回false。
	 * @param candidateParameterType
	 * @return
	 */
	private boolean maybeBindProceedingJoinPoint(Class<?> candidateParameterType) {
		if (ProceedingJoinPoint.class == candidateParameterType) {
			if (!supportsProceedingJoinPoint()) {
				throw new IllegalArgumentException("ProceedingJoinPoint is only supported for around advice");
			}
			this.joinPointArgumentIndex = 0;
			return true;
		}
		else {
			return false;
		}
	}

	protected boolean supportsProceedingJoinPoint() {
		return false;
	}

	/**
	 * 判断传入的参数类型是否为JoinPoint.StaticPart。若是，则将joinPointStaticPartArgumentIndex赋值为0并返回true；否则，返回false。
	 * @param candidateParameterType
	 * @return
	 */
	private boolean maybeBindJoinPointStaticPart(Class<?> candidateParameterType) {
		if (JoinPoint.StaticPart.class == candidateParameterType) {
			this.joinPointStaticPartArgumentIndex = 0;
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * 通过argumentNames的参数名匹配argumentBinding确定需要传递的参数
	 * @param numArgumentsExpectingToBind 需要绑定的参数个数
	 */
	private void bindArgumentsByName(int numArgumentsExpectingToBind) {
		//如果没有手动设置参数名
		if (this.argumentNames == null) {
			//那么通过构建一个ParameterNameDiscoverer（参数名称发现器）并且通过多个不同的发现器来自动获取
			//获取的参数名数组通常就是通知方法参数名数组
			this.argumentNames = createParameterNameDiscoverer().getParameterNames(this.aspectJAdviceMethod);
		}
		if (this.argumentNames != null) {
			// We have been able to determine the arg names.
			//根据参数名数组绑定参数
			bindExplicitArguments(numArgumentsExpectingToBind);
		}
		else {
			throw new IllegalStateException("Advice method [" + this.aspectJAdviceMethod.getName() + "] " +
					"requires " + numArgumentsExpectingToBind + " arguments to be bound by name, but " +
					"the argument names were not specified and could not be discovered.");
		}
	}

	/**
	 * Create a ParameterNameDiscoverer to be used for argument binding.
	 * <p>The default implementation creates a {@link DefaultParameterNameDiscoverer}
	 * and adds a specifically configured {@link AspectJAdviceParameterNameDiscoverer}.
	 */
	protected ParameterNameDiscoverer createParameterNameDiscoverer() {
		// We need to discover them, or if that fails, guess,
		// and if we can't guess with 100% accuracy, fail.
		DefaultParameterNameDiscoverer discoverer = new DefaultParameterNameDiscoverer();
		AspectJAdviceParameterNameDiscoverer adviceParameterNameDiscoverer =
				new AspectJAdviceParameterNameDiscoverer(this.pointcut.getExpression());
		adviceParameterNameDiscoverer.setReturningName(this.returningName);
		adviceParameterNameDiscoverer.setThrowingName(this.throwingName);
		// Last in chain, so if we're called and we fail, that's bad...
		adviceParameterNameDiscoverer.setRaiseExceptions(true);
		discoverer.addDiscoverer(adviceParameterNameDiscoverer);
		return discoverer;
	}

	/**
	 * 绑定参数
	 * @param numArgumentsLeftToBind  需要绑定的参数个数
	 */
	private void bindExplicitArguments(int numArgumentsLeftToBind) {
		Assert.state(this.argumentNames != null, "No argument names available");
		this.argumentBindings = new HashMap<>();

		int numExpectedArgumentNames = this.aspectJAdviceMethod.getParameterCount();
		if (this.argumentNames.length != numExpectedArgumentNames) {
			throw new IllegalStateException("Expecting to find " + numExpectedArgumentNames +
					" arguments to bind by name in advice, but actually found " +
					this.argumentNames.length + " arguments.");
		}

		//从指定位置开始绑定
		// So we match in number...
		int argumentIndexOffset = this.parameterTypes.length - numArgumentsLeftToBind;
		for (int i = argumentIndexOffset; i < this.argumentNames.length; i++) {
			this.argumentBindings.put(this.argumentNames[i], i);
		}

		//绑定返回值参数，用于接收目标方法的返回值
		// Check that returning and throwing were in the argument names list if
		// specified, and find the discovered argument types.
		if (this.returningName != null) {
			if (!this.argumentBindings.containsKey(this.returningName)) {
				throw new IllegalStateException("Returning argument name '" + this.returningName +
						"' was not bound in advice arguments");
			}
			else {
				Integer index = this.argumentBindings.get(this.returningName);
				this.discoveredReturningType = this.aspectJAdviceMethod.getParameterTypes()[index];
				this.discoveredReturningGenericType = this.aspectJAdviceMethod.getGenericParameterTypes()[index];
			}
		}

		//绑定抛出异常参数，用于接收抛出的异常
		if (this.throwingName != null) {
			if (!this.argumentBindings.containsKey(this.throwingName)) {
				throw new IllegalStateException("Throwing argument name '" + this.throwingName +
						"' was not bound in advice arguments");
			}
			else {
				Integer index = this.argumentBindings.get(this.throwingName);
				this.discoveredThrowingType = this.aspectJAdviceMethod.getParameterTypes()[index];
			}
		}

		// configure the pointcut expression accordingly.
		//相应地配置切入点表达式。
		configurePointcutParameters(this.argumentNames, argumentIndexOffset);
	}

	/**
	 * All parameters from argumentIndexOffset onwards are candidates for
	 * pointcut parameters - but returning and throwing vars are handled differently
	 * and must be removed from the list if present.
	 */
	private void configurePointcutParameters(String[] argumentNames, int argumentIndexOffset) {
		int numParametersToRemove = argumentIndexOffset;
		if (this.returningName != null) {
			numParametersToRemove++;
		}
		if (this.throwingName != null) {
			numParametersToRemove++;
		}
		String[] pointcutParameterNames = new String[argumentNames.length - numParametersToRemove];
		Class<?>[] pointcutParameterTypes = new Class<?>[pointcutParameterNames.length];
		Class<?>[] methodParameterTypes = this.aspectJAdviceMethod.getParameterTypes();

		int index = 0;
		for (int i = 0; i < argumentNames.length; i++) {
			if (i < argumentIndexOffset) {
				continue;
			}
			if (argumentNames[i].equals(this.returningName) ||
				argumentNames[i].equals(this.throwingName)) {
				continue;
			}
			pointcutParameterNames[index] = argumentNames[i];
			pointcutParameterTypes[index] = methodParameterTypes[i];
			index++;
		}

		this.pointcut.setParameterNames(pointcutParameterNames);
		this.pointcut.setParameterTypes(pointcutParameterTypes);
	}

	/**
	 * 参数绑定，获取方法执行连接点处的参数，并将一组参数输出到advice方法。
	 * Take the arguments at the method execution join point and output a set of arguments
	 * to the advice method.
	 * @param jp the current JoinPoint  当前方法连接点
	 * @param jpMatch the join point match that matched this execution join point
	 *                 匹配此执行连接点的JoinPointMatch，用于通知方法的参数匹配和传递，一般为null
	 *                 如果expression中配置了args()的PCD并且需要通知方法需要传递参数，那么不为null
	 *  			   如果有其他能够传参的PCD，那么jpMatch也不会为null，比如@annotation()的PCD就能传递方法上的注解作为参数
	 * @param returnValue the return value from the method execution (may be null) 方法执行的返回值（可能为null）
	 * @param ex the exception thrown by the method execution (may be null)   方法执行引发的异常（一般都是目标方法抛出的异常，可能为null）
	 * @return the empty array if there are no arguments   空数组（如果没有参数）
	 */
	protected Object[] argBinding(JoinPoint jp, @Nullable JoinPointMatch jpMatch,
			@Nullable Object returnValue, @Nullable Throwable ex) {

		//提前进行参数绑定的设置，以尽可能地提高后续通知调用的效率
		calculateArgumentBindings();

		// AMC start
		//绑定的参数数组
		Object[] adviceInvocationArgs = new Object[this.parameterTypes.length];
		//绑定的个数
		int numBound = 0;

		// 这个默认值是 -1 重新赋值是在calculateArgumentBindings中进行的
		//那么第一参数就是JoinPoint
		if (this.joinPointArgumentIndex != -1) {
			adviceInvocationArgs[this.joinPointArgumentIndex] = jp;
			numBound++;
		}
		//如果joinPointStaticPartArgumentIndex值不为-1（通过calculateArgumentBindings方法可以设置为0）
		//那么第一参数就是JoinPoint.StaticPart
		else if (this.joinPointStaticPartArgumentIndex != -1) {
			adviceInvocationArgs[this.joinPointStaticPartArgumentIndex] = jp.getStaticPart();
			numBound++;
		}

		// 这里主要是取通知方法中的参数类型 是除了 JoinPoint和ProceedingJoinPoint参数之外的参数
		// 如异常通知参数 返回通知参数
		if (!CollectionUtils.isEmpty(this.argumentBindings)) {
			// binding from pointcut match
			/*
			 * 从切入点匹配绑定，对于普通参数或者其他参数（比如方法的注解）传递，我们需要在execution中配置传参的PCD
			 * 比如args()、@annotation()，对于普通参数，这些PCD里面的参数位置就是对应着目标方法的参数位置
			 * PointcutParameter[]就是对应着位置进行 参数名 -> 传递的参数值 封装的
			 * 这些PCD中设置的参数名最好和通知方法的参数名一致，如果不一致，那么需要手动配置arg-names属性等于这个参数名
			 */
			if (jpMatch != null) {
				//获取PointcutParameter数组，这里面就是参数名 -> 传递的参数值的映射
				PointcutParameter[] parameterBindings = jpMatch.getParameterBindings();
				//遍历parameterBindings
				for (PointcutParameter parameter : parameterBindings) {
					//获取name
					String name = parameter.getName();
					//获取该name对应的索引
					Integer index = this.argumentBindings.get(name);
					//在给定索引位置设置参数值
					adviceInvocationArgs[index] = parameter.getBinding();
					numBound++;
				}
			}
			// binding from returning clause
			// 后置返回通知参数
			if (this.returningName != null) {
				Integer index = this.argumentBindings.get(this.returningName);
				adviceInvocationArgs[index] = returnValue;
				numBound++;
			}
			// binding from thrown exception
			// 异常通知参数
			if (this.throwingName != null) {
				Integer index = this.argumentBindings.get(this.throwingName);
				adviceInvocationArgs[index] = ex;
				numBound++;
			}
		}

		if (numBound != this.parameterTypes.length) {
			throw new IllegalStateException("Required to bind " + this.parameterTypes.length +
					" arguments, but only bound " + numBound + " (JoinPointMatch " +
					(jpMatch == null ? "was NOT" : "WAS") + " bound in invocation)");
		}

		return adviceInvocationArgs;
	}


	/**
	 * 这三个参数 JoinPointMatch 都是相同的
	 * returnValue 当执行后置返回通知的时候 传值 其他为null
	 * Throwable 当执行后置异常通知的时候 传值，其他为null
	 *
	 * Invoke the advice method.
	 * @param jpMatch the JoinPointMatch that matched this execution join point
	 *                    匹配此执行连接点的JoinPointMatch，用于通知方法的参数匹配和传递，一般为null
	 * 	                  如果expression中配置了args()的PCD并且需要通知方法需要传递参数，那么不为null
	 * 	                  如果有其他能够传参的PCD，那么jpMatch也不会为null，
	 * 	                  比如@annotation()的PCD就能传递方法上的注解	作为参数
	 * @param returnValue the return value from the method execution (may be null)
	 *                    方法执行的返回值（一般都是目标方法的返回值，可能为null）前置通知不能获取方法执行的返回值
	 * @param ex the exception thrown by the method execution (may be null)
	 *           方法执行引发的异常（一般都是目标方法抛出的异常，可能为null）,前置通知不能获取方法执行抛出的异常
	 * @return the invocation result
	 * @throws Throwable in case of invocation failure
	 */
	protected Object invokeAdviceMethod(
			@Nullable JoinPointMatch jpMatch, @Nullable Object returnValue, @Nullable Throwable ex)
			throws Throwable {

		return invokeAdviceMethodWithGivenArgs(argBinding(getJoinPoint(), jpMatch, returnValue, ex));
	}

	// As above, but in this case we are given the join point.
	protected Object invokeAdviceMethod(JoinPoint jp, @Nullable JoinPointMatch jpMatch,
			@Nullable Object returnValue, @Nullable Throwable t) throws Throwable {

		return invokeAdviceMethodWithGivenArgs(argBinding(jp, jpMatch, returnValue, t));
	}

	/**
	 * 通过反射调用切面类对象的通知方法，并传入给定的参数
	 * @param args  存储调用通知方法时实际传递的参数
	 * @return
	 * @throws Throwable
	 */
	protected Object invokeAdviceMethodWithGivenArgs(Object[] args) throws Throwable {
		Object[] actualArgs = args;
		// 判断通知方法是否有参数
		if (this.aspectJAdviceMethod.getParameterCount() == 0) {
			actualArgs = null;
		}
		try {
			//设置方法的可访问属性，即aspectJAdviceMethod.setAccessible(true)
			ReflectionUtils.makeAccessible(this.aspectJAdviceMethod);
			// TODO AopUtils.invokeJoinpointUsingReflection
			//反射调用切面类对象的通知方法并获取返回值
			// this.aspectInstanceFactory.getAspectInstance()获取的是切面的实例
			return this.aspectJAdviceMethod.invoke(this.aspectInstanceFactory.getAspectInstance(), actualArgs);
		}
		catch (IllegalArgumentException ex) {
			throw new AopInvocationException("Mismatch on arguments to advice method [" +
					this.aspectJAdviceMethod + "]; pointcut expression [" +
					this.pointcut.getPointcutExpression() + "]", ex);
		}
		catch (InvocationTargetException ex) {
			throw ex.getTargetException();
		}
	}

	/**
	 * Overridden in around advice to return proceeding join point.
	 */
	protected JoinPoint getJoinPoint() {
		return currentJoinPoint();
	}

	/**
	 * 在我们要调度的连接点上获取当前连接点匹配项。
	 * Get the current join point match at the join point we are being dispatched on.
	 */
	@Nullable
	protected JoinPointMatch getJoinPointMatch() {
		// 这里从线程上下文中获取MethodInvocation 看到这里你也许会感到奇怪  跟着文章分析来看 我们没有在设置过上下文的值啊
		// 这里是怎么获取到MethodInvocation 的对象的呢？
		// 不知道你是否还记得 我们在获取Advisor的时候 调用过这样的一个方法org.springframework.aop.aspectj.AspectJProxyUtils#makeAdvisorChainAspectJCapableIfNecessary
		// 在这个方法中会有这样的一段代码
		// if (foundAspectJAdvice && !advisors.contains(ExposeInvocationInterceptor.ADVISOR)) {
		//     如果获取到了 Advisor  则向 Advisor集合中添加第一个元素 即 ExposeInvocationInterceptor.ADVISOR
		// 也就是说 我们的 Advisor列表中的第一个元素为ExposeInvocationInterceptor.ADVISOR 它是一个DefaultPointcutAdvisor的实例
		// 对于任何的目标方法都返回true  它的Advice是ExposeInvocationInterceptor
		//      advisors.add(0, ExposeInvocationInterceptor.ADVISOR);
		//      return true;
		//  }
		// 这样我们就不难理解了，在调用ReflectiveMethodInvocation#proceed的时候第一个调用的MethodInterceptor是ExposeInvocationInterceptor
		// ExposeInvocationInterceptor的invoke方法的内容如下：
		//  public Object invoke(MethodInvocation mi) throws Throwable {
		//   先取出旧的MethodInvocation的值
		//    MethodInvocation oldInvocation = invocation.get();
		//    这里设置新的 MethodInvocation 就是这里了！！！
		//    invocation.set(mi);
		//    try {
		//      递归调用
		//      return mi.proceed();
		///   }
		//   finally {
		//     invocation.set(oldInvocation);
		//   }
		//      }

		//从线程上下文中获取MethodInvocation对象。
		MethodInvocation mi = ExposeInvocationInterceptor.currentInvocation();
		// 检查 MethodInvocation对象是否是Spring ProxyMethodInvocation的实例，
		if (!(mi instanceof ProxyMethodInvocation)) {
			throw new IllegalStateException("MethodInvocation is not a Spring ProxyMethodInvocation: " + mi);
		}
		// 获取连接点匹配的信息并返回。
		return getJoinPointMatch((ProxyMethodInvocation) mi);
	}

	/**
	 * 获取与给定的代理方法调用相匹配的连接点匹配对象(JoinPointMatch)。
	 * @param pmi
	 * @return
	 */
	// Note: We can't use JoinPointMatch.getClass().getName() as the key, since
	// Spring AOP does all the matching at a join point, and then all the invocations.
	// Under this scenario, if we just use JoinPointMatch as the key, then
	// 'last man wins' which is not what we want at all.
	// Using the expression is guaranteed to be safe, since 2 identical expressions
	// are guaranteed to bind in exactly the same way.
	@Nullable
	protected JoinPointMatch getJoinPointMatch(ProxyMethodInvocation pmi) {
		//获取当前切面（Aspect）定义的切点表达式。
		String expression = this.pointcut.getExpression();
		//函数检查获取到的表达式是否为非空，如果没有有效的表达式，就没有匹配的工作需要做，直接返回null。
		return (expression != null ? (JoinPointMatch) pmi.getUserAttribute(expression) : null);
	}


	@Override
	public String toString() {
		return getClass().getName() + ": advice method [" + this.aspectJAdviceMethod + "]; " +
				"aspect name '" + this.aspectName + "'";
	}

	private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
		inputStream.defaultReadObject();
		try {
			this.aspectJAdviceMethod = this.declaringClass.getMethod(this.methodName, this.parameterTypes);
		}
		catch (NoSuchMethodException ex) {
			throw new IllegalStateException("Failed to find advice method on deserialization", ex);
		}
	}


	/**
	 * MethodMatcher that excludes the specified advice method.
	 * @see AbstractAspectJAdvice#buildSafePointcut()
	 */
	private static class AdviceExcludingMethodMatcher extends StaticMethodMatcher {

		private final Method adviceMethod;

		public AdviceExcludingMethodMatcher(Method adviceMethod) {
			this.adviceMethod = adviceMethod;
		}

		@Override
		public boolean matches(Method method, Class<?> targetClass) {
			return !this.adviceMethod.equals(method);
		}

		@Override
		public boolean equals(@Nullable Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof AdviceExcludingMethodMatcher)) {
				return false;
			}
			AdviceExcludingMethodMatcher otherMm = (AdviceExcludingMethodMatcher) other;
			return this.adviceMethod.equals(otherMm.adviceMethod);
		}

		@Override
		public int hashCode() {
			return this.adviceMethod.hashCode();
		}

		@Override
		public String toString() {
			return getClass().getName() + ": " + this.adviceMethod;
		}
	}

}
