package com.lq.customAutowired;

import org.springframework.stereotype.Controller;

/**
 * @program: spring
 * @pageName com.lq.customAutowired
 * @className QiangController
 * @description:
 * @author: liqiang
 * @create: 2024-03-18 16:54
 **/
@Controller
public class QiangController {

    @Qiang
    private QiangService qiangService;

    public void show(){
        qiangService.show();
    }
}
