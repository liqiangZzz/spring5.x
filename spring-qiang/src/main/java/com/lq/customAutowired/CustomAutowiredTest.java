package com.lq.customAutowired;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.customAutowired
 * @className CustomAutowiredTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-18 16:56
 **/
public class CustomAutowiredTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("qiangAnnotation.xml");
        QiangController bean = applicationContext.getBean(QiangController.class);
        bean.show();
    }
}
