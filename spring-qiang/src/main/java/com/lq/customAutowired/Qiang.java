package com.lq.customAutowired;

import java.lang.annotation.*;

/**
 * @program: spring
 * @pageName com.lq.customAutowired
 * @className Qiang
 * @description:
 * @author: liqiang
 * @create: 2024-03-18 16:43
 **/
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Qiang {

    boolean required() default true;
}
