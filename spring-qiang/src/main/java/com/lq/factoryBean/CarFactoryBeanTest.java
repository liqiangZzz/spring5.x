package com.lq.factoryBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.factoryBean
 * @className CarFactoryBeanTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-05 16:59
 **/
public class CarFactoryBeanTest {


    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("factoryBean.xml");

        CarFactoryBean factoryBean = (CarFactoryBean) context.getBean("&carFactoryBean");
        System.out.println(factoryBean);
        Car bean = (Car) context.getBean("carFactoryBean");
        System.out.println(bean);
        Car bean2 = (Car) context.getBean("carFactoryBean");
        System.out.println(bean2);
    }
}
