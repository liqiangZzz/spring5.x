package com.lq.factoryBean;

import org.springframework.beans.factory.FactoryBean;

/**
 * @program: spring
 * @pageName com.lq.factoryBean
 * @className CarFactoryBean
 * @description:
 * @author: liqiang
 * @create: 2024-03-05 16:56
 **/
public class CarFactoryBean implements FactoryBean<Car> {

    @Override
    public Car getObject() {
        return new Car();
    }

    @Override
    public Class<?> getObjectType() {
        return Car.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}

class Car {

}