package com.lq;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * @program: spring
 * @pageName com.lq
 * @className MyBeanFactoryPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-01-31 17:27
 **/
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    protected Log logger = LogFactory.getLog(getClass().getName());
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println(">> BeanFactoryPostProcessor 开始执行了");
        String[] names = beanFactory.getBeanDefinitionNames();
        for (String name : names) {
            if("student".equals(name)){

                BeanDefinition beanDefinition = beanFactory.getBeanDefinition(name);
                MutablePropertyValues propertyValues = beanDefinition.getPropertyValues();
                // MutablePropertyValues如果设置了相关属性，可以修改，如果没有设置则可以添加相关属性信息
                if(propertyValues.contains("name")){
                    propertyValues.addPropertyValue("name", "test");
                    System.out.println("修改了属性信息");
                }
            }
        }
        System.out.println(">> BeanFactoryPostProcessor 执行结束");

        logger.info("MyBeanFactoryPostProcessor 实现 BeanFactoryPostProcessor接口 测试执行 postProcessBeanFactory");
    }
}
