package com.lq.selftag;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;

/**
 * @program: spring
 * @pageName com.lq.selftag
 * @className CommodityBeanDefinitionParser
 * @description: Bean定义分析解析器
 * @author: liqiang
 * @create: 2024-01-24 17:52
 **/
public class CommodityBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {

    /**
     * 返回属性值所对应的对象
     *
     * @param element the {@code Element} that is being parsed
     * @return
     */
    @Override
    protected Class<?> getBeanClass(Element element) {
        return Commodity.class;
    }

    @Override
    protected void doParse(Element element, BeanDefinitionBuilder builder) {
        // 获取标签具体的属性值
        String id = element.getAttribute("id");
        String name = element.getAttribute("name");
        String color = element.getAttribute("color");
        String price = element.getAttribute("price");

        if (StringUtils.hasText(id)) {
            builder.addPropertyValue("id", id);
        }
        if (StringUtils.hasText(name)) {
            builder.addPropertyValue("name", name);
        }
        if (StringUtils.hasText(color)) {
            builder.addPropertyValue("color", color);
        }
        if (StringUtils.hasText(price)) {
            builder.addPropertyValue("price", price);
        }
    }

}
