package com.lq.selftag;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.selftag
 * @className MyNamespaceHandlerTest
 * @description: 自定义命名空间程序
 * @author: liqiang
 * @create: 2024-01-25 10:26
 **/
public class MyNamespaceHandlerTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("customNamespaceHandler.xml");
        Commodity commodity = (Commodity)applicationContext.getBean("testBean");
        System.out.println(commodity);
    }
}
