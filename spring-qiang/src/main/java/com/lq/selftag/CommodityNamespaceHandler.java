package com.lq.selftag;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * @program: spring
 * @pageName com.lq.selftag
 * @className CommodityNamespaceHandler
 * @description: 命名空间处理程序支持
 * @author: liqiang
 * @create: 2024-01-24 17:55
 **/
public class CommodityNamespaceHandler extends NamespaceHandlerSupport {

    @Override
    public void init() {
        registerBeanDefinitionParser("commodity",new CommodityBeanDefinitionParser());
    }
}
