package com.lq.ignore;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 9:37
 */
public interface Ignore {

    void setPoJoA(IgnoreImpl.PoJoA poJoA);

    void setPoJoB(IgnoreImpl.PoJoB poJoB);
}
