package com.lq.ignore;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 9:37
 */
public class IgnoreImpl implements Ignore{

    private PoJoA poJoA;
    private PoJoB poJoB;

    @Override
    public void setPoJoA(PoJoA poJoA) {
        this.poJoA = poJoA;
    }
    @Override
    public void setPoJoB(PoJoB poJoB) {
        this.poJoB = poJoB;
    }

    public IgnoreImpl(PoJoA poJoA, PoJoB poJoB) {
        this.poJoA = poJoA;
        this.poJoB = poJoB;
    }

    public IgnoreImpl() {
    }

    @Override
    public String toString() {
        return "IgnoreImpl{" +
                "poJoA=" + poJoA +
                ", poJoB=" + poJoB +
                '}';
    }

    public static class PoJoA{

    }
    public static class PoJoB{

    }

}
