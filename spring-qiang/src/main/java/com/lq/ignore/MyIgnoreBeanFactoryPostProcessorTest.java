package com.lq.ignore;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 9:41
 */
public class MyIgnoreBeanFactoryPostProcessorTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("MyIgnore.xml");
        System.out.println(ac.getBean("ignoreByName"));
        System.out.println(ac.getBean("ignoreByType"));
        System.out.println(ac.getBean("ignoreByConstructor"));
    }
}
