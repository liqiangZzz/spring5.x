package com.lq.ignore;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 9:38
 */
public class MyIgnoreBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        //开启这段代码时，即使设置了byName或者byType自动注入并且满足条件
        //也不会调用Ignore接口及其实现类中的定义的setter方法进行自动注入

        //beanFactory.ignoreDependencyType(IgnoreImpl.PoJoA.class);
        //beanFactory.ignoreDependencyType(IgnoreImpl.PoJoB.class);

        // beanFactory.ignoreDependencyInterface(Ignore.class);
        //这样也行
        //beanFactory.ignoreDependencyInterface(IgnoreImpl.class);
    }
}
