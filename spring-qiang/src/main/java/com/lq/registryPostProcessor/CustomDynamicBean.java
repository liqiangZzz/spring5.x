package com.lq.registryPostProcessor;

import org.springframework.stereotype.Component;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 17:22
 */
@Component
public class CustomDynamicBean {

    public CustomDynamicBean() {
        System.out.println("customDynamicBean 初始化");
    }

}
