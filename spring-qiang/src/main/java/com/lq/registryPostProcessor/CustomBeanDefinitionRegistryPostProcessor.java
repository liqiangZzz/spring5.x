package com.lq.registryPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.stereotype.Component;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 17:21
 */
@Component
public class CustomBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {


    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory registry) throws BeansException {
        // 可以对 BeanFactory 进行自定义配置
        System.out.println("对 BeanFactory 进行自定义配置");
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        // 检查是否已存在同名 Bean
        if (!registry.containsBeanDefinition("customDynamicBean")) {
            // 动态注册一个 Bean 定义
            BeanDefinition beanDefinition = new GenericBeanDefinition();
            beanDefinition.setBeanClassName("com.lq.registryPostProcessor.CustomDynamicBean");
            registry.registerBeanDefinition("customDynamicBean", beanDefinition);
            System.out.println("动态注册了一个 Bean 定义：customDynamicBean");
        } else {
            System.out.println("Bean 'customDynamicBean' 已经存在，跳过注册");
        }
    }

}
