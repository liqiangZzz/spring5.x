package com.lq.registryPostProcessor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/10 17:32
 */
@Configuration
@ComponentScan(basePackages = "com.lq.registryPostProcessor")
public class CustomBeanDefinitionRegistryPostProcessorTest {


    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = null;
        try {
            // 1. 创建 Spring 上下文
            context = new AnnotationConfigApplicationContext(CustomBeanDefinitionRegistryPostProcessorTest.class);

            // 2. 打印所有 Bean 名称
            String[] names = context.getBeanDefinitionNames();
            for (String name : names) {
                System.out.println("beanName: " + name);

            }

            // 3. 验证 dynamicBean 是否被注册
            if (context.containsBean("customDynamicBean")) {
                Object customDynamicBean = context.getBean("customDynamicBean");
                System.out.println("customDynamicBean 是否被注册: " + customDynamicBean);
            } else {
                System.out.println("未找到名为 customDynamicBean 的 Bean");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 4. 关闭上下文
            if (context != null) {
                context.close();
            }
        }
    }
}
