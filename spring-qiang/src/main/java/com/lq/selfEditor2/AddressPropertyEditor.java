package com.lq.selfEditor2;

import java.beans.PropertyEditorSupport;

/**
 * @program: spring
 * @pageName com.lq.selfEditor2
 * @className AddressPropertyEditor
 * @description:
 * @author: liqiang
 * @create: 2024-01-29 17:35
 */
public class AddressPropertyEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String[] s = text.split("_");
        Address address = new Address();
        address.setProvince(s[0]);
        address.setCity(s[1]);
        address.setTown(s[2]);
        setValue(address);
    }
}
