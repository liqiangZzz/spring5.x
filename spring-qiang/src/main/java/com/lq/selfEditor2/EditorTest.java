package com.lq.selfEditor2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.selfEditor2
 * @className EditorTest
 * @description:
 * @author: liqiang
 * @create: 2024-01-29 17:35
 */
public class EditorTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext();
        applicationContext.register(AddressPropertyConfiguration.class);
        applicationContext.register(Customer.class);
        applicationContext.refresh();
        System.out.println(applicationContext.getBean(Customer.class));
    }
}
