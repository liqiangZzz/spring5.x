package com.lq.selfEditor2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @program: spring
 * @pageName com.lq.selfEditor2
 * @className Customer
 * @description:
 * @author: liqiang
 * @create: 2024-01-29 17:35
 */
@Configuration
@PropertySource("classpath:customer.properties")
public class Customer {

    @Value("${customer.name}")
    private String name;

    @Value("${customer.address}")
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
