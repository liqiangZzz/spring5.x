package com.lq.selfEditor2;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: spring
 * @pageName com.lq.selfEditor2
 * @className AddressPropertyConfiguration
 * @description:
 * @author: liqiang
 * @create: 2024-01-29 17:35
 */
@Configuration
public class AddressPropertyConfiguration {

    @Bean
    public static CustomEditorConfigurer editorConfigurer() {
        CustomEditorConfigurer customEditorConfigurer = new CustomEditorConfigurer();
        customEditorConfigurer.setPropertyEditorRegistrars(new PropertyEditorRegistrar[]{new AddressPropertyEditorRegistrar()});
        return customEditorConfigurer;
    }

}
