package com.lq.selfEditor2;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;

/**
 * @program: spring
 * @pageName com.lq.selfEditor2
 * @className AddressPropertyEditorRegistrar
 * @description:
 * @author: liqiang
 * @create: 2024-01-29 17:35
 */
public class AddressPropertyEditorRegistrar implements PropertyEditorRegistrar {

    @Override
    public void registerCustomEditors(PropertyEditorRegistry registry) {
        registry.registerCustomEditor(Address.class, new AddressPropertyEditor());
    }
}
