package com.lq.indexedAnnotation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/7 15:28
 */
public class MyIndexedAnnotationTest {

    public static void main(String[] args) {
        try {
            ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("spring-config-index.xml");
            System.out.println(ac.getBean("indexClass"));
            System.out.println(ac.getBean("indexClass2"));
            System.out.println(ac.getBean("indexClass3"));
            System.out.println(ac.getBean("indexClass4"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
