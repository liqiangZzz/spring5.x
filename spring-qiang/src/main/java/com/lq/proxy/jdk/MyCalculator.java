package com.lq.proxy.jdk;

/**
 * @program: spring
 * @pageName com.lq.proxy.jdk
 * @className MyCalculator
 * @description:
 * @author: liqiang
 * @create: 2024-01-31 17:30
 **/
public class MyCalculator implements Calculator {
    public int add(int i, int j) {
        return i + j;
    }

    public int sub(int i, int j) {
        return i - j;
    }

    public int mult(int i, int j) {
        return i * j;
    }

    public int div(int i, int j) {
        return i / j;
    }
}
