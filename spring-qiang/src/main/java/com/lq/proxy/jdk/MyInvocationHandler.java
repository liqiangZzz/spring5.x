package com.lq.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @program: spring
 * @pageName com.lq.proxy.jdk
 * @className MyInvocationHandler
 * @description:
 * @author: liqiang
 * @create: 2024-05-13 15:54
 **/
public class MyInvocationHandler implements InvocationHandler {

    /**
     * 目标对象
     */
    private Object target;

    public MyInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 执行相应的目标方法
        return method.invoke(target,args);
    }
}
