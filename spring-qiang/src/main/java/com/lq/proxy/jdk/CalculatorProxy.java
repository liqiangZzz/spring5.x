package com.lq.proxy.jdk;

import java.lang.reflect.Proxy;

/**
 * @program: spring
 * @pageName com.lq.proxy.jdk
 * @className CalculatorProxy
 * @description:
 * @author: liqiang
 * @create: 2024-05-13 13:44
 **/
public class CalculatorProxy {


    public static Calculator getProxy(Calculator calculator) {
        //获取 对象的类加载器和实现的接口列表
        ClassLoader classLoader = calculator.getClass().getClassLoader();
        Class<?>[] interfaces = calculator.getClass().getInterfaces();

        MyInvocationHandler invocationHandler = new MyInvocationHandler(calculator);

        Object proxyInstance = Proxy.newProxyInstance(classLoader, interfaces, invocationHandler);
        return (Calculator) proxyInstance;
    }
}
