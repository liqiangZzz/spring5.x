package com.lq.proxy.jdk;

/**
 * @program: spring
 * @pageName com.lq.proxy.jdk
 * @className MyTest
 * @description:
 * @author: liqiang
 * @create: 2024-05-13 13:54
 **/
public class MyTest {

    public static void main(String[] args) {
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        Calculator proxy = CalculatorProxy.getProxy(new MyCalculator());
        int add = proxy.add(1, 1);
        System.out.println(add);
        System.out.println(proxy.getClass());
    }
}
