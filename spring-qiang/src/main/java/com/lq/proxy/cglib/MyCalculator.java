package com.lq.proxy.cglib;

/**
 * @program: spring
 * @pageName com.lq.proxy.cglib
 * @className MyCalculator
 * @description:
 * @author: liqiang
 * @create: 2024-05-14 10:44
 **/
public class MyCalculator {

    public int add(int i, int j) {
        return i + j;
    }

    public int sub(int i, int j) {
        return i - j;
    }

    public int mult(int i, int j) {
        return i * j;
    }

    public int div(int i, int j) {
        return i / j;
    }
}
