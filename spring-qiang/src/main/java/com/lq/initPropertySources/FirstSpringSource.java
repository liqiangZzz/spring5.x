package com.lq.initPropertySources;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/2 14:43
 */
public class FirstSpringSource {

    public FirstSpringSource() {
        System.out.println("FirstSpringSource init");
    }

    public String str;

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "FirstSpringSource{" +
                "str='" + str + '\'' +
                '}';
    }
}
