package com.lq.initPropertySources;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/2 14:48
 */
public class InitPropertySourcesTest {

    public static void main(String[] args) {
        //RequiredPropertiesClassPathXmlApplicationContext加载了多个配置文件，其中部分文件名使用了${config}占位符。
        System.setProperty("config", "config");
        //前面讲过，也可以通过JVM属性设置要激活的profile
        System.setProperty("spring.profiles.active", "uat");
        //必备属性，如果注释掉这两个属性，那么启动报错
        System.setProperty("aa", "aaa");
        System.setProperty("bb", "bbb");
        //初始化容器,加入多套环境
        RequiredPropertiesClassPathXmlApplicationContext ac = new RequiredPropertiesClassPathXmlApplicationContext(new String[]{"spring-config.xml",
                "spring-${config}-dev.xml", "spring-${config}-uat.xml"});
        System.out.println(ac.getBean("firstSpringSource"));
    }
}
