package com.lq.initPropertySources;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/2 14:46
 */
public class RequiredPropertiesClassPathXmlApplicationContext extends ClassPathXmlApplicationContext {

    public RequiredPropertiesClassPathXmlApplicationContext(String[] s) {
        super(s);
    }



    /**
     * 子类容器的扩展
     */
    @Override
    protected void initPropertySources() {
        //获取环境
        ConfigurableEnvironment environment = getEnvironment();
        //设置必须属性名集合
        environment.setRequiredProperties("aa", "bb");
    }
}
