package com.lq.resolveBeforeInstantiation;

/**
 * @program: spring
 * @pageName com.lq.resolveBeforeInstantiation
 * @className BeforeInstantiation
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 11:05
 **/
public class BeforeInstantiation {

    public void doSomeThing(){
        System.out.println("执行do some thing ...");
    }
}
