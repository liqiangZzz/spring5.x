package com.lq.resolveBeforeInstantiation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.resolveBeforeInstantiation
 * @className MyResolveBeforeInstantiationTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 11:20
 **/
public class MyResolveBeforeInstantiationTest {

    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("resolveBeforeInstantiation.xml");
        BeforeInstantiation bean = ac.getBean(BeforeInstantiation.class);
        bean.doSomeThing();
    }
}
