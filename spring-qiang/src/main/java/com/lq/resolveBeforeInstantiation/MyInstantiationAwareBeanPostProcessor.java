package com.lq.resolveBeforeInstantiation;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.cglib.proxy.Enhancer;

/**
 * @program: spring
 * @pageName com.lq.resolveBeforeInstantiation
 * @className MyInstantiationAwareBeanPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 11:08
 **/
public class MyInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {


    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println("beanName :" + beanName + " -> 实例化前的后期处理-> 执行 postProcessBeforeInstantiation 方法");
        if (beanClass == BeforeInstantiation.class) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(beanClass);
            enhancer.setCallback(new MyMethodInterceptor());
            BeforeInstantiation beforeInstantiation = (BeforeInstantiation) enhancer.create();
            System.out.print("返回动态代理：\n");
            return beforeInstantiation;
        }
        return null;
    }

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        System.out.println("beanName :" + beanName + " -> 实例化后的后处理-> 执行 postProcessAfterInstantiation 方法");
        return false;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("beanName :" + beanName + " -> 初始化前的后期处理-> 执行 postProcessBeforeInitialization 方法");
        return bean;
    }


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("beanName :" + beanName + " -> 初始化后的后期处理-> 执行 postProcessAfterInitialization 方法");
        return bean;
    }

    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        System.out.println("beanName :" + beanName + " -> 后期处理属性-> 执行 postProcessProperties 方法");
        return pvs;
    }
}
