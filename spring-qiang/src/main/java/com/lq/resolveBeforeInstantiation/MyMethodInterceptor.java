package com.lq.resolveBeforeInstantiation;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @program: spring
 * @pageName com.lq.resolveBeforeInstantiation
 * @className MyMethodInterceptor
 * @description: 拦截器
 * @author: liqiang
 * @create: 2024-03-07 11:05
 **/
public class MyMethodInterceptor implements MethodInterceptor {


    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("目标方法前：" + method);
        Object o1 = methodProxy.invokeSuper(o, objects);
        System.out.println("目标方法后：" + method);
        return o1;
    }
}
