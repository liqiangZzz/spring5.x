package com.lq.supplier;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.supplier
 * @className MySupplierTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 14:03
 **/
public class MySupplierTest {

    public static void main(String[] args) {
        ApplicationContext context =new ClassPathXmlApplicationContext("supplier.xml");

        User user = context.getBean(User.class);
        System.out.println(user.getUsername());
    }
}
