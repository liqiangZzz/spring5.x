package com.lq.methodOverrides.lookup;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.lookup
 * @className Apple
 * @description:
 * @author: liqiang
 * @create: 2024-03-06 14:17
 **/
public class Apple extends Fruit {

   /* private Banana banana;
*/
    public Apple() {
        System.out.println("I got a fresh apple");
    }

 /*   public Banana getBanana() {
        return banana;
    }

    public void setBanana(Banana banana) {
        this.banana = banana;
    }*/
}