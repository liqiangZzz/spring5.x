package com.lq.methodOverrides.lookup;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.lookup
 * @className LookupMethodTest
 * @description: lookup-method  标签测试
 * @author: liqiang
 * @create: 2024-03-06 14:35
 **/
public class LookupMethodTest {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("methodOverride.xml");

        FruitPlate appleFruitPlate = (FruitPlate) context.getBean("appleFruitPlate");
        System.out.println(appleFruitPlate.getFruit());
        System.out.println(appleFruitPlate.getFruit());
        FruitPlate bananaFruitPlate = (FruitPlate) context.getBean("bananaFruitPlate");
        System.out.println(bananaFruitPlate.getFruit());

    }
}
