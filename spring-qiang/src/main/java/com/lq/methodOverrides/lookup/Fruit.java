package com.lq.methodOverrides.lookup;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.lookup
 * @className Fruit
 * @description:
 * @author: liqiang
 * @create: 2024-03-06 14:17
 **/
public class Fruit {

    public Fruit() {
        System.out.println("I got Fruit");
    }
}