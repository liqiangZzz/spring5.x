package com.lq.methodOverrides.lookup;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.lookup
 * @className Banana
 * @description:
 * @author: liqiang
 * @create: 2024-03-06 14:17
 **/
public class Banana extends Fruit {

    public Banana() {
        System.out.println("I got a  fresh banana");
    }
}