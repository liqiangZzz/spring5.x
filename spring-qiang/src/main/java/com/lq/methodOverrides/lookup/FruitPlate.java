package com.lq.methodOverrides.lookup;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.lookup
 * @className FruitPlate
 * @description:
 * @author: liqiang
 * @create: 2024-03-06 14:17
 **/
public abstract class FruitPlate {

    /**
     * 抽象方法获取新鲜水果
     *
     * @return
     */
    public abstract Fruit getFruit();
}