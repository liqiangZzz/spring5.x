package com.lq.methodOverrides.replace;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.replace
 * @className ReplaceDog
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 10:31
 **/
public class ReplaceDog implements MethodReplacer {

    @Override
    public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
        System.out.println("Hello, I am a white dog...");

        Arrays.stream(args).forEach(str -> System.out.println("参数:" + str));
        return obj;
    }
}
