package com.lq.methodOverrides.replace;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.replace
 * @className OriginalDog
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 10:30
 **/
public class OriginalDog {

    public void sayHello() {
        System.out.println("Hello,I am a black dog...");
    }

    public void sayHello(String name) {
        System.out.println("Hello,I am a black dog, my name is " + name);
    }
}
