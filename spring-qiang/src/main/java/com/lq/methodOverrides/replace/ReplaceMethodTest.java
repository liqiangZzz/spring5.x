package com.lq.methodOverrides.replace;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.methodOverrides.replace
 * @className ReplaceMethodTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 10:32
 **/
public class ReplaceMethodTest {
    public static void main(String[] args) {
        ApplicationContext app = new ClassPathXmlApplicationContext("methodOverride.xml");
        OriginalDog originalDogReplaceMethod = app.getBean("originalDogReplaceMethod", OriginalDog.class);
        originalDogReplaceMethod.sayHello("结果被替换");
    }

}
