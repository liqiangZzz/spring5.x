package com.lq.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * @program: spring
 * @pageName com.lq.config
 * @className MyComponentScan
 * @description:
 * @author: liqiang
 * @create: 2024-01-26 15:49
 **/
@Configuration
@ComponentScan("com.lq.selftag")
public class MyComponentScan {

    @ComponentScan("com.lq.selftag")
    @Configuration
    @Order(90)
    class InnerClass{

    }

}
