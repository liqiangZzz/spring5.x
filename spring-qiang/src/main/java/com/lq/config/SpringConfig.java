package com.lq.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @program: spring
 * @pageName com.lq.config
 * @className SpringConfig
 * @description:
 * @author: liqiang
 * @create: 2024-01-26 15:49
 **/
@Configuration
//@ComponentScan("com.lq")
@Component
public class SpringConfig {
}
