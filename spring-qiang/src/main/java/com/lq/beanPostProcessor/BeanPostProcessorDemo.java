package com.lq.beanPostProcessor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/9 15:34
 */
public class BeanPostProcessorDemo {

    private String string;

    public BeanPostProcessorDemo() {
        System.out.println("无参构造器调用");
    }

    public BeanPostProcessorDemo(String string) {
        System.out.println("带参构造器调用");
        this.string = string;
    }

    public void setString(String string) {
        System.out.println("setter调用");
        this.string = string;
    }

    public void init(){
        System.out.println("+++++++++++++++ init-method +++++++++++++++++++");
    }

    @Override
    public String toString() {
        return "BeanPostProcessorDemo{" +
                "string='" + string + '\'' +
                '}';
    }

}
