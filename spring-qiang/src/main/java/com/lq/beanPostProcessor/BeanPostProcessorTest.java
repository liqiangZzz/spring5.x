package com.lq.beanPostProcessor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/9 15:34
 */
public class BeanPostProcessorTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("MyBeanPostProcessor.xml");
        //手动注入，不会触发BeanPostProcessor回调
        ac.getBeanFactory().registerSingleton("beanPostProcessorDemo2", new BeanPostProcessorDemo("registerSingleton"));

    }
}
