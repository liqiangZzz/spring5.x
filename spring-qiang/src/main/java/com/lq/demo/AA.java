package com.lq.demo;

import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * @program: spring
 * @pageName com.lq.demo
 * @className AA
 * @description:
 * @author: liqiang
 * @create: 2024-01-25 13:49
 **/
//@Component
@Scope(scopeName = "prototype",proxyMode= ScopedProxyMode.INTERFACES)
@Description("测试代码")
@Primary
public class AA {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public AA() {
    }

    public AA(int id, String name) {
        this.id = id;
        this.name = name;
    }

/*    @Override
    public String toString() {
        return "AA{" +
                "id=" + id +
                ", name='" + name + 
                '}';
    }*/
}
