package com.lq.demo;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyAccessorFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @program: spring
 * @pageName com.lq.demo
 * @className Person
 * @description:
 * @author: liqiang
 * @create: 2024-03-14 16:09
 **/
public class Person {

    private String name;

    private String idCardNo;

    private Integer age;

    public Person() {
    }

    public Person(String name, String idCardNo, Integer age) {
        this.name = name;
        this.idCardNo = idCardNo;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", idCardNo='" + idCardNo + '\'' +
                ", age=" + age +
                '}';
    }
}

class Test {
    public static void main(String[] args) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        /**
         * BeanWrapper 测试案例
         */
        Person person = new Person();
        BeanWrapper beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess(person);
        beanWrapper.setPropertyValue("name", "liqiang");
        beanWrapper.setPropertyValue("idCardNo", "123456789");
        beanWrapper.setPropertyValue("age", 18);
        System.out.println(person);


        /**
         * PropertyValues  测试案例
         */
        MutablePropertyValues propertyValues = new MutablePropertyValues();
        propertyValues.addPropertyValue("name", "liqiang");
        propertyValues.addPropertyValue("idCardNo", "987654321");
        propertyValues.addPropertyValue("age", 11);
        System.out.println(propertyValues);

        BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(person);
        wrapper.setPropertyValues(propertyValues);
        System.out.println(person);


        /**
         * PropertyDescriptor  测试案例
         */
        PropertyDescriptor nameDescriptor = new PropertyDescriptor("name", Person.class);
        PropertyDescriptor idCardNoDescriptor = new PropertyDescriptor("idCardNo", Person.class);
        PropertyDescriptor ageDescriptor = new PropertyDescriptor("age", Person.class);

        // 获取属性的读取方法（getter 方法）
        Method nameGetter = nameDescriptor.getReadMethod();
        Method idCardNoGetter = idCardNoDescriptor.getReadMethod();
        Method ageGetter = ageDescriptor.getReadMethod();

        // 输出属性的类型和读取方法名
        System.out.println("Name type: " + nameDescriptor.getPropertyType());
        System.out.println("Name getter method: " + nameGetter.getName());

        System.out.println("IdCardNo type: " + idCardNoDescriptor.getPropertyType());
        System.out.println("IdCardNo getter method: " + idCardNoGetter.getName());

        System.out.println("Age type: " + ageDescriptor.getPropertyType());
        System.out.println("Age getter method: " + ageGetter.getName());

        // 设置属性的写入方法（setter 方法）
        Method nameSetter = nameDescriptor.getWriteMethod();
        Method idCardNoSetter = idCardNoDescriptor.getWriteMethod();
        Method ageSetter = ageDescriptor.getWriteMethod();

        System.out.println("Name setter method: " + nameSetter.getName());
        System.out.println("IdCardNo setter method: " + idCardNoSetter.getName());
        System.out.println("Age setter method: " + ageSetter.getName());

        nameSetter.invoke(person, "Alice");
        idCardNoSetter.invoke(person, "1853545151");
        ageSetter.invoke(person, 25);

        System.out.println("Name: " + person.getName());
        System.out.println("IdCardNo: " + person.getIdCardNo());
        System.out.println("Age: " + person.getAge());


    }
}