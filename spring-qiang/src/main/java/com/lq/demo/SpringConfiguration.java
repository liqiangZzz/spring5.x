package com.lq.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @program: spring
 * @pageName com.lq.demo
 * @className SpringConfiguration
 * @description:
 * @author: liqiang
 * @create: 2024-02-26 16:56
 **/
//@Component
@Configuration
public class SpringConfiguration {

    @Bean
    public A a() {
        System.out.println("创建A对象");
        return new A();
    }

    @Bean
    public B b() {
        a();
        return new B();
    }
}

class A {

}

class B {
}
