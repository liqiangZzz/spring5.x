package com.lq.demo;

/**
 * @program: spring
 * @pageName com.lq.demo
 * @className A
 * @description:
 * @author: liqiang
 * @create: 2024-03-11 14:09
 **/
public class AAA {

    private BBB bbb;

    private Integer id;

    private String name;

    public AAA() {
    }

    public AAA(BBB bbb, Integer id, String name) {
        this.bbb = bbb;
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "AAA{" +
                "bbb=" + bbb +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class BBB {

}
