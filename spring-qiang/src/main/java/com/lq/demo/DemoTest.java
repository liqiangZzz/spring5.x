package com.lq.demo;

import com.lq.config.SpringConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.demo
 * @className DemoTest
 * @description:
 * @author: liqiang
 * @create: 2024-01-23 15:54
 **/
public class DemoTest {

    public static void main(String[] args) {
        //ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("com.lq");
        //ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        AA bean = applicationContext.getBean(AA.class);
        System.out.println(bean.getClass());
    }
}
