package com.lq.entity;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @program: spring
 * @pageName com.lq.entity
 * @className Student
 * @description:
 * @author: liqiang
 * @create: 2024-02-18 14:52
 **/
public class Student implements ApplicationContextAware, BeanFactoryAware {

    private int id;

    private String name;

    private ApplicationContext applicationContext;

    private BeanFactory beanFactory;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    public void  init(){
        System.out.println("初始化方法执行");
    }

    @PreDestroy
    public void  destroy(){
        System.out.println("摧毁方法执行");
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
