package com.lq.selfEditor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq
 * @className MyPropertyEditorTest
 * @description:
 * @author: liqiang
 * @create: 2024-01-29 17:36
 **/
public class MyPropertyEditorTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("selfEditor.xml");
        Customer bean = applicationContext.getBean(Customer.class);
        System.out.println(bean);
    }
}
