package com.lq.selfEditor;


import java.beans.PropertyEditorSupport;

/**
 * @program: spring
 * @pageName com.lq.selfEditor
 * @className AddressPropertyEditor
 * @description: 属性编辑器
 * @author: liqiang
 * @create: 2024-01-29 17:43
 **/
public class AddressPropertyEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String[] s = text.split("_");
        Address address = new Address();
        address.setProvince(s[0]);
        address.setCity(s[1]);
        address.setTown(s[2]);
        this.setValue(address);
    }
}
