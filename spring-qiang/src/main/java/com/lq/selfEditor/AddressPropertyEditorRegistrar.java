package com.lq.selfEditor;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;

/**
 * @program: spring
 * @pageName com.lq.selfEditor
 * @className AddressPropertyEditorRegistrar
 * @description: 属性编辑器注册器
 * @author: liqiang
 * @create: 2024-01-29 17:41
 **/
public class AddressPropertyEditorRegistrar implements PropertyEditorRegistrar {
    @Override
    public void registerCustomEditors(PropertyEditorRegistry registry) {
        registry.registerCustomEditor(Address.class,new AddressPropertyEditor());
    }
}
