package com.lq.smartInitializingSingleton;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/16 17:34
 */
public class MySmartInitializingSingletonTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("spring-config-smart.xml");
    }
}
