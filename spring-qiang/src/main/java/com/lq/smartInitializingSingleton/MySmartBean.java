package com.lq.smartInitializingSingleton;

import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/16 17:32
 */
public class MySmartBean {

    @Component
    public static class MySmartBeanA implements SmartInitializingSingleton {
        @Override
        public void afterSingletonsInstantiated() {
            System.out.println("MySmartBeanA SmartInitializingSingleton");
        }

        @PostConstruct
        public void init() {
            System.out.println("MySmartBeanA initMethod ");
        }

        public MySmartBeanA() {
            System.out.println("MySmartBeanA constructor");
        }

    }

    @Component
    public static class MySmartBeanB implements SmartInitializingSingleton {
        @Override
        public void afterSingletonsInstantiated() {
            System.out.println("MySmartBeanB SmartInitializingSingleton");
        }

        @PostConstruct
        public void init() {
            System.out.println("MySmartBeanB initMethod ");
        }

        public MySmartBeanB() {
            System.out.println("MySmartBeanB constructor");
        }

    }

    @Component
    public static class MySmartBeanC  {

        @PostConstruct
        public void init() {
            System.out.println("MySmartBeanC initMethod ");
        }

        public MySmartBeanC() {
            System.out.println("MySmartBeanC constructor");
        }
    }

}
