package com.lq.tx.xml.dao;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @program: spring
 * @pageName com.lq.tx.xml.dao
 * @className BookStockDao
 * @description:
 * @author: liqiang
 * @create: 2024-06-06 15:52
 **/
public class BookStockDao {


    JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void updateBookStock(int id) {
        String sql = "update book_stock set stock = stock-1 where id=?";
        jdbcTemplate.update(sql, id);

        for (int i = 1; i >= 0; i--)
            System.out.println(10 / i);
    }

    public int getStock(int id) {
        String sql = "select stock from book_stock where id=?";
        return jdbcTemplate.queryForObject(sql, Integer.class, id);
    }

    public void updateBookDesc(int id, int stock) {
        String sql = "update book_stock set book_desc = ? where id=?";
        jdbcTemplate.update(sql, stock, id);
    }
}
