package com.lq.tx.xml;

import com.lq.tx.xml.service.BookStockService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.tx.xml
 * @className MyTest
 * @description:
 * @author: liqiang
 * @create: 2024-06-06 16:01
 **/
public class MyTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("tx.xml");
        BookStockService bookStockService = applicationContext.getBean(BookStockService.class);
        bookStockService.checkout("张三",1);
    }
}
