package com.lq.tx.xml.service;

import com.lq.tx.xml.dao.BookStockDao;

/**
 * @program: spring
 * @pageName com.lq.tx.xml.dao.service
 * @className BookStockService
 * @description:
 * @author: liqiang
 * @create: 2024-06-06 15:52
 **/
public class BookStockService {

    private BookStockDao bookStockDao;

    public void setBookStockDao(BookStockDao bookStockDao) {
        this.bookStockDao = bookStockDao;
    }


    public void checkout(String username, int id) {

        try {
            bookStockDao.updateBookStock(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println(10 / 0);
        // int stock = bookStockDao.getStock(id);
        // bookStockDao.updateBookDesc(id, stock);
    }
}
