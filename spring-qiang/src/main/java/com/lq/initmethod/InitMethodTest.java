package com.lq.initmethod;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.initmethod
 * @className InitMethodTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-19 11:37
 **/
public class InitMethodTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("initMethod.xml");
    }
}
