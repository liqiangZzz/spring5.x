package com.lq.initmethod;

import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;

/**
 * @program: spring
 * @pageName com.lq.initmethod
 * @className Person
 * @description:
 * @author: liqiang
 * @create: 2024-03-19 11:36
 **/
public class Person implements InitializingBean {


    /**
     * 如果 afterPropertiesSet 为 初始化方法名，只会执行一次
     * @throws Exception
     */
    //@PostConstruct
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("person afterPropertiesSet 执行");
    }
}
