package com.lq.event;

import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.stereotype.Component;

/**
 * @program: spring
 * @pageName com.lq.event
 * @className UserSimpleApplicationEventMulticaster
 * @description: 广播器
 * @author: liqiang
 * @create: 2024-02-28 18:05
 **/
@Component
public class UserSimpleApplicationEventMulticaster extends SimpleApplicationEventMulticaster {


}
