package com.lq.event;

import org.springframework.context.ApplicationEvent;

import java.io.Serializable;

/**
 * @program: spring
 * @pageName com.lq.event
 * @className UserRegisterEvent
 * @description: 事件
 * @author: liqiang
 * @create: 2024-02-28 17:15
 **/
public class UserRegisterEvent extends ApplicationEvent implements Serializable {

    private static final long serialVersionUID = 8900871448929256804L;

    public UserRegisterEvent(Object source) {
        super(source);
        System.out.println("注册事件发布完毕");
    }

}
