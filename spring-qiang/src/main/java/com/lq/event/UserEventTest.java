package com.lq.event;

import com.lq.MyClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.event
 * @className UserEventTest
 * @description:
 * @author: liqiang
 * @create: 2024-02-28 17:17
 **/
public class UserEventTest {

    public static void main(String[] args) {
        MyClassPathXmlApplicationContext applicationContext = new MyClassPathXmlApplicationContext("applicationContext.xml");

        UserRegisterEventSource userRegisterEventSource = applicationContext.getBean(UserRegisterEventSource.class);
        userRegisterEventSource.publishEvent();
        UserLoginEventSource userLoginEventSource = applicationContext.getBean(UserLoginEventSource.class);
        userLoginEventSource.publishEvent();

        UserSimpleApplicationEventMulticaster userEventBroadcaster = applicationContext.getBean(UserSimpleApplicationEventMulticaster.class);

        UserEventListener userEventListener = applicationContext.getBean(UserEventListener.class);
        userEventBroadcaster.addApplicationListener(userEventListener);

        applicationContext.close();
    }
}
