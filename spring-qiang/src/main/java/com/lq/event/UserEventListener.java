package com.lq.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @program: spring
 * @pageName com.lq.event
 * @className UserEventListener
 * @description: 事件监听器
 * @author: liqiang
 * @create: 2024-02-28 17:16
 **/
@Component
public class UserEventListener implements ApplicationListener<ApplicationEvent> {

    /**
     * 处理应用程序事件
     */
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("监听到用户事件  --->"+ event.getClass());
    }
}
