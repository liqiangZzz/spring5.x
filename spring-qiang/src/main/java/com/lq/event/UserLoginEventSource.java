package com.lq.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;


/**
 * @program: spring
 * @pageName com.lq.event
 * @className UserLoginEventSource
 * @description: 发布者 -> 事件源 用来发布事件
 * @author: liqiang
 * @create: 2024-02-28 17:15
 **/
@Component
public class UserLoginEventSource implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * 发布事件
     */
    public void publishEvent() {
        System.out.println("事件源开始发布登录事件");
        applicationEventPublisher.publishEvent(new UserLoginEvent(this));
        System.out.println("事件源完成发布登录事件");
    }

}
