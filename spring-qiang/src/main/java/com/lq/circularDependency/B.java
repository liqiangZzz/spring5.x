package com.lq.circularDependency;

/**
 * @program: spring
 * @pageName com.lq.circularDependency
 * @className B
 * @description:
 * @author: liqiang
 * @create: 2024-03-18 17:17
 **/
public class B {

    private A a;

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }


 /*尽量不要进行toString调用
 循环依赖本身不会阻止你重写 toString() 方法，但它可能会导致 toString() 方法的行为异常。因此，在处理循环依赖时，需要仔细考虑 toString() 方法的实现，避免出现无限递归或对象状态不完整等问题。
 @Override
    public String toString() {
        return "B{" +
                "a=" + a +
                '}';
    }*/
}
