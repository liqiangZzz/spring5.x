package com.lq.circularDependency;

/**
 * @program: spring
 * @pageName com.lq.circularDependency
 * @className A
 * @description:
 * @author: liqiang
 * @create: 2024-03-18 17:17
 **/
public class A {

    private B b;

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "A{" +
                "b=" + b +
                '}';
    }
}
