package com.lq.circularDependency;

/**
 * @program: spring
 * @pageName com.lq.circularReference
 * @className Logger
 * @description:
 * @author: liqiang
 * @create: 2024-03-18 17:40
 **/
public class Logger {

    public void recordBefore(){
        System.out.println("recordBefore");
    }

    public void recordAfter(){
        System.out.println("recordAfter");
    }

}
