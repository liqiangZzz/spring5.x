package com.lq;

import com.lq.entity.Student;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @program: spring
 * @pageName com.lq
 * @className MyBeanPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-02-18 14:52
 **/
public class MyBeanPostProcessor implements BeanPostProcessor {

    protected Log logger = LogFactory.getLog(getClass().getName());
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        logger.info("初始化之前操作");
        if (bean instanceof Student){
            Student student = (Student)bean;
            logger.info(student);
            return student;
        }else{
            return bean;
        }
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        logger.info("初始化之后操作");
        if (bean instanceof Student){
            Student student = (Student)bean;
            student.setName("哈哈哈");
            logger.info(student);
            return student;
        }else{
            return bean;
        }
    }
}
