package com.lq.smartFactoryBean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.SmartFactoryBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/17 11:15
 */
public class MySmartFactoryBeanIsEagerInit {

    public static class MySmartFactoryBean{

    }

    /**
     * 非延迟单例的SmartFactoryBean，isEagerInit返回false
     */
    @Component
    public static class MySmartFactoryBeanA implements SmartFactoryBean<MySmartFactoryBean> {

        @Override
        public MySmartFactoryBean getObject() {
            System.out.println("MySmartFactoryBeanA getObject");
            return null;
        }

        @Override
        public Class<?> getObjectType() {
            return Object.class;
        }

        @Override
        public boolean isEagerInit() {
            return false;
        }

    }

    /**
     * 非延迟单例的SmartFactoryBean，isEagerInit返回true
     */
    @Component
    public static class MySmartFactoryBeanB implements SmartFactoryBean<MySmartFactoryBean> {

        @Override
        public MySmartFactoryBean getObject() {
            System.out.println("MySmartFactoryBeanB getObject");
            return null;
        }

        @Override
        public Class<?> getObjectType() {
            return Object.class;
        }

        @Override
        public boolean isEagerInit() {
            return true;
        }
    }

    /**
     * prototype的SmartFactoryBean，isEagerInit返回true,不是单例
     */
    @Scope("prototype")
    @Component
    public static class MySmartFactoryBeanC implements SmartFactoryBean<MySmartFactoryBean> {

        @Override
        public MySmartFactoryBean getObject() {
            System.out.println("MySmartFactoryBeanC getObject");
            return null;
        }

        @Override
        public Class<?> getObjectType() {
            return Object.class;
        }

        @Override
        public boolean isEagerInit() {
            return true;
        }

    }

    /**
     * 非延迟单例的普通FactoryBean
     */
    @Component
    public static class MySmartFactoryBeanE implements FactoryBean<MySmartFactoryBean> {
        @Override
        public MySmartFactoryBean getObject() {
            System.out.println("MySmartFactoryBeanC getObject");
            return null;
        }

        @Override
        public Class<?> getObjectType() {
            return Object.class;
        }
    }
}
