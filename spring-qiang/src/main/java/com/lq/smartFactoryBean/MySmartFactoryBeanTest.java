package com.lq.smartFactoryBean;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : liqiang
 * @description :
 * @createDate : 2025/1/16 17:34
 */
public class MySmartFactoryBeanTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("spring-config-smart.xml");
    }
}
