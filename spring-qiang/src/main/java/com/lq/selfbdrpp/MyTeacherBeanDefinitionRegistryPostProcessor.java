package com.lq.selfbdrpp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * @program: spring
 * @pageName com.lq.selfbdrpp
 * @className MyTeacherBeanDefinitionRegistryPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-02-02 16:28
 **/
public class MyTeacherBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    protected Log logger = LogFactory.getLog(getClass().getName());

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MyTeacherBeanDefinitionRegistryPostProcessor{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        logger.info("MyTeacherBeanDefinitionRegistryPostProcessor ---->执行 postProcessBeanFactory--->" + toString());
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        logger.info("MyTeacherBeanDefinitionRegistryPostProcessor ---->执行 postProcessBeanDefinitionRegistry--->" + toString());
    }
}
