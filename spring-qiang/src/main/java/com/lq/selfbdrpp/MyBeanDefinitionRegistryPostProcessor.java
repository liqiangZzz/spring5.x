package com.lq.selfbdrpp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * @program: spring
 * @pageName com.lq.selfbdrpp
 * @className MyBeanDefinitionRegistryPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-02-02 16:17
 **/
public class MyBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    protected Log logger = LogFactory.getLog(getClass().getName());
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        logger.info("MyBeanDefinitionRegistryPostProcessor ---->执行 postProcessBeanDefinitionRegistry");

        BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(MyTeacherBeanDefinitionRegistryPostProcessor.class);
        builder.addPropertyValue("name", "lisi");
        registry.registerBeanDefinition("myTeacher", builder.getBeanDefinition());

        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(MyTeacherBeanDefinitionRegistryPostProcessor.class);
        registry.registerBeanDefinition("teacher", rootBeanDefinition);
        BeanDefinition teacher = registry.getBeanDefinition("teacher");
        MutablePropertyValues propertyValues = teacher.getPropertyValues();
        propertyValues.addPropertyValue("name", "张三");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        logger.info("MyBeanDefinitionRegistryPostProcessor ---->执行 postProcessBeanFactory");
    }

}
