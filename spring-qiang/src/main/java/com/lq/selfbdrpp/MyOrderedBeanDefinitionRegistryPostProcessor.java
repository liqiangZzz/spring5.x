package com.lq.selfbdrpp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.Ordered;

/**
 * @program: spring
 * @pageName com.lq.selfbdrpp
 * @className MyOrderedBeanDefinitionRegistryPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-02-02 16:28
 **/
public class MyOrderedBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor, Ordered {

    protected Log logger = LogFactory.getLog(getClass().getName());

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        logger.info("MyOrderedBeanDefinitionRegistryPostProcessor ---->执行 postProcessBeanDefinitionRegistry");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        logger.info("MyOrderedBeanDefinitionRegistryPostProcessor ---->执行 postProcessBeanFactory");
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
