package com.lq.candidateConstructors;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @program: spring
 * @pageName com.lq.entity
 * @className Student
 * @description:
 * @author: liqiang
 * @create: 2024-02-18 14:52
 **/
public class Student {

    private Integer id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //@Autowired(required = false)
    public Student() {
    }

    //@Autowired(required = false)
    public Student(Integer id) {
        this.id = id;
    }

    @Autowired(required = false)
    public Student(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
