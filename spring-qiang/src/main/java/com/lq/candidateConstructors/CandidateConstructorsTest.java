package com.lq.candidateConstructors;

import com.lq.MyClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq
 * @className CandidateConstructorsTest
 * @description:
 * @author: liqiang
 * @create: 2024-01-31 17:30
 **/
public class CandidateConstructorsTest {

    public static void main(String[] args) {
        MyClassPathXmlApplicationContext applicationContext = new MyClassPathXmlApplicationContext("candidateConstructors.xml");

        Student student = applicationContext.getBean("student", Student.class);

        Student student2 = applicationContext.getBean("student", Student.class);

        applicationContext.close();
    }
}
