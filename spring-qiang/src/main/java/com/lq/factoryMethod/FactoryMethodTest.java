package com.lq.factoryMethod;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.factoryMethod
 * @className FactoryMethodTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-07 16:55
 **/
public class FactoryMethodTest {

    public static void main(String[] args) {

        ApplicationContext context =new ClassPathXmlApplicationContext("factoryMethod.xml");

        Person person = context.getBean("person", Person.class);
        System.out.println(person);

        Person person2 = context.getBean("person2", Person.class);
        System.out.println(person2);

    }
}
