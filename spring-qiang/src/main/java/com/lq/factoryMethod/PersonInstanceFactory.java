package com.lq.factoryMethod;

/**
 * @program: spring
 * @pageName com.lq.factoryMethod
 * @className PersonInstanceFactory
 * @description: 实例工厂
 * @author: liqiang
 * @create: 2024-03-07 15:43
 **/
public class PersonInstanceFactory {
    public Person getPerson(String name) {
        Person person = new Person();
        person.setId(1);
        person.setName(name);
        return person;
    }

}
