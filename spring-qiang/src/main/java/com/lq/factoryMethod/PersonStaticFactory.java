package com.lq.factoryMethod;

/**
 * @program: spring
 * @pageName com.lq.factoryMethod
 * @className PersonStaticFactory
 * @description: 静态工厂
 * @author: liqiang
 * @create: 2024-03-07 16:54
 **/
public class PersonStaticFactory {

    public static Person getPerson(String name) {
        Person person = new Person();
        person.setId(1);
        person.setName(name);
        return person;
    }
}
