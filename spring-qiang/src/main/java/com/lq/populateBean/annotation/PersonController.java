package com.lq.populateBean.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @program: spring
 * @pageName com.lq.populateBean.annotation.PersonController
 * @className PersonController
 * @description:
 * @author: liqiang
 * @create: 2024-03-15 10:30
 **/
@Controller
public class PersonController {
    @Autowired
    private PersonService personService;
}
