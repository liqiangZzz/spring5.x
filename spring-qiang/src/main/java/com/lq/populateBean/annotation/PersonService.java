package com.lq.populateBean.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @program: spring
 * @pageName com.lq.populateBean.annotation.PersonService
 * @className PersonService
 * @description:
 * @author: liqiang
 * @create: 2024-03-15 10:30
 **/
@Service
public class PersonService {

    @Resource
    private PersonDao personDao;
}
