package com.lq.populateBean;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: spring
 * @pageName com.lq.populateBean
 * @className PopulateBeanTest
 * @description:
 * @author: liqiang
 * @create: 2024-03-13 17:24
 **/
public class PopulateBeanTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("populateBean.xml");
    }
}
