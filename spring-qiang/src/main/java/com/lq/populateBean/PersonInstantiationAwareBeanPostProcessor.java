package com.lq.populateBean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @program: spring
 * @pageName com.lq.populateBean
 * @className PersonInstantiationAwareBeanPostProcessor
 * @description:
 * @author: liqiang
 * @create: 2024-03-13 17:25
 **/

@Component
public class PersonInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {


    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        Person person = null;
        if (bean instanceof Person) {
            person = (Person) bean;
            person.setName("王麻子");
            return true;
        }
        return false;
    }
}
