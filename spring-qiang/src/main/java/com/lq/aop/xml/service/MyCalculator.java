package com.lq.aop.xml.service;

/**
 * @program: spring
 * @pageName com.lq.aop.xml.util
 * @className TestAop
 * @description:
 * @author: liqiang
 * @create: 2024-03-21 10:00
 **/
public class MyCalculator {
    public Integer add(Integer i, Integer j) {
        return i + j;
    }

    public Integer sub(Integer i, Integer j) {
        return i - j;
    }

    public Integer mul(Integer i, Integer j) {
        return i * j;
    }

    public Integer div(Integer i, Integer j) {
        return i / j;
    }

    public Integer show(Integer i) {
        System.out.println("show .....");
        return i;
    }

    @Override
    public String toString() {
        return "public String toString()";
    }
}
