package com.lq.aop.annotation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @program: spring
 * @pageName com.lq.aop.annotation.config
 * @className SpringConfiguration
 * @description:
 * @author: liqiang
 * @create: 2024-03-21 10:00
 **/
@Configuration
@ComponentScan(basePackages="com.lq.aop.annotation")
@EnableAspectJAutoProxy
public class SpringConfiguration { }
