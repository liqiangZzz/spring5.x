package com.lq.aop.annotation.service;

import org.springframework.stereotype.Service;

/**
 * @program: spring
 * @pageName com.lq.aop.annotation.service
 * @className MyCalculator
 * @description:
 * @author: liqiang
 * @create: 2024-03-21 10:00
 **/
@Service
public class MyCalculator {
    public Integer add(Integer i, Integer j) {
        return i+j;
    }

    public Integer sub(Integer i, Integer j) {
        return i-j;
    }

    public Integer mul(Integer i, Integer j) {
        return i*j;
    }

    public Integer div(Integer i, Integer j) {
        return i/j;
    }

    public Integer show(Integer i){
        System.out.println("show .....");
        return i;
    }
}
