package com.lq.aop.annotation;

import com.lq.aop.annotation.config.SpringConfiguration;
import com.lq.aop.annotation.service.MyCalculator;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
/**
 * @program: spring
 * @pageName com.lq.aop.annotation
 * @className TestAnnotationAop
 * @description:
 * @author: liqiang
 * @create: 2024-03-21 10:00
 **/
public class TestAnnotationAop {

    public static void main(String[] args) throws NoSuchMethodException {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();
        ac.register(SpringConfiguration.class);
        ac.refresh();
        MyCalculator bean = ac.getBean(MyCalculator.class);
        System.out.println(bean.add(1, 1));
    }
}
