package com.lq;

import com.lq.demo.AAA;
import com.lq.entity.Student;

/**
 * @program: spring
 * @pageName com.lq
 * @className MyTest
 * @description:
 * @author: liqiang
 * @create: 2024-01-31 17:30
 **/
public class MyTest {

    public static void main(String[] args) {
        MyClassPathXmlApplicationContext applicationContext = new MyClassPathXmlApplicationContext("applicationContext.xml");
        Student student = applicationContext.getBean("student", Student.class);
        System.out.println(student);
        //AAA aaa = applicationContext.getBean("aaa", AAA.class);
        applicationContext.close();
    }
}
