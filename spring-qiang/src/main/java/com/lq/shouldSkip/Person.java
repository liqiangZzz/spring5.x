package com.lq.shouldSkip;

/**
 * @program: spring
 * @pageName com.lq.shouldSkip
 * @className Person
 * @description:
 * @author: liqiang
 * @create: 2024-01-23 15:54
 **/
public class Person {
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
    @Override
    public String toString() {
        return "Person{" + "name='" + name + '\'' + ", age=" + age + '}';
    }
}