/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction;

import org.springframework.lang.Nullable;

/**
 * 命令式事务管理器
 *<p>
 * 该接口是Spring事务管理器的核心接口，用于实现命令式事务管理。它定义了开始、提交、回滚等事务管理操作的方法，
 * 但通常不直接被应用程序使用，而是通过TransactionTemplate或AOP进行声明式事务管理。
 * 实现该接口的推荐方式是继承AbstractPlatformTransactionManager，并实现特定事务状态下的模板方法。
 * 该接口的默认实现包括JtaTransactionManager和DataSourceTransactionManager等
 *<p>
 * This is the central interface in Spring's imperative transaction infrastructure.
 * Applications can use this directly, but it is not primarily meant as an API:
 * Typically, applications will work with either TransactionTemplate or
 * declarative transaction demarcation through AOP.
 *
 * <p>For implementors, it is recommended to derive from the provided
 * {@link org.springframework.transaction.support.AbstractPlatformTransactionManager}
 * class, which pre-implements the defined propagation behavior and takes care
 * of transaction synchronization handling. Subclasses have to implement
 * template methods for specific states of the underlying transaction,
 * for example: begin, suspend, resume, commit.
 *
 * <p>The default implementations of this strategy interface are
 * {@link org.springframework.transaction.jta.JtaTransactionManager} and
 * {link org.springframework.jdbc.datasource.DataSourceTransactionManager},
 * which can serve as an implementation guide for other transaction strategies.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @since 16.05.2003
 * @see org.springframework.transaction.support.TransactionTemplate
 * @see org.springframework.transaction.interceptor.TransactionInterceptor
 * @see org.springframework.transaction.ReactiveTransactionManager
 */
public interface PlatformTransactionManager extends TransactionManager {

	/**
	 * 返回当前活动事务（当前线程栈的事务）或根据指定的传播行为创建一个新事务
	 * <p>
	 * 根据指定的传播行为，返回当前活跃的事务或创建一个新的事务。如果传入的definition参数不为空，
	 * 则描述了事务的传播行为、隔离级别、超时时间等属性。注意，这些属性只会应用到新创建的事务中，
	 * 如果参与已经活跃的事务则会被忽略。此外，不是所有的事务定义设置都会被每个事务管理器支持，不支持的设置会被抛出异常。
	 * 其中，只读标志是一个例外，如果事务管理器不支持显式的只读模式，则会忽略该标志。
	 * 该函数返回一个表示新事务或当前事务的TransactionStatus对象。如果出现查找、创建或系统错误，会抛出TransactionException异常。
	 * 如果给定的事务定义无法执行（例如，当前活跃的事务与指定的传播行为冲突），则会抛出IllegalTransactionStateException异常。
	 * <p>
	 * Return a currently active transaction or create a new one, according to
	 * the specified propagation behavior.
	 * <p>Note that parameters like isolation level or timeout will only be applied
	 * to new transactions, and thus be ignored when participating in active ones.
	 * <p>Furthermore, not all transaction definition settings will be supported
	 * by every transaction manager: A proper transaction manager implementation
	 * should throw an exception when unsupported settings are encountered.
	 * <p>An exception to the above rule is the read-only flag, which should be
	 * ignored if no explicit read-only mode is supported. Essentially, the
	 * read-only flag is just a hint for potential optimization.
	 * @param definition the TransactionDefinition instance (can be {@code null} for defaults),
	 * describing propagation behavior, isolation level, timeout etc. 事务定义实例（可为null），如果传入的definition参数不为空，则描述了事务的传播行为、隔离级别、超时时间等属性
	 * @return transaction status object representing the new or current transaction  表示新事务或当前事务的事务状态对象
	 * @throws TransactionException in case of lookup, creation, or system errors  查找、创建或系统错误时抛出。
	 * @throws IllegalTransactionStateException if the given transaction definition  给定的事务定义无法执行时抛出。
	 * cannot be executed (for example, if a currently active transaction is in
	 * conflict with the specified propagation behavior)
	 * @see TransactionDefinition#getPropagationBehavior  获取事务的传播行为。
	 * @see TransactionDefinition#getIsolationLevel  获取事务的隔离级别。
	 * @see TransactionDefinition#getTimeout  获取事务的超时时间。
	 * @see TransactionDefinition#isReadOnly  判断事务是否为只读。
	 */
	TransactionStatus getTransaction(@Nullable TransactionDefinition definition)
			throws TransactionException;

	/**
	 * 提交给定事务，实际上和该事物的状态有关，如果事务状态被标记为回滚，则执行回滚而不是提交。
	 * <p>
	 * 根据事务的状态来执行相应的操作。如果事务被标记为回滚，就执行回滚操作。 如果事务不是新事务，则省略提交以正确参与周围事务。
	 * 如果之前有一个事务被挂起以便创建一个新的事务，则在提交新事务后恢复之前的事务。
	 * 在方法执行过程中，如果出现异常，必须完全完成并清理事务，不会期望有回滚调用。
	 * 如果方法抛出除TransactionException之外的异常，则表示在提交尝试之前发生了一些错误，原始异常将被传播给此提交方法的调用者。
	 *
	 * Commit the given transaction, with regard to its status. If the transaction
	 * has been marked rollback-only programmatically, perform a rollback.
	 * <p>If the transaction wasn't a new one, omit the commit for proper
	 * participation in the surrounding transaction. If a previous transaction
	 * has been suspended to be able to create a new one, resume the previous
	 * transaction after committing the new one.
	 * <p>Note that when the commit call completes, no matter if normally or
	 * throwing an exception, the transaction must be fully completed and
	 * cleaned up. No rollback call should be expected in such a case.
	 * <p>If this method throws an exception other than a TransactionException,
	 * then some before-commit error caused the commit attempt to fail. For
	 * example, an O/R Mapping tool might have tried to flush changes to the
	 * database right before commit, with the resulting DataAccessException
	 * causing the transaction to fail. The original exception will be
	 * propagated to the caller of this commit method in such a case.
	 * @param status object returned by the {@code getTransaction} method  任务状态对象，由getTransaction方法返回
	 * @throws UnexpectedRollbackException in case of an unexpected rollback
	 * that the transaction coordinator initiated  在事务协调器启动的意外回滚的情况下
	 * @throws HeuristicCompletionException in case of a transaction failure
	 * caused by a heuristic decision on the side of the transaction coordinator  在事务协调器一侧的启发式决策导致事务失败的情况下
	 * @throws TransactionSystemException in case of commit or system errors
	 * (typically caused by fundamental resource failures)  在提交或系统错误（通常由基本资源故障引起）的情况下
	 * @throws IllegalTransactionStateException if the given transaction
	 * is already completed (that is, committed or rolled back)  如果给定事务已经完成（即提交或回滚）
	 * @see TransactionStatus#setRollbackOnly 设置事务状态回滚
	 */
	void commit(TransactionStatus status) throws TransactionException;

	/**
	 * 回滚事务
	 * <p>
	 * 执行给定事务的回滚<p> 如果事务不是新事务，只需将其设置为回滚，以便正确参与周围的事务。
	 * 如果前一个事务已被挂起以便能够创建新的事务，请在回滚新事务后恢复前一事务<p> <b>如果提交引发异常，则不要对事务调用回滚<b> ，
	 * 即使在提交异常的情况下，当提交返回时，事务也将已经完成并清理完毕。因此，提交失败后的回滚调用将导致IllegalTransactionStateException。
	 *
	 * Perform a rollback of the given transaction.
	 * <p>If the transaction wasn't a new one, just set it rollback-only for proper
	 * participation in the surrounding transaction. If a previous transaction
	 * has been suspended to be able to create a new one, resume the previous
	 * transaction after rolling back the new one.
	 * <p><b>Do not call rollback on a transaction if commit threw an exception.</b>
	 * The transaction will already have been completed and cleaned up when commit
	 * returns, even in case of a commit exception. Consequently, a rollback call
	 * after commit failure will lead to an IllegalTransactionStateException.
	 * @param status object returned by the {@code getTransaction} method 任务状态对象，由getTransaction方法返回
	 * @throws TransactionSystemException in case of rollback or system errors
	 * (typically caused by fundamental resource failures) 在回滚或系统错误（通常由基本资源故障引起）的情况下
	 * @throws IllegalTransactionStateException if the given transaction
	 * is already completed (that is, committed or rolled back)  如果给定事务已经完成（即提交或回滚）
	 */
	void rollback(TransactionStatus status) throws TransactionException;

}
