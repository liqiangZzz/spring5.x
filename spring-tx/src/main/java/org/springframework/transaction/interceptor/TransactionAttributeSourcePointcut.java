/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.interceptor;

import java.io.Serializable;
import java.lang.reflect.Method;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.lang.Nullable;
import org.springframework.transaction.TransactionManager;
import org.springframework.util.ObjectUtils;

/**
 * Abstract class that implements a Pointcut that matches if the underlying
 * {@link TransactionAttributeSource} has an attribute for a given method.
 *
 * @author Juergen Hoeller
 * @since 2.5.5
 */
@SuppressWarnings("serial")
abstract class TransactionAttributeSourcePointcut extends StaticMethodMatcherPointcut implements Serializable {

	/**
	 *  设置ClassFilter为一个TransactionAttributeSourceClassFilter实例
	 */
	protected TransactionAttributeSourcePointcut() {
		setClassFilter(new TransactionAttributeSourceClassFilter());
	}

	/**
	 *  通过委托 AnnotationTransactionAttributeSource 的getTransactionAttribute方法来判断该方法是否匹配
	 * @param method the candidate method  需要匹配的方法
	 * @param targetClass the target class  实际目标类型
	 * @return  true 可以被代理，false不可以被代理
	 */
	@Override
	public boolean matches(Method method, Class<?> targetClass) {
		/**
		 * 获取我们@EnableTransactionManagement注解为我们容器中导入的ProxyTransactionManagementConfiguration
		 * 配置类中的TransactionAttributeSource对象 及 AnnotationTransactionAttributeSource
		 */
		TransactionAttributeSource tas = getTransactionAttributeSource();
		// 若事务属性原为null或者解析出来的事务注解属性不为空，表示方法匹配
		return (tas == null || tas.getTransactionAttribute(method, targetClass) != null);
	}

	@Override
	public boolean equals(@Nullable Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TransactionAttributeSourcePointcut)) {
			return false;
		}
		TransactionAttributeSourcePointcut otherPc = (TransactionAttributeSourcePointcut) other;
		return ObjectUtils.nullSafeEquals(getTransactionAttributeSource(), otherPc.getTransactionAttributeSource());
	}

	@Override
	public int hashCode() {
		return TransactionAttributeSourcePointcut.class.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getName() + ": " + getTransactionAttributeSource();
	}


	/**
	 * 用于获取底层的TransactionAttributeSource对象，该对象可能为null。该方法需要由子类实现。
	 * <p>
	 * Obtain the underlying TransactionAttributeSource (may be {@code null}).
	 * To be implemented by subclasses.
	 */
	@Nullable
	protected abstract TransactionAttributeSource getTransactionAttributeSource();


	/**
	 * 委托给 TransactionAttributeSource类，isCandidateClass方法用于过滤那些方法不值继续得搜索的类。
	 * {@link ClassFilter} that delegates to {@link TransactionAttributeSource#isCandidateClass}
	 * for filtering classes whose methods are not worth searching to begin with.
	 */
	private class TransactionAttributeSourceClassFilter implements ClassFilter {
		/**
		 * TransactionalProxy用于手动创建的事务代理的标记接口，通常与Spring的AOP代理有关，直接对代理类进行事务管理可能会导致循环引用或其他问题。
		 * TransactionManager 事务管理器接口，事务管理器自身不应该成为事务管理的对象，因为要负责事务的开始、提交、回滚等操作。
		 * PersistenceExceptionTranslator 持久化异常转换器，用于将底层数据访问技术的异常转换为Spring的DataAccessException层次结构中的异常。
		 * @param clazz the candidate target class
		 * @return
		 */
		@Override
		public boolean matches(Class<?> clazz) {
			if (TransactionalProxy.class.isAssignableFrom(clazz) ||
					TransactionManager.class.isAssignableFrom(clazz) ||
					PersistenceExceptionTranslator.class.isAssignableFrom(clazz)) {
				return false;
			}
			//如果不是上面那些类型的bean实例，就通过设置的TransactionAttributeSource来判断
			//如果TransactionAttributeSource不为null并且isCandidateClass方法返回true，
			//那么表示当前bean的class允许继续匹配方法，否则表示不会继续匹配，即当前bean实例不会进行事务代理
			TransactionAttributeSource tas = getTransactionAttributeSource();
			return (tas == null || tas.isCandidateClass(clazz));
		}
	}

}
