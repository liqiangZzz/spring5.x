/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.interceptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.lang.Nullable;

/**
 * TransactionAttribute implementation that works out whether a given exception
 * should cause transaction rollback by applying a number of rollback rules,
 * both positive and negative. If no rules are relevant to the exception, it
 * behaves like DefaultTransactionAttribute (rolling back on runtime exceptions).
 *
 * <p>{@link TransactionAttributeEditor} creates objects of this class.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @since 09.04.2003
 * @see TransactionAttributeEditor
 */
@SuppressWarnings("serial")
public class RuleBasedTransactionAttribute extends DefaultTransactionAttribute implements Serializable {

	/**
	 *  回滚异常字符串的前缀
	 * Prefix for rollback-on-exception rules in description strings.
	 */
	public static final String PREFIX_ROLLBACK_RULE = "-";

	/**
	 * 不回滚异常字符串的前缀
	 * Prefix for commit-on-exception rules in description strings.
	 */
	public static final String PREFIX_COMMIT_RULE = "+";


	/** Static for optimal serializability. */
	private static final Log logger = LogFactory.getLog(RuleBasedTransactionAttribute.class);

	/**
	 * 回滚规则的属性集合
	 * 根据parseAttributeSource方法的逻辑，回滚规则在集合前面，不回滚规则在集合后面
	 */
	@Nullable
	private List<RollbackRuleAttribute> rollbackRules;


	/**
	 * 使用默认设置创建一个新的RuleBasedTransactionAttribute。可以通过bean属性setter进行修改
	 * Create a new RuleBasedTransactionAttribute, with default settings.
	 * Can be modified through bean property setters.
	 * @see #setPropagationBehavior
	 * @see #setIsolationLevel
	 * @see #setTimeout
	 * @see #setReadOnly
	 * @see #setName
	 * @see #setRollbackRules
	 */
	public RuleBasedTransactionAttribute() {
		super();
	}

	/**
	 * 复制构造函数。定义可以通过bean属性设置器进行修改。
	 * Copy constructor. Definition can be modified through bean property setters.
	 * @see #setPropagationBehavior
	 * @see #setIsolationLevel
	 * @see #setTimeout
	 * @see #setReadOnly
	 * @see #setName
	 * @see #setRollbackRules
	 */
	public RuleBasedTransactionAttribute(RuleBasedTransactionAttribute other) {
		super(other);
		this.rollbackRules = (other.rollbackRules != null ? new ArrayList<>(other.rollbackRules) : null);
	}

	/**
	 * 使用给定的传播行为创建一个新的DefaultTransactionAttribute。可以通过bean属性setter进行修改。
	 * Create a new DefaultTransactionAttribute with the given
	 * propagation behavior. Can be modified through bean property setters.
	 * @param propagationBehavior one of the propagation constants in the
	 * TransactionDefinition interface TransactionDefinition接口中的一个传播常数
	 * @param rollbackRules the list of RollbackRuleAttributes to apply  RollbackRuleAttributes的列表
	 * @see #setIsolationLevel
	 * @see #setTimeout
	 * @see #setReadOnly
	 */
	public RuleBasedTransactionAttribute(int propagationBehavior, List<RollbackRuleAttribute> rollbackRules) {
		super(propagationBehavior);
		this.rollbackRules = rollbackRules;
	}


	/**
	 * 设置要应用的  RollbackRuleAttribute对象列表 和 NoRollbackRuleAttribute对象。
	 * Set the list of {@code RollbackRuleAttribute} objects
	 * (and/or {@code NoRollbackRuleAttribute} objects) to apply.
	 * @see RollbackRuleAttribute
	 * @see NoRollbackRuleAttribute
	 */
	public void setRollbackRules(List<RollbackRuleAttribute> rollbackRules) {
		this.rollbackRules = rollbackRules;
	}

	/**
	 * 返回  RollbackRuleAttribute 对象的列表
	 * Return the list of {@code RollbackRuleAttribute} objects
	 * (never {@code null}).
	 */
	public List<RollbackRuleAttribute> getRollbackRules() {
		if (this.rollbackRules == null) {
			this.rollbackRules = new LinkedList<>();
		}
		return this.rollbackRules;
	}


	/**
	 * 先处理回滚的规则，就是事务注解里的 rollbackFor，rollbackForClassName，noRollbackFor，noRollbackForClassName属性
	 * 如果没有设置就会调用父类的rollbackOn
	 * 采用"Winning rule"机制来判断对当前异常是否需要进行回滚
	 * 这是最简单的规则，即选择异常的继承层次结构中最接近当前异常的规则。
	 *<p>
	 * 如果需要回滚，则返回true，否则返回false
	 *
	 * Winning rule is the shallowest rule (that is, the closest in the
	 * inheritance hierarchy to the exception). If no rule applies (-1),
	 * return false.
	 * @see TransactionAttribute#rollbackOn(java.lang.Throwable)
	 */
	@Override
	public boolean rollbackOn(Throwable ex) {
		if (logger.isTraceEnabled()) {
			logger.trace("Applying rules to determine whether transaction should rollback on " + ex);
		}
		//rollbackRules中最匹配的回滚规则，默认为null
		RollbackRuleAttribute winner = null;
		//回滚规则匹配成功时的匹配异常栈深度，用来查找最匹配的那一个回滚规则
		int deepest = Integer.MAX_VALUE;

		// 如果rollbackRules回滚规则集合不为null，那么判断回滚规则是否匹配
		if (this.rollbackRules != null) {
			//从前向后遍历，因此回滚规则 RollbackRuleAttribute将会先进行匹配，不回滚规则NoRollbackRuleAttribute将会后进行匹配
			//NoRollbackRuleAttribute是RollbackRuleAttribute的子类
			for (RollbackRuleAttribute rule : this.rollbackRules) {
				//根据当前规则获取匹配时的异常栈深度
				int depth = rule.getDepth(ex);
				//如果规则适用于当前异常（深度大于等于0且小于当前最深的深度），则更新最深的深度和获胜规则（winner）。
				if (depth >= 0 && depth < deepest) {
					deepest = depth;
					winner = rule;
				}
			}
		}

		if (logger.isTraceEnabled()) {
			logger.trace("Winning rollback rule is: " + winner);
		}

		// User superclass behavior (rollback on unchecked) if no rule matches.
		//如果没有找到适用的规则，winner为null，那么说明没有任何匹配，函数会调用父类的rollbackOn方法，使用默认规则（通常回滚未检查异常，即运行时异常）来判断
		// 即如果当前异常属于RuntimeException或者Error级别的异常时，事务才会回滚
		if (winner == null) {
			logger.trace("No relevant rollback rule found: applying default rules");
			return super.rollbackOn(ex);
		}
		//如果有获胜规则，函数会检查该规则是否是NoRollbackRuleAttribute类型。如果不是，表示需要回滚事务，函数返回true；如果是，则不需要回滚，返回false。
		return !(winner instanceof NoRollbackRuleAttribute);
	}


	@Override
	public String toString() {
		StringBuilder result = getAttributeDescription();
		if (this.rollbackRules != null) {
			for (RollbackRuleAttribute rule : this.rollbackRules) {
				//如果是回滚规则，则使用"-"前缀，如果是不回滚规则，则使用"+"前缀
				String sign = (rule instanceof NoRollbackRuleAttribute ? PREFIX_COMMIT_RULE : PREFIX_ROLLBACK_RULE);
				result.append(',').append(sign).append(rule.getExceptionName());
			}
		}
		return result.toString();
	}

}
