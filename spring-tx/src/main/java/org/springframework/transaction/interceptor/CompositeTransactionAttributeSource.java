/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.interceptor;

import java.io.Serializable;
import java.lang.reflect.Method;

import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

/**
 * Composite {@link TransactionAttributeSource} implementation that iterates
 * over a given array of {@link TransactionAttributeSource} instances.
 *
 * @author Juergen Hoeller
 * @since 2.0
 */
@SuppressWarnings("serial")
public class CompositeTransactionAttributeSource implements TransactionAttributeSource, Serializable {

		/**
		 * 用于存储事务属性源，事务属性源是用来获取方法或类的事务属性的策略接口的实现。
		 */
		private final TransactionAttributeSource[] transactionAttributeSources;


		/**
		 * Create a new CompositeTransactionAttributeSource for the given sources.
		 * @param transactionAttributeSources the TransactionAttributeSource instances to combine
		 */
		public CompositeTransactionAttributeSource(TransactionAttributeSource... transactionAttributeSources) {
			Assert.notNull(transactionAttributeSources, "TransactionAttributeSource array must not be null");
			this.transactionAttributeSources = transactionAttributeSources;
		}

		/**
		 * Return the TransactionAttributeSource instances that this
		 * CompositeTransactionAttributeSource combines.
		 */
		public final TransactionAttributeSource[] getTransactionAttributeSources() {
			return this.transactionAttributeSources;
		}

	/**
	 * 判断给定的类是否可以作为事务处理的候选类
	 * @param targetClass the class to introspect 表示要检查的类对象。
	 * @return
	 */
		@Override
		public boolean isCandidateClass(Class<?> targetClass) {
			for (TransactionAttributeSource source : this.transactionAttributeSources) {
				if (source.isCandidateClass(targetClass)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * 用于获取给定方法和类对应的事务属性。该方法遍历所有TransactionAttributeSource实例，如果任意一个实例能够为给定方法和类提供事务属性，则返回该属性，否则返回null。
		 * @param method the method to introspect 需要被检查其事务属性的方法对象。通过反射API，我们可以获取到方法的所有元数据，包括注解信息，这对于判断方法的事务特性至关重要。
		 * @param targetClass the target class (may be {@code null},
		 * in which case the declaring class of the method must be used)  目标类，可能是null，此时将使用方法反射获取的class
		 * @return
		 */
		@Override
		@Nullable
		public TransactionAttribute getTransactionAttribute(Method method, @Nullable Class<?> targetClass) {
			for (TransactionAttributeSource source : this.transactionAttributeSources) {
				TransactionAttribute attr = source.getTransactionAttribute(method, targetClass);
				if (attr != null) {
					return attr;
				}
			}
			return null;
		}

}
