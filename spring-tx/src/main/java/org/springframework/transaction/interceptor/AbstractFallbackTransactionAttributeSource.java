/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.interceptor;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.aop.support.AopUtils;
import org.springframework.core.MethodClassKey;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;

/**
 * Abstract implementation of {@link TransactionAttributeSource} that caches
 * attributes for methods and implements a fallback policy: 1. specific target
 * method; 2. target class; 3. declaring method; 4. declaring class/interface.
 *
 * <p>Defaults to using the target class's transaction attribute if none is
 * associated with the target method. Any transaction attribute associated with
 * the target method completely overrides a class transaction attribute.
 * If none found on the target class, the interface that the invoked method
 * has been called through (in case of a JDK proxy) will be checked.
 *
 * <p>This implementation caches attributes by method after they are first used.
 * If it is ever desirable to allow dynamic changing of transaction attributes
 * (which is very unlikely), caching could be made configurable. Caching is
 * desirable because of the cost of evaluating rollback rules.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @since 1.1
 */
public abstract class AbstractFallbackTransactionAttributeSource implements TransactionAttributeSource {

	/**
	 * 空事务属性，用于表示没有找到方法的事务属性，并且不需要再次查找。为了避免每次查找都进行判断，将其缓存起来以提高效率
	 *
	 * Canonical value held in cache to indicate no transaction attribute was
	 * found for this method, and we don't need to look again.
	 */
	@SuppressWarnings("serial")
	private static final TransactionAttribute NULL_TRANSACTION_ATTRIBUTE = new DefaultTransactionAttribute() {
		@Override
		public String toString() {
			return "null";
		}
	};


	/**
	 * Logger available to subclasses.
	 * <p>As this base class is not marked Serializable, the logger will be recreated
	 * after serialization - provided that the concrete subclass is Serializable.
	 */
	protected final Log logger = LogFactory.getLog(getClass());

	/**
	 * TransactionAttributes的缓存，key定位到目标类上的某个方法。
	 *
	 * Cache of TransactionAttributes, keyed by method on a specific target class.
	 * <p>As this base class is not marked Serializable, the cache will be recreated
	 * after serialization - provided that the concrete subclass is Serializable.
	 */
	private final Map<Object, TransactionAttribute> attributeCache = new ConcurrentHashMap<>(1024);


	/**
	 * 确定此方法调用的事务属性。如果没有找到方法属性，则默认为类的事务属性。
	 *
	 * Determine the transaction attribute for this method invocation.
	 * <p>Defaults to the class's transaction attribute if no method attribute is found.
	 * @param method the method for the current invocation (never {@code null})  当前调用的方法 不能null
	 * @param targetClass the target class for this invocation (may be {@code null}) 当前调用的方法 有可能为 null
	 * @return a TransactionAttribute for this method, or {@code null} if the method 此方法的TransactionAttribute，
	 * is not transactional   如果该方法不是事务性的，则为null
	 */
	@Override
	@Nullable
	public TransactionAttribute getTransactionAttribute(Method method, @Nullable Class<?> targetClass) {
		// 判断method所在的class是不是Object类型，如果是直接返回null，因为基类方法通常不应包含业务逻辑或特定的事务配置。
		if (method.getDeclaringClass() == Object.class) {
			return null;
		}

		// First, see if we have a cached value.
		// 构建缓存key
		Object cacheKey = getCacheKey(method, targetClass);
		// 从缓存中获取
		TransactionAttribute cached = this.attributeCache.get(cacheKey);
		// 有缓存，不会每次computeTransactionAttribute，如果此前解析过该方法以及目标类，那么Value要么是指示没有事务属性，要么是一个实际的事务属性，一定不会为null
		if (cached != null) {
			// Value will either be canonical value indicating there is no transaction attribute,
			// or an actual transaction attribute.
			// 判断缓存中的对象是不是空事务属性的对象
			if (cached == NULL_TRANSACTION_ATTRIBUTE) {
				return null;
			}
			else {
				// 存在就直接返回事务属性
				return cached;
			}
		}
		else {
			// We need to work it out.
			//那么根据当前方法和目标类型计算出TransactionAttribute
			TransactionAttribute txAttr = computeTransactionAttribute(method, targetClass);
			// Put it in the cache.
			// 若解析出来的事务注解属性为空
			if (txAttr == null) {
				// 往缓存中存放DefaultTransactionAttribute 默认事务属性
				this.attributeCache.put(cacheKey, NULL_TRANSACTION_ATTRIBUTE);
			}
			else {
				// 获取给定方法的全限定， 包名+类名+方法名
				String methodIdentification = ClassUtils.getQualifiedMethodName(method, targetClass);
				//如果事务属性属于DefaultTransactionAttribute 默认事务属性
				if (txAttr instanceof DefaultTransactionAttribute) {
					//设置描述符
					((DefaultTransactionAttribute) txAttr).setDescriptor(methodIdentification);
				}
				if (logger.isTraceEnabled()) {
					logger.trace("Adding transactional method '" + methodIdentification + "' with attribute: " + txAttr);
				}
				// 加入缓存
				this.attributeCache.put(cacheKey, txAttr);
			}
			return txAttr;
		}
	}

	/**
	 * 函数的主要职责是根据提供的方法和目标类创建一个缓存键（Object类型）
	 * 有效性和唯一性：
	 * 区分重载方法: 函数注释中明确指出，它必须为不同的重载方法生成不同的缓存键。这意味着即使两个方法名称相同但参数列表或返回类型不同，它们也会得到不同的缓存键，从而避免了缓存数据的混淆。
	 * 同一方法实例一致性: 不管是同一个方法的哪个实例（即在不同对象上调用的同名同参数列表方法），只要这些方法在逻辑上等价，该函数都应为它们生成相同的缓存键。这有助于共享缓存结果，减少重复计算。
	 * Determine a cache key for the given method and target class.
	 * <p>Must not produce same key for overloaded methods.
	 * Must produce same key for different instances of the same method.
	 * @param method the method (never {@code null}) 代表了某个类的具体方法。此参数保证不为null，意味着总是有一个明确的方法需要用来生成缓存键。
	 * @param targetClass the target class (may be {@code null})  表示目标类，即方法所属的类或其实现/继承类。这个参数可以是null，意味着目标类信息可能是未知的或不适用的。
	 * @return the cache key (never {@code null})
	 */
	protected Object getCacheKey(Method method, @Nullable Class<?> targetClass) {
		return new MethodClassKey(method, targetClass);
	}

	/**
	 * 获取事务属性的核心方法
	 *
	 * Same signature as {@link #getTransactionAttribute}, but doesn't cache the result.
	 * {@link #getTransactionAttribute} is effectively a caching decorator for this method.
	 * <p>As of 4.1.8, this method can be overridden.
	 * @since 4.1.8
	 * @see #getTransactionAttribute
	 */
	@Nullable
	protected TransactionAttribute computeTransactionAttribute(Method method, @Nullable Class<?> targetClass) {
		// Don't allow no-public methods as required.
		// 首先判断方法是否是public，默认是支持public的
		if (allowPublicMethodsOnly() && !Modifier.isPublic(method.getModifiers())) {
			return null;
		}

		// The method may be on an interface, but we need attributes from the target class.
		// If the target class is null, the method will be unchanged.
		// method代表接口中的方法，specificMethod代表实现类中的方法
		//为了处理当方法定义在接口上，而实际调用发生在实现类的情况，确保获取到的是实际执行时的方法定义。
		Method specificMethod = AopUtils.getMostSpecificMethod(method, targetClass);

		// First try is the method in the target class.
		// 优先方法上解析的事务注解的属性，如果方法上有就直接返回，不会再看类上的了事务注解了。
		TransactionAttribute txAttr = findTransactionAttribute(specificMethod);
		if (txAttr != null) {
			return txAttr;
		}

		// Second try is the transaction attribute on the target class.
		// 如果方法上没有，再尝试查找声明该方法的类上的注解属性，会去父类或者接口找
		txAttr = findTransactionAttribute(specificMethod.getDeclaringClass());
		//ClassUtils.isUserLevelMethod(method)，确保只有用户自定义的方法才考虑类级别的事务属性。
		if (txAttr != null && ClassUtils.isUserLevelMethod(method)) {
			return txAttr;
		}

		// 当具体方法与最初传入的方法不一致时（即原始方法可能是接口方法）
		if (specificMethod != method) {
			// Fallback is to look at the original method.
			// 回退到原始方法上尝试查找事务属性
			txAttr = findTransactionAttribute(method);
			if (txAttr != null) {
				return txAttr;
			}
			// Last fallback is the class of the original method.
			// 检查原始方法的声明类是否有事务属性，并同样确认这是否为用户级方法。
			txAttr = findTransactionAttribute(method.getDeclaringClass());
			if (txAttr != null && ClassUtils.isUserLevelMethod(method)) {
				return txAttr;
			}
		}

		return null;
	}


	/**
	 * Subclasses need to implement this to return the transaction attribute for the
	 * given class, if any.
	 * @param clazz the class to retrieve the attribute for
	 * @return all transaction attribute associated with this class, or {@code null} if none
	 */
	@Nullable
	protected abstract TransactionAttribute findTransactionAttribute(Class<?> clazz);

	/**
	 * Subclasses need to implement this to return the transaction attribute for the
	 * given method, if any.
	 * @param method the method to retrieve the attribute for
	 * @return all transaction attribute associated with this method, or {@code null} if none
	 */
	@Nullable
	protected abstract TransactionAttribute findTransactionAttribute(Method method);

	/**
	 * Should only public methods be allowed to have transactional semantics?
	 * <p>The default implementation returns {@code false}.
	 */
	protected boolean allowPublicMethodsOnly() {
		return false;
	}

}
