/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.interceptor;

import java.lang.reflect.Method;

import org.springframework.lang.Nullable;

/**
 * 事务属性源，就是事务注解的一些属性，也用来解析事务注解属性
 *
 * Strategy interface used by {@link TransactionInterceptor} for metadata retrieval.
 *
 * <p>Implementations know how to source transaction attributes, whether from configuration,
 * metadata attributes at source level (such as Java 5 annotations), or anywhere else.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @since 15.04.2003
 * @see TransactionInterceptor#setTransactionAttributeSource
 * @see TransactionProxyFactoryBean#setTransactionAttributeSource
 * @see org.springframework.transaction.annotation.AnnotationTransactionAttributeSource
 */
public interface TransactionAttributeSource {

	/**
	 * 来自Spring 5.2的一个新方法
	 * <p>
	 * 确定给定的类是否是此事务切入点的候选者，即是否有资格继续进行类方法匹配来判断是否能被进行事务增强
	 * <p>
	 * 如果此方法返回false，表示已知该类及其方法没有事务属性，
	 * 则不会遍历给定类上的方法并进行getTransactionAttribute进行事务属性的检查，以提高性能。
	 * 因此，该方法是对不受事务影响的类的优化，避免了多次方法级别的校验
	 * 如果返回true，这意味着系统将对targetClass的所有方法进行遍历和检查，以查找事务相关配置。
	 *
	 * @param targetClass the class to introspect 表示要检查的类对象。
	 * @return 如果已知该类在类或方法级别没有事务属性，则返回false；否则返回false。
	 * 默认实现返回true，从而导致常规的方法自省。
	 * <p>
	 * Determine whether the given class is a candidate for transaction attributes
	 * in the metadata format of this {@code TransactionAttributeSource}.
	 * <p>If this method returns {@code false}, the methods on the given class
	 * will not get traversed for {@link #getTransactionAttribute} introspection.
	 * Returning {@code false} is therefore an optimization for non-affected
	 * classes, whereas {@code true} simply means that the class needs to get
	 * fully introspected for each method on the given class individually.
	 * @param targetClass the class to introspect
	 * @return {@code false} if the class is known to have no transaction
	 * attributes at class or method level; {@code true} otherwise. The default
	 * implementation returns {@code true}, leading to regular introspection.
	 * @since 5.2
	 */
	default boolean isCandidateClass(Class<?> targetClass) {
		return true;
	}

	/**
	 * 返回给定方法的transaction属性，如果该方法是非事务性的，则返回 null。
	 * Return the transaction attribute for the given method,
	 * or {@code null} if the method is non-transactional.
	 * @param method the method to introspect 需要被检查其事务属性的方法对象。通过反射API，我们可以获取到方法的所有元数据，包括注解信息，这对于判断方法的事务特性至关重要。
	 * @param targetClass the target class (may be {@code null},
	 * in which case the declaring class of the method must be used)  目标类，可能是null，此时将使用方法反射获取的class
	 * @return the matching transaction attribute, or {@code null} if none found  匹配的事务属性，如果没有发现则返回null
	 */
	@Nullable
	TransactionAttribute getTransactionAttribute(Method method, @Nullable Class<?> targetClass);

}
