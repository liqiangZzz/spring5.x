/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.config;

import org.w3c.dom.Element;

import org.springframework.aop.config.AopNamespaceUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.parsing.CompositeComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.lang.Nullable;
import org.springframework.transaction.event.TransactionalEventListenerFactory;
import org.springframework.transaction.interceptor.BeanFactoryTransactionAttributeSourceAdvisor;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import org.springframework.util.ClassUtils;

/**
 * {@link org.springframework.beans.factory.xml.BeanDefinitionParser
 * BeanDefinitionParser} implementation that allows users to easily configure
 * all the infrastructure beans required to enable annotation-driven transaction
 * demarcation.
 *
 * <p>By default, all proxies are created as JDK proxies. This may cause some
 * problems if you are injecting objects as concrete classes rather than
 * interfaces. To overcome this restriction you can set the
 * '{@code proxy-target-class}' attribute to '{@code true}', which
 * will result in class-based proxies being created.
 *
 * @author Juergen Hoeller
 * @author Rob Harrop
 * @author Chris Beams
 * @author Stephane Nicoll
 * @since 2.0
 */
class AnnotationDrivenBeanDefinitionParser implements BeanDefinitionParser {

	/**
	 *
	 * 解析<tx:annotation-driven/>标签
	 * 将在必要时调用AopNamespaceUtils#registerAutoProxyCreatorIfNecessary方法
	 * 尝试向容器中注册一个InfrastructureAdvisorAutoProxyCreator类型的AutoProxyCreator
	 *
	 * Parses the {@code <tx:annotation-driven/>} tag. Will
	 * {@link AopNamespaceUtils#registerAutoProxyCreatorIfNecessary register an AutoProxyCreator}
	 * with the container as necessary.
	 * @param element 代表当前正在解析的XML配置元素，即<tx:annotation-driven/>。
	 * @param parserContext  提供了解析上下文信息，包括注册BeanDefinition到容器的能力。
	 */
	@Override
	@Nullable
	public BeanDefinition parse(Element element, ParserContext parserContext) {
		// 注册事务事件监听器工厂 TransactionalEventListenerFactory 的bean定义
		// 用于解析 @TransactionalEventListener 注解
		// createApplicationListener方法指定一个ApplicationListenerMethodTransactionalAdapter监听器
		registerTransactionalEventListenerFactory(parserContext);
		//获取<tx:annotation-driven/>标签的mode属性
		//该属性用于指示应该采用Spring AOP来对异步方法进行动态代理，还是采用AspectJ来进行静态织入
		//默认为proxy，即Spring AOP代理，如果设置为aspectj，那么还需要spring-aspects.jar（其内部包含了aspectjweaver依赖）。
		String mode = element.getAttribute("mode");
		if ("aspectj".equals(mode)) {
			// mode="aspectj"
			//注册一个事务切面（Aspect），以便在应用程序中实现声明式事务管理
			registerTransactionAspect(element, parserContext);
			//检查系统中是否包含javax.transaction.Transactional类（意味着JTA支持可用）
			if (ClassUtils.isPresent("javax.transaction.Transactional", getClass().getClassLoader())) {
				// 注册JTA事务切面，以支持跨资源管理器的事务。
				registerJtaTransactionAspect(element, parserContext);
			}
		}
		else {
			// mode="proxy"
			AopAutoProxyConfigurer.configureAutoProxyCreator(element, parserContext);
		}
		return null;
	}

	private void registerTransactionAspect(Element element, ParserContext parserContext) {
		//TransactionManagementConfigUtils中获取事务切面的bean名称和类名称
		String txAspectBeanName = TransactionManagementConfigUtils.TRANSACTION_ASPECT_BEAN_NAME;
		String txAspectClassName = TransactionManagementConfigUtils.TRANSACTION_ASPECT_CLASS_NAME;
		//检查当前Spring配置解析上下文中是否已经注册了具有相同名称的事务切面。避免重复注册。
		if (!parserContext.getRegistry().containsBeanDefinition(txAspectBeanName)) {
			RootBeanDefinition def = new RootBeanDefinition();
			def.setBeanClassName(txAspectClassName);
			//设置factoryMethodName为"aspectOf"
			// 用于获取切面实例的工厂方法，允许以静态方式访问切面实例，通常由Spring的AspectJ支持库提供。
			def.setFactoryMethodName("aspectOf");
			//注册事务管理器
			registerTransactionManager(element, def);
			//注册到容器中 和 注册一个组件
			parserContext.registerBeanComponent(new BeanComponentDefinition(def, txAspectBeanName));
		}
	}

	private void registerJtaTransactionAspect(Element element, ParserContext parserContext) {
		//内部管理的JTA事务方面的bean名称（当mode==ASPECTJ时使用）。
		String txAspectBeanName = TransactionManagementConfigUtils.JTA_TRANSACTION_ASPECT_BEAN_NAME;
		//AspectJ事务管理方面的类名。
		String txAspectClassName = TransactionManagementConfigUtils.JTA_TRANSACTION_ASPECT_CLASS_NAME;
		//检查当前的Bean定义注册表中是否已经包含了这个事务切面的Bean
		if (!parserContext.getRegistry().containsBeanDefinition(txAspectBeanName)) {
			RootBeanDefinition def = new RootBeanDefinition();
			def.setBeanClassName(txAspectClassName);
			//设置factoryMethodName为"aspectOf"。这是AOP切面编程的一个常见模式，用于从切面类中获取切面实例，通常是通过静态工厂方法实现的。
			def.setFactoryMethodName("aspectOf");
			//注册事务管理器
			registerTransactionManager(element, def);
			//注册到容器中 和 注册一个组件
			parserContext.registerBeanComponent(new BeanComponentDefinition(def, txAspectBeanName));
		}
	}

	/**
	 * 注册事务管理器
	 * @param element  <tx:annotation-driven/>标签元素
	 * @param def 事务拦截器的bean定义
	 */
	private static void registerTransactionManager(Element element, BeanDefinition def) {
		//通过TxNamespaceHandler.getTransactionManagerName来获取事务管理器的beanName
		//将会解析<tx:annotation-driven/>标签的transaction-manager属性，默认值同样是transactionManager
		def.getPropertyValues().add("transactionManagerBeanName",
				TxNamespaceHandler.getTransactionManagerName(element));
	}

	/**
	 * 注册TransactionalEventListenerFactory的bean定义
	 */
	private void registerTransactionalEventListenerFactory(ParserContext parserContext) {
		//创建bean定义
		RootBeanDefinition def = new RootBeanDefinition();
		//设置为TransactionalEventListenerFactory类型
		def.setBeanClass(TransactionalEventListenerFactory.class);
		//注册bean定义到beanFactory中，beanName为"org.springframework.transaction.config.internalTransactionalEventListenerFactory"
		parserContext.registerBeanComponent(new BeanComponentDefinition(def,
				TransactionManagementConfigUtils.TRANSACTIONAL_EVENT_LISTENER_FACTORY_BEAN_NAME));
	}


	/**
	 * Inner class to just introduce an AOP framework dependency when actually in proxy mode.
	 */
	private static class AopAutoProxyConfigurer {

		public static void configureAutoProxyCreator(Element element, ParserContext parserContext) {
			//注册一个 InfrastructureAdvisorAutoProxyCreator类型的AutoProxyCreator
			// 并且配置proxy-target-class与expose-proxy属性
			AopNamespaceUtils.registerAutoProxyCreatorIfNecessary(parserContext, element);
			//内部管理的事务顾问的bean名称（当mode==PROXY时使用）。 "org.springframework.transaction.config.internalTransactionAdvisor";
			String txAdvisorBeanName = TransactionManagementConfigUtils.TRANSACTION_ADVISOR_BEAN_NAME;
			// 如果目前不包含该名称的bean，那么将会注册一个BeanFactoryTransactionAttributeSourceAdvisor类型的注解专用事务通知器
			// 并且配置TransactionInterceptor事务拦截器以及AnnotationTransactionAttributeSource注解事务属性源的bean定义
			if (!parserContext.getRegistry().containsBeanDefinition(txAdvisorBeanName)) {
				Object eleSource = parserContext.extractSource(element);

				// Create the TransactionAttributeSource definition.
				// 创建TransactionAttributeSource的bean
				RootBeanDefinition sourceDef = new RootBeanDefinition(
						"org.springframework.transaction.annotation.AnnotationTransactionAttributeSource");
				sourceDef.setSource(eleSource);
				sourceDef.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
				// 注册bean，并使用Spring中的定义规则生成beanName
				String sourceName = parserContext.getReaderContext().registerWithGeneratedName(sourceDef);

				// Create the TransactionInterceptor definition.
				// 创建TransactionInterceptor的bean
				RootBeanDefinition interceptorDef = new RootBeanDefinition(TransactionInterceptor.class);
				interceptorDef.setSource(eleSource);
				interceptorDef.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
				//注册事务管理器到事务拦截器中，实际上就是设置TransactionInterceptor的transactionManagerBeanName属性指向一个事务管理器的bean
				registerTransactionManager(element, interceptorDef);
				interceptorDef.getPropertyValues().add("transactionAttributeSource", new RuntimeBeanReference(sourceName));
				//注册拦截器到容器中并且返回beanName
				String interceptorName = parserContext.getReaderContext().registerWithGeneratedName(interceptorDef);

				// Create the TransactionAttributeSourceAdvisor definition.
				// 创建TransactionAttributeSourceAdvisor的 bean，一个事务通知器
				RootBeanDefinition advisorDef = new RootBeanDefinition(BeanFactoryTransactionAttributeSourceAdvisor.class);
				advisorDef.setSource(eleSource);
				advisorDef.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
				// 将sourceName的bean注入advisorDef的transactionAttributeSource属性中，就是AnnotationTransactionAttributeSource事务属性源
				advisorDef.getPropertyValues().add("transactionAttributeSource", new RuntimeBeanReference(sourceName));
				// 将interceptorName的bean注入advisorDef的adviceBeanName属性中，就是TransactionInterceptor事务拦截器
				advisorDef.getPropertyValues().add("adviceBeanName", interceptorName);
				// 如果配置了order属性，则加入到bean中
				if (element.hasAttribute("order")) {
					advisorDef.getPropertyValues().add("order", element.getAttribute("order"));
				}
				//将当前事务通知器的bean定义注册到容器中，name为"org.springframework.transaction.config.internalTransactionAdvisor"
				parserContext.getRegistry().registerBeanDefinition(txAdvisorBeanName, advisorDef);
				//发布事件
				CompositeComponentDefinition compositeDef = new CompositeComponentDefinition(element.getTagName(), eleSource);
				compositeDef.addNestedComponent(new BeanComponentDefinition(sourceDef, sourceName));
				compositeDef.addNestedComponent(new BeanComponentDefinition(interceptorDef, interceptorName));
				compositeDef.addNestedComponent(new BeanComponentDefinition(advisorDef, txAdvisorBeanName));
				parserContext.registerComponent(compositeDef);
			}
		}
	}

}
