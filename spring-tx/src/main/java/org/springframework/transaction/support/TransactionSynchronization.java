/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.support;

import java.io.Flushable;

/**
 * 事务同步接口，记录是无状态并执行事务状态回调。这意味着它不应该在实例中持有任何依赖于事务状态的数据。
 * 这是因为TransactionSynchronization的实例可能会被多次调用，每次调用可能对应事务的不同阶段。
 * 如果在实例中保存了事务相关状态，可能会导致错误的行为或数据不一致。
 *
 * 事务同步回调的接口。由AbstractPlatformTransactionManager支持
 * <p> TransactionSynchronization实现可以实现Ordered接口来影响其执行顺序。
 * 未实现有序接口的同步将附加到同步链的末尾
 * <p> Spring本身执行的系统同步使用特定的顺序值，允许与它们的执行顺序进行细粒度的交互（如有必要）。
 *
 * Interface for transaction synchronization callbacks.
 * Supported by AbstractPlatformTransactionManager.
 *
 * <p>TransactionSynchronization implementations can implement the Ordered interface
 * to influence their execution order. A synchronization that does not implement the
 * Ordered interface is appended to the end of the synchronization chain.
 *
 * <p>System synchronizations performed by Spring itself use specific order values,
 * allowing for fine-grained interaction with their execution order (if necessary).
 *
 * @author Juergen Hoeller
 * @since 02.06.2003
 * @see TransactionSynchronizationManager
 * @see AbstractPlatformTransactionManager
 * @see org.springframework.jdbc.datasource.DataSourceUtils#CONNECTION_SYNCHRONIZATION_ORDER
 */
public interface TransactionSynchronization extends Flushable {

	/**
	 * 正确提交的情况下的完成状态。
	 * Completion status in case of proper commit.
	 */
	int STATUS_COMMITTED = 0;

	/**
	 * 正确回滚时的完成状态。
	 * Completion status in case of proper rollback.
	 */
	int STATUS_ROLLED_BACK = 1;

	/**
	 * 启发式混合完成或系统错误情况下的完成状态。
	 * Completion status in case of heuristic mixed completion or system errors.
	 */
	int STATUS_UNKNOWN = 2;


	/**
	 * 挂起此同步。如果正在管理资源，则应解除TransactionSynchronizationManager的资源绑定。
	 * <p>
	 * Suspend this synchronization.
	 * Supposed to unbind resources from TransactionSynchronizationManager if managing any.
	 * @see TransactionSynchronizationManager#unbindResource
	 */
	default void suspend() {
	}

	/**
	 * 继续此同步。如果正在管理资源，则应将资源重新绑定到TransactionSynchronizationManager。
	 * <p>
	 * Resume this synchronization.
	 * Supposed to rebind resources to TransactionSynchronizationManager if managing any.
	 * @see TransactionSynchronizationManager#bindResource
	 */
	default void resume() {
	}

	/**
	 * 将当前的会话（比如在使用Hibernate或JPA时的会话）中的所有未提交的更改强制同步到数据库中。
	 * 换句话说，当你调用flush()方法时，它会促使你的持久化层（如Hibernate或JPA）立即把那些在内存中已经修改但还未持久化的数据，写入到数据库中去。
	 * 在一般的数据库操作流程中，当我们对实体对象进行增删改查时，这些操作首先是在内存中进行的，直到我们明确地提交事务或达到某种条件（如事务结束）时，这些更改才会被真正地保存到数据库中。
	 * flush()方法就是用来提前触发这个保存动作，确保数据的一致性和及时性。
	 *
	 * Flush the underlying session to the datastore, if applicable:
	 * for example, a Hibernate/JPA session.
	 * @see org.springframework.transaction.TransactionStatus#flush()
	 */
	@Override
	default void flush() {
	}

	/**
	 * 用于在事务提交之前执行一些操作。它可以在事务仍然有可能提交的情况下，执行一些只与提交有关的工作，例如将SQL语句刷新到数据库。
	 * 方法参数readOnly表示事务是否被定义为只读事务。如果在该方法中抛出RuntimeException异常，将会被传播到调用者，并导致事务回滚。
	 * 需要注意的是，不应该在此方法中抛出TransactionException子类的异常。
	 * <p>
	 * Invoked before transaction commit (before "beforeCompletion").
	 * Can e.g. flush transactional O/R Mapping sessions to the database.
	 * <p>This callback does <i>not</i> mean that the transaction will actually be committed.
	 * A rollback decision can still occur after this method has been called. This callback
	 * is rather meant to perform work that's only relevant if a commit still has a chance
	 * to happen, such as flushing SQL statements to the database.
	 * <p>Note that exceptions will get propagated to the commit caller and cause a
	 * rollback of the transaction.
	 * @param readOnly whether the transaction is defined as read-only transaction
	 * @throws RuntimeException in case of errors; will be <b>propagated to the caller</b>
	 * (note: do not throw TransactionException subclasses here!)
	 * @see #beforeCompletion
	 */
	default void beforeCommit(boolean readOnly) {
	}

	/**
	 * 用于在事务提交/回滚之前调用。它的主要功能是在事务完成之前清理资源。
	 * 具体来说：
	 * 该方法将在beforeCommit方法之后调用，即使beforeCommit方法抛出异常。
	 * 该回调方法允许在事务完成之前关闭资源，无论事务结果如何。
	 * 如果在该方法中抛出RuntimeException异常，将会被记录但不会被传播（注意：不要在这里抛出TransactionException子类异常）。
	 * 该方法与beforeCommit和afterCompletion方法一起使用，用于在事务的不同阶段执行相应的操作。
	 * 总之，beforeCompletion方法用于在事务完成之前进行资源清理，确保资源在任何情况下都能被正确关闭。
	 *
	 * Invoked before transaction commit/rollback.
	 * Can perform resource cleanup <i>before</i> transaction completion.
	 * <p>This method will be invoked after {@code beforeCommit}, even when
	 * {@code beforeCommit} threw an exception. This callback allows for
	 * closing resources before transaction completion, for any outcome.
	 * @throws RuntimeException in case of errors; will be <b>logged but not propagated</b>
	 * (note: do not throw TransactionException subclasses here!)
	 * @see #beforeCommit
	 * @see #afterCompletion
	 */
	default void beforeCompletion() {
	}

	/**
	 * 用于在事务提交后执行进一步的操作。主事务成功提交后立即执行后续操作，例如发送确认消息或电子邮件。
	 * <p>
	 * 事务已经提交，但事务资源可能仍然活跃并可访问。
	 * 因此，此时触发的任何数据访问代码仍将“参与”原始事务，允许执行一些清理（不再有提交后续！），除非它明确声明需要在单独的事务中运行。
	 * <p>
	 * 如果需要从此处调用的任何事务操作，那么需要使用PROPAGATION_REQUIRES_NEW。
	 * 如果发生错误，该方法可能会抛出RuntimeException异常，并将其传播给调用者。
	 *
	 * Invoked after transaction commit. Can perform further operations right
	 * <i>after</i> the main transaction has <i>successfully</i> committed.
	 * <p>Can e.g. commit further operations that are supposed to follow on a successful
	 * commit of the main transaction, like confirmation messages or emails.
	 * <p><b>NOTE:</b> The transaction will have been committed already, but the
	 * transactional resources might still be active and accessible. As a consequence,
	 * any data access code triggered at this point will still "participate" in the
	 * original transaction, allowing to perform some cleanup (with no commit following
	 * anymore!), unless it explicitly declares that it needs to run in a separate
	 * transaction. Hence: <b>Use {@code PROPAGATION_REQUIRES_NEW} for any
	 * transactional operation that is called from here.</b>
	 * @throws RuntimeException in case of errors; will be <b>propagated to the caller</b>
	 * (note: do not throw TransactionException subclasses here!)
	 */
	default void afterCommit() {
	}

	/**
	 * 用于在事务提交或回滚后进行资源清理。
	 * <p>
	 * 事务将已经提交或回滚，但事务资源可能仍然处于活动状态并且可以访问。
	 * 因此，此时触发的任何数据访问代码仍将“参与”原始事务，允许执行一些清理（不再有提交后续！），除非它明确声明需要在单独的事务中运行。
	 * 因此：<b>对从此处调用的任何事务操作使用 PROPAGATION_REQUIRES_NEW
	 * 函数应抛出RuntimeException类型的异常以指示错误，但这些异常将被记录但不会被传播（不要在此处抛出TransactionException子类的异常）
	 *
	 * Invoked after transaction commit/rollback.
	 * Can perform resource cleanup <i>after</i> transaction completion.
	 * <p><b>NOTE:</b> The transaction will have been committed or rolled back already,
	 * but the transactional resources might still be active and accessible. As a
	 * consequence, any data access code triggered at this point will still "participate"
	 * in the original transaction, allowing to perform some cleanup (with no commit
	 * following anymore!), unless it explicitly declares that it needs to run in a
	 * separate transaction. Hence: <b>Use {@code PROPAGATION_REQUIRES_NEW}
	 * for any transactional operation that is called from here.</b>
	 * @param status completion status according to the {@code STATUS_*} constants  完成状态
	 * @throws RuntimeException in case of errors; will be <b>logged but not propagated</b>
	 * (note: do not throw TransactionException subclasses here!)
	 * @see #STATUS_COMMITTED
	 * @see #STATUS_ROLLED_BACK
	 * @see #STATUS_UNKNOWN
	 * @see #beforeCompletion
	 */
	default void afterCompletion(int status) {
	}

}
