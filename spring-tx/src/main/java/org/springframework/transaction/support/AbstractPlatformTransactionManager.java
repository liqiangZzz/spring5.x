/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.support;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.core.Constants;
import org.springframework.lang.Nullable;
import org.springframework.transaction.IllegalTransactionStateException;
import org.springframework.transaction.InvalidTimeoutException;
import org.springframework.transaction.NestedTransactionNotSupportedException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.TransactionSuspensionNotSupportedException;
import org.springframework.transaction.UnexpectedRollbackException;

/**
 * Abstract base class that implements Spring's standard transaction workflow,
 * serving as basis for concrete platform transaction managers like
 * {@link org.springframework.transaction.jta.JtaTransactionManager}.
 *
 * <p>This base class provides the following workflow handling:
 * <ul>
 * <li>determines if there is an existing transaction;
 * <li>applies the appropriate propagation behavior;
 * <li>suspends and resumes transactions if necessary;
 * <li>checks the rollback-only flag on commit;
 * <li>applies the appropriate modification on rollback
 * (actual rollback or setting rollback-only);
 * <li>triggers registered synchronization callbacks
 * (if transaction synchronization is active).
 * </ul>
 *
 * <p>Subclasses have to implement specific template methods for specific
 * states of a transaction, e.g.: begin, suspend, resume, commit, rollback.
 * The most important of them are abstract and must be provided by a concrete
 * implementation; for the rest, defaults are provided, so overriding is optional.
 *
 * <p>Transaction synchronization is a generic mechanism for registering callbacks
 * that get invoked at transaction completion time. This is mainly used internally
 * by the data access support classes for JDBC, Hibernate, JPA, etc when running
 * within a JTA transaction: They register resources that are opened within the
 * transaction for closing at transaction completion time, allowing e.g. for reuse
 * of the same Hibernate Session within the transaction. The same mechanism can
 * also be leveraged for custom synchronization needs in an application.
 *
 * <p>The state of this class is serializable, to allow for serializing the
 * transaction strategy along with proxies that carry a transaction interceptor.
 * It is up to subclasses if they wish to make their state to be serializable too.
 * They should implement the {@code java.io.Serializable} marker interface in
 * that case, and potentially a private {@code readObject()} method (according
 * to Java serialization rules) if they need to restore any transient state.
 *
 * @author Juergen Hoeller
 * @since 28.03.2003
 * @see #setTransactionSynchronization
 * @see TransactionSynchronizationManager
 * @see org.springframework.transaction.jta.JtaTransactionManager
 */
@SuppressWarnings("serial")
public abstract class AbstractPlatformTransactionManager implements PlatformTransactionManager, Serializable {

	/**
	 * Always activate transaction synchronization, even for "empty" transactions
	 * that result from PROPAGATION_SUPPORTS with no existing backend transaction.
	 * @see org.springframework.transaction.TransactionDefinition#PROPAGATION_SUPPORTS
	 * @see org.springframework.transaction.TransactionDefinition#PROPAGATION_NOT_SUPPORTED
	 * @see org.springframework.transaction.TransactionDefinition#PROPAGATION_NEVER
	 */
	public static final int SYNCHRONIZATION_ALWAYS = 0;

	/**
	 * Activate transaction synchronization only for actual transactions,
	 * that is, not for empty ones that result from PROPAGATION_SUPPORTS with
	 * no existing backend transaction.
	 * @see org.springframework.transaction.TransactionDefinition#PROPAGATION_REQUIRED
	 * @see org.springframework.transaction.TransactionDefinition#PROPAGATION_MANDATORY
	 * @see org.springframework.transaction.TransactionDefinition#PROPAGATION_REQUIRES_NEW
	 */
	public static final int SYNCHRONIZATION_ON_ACTUAL_TRANSACTION = 1;

	/**
	 * Never active transaction synchronization, not even for actual transactions.
	 */
	public static final int SYNCHRONIZATION_NEVER = 2;


	/** Constants instance for AbstractPlatformTransactionManager. */
	private static final Constants constants = new Constants(AbstractPlatformTransactionManager.class);


	protected transient Log logger = LogFactory.getLog(getClass());

	private int transactionSynchronization = SYNCHRONIZATION_ALWAYS;

	/**
	 * 默认超时时间（秒），默认值为-1，表示使用基础事务系统的默认超时；
	 */
	private int defaultTimeout = TransactionDefinition.TIMEOUT_DEFAULT;

	private boolean nestedTransactionAllowed = false;

	private boolean validateExistingTransaction = false;

	private boolean globalRollbackOnParticipationFailure = true;

	private boolean failEarlyOnGlobalRollbackOnly = false;

	private boolean rollbackOnCommitFailure = false;


	/**
	 * Set the transaction synchronization by the name of the corresponding constant
	 * in this class, e.g. "SYNCHRONIZATION_ALWAYS".
	 * @param constantName name of the constant
	 * @see #SYNCHRONIZATION_ALWAYS
	 */
	public final void setTransactionSynchronizationName(String constantName) {
		setTransactionSynchronization(constants.asNumber(constantName).intValue());
	}

	/**
	 * Set when this transaction manager should activate the thread-bound
	 * transaction synchronization support. Default is "always".
	 * <p>Note that transaction synchronization isn't supported for
	 * multiple concurrent transactions by different transaction managers.
	 * Only one transaction manager is allowed to activate it at any time.
	 * @see #SYNCHRONIZATION_ALWAYS
	 * @see #SYNCHRONIZATION_ON_ACTUAL_TRANSACTION
	 * @see #SYNCHRONIZATION_NEVER
	 * @see TransactionSynchronizationManager
	 * @see TransactionSynchronization
	 */
	public final void setTransactionSynchronization(int transactionSynchronization) {
		this.transactionSynchronization = transactionSynchronization;
	}

	/**
	 * Return if this transaction manager should activate the thread-bound
	 * transaction synchronization support.
	 */
	public final int getTransactionSynchronization() {
		return this.transactionSynchronization;
	}

	/**
	 * Specify the default timeout that this transaction manager should apply
	 * if there is no timeout specified at the transaction level, in seconds.
	 * <p>Default is the underlying transaction infrastructure's default timeout,
	 * e.g. typically 30 seconds in case of a JTA provider, indicated by the
	 * {@code TransactionDefinition.TIMEOUT_DEFAULT} value.
	 * @see org.springframework.transaction.TransactionDefinition#TIMEOUT_DEFAULT
	 */
	public final void setDefaultTimeout(int defaultTimeout) {
		if (defaultTimeout < TransactionDefinition.TIMEOUT_DEFAULT) {
			throw new InvalidTimeoutException("Invalid default timeout", defaultTimeout);
		}
		this.defaultTimeout = defaultTimeout;
	}

	/**
	 * 如果在事务级别没有指定超时，则返回此事务管理器应应用的默认超时（以秒为单位）
	 * <p>
	 * 返回 TransactionDefinition.TIMEOUT_DEFAULT 以指示底层事务基础结构的默认超时。
	 * Return the default timeout that this transaction manager should apply
	 * if there is no timeout specified at the transaction level, in seconds.
	 * <p>Returns {@code TransactionDefinition.TIMEOUT_DEFAULT} to indicate
	 * the underlying transaction infrastructure's default timeout.
	 */
	public final int getDefaultTimeout() {
		return this.defaultTimeout;
	}

	/**
	 * Set whether nested transactions are allowed. Default is "false".
	 * <p>Typically initialized with an appropriate default by the
	 * concrete transaction manager subclass.
	 */
	public final void setNestedTransactionAllowed(boolean nestedTransactionAllowed) {
		this.nestedTransactionAllowed = nestedTransactionAllowed;
	}

	/**
	 * Return whether nested transactions are allowed.
	 */
	public final boolean isNestedTransactionAllowed() {
		return this.nestedTransactionAllowed;
	}

	/**
	 * Set whether existing transactions should be validated before participating
	 * in them.
	 * <p>When participating in an existing transaction (e.g. with
	 * PROPAGATION_REQUIRED or PROPAGATION_SUPPORTS encountering an existing
	 * transaction), this outer transaction's characteristics will apply even
	 * to the inner transaction scope. Validation will detect incompatible
	 * isolation level and read-only settings on the inner transaction definition
	 * and reject participation accordingly through throwing a corresponding exception.
	 * <p>Default is "false", leniently ignoring inner transaction settings,
	 * simply overriding them with the outer transaction's characteristics.
	 * Switch this flag to "true" in order to enforce strict validation.
	 * @since 2.5.1
	 */
	public final void setValidateExistingTransaction(boolean validateExistingTransaction) {
		this.validateExistingTransaction = validateExistingTransaction;
	}

	/**
	 * Return whether existing transactions should be validated before participating
	 * in them.
	 * @since 2.5.1
	 */
	public final boolean isValidateExistingTransaction() {
		return this.validateExistingTransaction;
	}

	/**
	 * Set whether to globally mark an existing transaction as rollback-only
	 * after a participating transaction failed.
	 * <p>Default is "true": If a participating transaction (e.g. with
	 * PROPAGATION_REQUIRED or PROPAGATION_SUPPORTS encountering an existing
	 * transaction) fails, the transaction will be globally marked as rollback-only.
	 * The only possible outcome of such a transaction is a rollback: The
	 * transaction originator <i>cannot</i> make the transaction commit anymore.
	 * <p>Switch this to "false" to let the transaction originator make the rollback
	 * decision. If a participating transaction fails with an exception, the caller
	 * can still decide to continue with a different path within the transaction.
	 * However, note that this will only work as long as all participating resources
	 * are capable of continuing towards a transaction commit even after a data access
	 * failure: This is generally not the case for a Hibernate Session, for example;
	 * neither is it for a sequence of JDBC insert/update/delete operations.
	 * <p><b>Note:</b>This flag only applies to an explicit rollback attempt for a
	 * subtransaction, typically caused by an exception thrown by a data access operation
	 * (where TransactionInterceptor will trigger a {@code PlatformTransactionManager.rollback()}
	 * call according to a rollback rule). If the flag is off, the caller can handle the exception
	 * and decide on a rollback, independent of the rollback rules of the subtransaction.
	 * This flag does, however, <i>not</i> apply to explicit {@code setRollbackOnly}
	 * calls on a {@code TransactionStatus}, which will always cause an eventual
	 * global rollback (as it might not throw an exception after the rollback-only call).
	 * <p>The recommended solution for handling failure of a subtransaction
	 * is a "nested transaction", where the global transaction can be rolled
	 * back to a savepoint taken at the beginning of the subtransaction.
	 * PROPAGATION_NESTED provides exactly those semantics; however, it will
	 * only work when nested transaction support is available. This is the case
	 * with DataSourceTransactionManager, but not with JtaTransactionManager.
	 * @see #setNestedTransactionAllowed
	 * @see org.springframework.transaction.jta.JtaTransactionManager
	 */
	public final void setGlobalRollbackOnParticipationFailure(boolean globalRollbackOnParticipationFailure) {
		this.globalRollbackOnParticipationFailure = globalRollbackOnParticipationFailure;
	}

	/**
	 * Return whether to globally mark an existing transaction as rollback-only
	 * after a participating transaction failed.
	 */
	public final boolean isGlobalRollbackOnParticipationFailure() {
		return this.globalRollbackOnParticipationFailure;
	}

	/**
	 * Set whether to fail early in case of the transaction being globally marked
	 * as rollback-only.
	 * <p>Default is "false", only causing an UnexpectedRollbackException at the
	 * outermost transaction boundary. Switch this flag on to cause an
	 * UnexpectedRollbackException as early as the global rollback-only marker
	 * has been first detected, even from within an inner transaction boundary.
	 * <p>Note that, as of Spring 2.0, the fail-early behavior for global
	 * rollback-only markers has been unified: All transaction managers will by
	 * default only cause UnexpectedRollbackException at the outermost transaction
	 * boundary. This allows, for example, to continue unit tests even after an
	 * operation failed and the transaction will never be completed. All transaction
	 * managers will only fail earlier if this flag has explicitly been set to "true".
	 * @since 2.0
	 * @see org.springframework.transaction.UnexpectedRollbackException
	 */
	public final void setFailEarlyOnGlobalRollbackOnly(boolean failEarlyOnGlobalRollbackOnly) {
		this.failEarlyOnGlobalRollbackOnly = failEarlyOnGlobalRollbackOnly;
	}

	/**
	 * Return whether to fail early in case of the transaction being globally marked
	 * as rollback-only.
	 * @since 2.0
	 */
	public final boolean isFailEarlyOnGlobalRollbackOnly() {
		return this.failEarlyOnGlobalRollbackOnly;
	}

	/**
	 * Set whether {@code doRollback} should be performed on failure of the
	 * {@code doCommit} call. Typically not necessary and thus to be avoided,
	 * as it can potentially override the commit exception with a subsequent
	 * rollback exception.
	 * <p>Default is "false".
	 * @see #doCommit
	 * @see #doRollback
	 */
	public final void setRollbackOnCommitFailure(boolean rollbackOnCommitFailure) {
		this.rollbackOnCommitFailure = rollbackOnCommitFailure;
	}

	/**
	 * Return whether {@code doRollback} should be performed on failure of the
	 * {@code doCommit} call.
	 */
	public final boolean isRollbackOnCommitFailure() {
		return this.rollbackOnCommitFailure;
	}


	//---------------------------------------------------------------------
	// Implementation of PlatformTransactionManager
	//---------------------------------------------------------------------

	/**
	 * 根据事务的传播行为做出不同的处理
	 * This implementation handles propagation behavior. Delegates to
	 * {@code doGetTransaction}, {@code isExistingTransaction}
	 * and {@code doBegin}.
	 * @param definition TransactionDefinition实例，描述传播行为，隔离级别，超时时间等（默认情况下可以为null）。
	 * @see #doGetTransaction
	 * @see #isExistingTransaction
	 * @see #doBegin
	 */
	@Override
	public final TransactionStatus getTransaction(@Nullable TransactionDefinition definition)
			throws TransactionException {

		// Use defaults if no transaction definition given.
		//如果未提供事务定义，那么使用一个默认事务定义对象StaticTransactionDefinition，这是单例模式的应用。
		//该默认事务定义对象具有默认的事务定义属性，默认值就是TransactionDefinition接口中定义的默认方法返回值
		TransactionDefinition def = (definition != null ? definition : TransactionDefinition.withDefaults());

		//获取当前事务，该方法由具体的事务管理器子类自己实现，比如DataSourceTransactionManager、JtaTransactionManager，一般都使用 DataSourceTransactionManager 事务管理器
		//对于DataSourceTransactionManager这里获取的实际上是一个数据库的事务连接对象，即DataSourceTransactionObject对象
		Object transaction = doGetTransaction();
		boolean debugEnabled = logger.isDebugEnabled();

		//isExistingTransaction，默认返回false，被具体的事务管理器子类重写
		//DataSourceTransactionManager的方法将会判断上面获取的DataSourceTransactionObject内部的数据库连接connectionHolder属性是否不为null并且连接中的transactionActive属性不为空
		// 判断当前线程是否存在事务，判断依据为当前线程记录的连接不为空且连接中的transactionActive属性不为空（事务处于活跃状态）
		if (isExistingTransaction(transaction)) {
			// Existing transaction found -> check propagation behavior to find out how to behave.
			// 当前线程已经存在事务，那么将检查传播行为并进行不同的处理，随后返回
			return handleExistingTransaction(def, transaction, debugEnabled);
		}
		/**
		 * 到这里表示没有已存在的事务，进入第一个事务方法时将会走这个逻辑
		 */
		// Check definition settings for new transaction.
		// 事务超时设置验证
		if (def.getTimeout() < TransactionDefinition.TIMEOUT_DEFAULT) {
			throw new InvalidTimeoutException("Invalid transaction timeout", def.getTimeout());
		}

		// No existing transaction found -> check propagation behavior to find out how to proceed.
		// 如果当前线程不存在事务，但是PropagationBehavior却被声明为PROPAGATION_MANDATORY抛出异常
		// PROPAGATION_MANDATORY---> 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常
		if (def.getPropagationBehavior() == TransactionDefinition.PROPAGATION_MANDATORY) {
			throw new IllegalTransactionStateException(
					"No existing transaction found for transaction marked with propagation 'mandatory'");
		}
		// PROPAGATION_REQUIRED，PROPAGATION_REQUIRES_NEW，PROPAGATION_NESTED 这几个传播行为的含义的共同点之一就是：如果当前不存在事务，就创建一个新事务运行。
		else if (def.getPropagationBehavior() == TransactionDefinition.PROPAGATION_REQUIRED ||
				def.getPropagationBehavior() == TransactionDefinition.PROPAGATION_REQUIRES_NEW ||
				def.getPropagationBehavior() == TransactionDefinition.PROPAGATION_NESTED) {
			//没有当前事务的话，PROPAGATION_REQUIRED，PROPAGATION_REQUIRES_NEW，PROPAGATION_NESTED 挂起的是空事务，然后创建一个新事务，此前没有事务，所以参数事务为null
			SuspendedResourcesHolder suspendedResources = suspend(null);
			if (debugEnabled) {
				logger.debug("Creating new transaction with name [" + def.getName() + "]: " + def);
			}
			try {
				//开启一个新事务并返回
				return startTransaction(def, transaction, debugEnabled, suspendedResources);
			}
			catch (RuntimeException | Error ex) {
				// 恢复挂起的事务
				resume(null, suspendedResources);
				throw ex;
			}
		}
		//配置的事务的传播行为就是剩下的三种：PROPAGATION_SUPPORTS或PROPAGATION_NEVER或PROPAGATION_NOT_SUPPORTED，
		//这几个传播行为的含义的共同点之一就是：当前方法一定以非事务的方式运行。
		else {
			// Create "empty" transaction: no actual transaction, but potentially synchronization.
			// 创建一个空的事务
			//检查一个事务定义对象（def）是否设置了自定义的隔离级别，但没有实际启动事务。
			//如果满足这个条件，将通过日志记录器（logger）输出一条警告信息，并附带事务定义对象的详细信息。该信息指出自定义的隔离级别将被忽略。
			if (def.getIsolationLevel() != TransactionDefinition.ISOLATION_DEFAULT && logger.isWarnEnabled()) {
				logger.warn("Custom isolation level specified but no actual transaction initiated; " +
						"isolation level will effectively be ignored: " + def);
			}
			//判断当前事务的同步化策略是否为始终同步
			boolean newSynchronization = (getTransactionSynchronization() == SYNCHRONIZATION_ALWAYS);
			//为给定参数创建一个新的TransactionStatus，并在适当时初始化事务同步，但是不会真正开启事务。
			//和startTransaction相比，其内部会调用newTransactionStatus和prepareSynchronization，但不会调用doBegin方法
			return prepareTransactionStatus(def, null, true, newSynchronization, debugEnabled, null);
		}
	}

	/**
	 * 开启一个新事物
	 * Start a new transaction.
	 * @param definition         为当前方法配置的事务定义
	 * @param transaction        获取的事务对象，对于DataSourceTransactionManager来说就是DataSourceTransactionObject
	 * @param debugEnabled       日志级别
	 * @param suspendedResources 被挂起的资源
	 * @return 新开启的事务TransactionStatus
	 */
	private TransactionStatus startTransaction(TransactionDefinition definition, Object transaction,
			boolean debugEnabled, @Nullable SuspendedResourcesHolder suspendedResources) {

		// 判断当前事务的同步化策略是否为始终同步，默认是始终同步,即需要开启
		boolean newSynchronization = (getTransactionSynchronization() != SYNCHRONIZATION_NEVER);
		// 创建新的事务，内部持有transaction对象
		DefaultTransactionStatus status = newTransactionStatus(
				definition, transaction, true, newSynchronization, debugEnabled, suspendedResources);
		// 开启事务和连接，该方法由具体的子类自己实现
		doBegin(transaction, definition);
		// 准备事务同步的设置，针对于当前线程的设置
		prepareSynchronization(status, definition);
		return status;
	}


	/**
	 * 根据现有事务创建一个TransactionStatus，处理事务的传播行为
	 * Create a TransactionStatus for an existing transaction.
	 * @param definition 当前事务定义
	 * @param transaction 事物对象，内部包含此前的事务信息
	 * @param debugEnabled  日志级别支持
	 * @return
	 * @throws TransactionException
	 */
	private TransactionStatus handleExistingTransaction(
			TransactionDefinition definition, Object transaction, boolean debugEnabled)
			throws TransactionException {

		 // 判断当前的事务传播行为是不是PROPAGATION_NEVER
		 // 以非事务方式运行，如果当前存在事务，则抛出异常
		if (definition.getPropagationBehavior() == TransactionDefinition.PROPAGATION_NEVER) {
			throw new IllegalTransactionStateException(
					"Existing transaction found for transaction marked with propagation 'never'");
		}

		// 如果当前配置的传播行为是PROPAGATION_NOT_SUPPORTED -> 以非事务方式运行，如果当前存在事务，则把当前事务挂起
		// 当前方法一定以非事务的方式运行，如果当前存在事务，则把当前事务挂起，直到当前方法执行完毕，才恢复外层事务。
		if (definition.getPropagationBehavior() == TransactionDefinition.PROPAGATION_NOT_SUPPORTED) {
			if (debugEnabled) {
				logger.debug("Suspending current transaction");
			}
			//挂起外层事务，返回被挂起的资源
			Object suspendedResources = suspend(transaction);
			//判断当前事务的同步化策略是否为始终同步，默认是始终同步
			boolean newSynchronization = (getTransactionSynchronization() == SYNCHRONIZATION_ALWAYS);
			// 创建一个新的非事务状态 TransactionStatus(保存了上一个存在事务状态的属性) 并在适当时初始化事务同步。
			//这里的第二个参数事务属性为 null，表示当前方法以非事务的方式执行
			return prepareTransactionStatus(
					definition, null, false, newSynchronization, debugEnabled, suspendedResources);
		}

		/**
		 * 当前的事务属性状态是PROPAGATION_REQUIRES_NEW表示需要新开启一个事务状态，外部事务（如果存在）将被挂起，内务事务结束时，外部事务将继续执行。
		 */
		if (definition.getPropagationBehavior() == TransactionDefinition.PROPAGATION_REQUIRES_NEW) {
			if (debugEnabled) {
				logger.debug("Suspending current transaction, creating new transaction with name [" +
						definition.getName() + "]");
			}
			//挂起外层事务，返回被挂起的资源
			SuspendedResourcesHolder suspendedResources = suspend(transaction);
			try {
				//开启一个新事务并返回
				return startTransaction(definition, transaction, debugEnabled, suspendedResources);
			}
			catch (RuntimeException | Error beginEx) {
				resumeAfterBeginException(transaction, suspendedResources, beginEx);
				throw beginEx;
			}
		}

		// 嵌套事务，如果存在当前事务，则在嵌套事务中执行，否则行为类似于 PROPAGATION_REQUIRED，即会新建一个事务运行。
		if (definition.getPropagationBehavior() == TransactionDefinition.PROPAGATION_NESTED) {
			//判断是否允许PROPAGATION_NESTED行为，默认不允许，子类可以重写该方法，DataSourceTransactionManager重写为为允许
			if (!isNestedTransactionAllowed()) {
				throw new NestedTransactionNotSupportedException(
						"Transaction manager does not allow nested transactions by default - " +
						"specify 'nestedTransactionAllowed' property with value 'true'");
			}
			if (debugEnabled) {
				logger.debug("Creating nested transaction with name [" + definition.getName() + "]");
			}
			//返回是否对嵌套事务使用保存点，默认true，JtaTransactionManager设置为false
			//PROPAGATION_NESTED就是通过Savepoint保存点来实现的
			if (useSavepointForNestedTransaction()) {
				// Create savepoint within existing Spring-managed transaction,
				// through the SavepointManager API implemented by TransactionStatus.
				// Usually uses JDBC 3.0 savepoints. Never activates Spring synchronization.
				//并没有挂起当前事务，创建TransactionStatus，transaction参数就是当前事务
				DefaultTransactionStatus status =
						prepareTransactionStatus(definition, transaction, false, false, debugEnabled, null);
				// 为事务设置一个回退点
				//通过TransactionStatus实现的SavepointManager API在现有的Spring管理的事务中创建保存点，通常使用JDBC 3.0保存点。
				status.createAndHoldSavepoint();
				return status;
			}
			else {
				// Nested transaction through nested begin and commit/rollback calls.
				// Usually only for JTA: Spring synchronization might get activated here
				// in case of a pre-existing JTA transaction.
				// 通过嵌套的begin和commit/rollback调用的嵌套事务。通常仅针对JTA：在存在预先存在的JTA事务的情况下，这里可能会激活Spring同步。
				return startTransaction(definition, transaction, debugEnabled, null);
			}
		}

		//剩下的传播行为就是PROPAGATION_REQUIRED、 PROPAGATION_SUPPORTS、PROPAGATION_MANDATORY 三个。

		// Assumably PROPAGATION_SUPPORTS or PROPAGATION_REQUIRED.
		if (debugEnabled) {
			logger.debug("Participating in existing transaction");
		}
		//是否在参与现有事务之前进行验证，默认false
		if (isValidateExistingTransaction()) {
			if (definition.getIsolationLevel() != TransactionDefinition.ISOLATION_DEFAULT) {
				Integer currentIsolationLevel = TransactionSynchronizationManager.getCurrentTransactionIsolationLevel();
				if (currentIsolationLevel == null || currentIsolationLevel != definition.getIsolationLevel()) {
					Constants isoConstants = DefaultTransactionDefinition.constants;
					throw new IllegalTransactionStateException("Participating transaction with definition [" +
							definition + "] specifies isolation level which is incompatible with existing transaction: " +
							(currentIsolationLevel != null ?
									isoConstants.toCode(currentIsolationLevel, DefaultTransactionDefinition.PREFIX_ISOLATION) :
									"(unknown)"));
				}
			}
			if (!definition.isReadOnly()) {
				if (TransactionSynchronizationManager.isCurrentTransactionReadOnly()) {
					throw new IllegalTransactionStateException("Participating transaction with definition [" +
							definition + "] is not marked as read-only but existing transaction is");
				}
			}
		}
		// 判断当前事务的同步化策略是否为始终同步，默认是始终同步,即需要开启
		boolean newSynchronization = (getTransactionSynchronization() != SYNCHRONIZATION_NEVER);
		//并没有挂起当前事务，而是直接参与到当前事务中去，transaction参数就是当前的事务
		return prepareTransactionStatus(definition, transaction, false, newSynchronization, debugEnabled, null);
	}


	/**
	 * 根据给定的参数创建一个新的TransactionStatus，并根据需要初始化事务同步。不会真正的开启事务
	 * Create a new TransactionStatus for the given arguments,
	 * also initializing transaction synchronization as appropriate.
	 * @param definition  为当前方法设置的事务定义
	 * @param transaction  获取的事务对象，对于DataSourceTransactionManager来说就是DataSourceTransactionObject
	 * @param newTransaction 是否是新事物，如果是外层事务方法，则为true，如果是内层方法则为false
	 * @param newSynchronization  是否是新事务同步
	 * @param debug 支持debug日志
	 * @param suspendedResources  被挂起的资源
	 * @see #newTransactionStatus
	 * @see #prepareTransactionStatus
	 * @return
	 */
	protected final DefaultTransactionStatus prepareTransactionStatus(
			TransactionDefinition definition, @Nullable Object transaction, boolean newTransaction,
			boolean newSynchronization, boolean debug, @Nullable Object suspendedResources) {
		//通过newTransactionStatus创建一个DefaultTransactionStatus
		DefaultTransactionStatus status = newTransactionStatus(
				definition, transaction, newTransaction, newSynchronization, debug, suspendedResources);

		//没有调用doBegin方法，不会真正的开启事物

		// 准备事务同步的设置，针对于当前线程的设置
		prepareSynchronization(status, definition);
		return status;
	}

	/**
	 * 通过给定的参数创建事务状态,有一个非常关键的参数是newTransaction,用于判断是否是新连接，如果事务不存在，那么肯定是true，如果存在，就需要根据传播特性来决定是否是false
     * 为给定的参数创建TransactionStatus实例
	 * Create a TransactionStatus instance for the given arguments.
	 * @param definition 为当前方法配置的事务定义
	 * @param transaction  获取的事务对象，对于DataSourceTransactionManager来说就是DataSourceTransactionObject
	 * @param newTransaction  是否是新事物
	 * @param newSynchronization 是否开启事务同步
	 * @param debug  是否支持debug级别的日志
	 * @param suspendedResources  被挂起的资源，比如此前的事务同步
	 * @return
	 */
	protected DefaultTransactionStatus newTransactionStatus(
			TransactionDefinition definition, @Nullable Object transaction, boolean newTransaction,
			boolean newSynchronization, boolean debug, @Nullable Object suspendedResources) {

        //如果newSynchronization为true并且当前线程没有绑定的事务同步，那么确定开启新事物同步
        //由于此前调用了suspend方法清理了此前的事务同步，因此一般都是需要开启新事务同步，即为true
		boolean actualNewSynchronization = newSynchronization &&
				!TransactionSynchronizationManager.isSynchronizationActive();
		//返回一个新建的DefaultTransactionStatus对象，该对象被用来表示新开启的事务，是TransactionStatus的默认实现
		//内部包括了各种新开启的事务状态，也包括此前挂起的事务的资源
		return new DefaultTransactionStatus(
				transaction, newTransaction, actualNewSynchronization,
				definition.isReadOnly(), debug, suspendedResources);
	}

	/**
	 * 根据需要初始化事务同步。设置各种线程私有变量的状态
	 * Initialize transaction synchronization as appropriate.
	 */
	protected void prepareSynchronization(DefaultTransactionStatus status, TransactionDefinition definition) {
		//是否是新同步，在真正的开启新事务的时候（比如第一次进入事务方法或者传播行为是REQUIRES_NEW），一般都是true
		if (status.isNewSynchronization()) {
			//是否具有事务，绑定事务激活
			TransactionSynchronizationManager.setActualTransactionActive(status.hasTransaction());
			// 当前事务的隔离级别
			TransactionSynchronizationManager.setCurrentTransactionIsolationLevel(
					definition.getIsolationLevel() != TransactionDefinition.ISOLATION_DEFAULT ?
							definition.getIsolationLevel() : null);
			// 是否为只读事务
			TransactionSynchronizationManager.setCurrentTransactionReadOnly(definition.isReadOnly());
			// 事务的名称
			TransactionSynchronizationManager.setCurrentTransactionName(definition.getName());
			//初始化同步
			TransactionSynchronizationManager.initSynchronization();
		}
	}

	/**
	 * 确定用于给定定义的实际超时。如果事务定义没有指定非默认值，则将回退到此管理器的默认超时。
	 * Determine the actual timeout to use for the given definition.
	 * Will fall back to this manager's default timeout if the
	 * transaction definition doesn't specify a non-default value.
	 * @param definition the transaction definition 事务定义
	 * @return the actual timeout to use  实际使用的超时时间
	 * @see org.springframework.transaction.TransactionDefinition#getTimeout()
	 * @see #setDefaultTimeout
	 */
	protected int determineTimeout(TransactionDefinition definition) {
		//如果不是默认超时时间，那么使用指定的时间
		if (definition.getTimeout() != TransactionDefinition.TIMEOUT_DEFAULT) {
			return definition.getTimeout();
		}
		//否则使用默认超时
		return getDefaultTimeout();
	}


	/**
	 * 有些传播机制需要挂起当前的事务，比如 PROPAGATION_NOT_SUPPORTED,PROPAGATION_REQUIRES_NEW。
	 * 首先会清除所有线程相关的同步状态，如果当前事务存在的话，就进行一些属性的清除，比如清空连接持有器，清空线程私有变量的同步状态，
	 * 最后把当前事务清除的属性保存到一个SuspendedResourcesHolder里，以便于恢复根据这个对象重新设置回原先的状态
	 *
	 * Suspend the given transaction. Suspends transaction synchronization first,
	 * then delegates to the {@code doSuspend} template method.
	 * @param transaction the current transaction object
	 * (or {@code null} to just suspend active synchronizations, if any) 当前事务对象（如果为null，则仅挂起活动同步）
	 * @return an object that holds suspended resources
	 * (or {@code null} if neither transaction nor synchronization active)  一个拥有挂起的资源的对象（如果没有事务或同步未激活，则为null）
	 * @see #doSuspend
	 * @see #resume
	 */
	@Nullable
	protected final SuspendedResourcesHolder suspend(@Nullable Object transaction) throws TransactionException {
		// 如果当前线程的事务同步处于活动状态，即存在绑定的TransactionSynchronization，则返回true。如果是第一次因为进来，返回false
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			//挂起当前线程的所有事务同步回调，这类似于@TransactionalEventListener，并返回"被挂起"的回调
			List<TransactionSynchronization> suspendedSynchronizations = doSuspendSynchronization();
			try {
				Object suspendedResources = null;
				if (transaction != null) {
					//挂起的资源，连接持有器
					suspendedResources = doSuspend(transaction);
				}
				//获取当前事务的信息，并且清空各个ThreadLocal缓存中的当前线程的当前事务信息（恢复为默认值）

				// 获取当前事务名称
				String name = TransactionSynchronizationManager.getCurrentTransactionName();
				// 清空线程变量
				TransactionSynchronizationManager.setCurrentTransactionName(null);
				// 获取出只读事务的名称
				boolean readOnly = TransactionSynchronizationManager.isCurrentTransactionReadOnly();
				// 清空线程变量
				TransactionSynchronizationManager.setCurrentTransactionReadOnly(false);
				// 获取已存在事务的隔离级别
				Integer isolationLevel = TransactionSynchronizationManager.getCurrentTransactionIsolationLevel();
				// 清空隔离级别
				TransactionSynchronizationManager.setCurrentTransactionIsolationLevel(null);
				// 判断当前事务激活状态
				boolean wasActive = TransactionSynchronizationManager.isActualTransactionActive();
				// 清空标记
				TransactionSynchronizationManager.setActualTransactionActive(false);
				// 把上诉从线程变量中获取出来的存在事务属性封装为挂起的事务属性返回出去
				return new SuspendedResourcesHolder(
						suspendedResources, suspendedSynchronizations, name, readOnly, isolationLevel, wasActive);
			}
			catch (RuntimeException | Error ex) {
				// doSuspend failed - original transaction is still active...
				doResumeSynchronization(suspendedSynchronizations);
				throw ex;
			}
		}
		//如果没有事务同步但是开启了事务，那么挂起事务
		else if (transaction != null) {
			// Transaction active but no synchronization active.
			//挂起的资源，连接持有器
			Object suspendedResources = doSuspend(transaction);
			//将挂起的资源存入一个SuspendedResourcesHolder对象中返回
			return new SuspendedResourcesHolder(suspendedResources);
		}
		else {
			// Neither transaction nor synchronization active.
			//事务或者事务同步均未激活，返回null，什么也不干
			return null;
		}
	}

	/**
	 * 恢复给定的事务。首先委托doResume模板方法，然后恢复事务同步。
	 *
	 * Resume the given transaction. Delegates to the {@code doResume}
	 * template method first, then resuming transaction synchronization.
	 * @param transaction the current transaction object当前事务对象
	 * @param resourcesHolder the object that holds suspended resources,
	 * as returned by {@code suspend} (or {@code null} to just
	 * resume synchronizations, if any)  持有挂起的资源的对象，如果有的话，由 suspend或null 返回以仅恢复同步，
	 * @see #doResume
	 * @see #suspend
	 */
	protected final void resume(@Nullable Object transaction, @Nullable SuspendedResourcesHolder resourcesHolder)
			throws TransactionException {

		// 设置属性和状态
		if (resourcesHolder != null) {
			//被挂起的事务资源
			Object suspendedResources = resourcesHolder.suspendedResources;
			if (suspendedResources != null) {
				//首先调用doResume恢复当前事务的资源
				doResume(transaction, suspendedResources);
			}
			//被挂起的事物同步
			List<TransactionSynchronization> suspendedSynchronizations = resourcesHolder.suspendedSynchronizations;
			//如果有挂起同步器的话，要设置线程私有变量的值为挂起事务的相关属性
			if (suspendedSynchronizations != null) {
				//设置当前是否存在实际的事务活动
				TransactionSynchronizationManager.setActualTransactionActive(resourcesHolder.wasActive);
				//设置当前事务的隔离级别
				TransactionSynchronizationManager.setCurrentTransactionIsolationLevel(resourcesHolder.isolationLevel);
				//设置当前事务是否为只读事务
				TransactionSynchronizationManager.setCurrentTransactionReadOnly(resourcesHolder.readOnly);
				//设置当前事务的名称
				TransactionSynchronizationManager.setCurrentTransactionName(resourcesHolder.name);
				// 唤醒事务同步
				doResumeSynchronization(suspendedSynchronizations);
			}
		}
	}

	/**
	 * Resume outer transaction after inner transaction begin failed.
	 */
	private void resumeAfterBeginException(
			Object transaction, @Nullable SuspendedResourcesHolder suspendedResources, Throwable beginEx) {

		try {
			resume(transaction, suspendedResources);
		}
		catch (RuntimeException | Error resumeEx) {
			String exMessage = "Inner transaction begin exception overridden by outer transaction resume exception";
			logger.error(exMessage, beginEx);
			throw resumeEx;
		}
	}

	/**
	 *  挂起所有当前同步并停用当前线程的事务同步。
	 * Suspend all current synchronizations and deactivate transaction
	 * synchronization for the current thread.
	 * @return the List of suspended TransactionSynchronization objects  挂起的TransactionSynchronization对象的列表
	 */
	private List<TransactionSynchronization> doSuspendSynchronization() {
		//获取线程的当前的所有事务同步列表
		List<TransactionSynchronization> suspendedSynchronizations =
				TransactionSynchronizationManager.getSynchronizations();
		//遍历，依次挂起每一个事务同步
		for (TransactionSynchronization synchronization : suspendedSynchronizations) {
			synchronization.suspend();
		}
		//清除synchronizations属性中保存的当前线程的当前事务同步集合的引用
		TransactionSynchronizationManager.clearSynchronization();
		//返回被挂起的事务同步
		return suspendedSynchronizations;
	}

	/**
	 * 重新激活当前线程的事务同步并恢复所有给定的同步。
	 * Reactivate transaction synchronization for the current thread
	 * and resume all given synchronizations.
	 * @param suspendedSynchronizations a List of TransactionSynchronization objects 列表包含了所有需要被恢复（或重新激活）的事务同步对象。
	 */
	private void doResumeSynchronization(List<TransactionSynchronization> suspendedSynchronizations) {
		//激活当前线程的事务同步。由事务管理器在事务开始时调用。
		TransactionSynchronizationManager.initSynchronization();
		for (TransactionSynchronization synchronization : suspendedSynchronizations) {
			//恢复事务同步对象的状态，使其能继续跟踪事务的进展或响应事务事件，比如在事务提交或回滚时执行相应的回调逻辑。
			synchronization.resume();
			//恢复后的事务同步对象重新注册到Spring的事务管理框架中。当事务生命周期中的关键事件发生时，这些同步对象会接收到通知并执行相应的处理逻辑。
			TransactionSynchronizationManager.registerSynchronization(synchronization);
		}
	}


	/**
	 * 提交事务，就算没有异常，但是提交的时候也可能会回滚，因为有内层事务可能会标记回滚。所以这里先判断是否状态是否需要本地回滚，
	 * 也就是设置回滚标记为全局回滚，不会进行回滚，再判断是否需要全局回滚，就是真的执行回滚。但是这里如果是发现有全局回滚，还要进行提交，就会报异常
	 *
	 * This implementation of commit handles participating in existing
	 * transactions and programmatic rollback requests.
	 * Delegates to {@code isRollbackOnly}, {@code doCommit}
	 * and {@code rollback}.
	 * @see org.springframework.transaction.TransactionStatus#isRollbackOnly()
	 * @see #doCommit
	 * @see #rollback
	 */
	@Override
	public final void commit(TransactionStatus status) throws TransactionException {
		// 如果已经完成，则不能再提交或回滚
		if (status.isCompleted()) {
			throw new IllegalTransactionStateException(
					"Transaction is already completed - do not call commit or rollback more than once per transaction");
		}

		// 获取当前事务状态
		DefaultTransactionStatus defStatus = (DefaultTransactionStatus) status;
		// 如果在事务链中已经被标记回滚，那么不会尝试提交事务，直接回滚
		if (defStatus.isLocalRollbackOnly()) {
			if (defStatus.isDebug()) {
				logger.debug("Transactional code has requested rollback");
			}
			// 不可预期的回滚
			processRollback(defStatus, false);
			return;
		}

		// 设置了全局回滚
		// shouldCommitOnGlobalRollbackOnly 方法确实用于决策是否应对一个已被标记为全局回滚的事务尝试执行doCommit提交操作。默认实现返回false，即不会提交，而是一起回滚
        // JtaTransactionManager重写返回true
		// 当事务被设置为全局回滚时，特别是对于使用DataSourceTransactionObject的场景，事务管理器会检查如ConnectionHolder的rollbackOnly属性来确定事务的最终处理方式
		if (!shouldCommitOnGlobalRollbackOnly() && defStatus.isGlobalRollbackOnly()) {
			if (defStatus.isDebug()) {
				logger.debug("Global transaction is marked as rollback-only but transactional code requested commit");
			}


			 //处理回滚，注意unexpected参数为true
			 //对于加入到外层事务的行为，如果在内层方法中进行了回滚，即使异常被捕获，由于被设置为了仅回滚，那么该事物的所有操作仍然会回滚
			 //并且还会在处理最外层事务方法时抛出UnexpectedRollbackException异常，来提醒开发者所有的外部和内部操作都已被回滚！
			 //内层事务回滚影响，如果内层事务方法中请求了回滚（通过异常或显式设置），并且内层事务传播行为为PROPAGATION_REQUIRED、PROPAGATION_SUPPORTS或PROPAGATION_MANDATORY，那么这一回滚请求会传播到外层事务，导致整个事务链路中的操作被回滚。
			 //对于内层PROPAGATION_NOT_SUPPORTED则无效，该事务策略明确指示方法不应该运行在事务中。因此，如果内层方法使用此传播行为，它实际上不会参与到任何外层事务中，自然也就不存在将其回滚影响传递到外层事务的情况。

			// 可预期的回滚，可能会报异常
			processRollback(defStatus, true);
			return;
		}

		// 处理事务提交
		processCommit(defStatus);
	}

	/**
	 * 理实际的提交。Rollback-only标志已被检查和应用。
	 * 先处理保存点，然后处理新事务，如果不是新事务不会真正提交，要等外层是新事务的才提交，
	 * 最后根据条件执行数据清除,线程的私有资源解绑，重置连接自动提交，隔离级别，是否只读，释放连接，恢复挂起事务等
	 *
	 * Process an actual commit.
	 * Rollback-only flags have already been checked and applied.
	 * @param status object representing the transaction 代表当前事务对象
	 * @throws TransactionException in case of commit failure  在提交失败的情况下
	 */
	private void processCommit(DefaultTransactionStatus status) throws TransactionException {
		try {
			// beforeCompletion方法已回调的标志位
			boolean beforeCompletionInvoked = false;

			try {
				// 意外的回滚标志
				boolean unexpectedRollback = false;
				// 预留，要在beforeCommit同步回调发生之前执行， 该方法为空实现，留给子类重写
				prepareForCommit(status);
				// 提交完成前回调，触发beforeCommit回调。在当前线程所有已注册的TransactionSynchronization上触发beforeCommit方法回调
				triggerBeforeCommit(status);
				//在事务提交/回滚之前调用，触发beforeCompletion回调。 在当前线程所有已注册的TransactionSynchronization上触发beforeCompletion方法回调
				triggerBeforeCompletion(status);
				//标志位改为true
				beforeCompletionInvoked = true;

				// 是否有保存点，内层PROPAGATION_NESTED事务会开启保存点，即嵌套事务
				if (status.hasSavepoint()) {
					if (status.isDebug()) {
						logger.debug("Releasing transaction savepoint");
					}
					//是否有全局回滚标记
					unexpectedRollback = status.isGlobalRollbackOnly();
					// 如果存在保存点则清除保存点信息，并不会提交事务，而是需要等待外层事务方法去提交
					status.releaseHeldSavepoint();
				}
				//当前状态是新事务
				else if (status.isNewTransaction()) {
					if (status.isDebug()) {
						logger.debug("Initiating transaction commit");
					}
					//是否有全局回滚标记
					unexpectedRollback = status.isGlobalRollbackOnly();
					// 如果是独立的事务则直接提交（新开启的事务或者最外层事务）
					doCommit(status);
				}
				//返回在事务被全局标记为"仅回滚"的情况下是否尽早失败，即是否需要立即抛出异常
				else if (isFailEarlyOnGlobalRollbackOnly()) {
					unexpectedRollback = status.isGlobalRollbackOnly();
				}

				// Throw UnexpectedRollbackException if we have a global rollback-only
				// marker but still didn't get a corresponding exception from commit.
				// 如果我们有一个全局rollback-only的标记，但仍未从提交中获得相应的异常，
				// 则抛出UnexpectedRollbackException,但是此时事务已经被提交了
				if (unexpectedRollback) {
					throw new UnexpectedRollbackException(
							"Transaction silently rolled back because it has been marked as rollback-only");
				}
			}
			/**
			 * 处理各种异常
			 */
			catch (UnexpectedRollbackException ex) {
				// can only be caused by doCommit
				//如果在执行以上方法过程中抛出UnexpectedRollbackException异常(只能由doCommit引起)
				//那么在当前线程所有已注册的TransactionSynchronization上触发afterCompletion方法回调
				triggerAfterCompletion(status, TransactionSynchronization.STATUS_ROLLED_BACK);
				throw ex;
			}
			catch (TransactionException ex) {
				// can only be caused by doCommit
				//如果在执行以上方法过程中抛出TransactionException异常(只能由doCommit引起)
				//返回是否在doCommit调用失败时执行doRollback，一般为false，即不会
				if (isRollbackOnCommitFailure()) {
					doRollbackOnCommitException(status, ex);
				}
				else {
					//在当前线程所有已注册的TransactionSynchronization上触发afterCompletion方法回调
					triggerAfterCompletion(status, TransactionSynchronization.STATUS_UNKNOWN);
				}
				throw ex;
			}
			catch (RuntimeException | Error ex) {
				//如果在执行beforeCompletion回调、回滚保存点、回滚事务等过程中抛出RuntimeException或者Error异常
				//那么在当前线程所有当前已注册的TransactionSynchronization上触发afterCompletion方法回调
				//如果还没有触发beforeCompletion方法回调
				if (!beforeCompletionInvoked) {
					//触发beforeCompletion回调。
					triggerBeforeCompletion(status);
				}
				//调用doRollback方法处理提交事务时的异常，随后触发afterCompletion方法回调
				doRollbackOnCommitException(status, ex);
				throw ex;
			}

			//触发器afterCommit回调，其中抛出的异常传播到调用方，但事务仍被视为已提交
			// Trigger afterCommit callbacks, with an exception thrown there
			// propagated to callers but the transaction still considered as committed.
			try {
				// 提交后回调
				triggerAfterCommit(status);
			}
			finally {
				// 提交后清除线程私有同步状态
				triggerAfterCompletion(status, TransactionSynchronization.STATUS_COMMITTED);
			}

		}
		finally {
			//根据条件，完成后数据清除,和线程的私有资源解绑，重置连接自动提交，隔离级别，是否只读，释放连接，恢复挂起事务等
			cleanupAfterCompletion(status);
		}
	}

	/**
	 * 事务管理器根据事务状态来处理回滚
	 * 回滚事务。核心方法是doRollback和doSetRollbackOnly。
	 * This implementation of rollback handles participating in existing
	 * transactions. Delegates to {@code doRollback} and
	 * {@code doSetRollbackOnly}.
	 * @see #doRollback
	 * @see #doSetRollbackOnly
	 */
	@Override
	public final void rollback(TransactionStatus status) throws TransactionException {
		//如果当前事务已完成，即已提交或者回滚，那么抛出异常
		if (status.isCompleted()) {
			throw new IllegalTransactionStateException(
					"Transaction is already completed - do not call commit or rollback more than once per transaction");
		}

		DefaultTransactionStatus defStatus = (DefaultTransactionStatus) status;
		//处理回滚
		processRollback(defStatus, false);
	}

	/**
	 * unexpected这个一般是false，除非是设置rollback-only=true，才是true，表示是全局的回滚标记。
	 * 首先会进行回滚前回调，然后判断是否设置了保存点，比如NESTED会设置，要先回滚到保存点。
	 * 如果状态是新的事务，那就进行回滚，如果不是新的，就设置一个回滚标记，内部是设置连接持有器回滚标记。
	 * 然后回滚完成回调，根据事务状态信息，完成后数据清除,和线程的私有资源解绑，
	 * 重置连接自动提交，隔离级别，是否只读，释放连接，恢复挂起事务等
	 *
	 * Process an actual rollback.
	 * The completed flag has already been checked.
	 * @param status object representing the transaction 当前事务状态
	 * @throws TransactionException in case of rollback failure 在回滚失败的情况下
	 */
	private void processRollback(DefaultTransactionStatus status, boolean unexpected) {
		try {
			// 意外的回滚
			boolean unexpectedRollback = unexpected;

			try {
				// 在当前线程所有已注册的TransactionSynchronization上触发beforeCompletion方法回调，回滚完成前回调
				triggerBeforeCompletion(status);
				// 判断是否具有保存点，（常见于嵌套事务，如PROPAGATION_NESTED）。如果存在保存点，则通过status.rollbackToHeldSavepoint()回滚到最近的保存点，而非完全回滚整个事务。
				if (status.hasSavepoint()) {
					if (status.isDebug()) {
						logger.debug("Rolling back transaction to savepoint");
					}
					//回滚到为事务保留的保存点，然后立即释放该保存点。
					status.rollbackToHeldSavepoint();
				}
				// 当前状态是一个新事务
				// 如果事务传播行为是 PROPAGATION_REQUIRED 并且之前没有事务存在，或者是 PROPAGATION_REQUIRES_NEW（无论当前是否有事务在进行，都会创建一个新的事务），那么这个方法返回 true。这是因为这两种传播行为都会导致一个新的事务被创建。
				// 关于PROPAGATION_NESTED在无外层事务时的具体行为，虽然它也可能导致事务创建，但从事务管理的视角看，这种“新”更倾向于逻辑处理上的新开始，而非严格按照isNewTransaction()所定义的“全新事务”。
				// 即便在某些情况下它表现得像创建了一个新事务，status.isNewTransaction()的返回值逻辑更多依赖于事务管理器的具体实现细节。
				// 在Spring框架中，当使用PROPAGATION_NESTED且外部没有事务时，尽管可能启动了一个事务，但这个事务在概念上可能仍不被视为“全新”的，因为它是作为唯一事务上下文存在，没有外层事务与之对比。
				//status.isNewTransaction()的返回逻辑更多基于事务管理器对事务上下文的内部理解和定义，可能不会直接反映这一特殊情况下的“新”创建行为
				else if (status.isNewTransaction()) {
					if (status.isDebug()) {
						logger.debug("Initiating transaction rollback");
					}
					// 进行回滚
					doRollback(status);
				}
				else {
					// 如果status具有事务，那么这里表示外层的事务，这里就参与到外层事务的回滚操作中
					// Participating in larger transaction
					if (status.hasTransaction()) {
						// 当status.isLocalRollbackOnly()为true时，意味着当前事务上下文已经标记为仅回滚标志，这可能是由于之前的操作失败或者其他原因导致事务需要被标记不可提交。
						// isGlobalRollbackOnParticipationFailure()是一个配置属性检查，如果为true（默认值），表示当任何参与的事务失败时，整个事务（不仅仅是当前参与的部分）都应被标记为只回滚。这意味着，即使内部的异常被捕获并尝试逻辑上的恢复，外部事务仍会被指示必须回滚，确保事务的原子性和一致性不受影响。
						// 整体配置要求参与失败的事务导致全局回滚时，事务会被适当地标记为只回滚（rollback-only）。一旦事务被标记为rollback-only，它就不能再被提交，即使后续的代码尝试正常结束事务，事务管理器也会强制执行回滚操作。这保证了在分布式或嵌套事务场景下，一旦部分失败，能够维持整个事务的完整性。
						if (status.isLocalRollbackOnly() || isGlobalRollbackOnParticipationFailure()) {
							if (status.isDebug()) {
								logger.debug("Participating transaction failed - marking existing transaction as rollback-only");
							}
							//设置连接要回滚标记，也就是全局回滚
							doSetRollbackOnly(status);
						}
						else {
							if (status.isDebug()) {
								logger.debug("Participating transaction failed - letting transaction originator decide on rollback");
							}
						}
					}
					else {
						logger.debug("Should roll back transaction but cannot - no transaction available");
					}
					//返回事务被全局标记为仅回滚时是否提前失败。一般为false，因此结果就是true，导致unexpectedRollback为false
					// Unexpected rollback only matters here if we're asked to fail early
					if (!isFailEarlyOnGlobalRollbackOnly()) {
						unexpectedRollback = false;
					}
				}
			}
			catch (RuntimeException | Error ex) {
				//即使在执行beforeCompletion回调、回滚保存点、回滚事务等过程中抛出RuntimeException或者Error异常
				//仍然会在当前线程所有当前已注册的TransactionSynchronization上触发afterCompletion方法回调
				triggerAfterCompletion(status, TransactionSynchronization.STATUS_UNKNOWN);
				throw ex;
			}

			// 在当前线程所有已注册的TransactionSynchronization上触发afterCompletion方法回调，回滚完成后回调
			triggerAfterCompletion(status, TransactionSynchronization.STATUS_ROLLED_BACK);
			// Raise UnexpectedRollbackException if we had a global rollback-only marker
			//如果我们有一个仅全局回滚标记，则引发UnexpectedRollbackException
			if (unexpectedRollback) {
				throw new UnexpectedRollbackException(
						"Transaction rolled back because it has been marked as rollback-only");
			}
		}
		finally {
			// 根据事务状态信息，完成后数据清除，和线程的私有资源解绑，重置连接自动提交，隔离级别，是否只读，释放连接，恢复挂起事务等
			cleanupAfterCompletion(status);
		}
	}

	/**
	 * Invoke {@code doRollback}, handling rollback exceptions properly.
	 * @param status object representing the transaction
	 * @param ex the thrown application exception or error
	 * @throws TransactionException in case of rollback failure
	 * @see #doRollback
	 */
	private void doRollbackOnCommitException(DefaultTransactionStatus status, Throwable ex) throws TransactionException {
		try {
			if (status.isNewTransaction()) {
				if (status.isDebug()) {
					logger.debug("Initiating transaction rollback after commit exception", ex);
				}
				doRollback(status);
			}
			else if (status.hasTransaction() && isGlobalRollbackOnParticipationFailure()) {
				if (status.isDebug()) {
					logger.debug("Marking existing transaction as rollback-only after commit exception", ex);
				}
				doSetRollbackOnly(status);
			}
		}
		catch (RuntimeException | Error rbex) {
			logger.error("Commit exception overridden by rollback exception", ex);
			triggerAfterCompletion(status, TransactionSynchronization.STATUS_UNKNOWN);
			throw rbex;
		}
		triggerAfterCompletion(status, TransactionSynchronization.STATUS_ROLLED_BACK);
	}


	/**
	 * Trigger {@code beforeCommit} callbacks.
	 * @param status object representing the transaction
	 */
	protected final void triggerBeforeCommit(DefaultTransactionStatus status) {
		if (status.isNewSynchronization()) {
			if (status.isDebug()) {
				logger.trace("Triggering beforeCommit synchronization");
			}
			TransactionSynchronizationUtils.triggerBeforeCommit(status.isReadOnly());
		}
	}

	/**
	 * 触发beforeCompletion回调。
	 * <p>
	 * Trigger {@code beforeCompletion} callbacks.
	 * @param status object representing the transaction  当前事务状态
	 */
	protected final void triggerBeforeCompletion(DefaultTransactionStatus status) {
		//如果存在事务同步
		if (status.isNewSynchronization()) {
			if (status.isDebug()) {
				logger.trace("Triggering beforeCompletion synchronization");
			}
			//触发所有已注册到当前线程的TransactionSynchronization接口实现类的beforeCompletion方法
			TransactionSynchronizationUtils.triggerBeforeCompletion();
		}
	}

	/**
	 * Trigger {@code afterCommit} callbacks.
	 * @param status object representing the transaction
	 */
	private void triggerAfterCommit(DefaultTransactionStatus status) {
		if (status.isNewSynchronization()) {
			if (status.isDebug()) {
				logger.trace("Triggering afterCommit synchronization");
			}
			TransactionSynchronizationUtils.triggerAfterCommit();
		}
	}

	/**
	 * 触发afterCompletion回调。
	 * <p>
	 * Trigger {@code afterCompletion} callbacks.
	 * @param status object representing the transaction  当前事务状态
	 * @param completionStatus completion status according to TransactionSynchronization constants TransactionSynchronization常量表示的事务完成状态，如果回滚处理成功则是STATUS_ROLLED_BACK，回滚过程中抛出异常则是STATUS_UNKNOWN
	 */
	private void triggerAfterCompletion(DefaultTransactionStatus status, int completionStatus) {
		//如果是新事务同步
		if (status.isNewSynchronization()) {
			//获取目前注册的TransactionSynchronization集合
			List<TransactionSynchronization> synchronizations = TransactionSynchronizationManager.getSynchronizations();
			//清空当前线程绑定的TransactionSynchronization集合
			TransactionSynchronizationManager.clearSynchronization();
			//如果没有事务或者是新事物
			if (!status.hasTransaction() || status.isNewTransaction()) {
				if (status.isDebug()) {
					logger.trace("Triggering afterCompletion synchronization");
				}
				// No transaction or new transaction for the current scope ->
				// invoke the afterCompletion callbacks immediately
				//当前作用域没有事务或新事务->立即调用afterCompletion回调
				invokeAfterCompletion(synchronizations, completionStatus);
			}
			else if (!synchronizations.isEmpty()) {
				// Existing transaction that we participate in, controlled outside
				// of the scope of this Spring transaction manager -> try to register
				// an afterCompletion callback with the existing (JTA) transaction.
				//否则表示有事务但不是新事务
				//我们参与的现有事务在此Spring事务管理器的范围之外进行了控制，
				//那么尝试向现有（JTA）事务注册一个 afterCompletion 回调
				registerAfterCompletionWithExistingTransaction(status.getTransaction(), synchronizations);
			}
		}
	}

	/**
	 * 实际调用给定Spring TransactionSynchronization对象的 afterCompletion方法
	 * <p> 由该抽象管理器本身调用，或由 registerAfterCompleteWithExistingTransaction 回调的特殊实现调用
	 * <p>
	 * Actually invoke the {@code afterCompletion} methods of the
	 * given Spring TransactionSynchronization objects.
	 * <p>To be called by this abstract manager itself, or by special implementations
	 * of the {@code registerAfterCompletionWithExistingTransaction} callback.
	 * @param synchronizations a List of TransactionSynchronization objects TransactionSynchronization对象的列表
	 * @param completionStatus the completion status according to the
	 * constants in the TransactionSynchronization interface  根据TransactionSynchronization接口中的常量的完成状态
	 * @see #registerAfterCompletionWithExistingTransaction(Object, java.util.List)
	 * @see TransactionSynchronization#STATUS_COMMITTED
	 * @see TransactionSynchronization#STATUS_ROLLED_BACK
	 * @see TransactionSynchronization#STATUS_UNKNOWN
	 */
	protected final void invokeAfterCompletion(List<TransactionSynchronization> synchronizations, int completionStatus) {
		TransactionSynchronizationUtils.invokeAfterCompletion(synchronizations, completionStatus);
	}

	/**
	 * 回滚后的处理工作，如果是新的事务同步状态的话，要把线程的同步状态清除了，
	 * 如果是新事务的话，进行数据清除,线程的私有资源解绑，重置连接自动提交，隔离级别，是否只读，释放连接等。
	 * 如果有挂起的事务，还要把这个事务给恢复，其实就是把属性设置回去
	 *
	 * Clean up after completion, clearing synchronization if necessary,
	 * and invoking doCleanupAfterCompletion.
	 * @param status object representing the transaction 表示事务的对象
	 * @see #doCleanupAfterCompletion
	 */
	private void cleanupAfterCompletion(DefaultTransactionStatus status) {
		// 设置完成状态。即 completed 属性为 ture
		status.setCompleted();
		//如果是新的事务同步状态的话，要把线程的同步状态清除了，
		if (status.isNewSynchronization()) {
			// 线程同步状态清除
			TransactionSynchronizationManager.clear();
		}
		// 如果是新事务的话，进行数据清除，线程的私有资源解绑，重置连接自动提交，隔离级别，是否只读，释放连接等
		if (status.isNewTransaction()) {
			doCleanupAfterCompletion(status.getTransaction());
		}
		// 如果存在此前已经被挂起的事务资源，那么这里需要恢复此前的资源
		if (status.getSuspendedResources() != null) {
			if (status.isDebug()) {
				logger.debug("Resuming suspended transaction after completion of inner transaction");
			}
			//获取当前的内部事务对象，比如对于DataSourceTransactionManager事务管理器
			//DataSourceTransactionManager创建的内部事务对象就是DataSourceTransactionObject
			Object transaction = (status.hasTransaction() ? status.getTransaction() : null);
			// 唤醒挂起的事务资源
			// 重新绑定之前挂起的数据库资源，重新唤醒并注册此前的同步器，重新绑定各种事务信息
			resume(transaction, (SuspendedResourcesHolder) status.getSuspendedResources());
		}
	}


	//---------------------------------------------------------------------
	// Template methods to be implemented in subclasses
	//---------------------------------------------------------------------

	/**
	 * 用于获取当前事务状态对应的事务对象。返回的事务对象通常包含具体的事务状态信息
	 * 该方法应该检查是否存在已经开启的事务，并将对应的状态信息存储在返回的事务对象中。
	 * Return a transaction object for the current transaction state.
	 * <p>The returned object will usually be specific to the concrete transaction
	 * manager implementation, carrying corresponding transaction state in a
	 * modifiable fashion. This object will be passed into the other template
	 * methods (e.g. doBegin and doCommit), either directly or as part of a
	 * DefaultTransactionStatus instance.
	 * <p>The returned object should contain information about any existing
	 * transaction, that is, a transaction that has already started before the
	 * current {@code getTransaction} call on the transaction manager.
	 * Consequently, a {@code doGetTransaction} implementation will usually
	 * look for an existing transaction and store corresponding state in the
	 * returned transaction object.
	 * @return the current transaction object
	 * @throws org.springframework.transaction.CannotCreateTransactionException
	 * if transaction support is not available
	 * @throws TransactionException in case of lookup or system errors
	 * @see #doBegin
	 * @see #doCommit
	 * @see #doRollback
	 * @see DefaultTransactionStatus#getTransaction
	 */
	protected abstract Object doGetTransaction() throws TransactionException;

	/**
	 * Check if the given transaction object indicates an existing transaction
	 * (that is, a transaction which has already started).
	 * <p>The result will be evaluated according to the specified propagation
	 * behavior for the new transaction. An existing transaction might get
	 * suspended (in case of PROPAGATION_REQUIRES_NEW), or the new transaction
	 * might participate in the existing one (in case of PROPAGATION_REQUIRED).
	 * <p>The default implementation returns {@code false}, assuming that
	 * participating in existing transactions is generally not supported.
	 * Subclasses are of course encouraged to provide such support.
	 * @param transaction the transaction object returned by doGetTransaction
	 * @return if there is an existing transaction
	 * @throws TransactionException in case of system errors
	 * @see #doGetTransaction
	 */
	protected boolean isExistingTransaction(Object transaction) throws TransactionException {
		return false;
	}

	/**
	 * Return whether to use a savepoint for a nested transaction.
	 * <p>Default is {@code true}, which causes delegation to DefaultTransactionStatus
	 * for creating and holding a savepoint. If the transaction object does not implement
	 * the SavepointManager interface, a NestedTransactionNotSupportedException will be
	 * thrown. Else, the SavepointManager will be asked to create a new savepoint to
	 * demarcate the start of the nested transaction.
	 * <p>Subclasses can override this to return {@code false}, causing a further
	 * call to {@code doBegin} - within the context of an already existing transaction.
	 * The {@code doBegin} implementation needs to handle this accordingly in such
	 * a scenario. This is appropriate for JTA, for example.
	 * @see DefaultTransactionStatus#createAndHoldSavepoint
	 * @see DefaultTransactionStatus#rollbackToHeldSavepoint
	 * @see DefaultTransactionStatus#releaseHeldSavepoint
	 * @see #doBegin
	 */
	protected boolean useSavepointForNestedTransaction() {
		return true;
	}

	/**
	 * Begin a new transaction with semantics according to the given transaction
	 * definition. Does not have to care about applying the propagation behavior,
	 * as this has already been handled by this abstract manager.
	 * <p>This method gets called when the transaction manager has decided to actually
	 * start a new transaction. Either there wasn't any transaction before, or the
	 * previous transaction has been suspended.
	 * <p>A special scenario is a nested transaction without savepoint: If
	 * {@code useSavepointForNestedTransaction()} returns "false", this method
	 * will be called to start a nested transaction when necessary. In such a context,
	 * there will be an active transaction: The implementation of this method has
	 * to detect this and start an appropriate nested transaction.
	 * @param transaction the transaction object returned by {@code doGetTransaction}
	 * @param definition a TransactionDefinition instance, describing propagation
	 * behavior, isolation level, read-only flag, timeout, and transaction name
	 * @throws TransactionException in case of creation or system errors
	 * @throws org.springframework.transaction.NestedTransactionNotSupportedException
	 * if the underlying transaction does not support nesting
	 */
	protected abstract void doBegin(Object transaction, TransactionDefinition definition)
			throws TransactionException;

	/**
	 * Suspend the resources of the current transaction.
	 * Transaction synchronization will already have been suspended.
	 * <p>The default implementation throws a TransactionSuspensionNotSupportedException,
	 * assuming that transaction suspension is generally not supported.
	 * @param transaction the transaction object returned by {@code doGetTransaction}
	 * @return an object that holds suspended resources
	 * (will be kept unexamined for passing it into doResume)
	 * @throws org.springframework.transaction.TransactionSuspensionNotSupportedException
	 * if suspending is not supported by the transaction manager implementation
	 * @throws TransactionException in case of system errors
	 * @see #doResume
	 */
	protected Object doSuspend(Object transaction) throws TransactionException {
		throw new TransactionSuspensionNotSupportedException(
				"Transaction manager [" + getClass().getName() + "] does not support transaction suspension");
	}

	/**
	 * Resume the resources of the current transaction.
	 * Transaction synchronization will be resumed afterwards.
	 * <p>The default implementation throws a TransactionSuspensionNotSupportedException,
	 * assuming that transaction suspension is generally not supported.
	 * @param transaction the transaction object returned by {@code doGetTransaction}
	 * @param suspendedResources the object that holds suspended resources,
	 * as returned by doSuspend
	 * @throws org.springframework.transaction.TransactionSuspensionNotSupportedException
	 * if resuming is not supported by the transaction manager implementation
	 * @throws TransactionException in case of system errors
	 * @see #doSuspend
	 */
	protected void doResume(@Nullable Object transaction, Object suspendedResources) throws TransactionException {
		throw new TransactionSuspensionNotSupportedException(
				"Transaction manager [" + getClass().getName() + "] does not support transaction suspension");
	}

	/**
	 * Return whether to call {@code doCommit} on a transaction that has been
	 * marked as rollback-only in a global fashion.
	 * <p>Does not apply if an application locally sets the transaction to rollback-only
	 * via the TransactionStatus, but only to the transaction itself being marked as
	 * rollback-only by the transaction coordinator.
	 * <p>Default is "false": Local transaction strategies usually don't hold the rollback-only
	 * marker in the transaction itself, therefore they can't handle rollback-only transactions
	 * as part of transaction commit. Hence, AbstractPlatformTransactionManager will trigger
	 * a rollback in that case, throwing an UnexpectedRollbackException afterwards.
	 * <p>Override this to return "true" if the concrete transaction manager expects a
	 * {@code doCommit} call even for a rollback-only transaction, allowing for
	 * special handling there. This will, for example, be the case for JTA, where
	 * {@code UserTransaction.commit} will check the read-only flag itself and
	 * throw a corresponding RollbackException, which might include the specific reason
	 * (such as a transaction timeout).
	 * <p>If this method returns "true" but the {@code doCommit} implementation does not
	 * throw an exception, this transaction manager will throw an UnexpectedRollbackException
	 * itself. This should not be the typical case; it is mainly checked to cover misbehaving
	 * JTA providers that silently roll back even when the rollback has not been requested
	 * by the calling code.
	 * @see #doCommit
	 * @see DefaultTransactionStatus#isGlobalRollbackOnly()
	 * @see DefaultTransactionStatus#isLocalRollbackOnly()
	 * @see org.springframework.transaction.TransactionStatus#setRollbackOnly()
	 * @see org.springframework.transaction.UnexpectedRollbackException
	 * @see javax.transaction.UserTransaction#commit()
	 * @see javax.transaction.RollbackException
	 */
	protected boolean shouldCommitOnGlobalRollbackOnly() {
		return false;
	}

	/**
	 * Make preparations for commit, to be performed before the
	 * {@code beforeCommit} synchronization callbacks occur.
	 * <p>Note that exceptions will get propagated to the commit caller
	 * and cause a rollback of the transaction.
	 * @param status the status representation of the transaction
	 * @throws RuntimeException in case of errors; will be <b>propagated to the caller</b>
	 * (note: do not throw TransactionException subclasses here!)
	 */
	protected void prepareForCommit(DefaultTransactionStatus status) {
	}

	/**
	 * Perform an actual commit of the given transaction.
	 * <p>An implementation does not need to check the "new transaction" flag
	 * or the rollback-only flag; this will already have been handled before.
	 * Usually, a straight commit will be performed on the transaction object
	 * contained in the passed-in status.
	 * @param status the status representation of the transaction
	 * @throws TransactionException in case of commit or system errors
	 * @see DefaultTransactionStatus#getTransaction
	 */
	protected abstract void doCommit(DefaultTransactionStatus status) throws TransactionException;

	/**
	 * Perform an actual rollback of the given transaction.
	 * <p>An implementation does not need to check the "new transaction" flag;
	 * this will already have been handled before. Usually, a straight rollback
	 * will be performed on the transaction object contained in the passed-in status.
	 * @param status the status representation of the transaction
	 * @throws TransactionException in case of system errors
	 * @see DefaultTransactionStatus#getTransaction
	 */
	protected abstract void doRollback(DefaultTransactionStatus status) throws TransactionException;

	/**
	 * Set the given transaction rollback-only. Only called on rollback
	 * if the current transaction participates in an existing one.
	 * <p>The default implementation throws an IllegalTransactionStateException,
	 * assuming that participating in existing transactions is generally not
	 * supported. Subclasses are of course encouraged to provide such support.
	 * @param status the status representation of the transaction
	 * @throws TransactionException in case of system errors
	 */
	protected void doSetRollbackOnly(DefaultTransactionStatus status) throws TransactionException {
		throw new IllegalTransactionStateException(
				"Participating in existing transactions is not supported - when 'isExistingTransaction' " +
				"returns true, appropriate 'doSetRollbackOnly' behavior must be provided");
	}

	/**
	 * 用于向现有的事务注册给定的事务同步列表。
	 * 在Spring事务管理器的控制结束时调用，但事务尚未完成。
	 * 默认实现立即调用afterCompletion方法，并传递“STATUS_UNKNOWN”作为参数。
	 * 如果无法确定外部事务的实际结果，这是最好的做法。
	 * <p>
	 * Register the given list of transaction synchronizations with the existing transaction.
	 * <p>Invoked when the control of the Spring transaction manager and thus all Spring
	 * transaction synchronizations end, without the transaction being completed yet. This
	 * is for example the case when participating in an existing JTA or EJB CMT transaction.
	 * <p>The default implementation simply invokes the {@code afterCompletion} methods
	 * immediately, passing in "STATUS_UNKNOWN". This is the best we can do if there's no
	 * chance to determine the actual outcome of the outer transaction.
	 * @param transaction the transaction object returned by {@code doGetTransaction} 此前的doGetTransaction方法返回的事务对象，也就是DataSourceTransactionObject
	 * @param synchronizations a List of TransactionSynchronization objects TransactionSynchronization对象的列表
	 * @throws TransactionException in case of system errors
	 * @see #invokeAfterCompletion(java.util.List, int)
	 * @see TransactionSynchronization#afterCompletion(int)
	 * @see TransactionSynchronization#STATUS_UNKNOWN
	 */
	protected void registerAfterCompletionWithExistingTransaction(
			Object transaction, List<TransactionSynchronization> synchronizations) throws TransactionException {

		logger.debug("Cannot register Spring after-completion synchronization with existing transaction - " +
				"processing Spring after-completion callbacks immediately, with outcome status 'unknown'");
		invokeAfterCompletion(synchronizations, TransactionSynchronization.STATUS_UNKNOWN);
	}

	/**
	 * Cleanup resources after transaction completion.
	 * <p>Called after {@code doCommit} and {@code doRollback} execution,
	 * on any outcome. The default implementation does nothing.
	 * <p>Should not throw any exceptions but just issue warnings on errors.
	 * @param transaction the transaction object returned by {@code doGetTransaction}
	 */
	protected void doCleanupAfterCompletion(Object transaction) {
	}


	//---------------------------------------------------------------------
	// Serialization support
	//---------------------------------------------------------------------

	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		// Rely on default serialization; just initialize state after deserialization.
		ois.defaultReadObject();

		// Initialize transient fields.
		this.logger = LogFactory.getLog(getClass());
	}


	/**
	 * Holder for suspended resources.
	 * Used internally by {@code suspend} and {@code resume}.
	 */
	protected static final class SuspendedResourcesHolder {

		// 连接持有器
		@Nullable
		private final Object suspendedResources;

		// 同步状态
		@Nullable
		private List<TransactionSynchronization> suspendedSynchronizations;

		// 方法名
		@Nullable
		private String name;

		// 是否只读
		private boolean readOnly;

		// 隔离级别，默认mysql是可重复读，oracle是读已提交
		@Nullable
		private Integer isolationLevel;

		// 事务是否激活
		private boolean wasActive;

		private SuspendedResourcesHolder(Object suspendedResources) {
			this.suspendedResources = suspendedResources;
		}

		private SuspendedResourcesHolder(
				@Nullable Object suspendedResources, List<TransactionSynchronization> suspendedSynchronizations,
				@Nullable String name, boolean readOnly, @Nullable Integer isolationLevel, boolean wasActive) {

			this.suspendedResources = suspendedResources;
			this.suspendedSynchronizations = suspendedSynchronizations;
			this.name = name;
			this.readOnly = readOnly;
			this.isolationLevel = isolationLevel;
			this.wasActive = wasActive;
		}
	}

}
