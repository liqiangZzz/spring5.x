/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.annotation;

import java.io.Serializable;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.lang.Nullable;
import org.springframework.transaction.interceptor.AbstractFallbackTransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

/**
 * Implementation of the
 * {@link org.springframework.transaction.interceptor.TransactionAttributeSource}
 * interface for working with transaction metadata in JDK 1.5+ annotation format.
 *
 * <p>This class reads Spring's JDK 1.5+ {@link Transactional} annotation and
 * exposes corresponding transaction attributes to Spring's transaction infrastructure.
 * Also supports JTA 1.2's {@link javax.transaction.Transactional} and EJB3's
 * {@link javax.ejb.TransactionAttribute} annotation (if present).
 * This class may also serve as base class for a custom TransactionAttributeSource,
 * or get customized through {@link TransactionAnnotationParser} strategies.
 *
 * @author Colin Sampaleanu
 * @author Juergen Hoeller
 * @since 1.2
 * @see Transactional
 * @see TransactionAnnotationParser
 * @see SpringTransactionAnnotationParser
 * @see Ejb3TransactionAnnotationParser
 * @see org.springframework.transaction.interceptor.TransactionInterceptor#setTransactionAttributeSource
 * @see org.springframework.transaction.interceptor.TransactionProxyFactoryBean#setTransactionAttributeSource
 */
@SuppressWarnings("serial")
public class AnnotationTransactionAttributeSource extends AbstractFallbackTransactionAttributeSource
		implements Serializable {
	/**
	 * 是否存在JTA 1.2的javax.transaction.Transactional事务注解
	 */
	private static final boolean jta12Present;

	/**
	 *  是否存在EJB3的javax.ejb.TransactionAttribute事务注解
	 */
	private static final boolean ejb3Present;

	/*
	 * 在静态块中判断是否存在其他外部框架的事务注解
	 * JTA 1.2的javax.transaction.Transactional事务注解
	 * EJB3的javax.ejb.TransactionAttribute事务注解
	 */
	static {
		ClassLoader classLoader = AnnotationTransactionAttributeSource.class.getClassLoader();
		jta12Present = ClassUtils.isPresent("javax.transaction.Transactional", classLoader);
		ejb3Present = ClassUtils.isPresent("javax.ejb.TransactionAttribute", classLoader);
	}

	/**
	 * 是否仅支持公共的方法应用事务
	 */
	private final boolean publicMethodsOnly;

	/**
	 * 事务注解的解析器
	 */
	private final Set<TransactionAnnotationParser> annotationParsers;


	/**
	 * 创建一个默认的AnnotationTransactionAttributeSource，除了Spring的org.springframework.transaction.annotation.Transactional注解之外
	 * 还支持JTA 1.2的javax.transaction.Transactional事务注解以及EJB3的javax.ejb.TransactionAttribute事务注解
	 * <p>
	 * 并且仅支持公共的方法应用于事务中
	 * Create a default AnnotationTransactionAttributeSource, supporting
	 * public methods that carry the {@code Transactional} annotation
	 * or the EJB3 {@link javax.ejb.TransactionAttribute} annotation.
	 */
	public AnnotationTransactionAttributeSource() {
		this(true);
	}

	/**
	 * 创建一个自定义的 AnnotationTransactionAttributeSource，
	 * 支持带有 Spring框架的@Transactional注解 、JTA1.2的 javax.transaction.Transactional注解、EJB3 的javax.ejb.TransactionAttribute 注解的公共方法。
	 * Create a custom AnnotationTransactionAttributeSource, supporting
	 * public methods that carry the {@code Transactional} annotation
	 * or the EJB3 {@link javax.ejb.TransactionAttribute} annotation.
	 * @param publicMethodsOnly whether to support public methods that carry
	 * the {@code Transactional} annotation only (typically for use
	 * with proxy-based AOP), or protected/private methods as well
	 * (typically used with AspectJ class weaving)  指是否仅处理带有事务注解的公共方法（true表示是，通常配合代理AOP使用；false表示也包括 protected/private方法，通常与AspectJ类织入一起使用）。
	 */
	public AnnotationTransactionAttributeSource(boolean publicMethodsOnly) {
		this.publicMethodsOnly = publicMethodsOnly;
		if (jta12Present || ejb3Present) {
			this.annotationParsers = new LinkedHashSet<>(4);
			// 用于解析Spring框架的@Transactional注解
			this.annotationParsers.add(new SpringTransactionAnnotationParser());
			//如果存在JTA1.2支持
			if (jta12Present) {
				//添加JtaTransactionAnnotationParser解析器。
				this.annotationParsers.add(new JtaTransactionAnnotationParser());
			}
			// 如果存在JTA1.2支持
			if (ejb3Present) {
				// 添加Ejb3TransactionAnnotationParser解析器
				this.annotationParsers.add(new Ejb3TransactionAnnotationParser());
			}
		}
		else {
			// 使用Collections.singleton初始化一个只包含SpringTransactionAnnotationParser的集合，意味着仅支持Spring的@Transactional注解解析。
			this.annotationParsers = Collections.singleton(new SpringTransactionAnnotationParser());
		}
	}

	/**
	 * Create a custom AnnotationTransactionAttributeSource.
	 * @param annotationParser the TransactionAnnotationParser to use
	 */
	public AnnotationTransactionAttributeSource(TransactionAnnotationParser annotationParser) {
		this.publicMethodsOnly = true;
		Assert.notNull(annotationParser, "TransactionAnnotationParser must not be null");
		this.annotationParsers = Collections.singleton(annotationParser);
	}

	/**
	 * Create a custom AnnotationTransactionAttributeSource.
	 * @param annotationParsers the TransactionAnnotationParsers to use
	 */
	public AnnotationTransactionAttributeSource(TransactionAnnotationParser... annotationParsers) {
		this.publicMethodsOnly = true;
		Assert.notEmpty(annotationParsers, "At least one TransactionAnnotationParser needs to be specified");
		this.annotationParsers = new LinkedHashSet<>(Arrays.asList(annotationParsers));
	}

	/**
	 * Create a custom AnnotationTransactionAttributeSource.
	 * @param annotationParsers the TransactionAnnotationParsers to use
	 */
	public AnnotationTransactionAttributeSource(Set<TransactionAnnotationParser> annotationParsers) {
		this.publicMethodsOnly = true;
		Assert.notEmpty(annotationParsers, "At least one TransactionAnnotationParser needs to be specified");
		this.annotationParsers = annotationParsers;
	}

	/**
	 * 判断传入的类对象是否为候选类
	 * @param targetClass  表示要检查的类对象。
	 * @return true 表示属于候选class，false 表示不会继续进行方法匹配
	 */
	@Override
	public boolean isCandidateClass(Class<?> targetClass) {
		//遍历注解解析器集合
		for (TransactionAnnotationParser parser : this.annotationParsers) {
			//判断是否可以解析目标类型
			if (parser.isCandidateClass(targetClass)) {
				//只要有一个解析器能够解析，就算合格
				return true;
			}
		}
		return false;
	}

	/**
	 * 查找Transactional注解的属性，而且会对父类和接口也寻找，
	 * @param clazz the class to retrieve the attribute for  要检索其属性的类
	 * @return
	 */
	@Override
	@Nullable
	protected TransactionAttribute findTransactionAttribute(Class<?> clazz) {
		return determineTransactionAttribute(clazz);
	}

	/**
	 * 检索的属性的方法,从给定的Method对象所代表的方法中寻找@Transactional注解，并解析出该注解定义的事务属性。
	 * @param method the method to retrieve the attribute for 检索的属性的方法
	 * @return
	 */
	@Override
	@Nullable
	protected TransactionAttribute findTransactionAttribute(Method method) {
		return determineTransactionAttribute(method);
	}

	/**
	 *
	 * 确定给定方法或类的事务属性
	 * <p>
	 * 此实现委托已配置的 TransactionAnnotationParser 将已知注释解析到Spring的元数据属性类中。
	 * 如果不是事务性的，则返回 null
	 * <p>
	 * 可以重写,以支持携带事务元数据的自定义注释
	 *
	 * Determine the transaction attribute for the given method or class.
	 * <p>This implementation delegates to configured
	 * {@link TransactionAnnotationParser TransactionAnnotationParsers}
	 * for parsing known annotations into Spring's metadata attribute class.
	 * Returns {@code null} if it's not transactional.
	 * <p>Can be overridden to support custom annotations that carry transaction metadata.
	 * @param element the annotated method or class 元素带注释的方法或类
	 * @return the configured transaction attribute, or {@code null} if none was found  返回配置的事务属性,或者 null（如果未找到）
	 */
	@Nullable
	protected TransactionAttribute determineTransactionAttribute(AnnotatedElement element) {
		// 遍历设置的注解解析器集合，委托内部的解析器解析
		for (TransactionAnnotationParser parser : this.annotationParsers) {
			// 通过注解解析器去解析我们的元素(方法或者类)上的注解
			TransactionAttribute attr = parser.parseTransactionAnnotation(element);
			//找到一个就返回，不会应用后续的解析器
			//什么意思呢，如果一个方法使用了多种事务注解，仍然会按照解析器的顺序解析，最终只有一个注解会生效
			//默认头部解析器就是SpringTransactionAnnotationParser，支持Spring的@Transactional注解
			if (attr != null) {
				return attr;
			}
		}
		return null;
	}

	/**
	 * By default, only public methods can be made transactional.
	 */
	@Override
	protected boolean allowPublicMethodsOnly() {
		return this.publicMethodsOnly;
	}


	@Override
	public boolean equals(@Nullable Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AnnotationTransactionAttributeSource)) {
			return false;
		}
		AnnotationTransactionAttributeSource otherTas = (AnnotationTransactionAttributeSource) other;
		return (this.annotationParsers.equals(otherTas.annotationParsers) &&
				this.publicMethodsOnly == otherTas.publicMethodsOnly);
	}

	@Override
	public int hashCode() {
		return this.annotationParsers.hashCode();
	}

}
