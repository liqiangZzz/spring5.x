/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.transaction.annotation;

import java.io.Serializable;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.transaction.interceptor.NoRollbackRuleAttribute;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;
import org.springframework.transaction.interceptor.RuleBasedTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;

/**
 * 解析Spring的 @Transactional注解 的策略实现。
 *
 * Strategy implementation for parsing Spring's {@link Transactional} annotation.
 *
 * @author Juergen Hoeller
 * @since 2.5
 */
@SuppressWarnings("serial")
public class SpringTransactionAnnotationParser implements TransactionAnnotationParser, Serializable {
	/**
	 * 确定给定的类是否是携带指定注解的候选者(在类型或者方法级别）
	 * @param targetClass 目标类型
	 * @return 是否是候选class
	 */
	@Override
	public boolean isCandidateClass(Class<?> targetClass) {
		// 如果任何一个注解的全路径名都不是以"java."开始，并且该Class全路径名以"start."开始或者Class的类型为Ordered.class，
		// 那么返回false，否则其他情况都返回true。
		return AnnotationUtils.isCandidateClass(targetClass, Transactional.class);
	}

	/**
	 * 解析方法或者类上的@Transactional注解并解析为一个TransactionAttribute返回
	 * @param element 方法或者类元数据
	 * @return
	 */
	@Override
	@Nullable
	public TransactionAttribute parseTransactionAnnotation(AnnotatedElement element) {
		// 查找方法或者类上的@Transactional注解，这里会递归的进行向上查找
		// 如果在当前方法/类上没找到，那么会继续在父类/接口的方法/类上查找，最终将使用找到的第一个注解数据
		// 这就是在接口方法或者接口上的事务注解也能生效的原因，但是它们的优先级更低
		AnnotationAttributes attributes = AnnotatedElementUtils.findMergedAnnotationAttributes(
				element, Transactional.class, false, false);
		if (attributes != null) {
			//解析注解属性，封装为一个TransactionAttribute并返回，实际类型为RuleBasedTransactionAttribute
			return parseTransactionAnnotation(attributes);
		}
		else {
			return null;
		}
	}

	public TransactionAttribute parseTransactionAnnotation(Transactional ann) {
		return parseTransactionAnnotation(AnnotationUtils.getAnnotationAttributes(ann, false, false));
	}

	/**
	 * 解析注解的各种属性，封装到一个RuleBasedTransactionAttribute对象中
	 * @param attributes
	 * @return
	 */
	protected TransactionAttribute parseTransactionAnnotation(AnnotationAttributes attributes) {

		// 创建一个基础规则的事务属性对象
		RuleBasedTransactionAttribute rbta = new RuleBasedTransactionAttribute();

		// 解析@Transactionl上的传播行为
		Propagation propagation = attributes.getEnum("propagation");
		rbta.setPropagationBehavior(propagation.value());
		// 解析@Transactionl上的隔离级别
		Isolation isolation = attributes.getEnum("isolation");
		rbta.setIsolationLevel(isolation.value());
		// 解析@Transactionl上的事务超时事件
		rbta.setTimeout(attributes.getNumber("timeout").intValue());
		// 解析readOnly 只读标志
		rbta.setReadOnly(attributes.getBoolean("readOnly"));
		// 解析@Transactionl上的事务管理器的名称,(默认是事务管理器的bean名称），使用setQualifier方法设置。
		rbta.setQualifier(attributes.getString("value"));

		//设置回滚规则
		//同样，回滚规则在前，不回滚规则在后，如果回滚和不回滚设置了相同的异常，那么抛出异常时将会回
		List<RollbackRuleAttribute> rollbackRules = new ArrayList<>();
		// 解析针对哪种异常回滚
		for (Class<?> rbRule : attributes.getClassArray("rollbackFor")) {
			rollbackRules.add(new RollbackRuleAttribute(rbRule));
		}
		// 对哪种异常进行回滚
		for (String rbRule : attributes.getStringArray("rollbackForClassName")) {
			rollbackRules.add(new RollbackRuleAttribute(rbRule));
		}
		// 对哪种异常不回滚
		for (Class<?> rbRule : attributes.getClassArray("noRollbackFor")) {
			rollbackRules.add(new NoRollbackRuleAttribute(rbRule));
		}
		// 对哪种类型不回滚
		for (String rbRule : attributes.getStringArray("noRollbackForClassName")) {
			rollbackRules.add(new NoRollbackRuleAttribute(rbRule));
		}
		rbta.setRollbackRules(rollbackRules);

		return rbta;
	}


	@Override
	public boolean equals(@Nullable Object other) {
		return (this == other || other instanceof SpringTransactionAnnotationParser);
	}

	@Override
	public int hashCode() {
		return SpringTransactionAnnotationParser.class.hashCode();
	}

}
