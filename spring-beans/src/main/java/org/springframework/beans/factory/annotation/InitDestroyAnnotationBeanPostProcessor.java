/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.annotation;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

/**
 * {@link org.springframework.beans.factory.config.BeanPostProcessor} implementation
 * that invokes annotated init and destroy methods. Allows for an annotation
 * alternative to Spring's {@link org.springframework.beans.factory.InitializingBean}
 * and {@link org.springframework.beans.factory.DisposableBean} callback interfaces.
 *
 * <p>The actual annotation types that this post-processor checks for can be
 * configured through the {@link #setInitAnnotationType "initAnnotationType"}
 * and {@link #setDestroyAnnotationType "destroyAnnotationType"} properties.
 * Any custom annotation can be used, since there are no required annotation
 * attributes.
 *
 * <p>Init and destroy annotations may be applied to methods of any visibility:
 * public, package-protected, protected, or private. Multiple such methods
 * may be annotated, but it is recommended to only annotate one single
 * init method and destroy method, respectively.
 *
 * <p>Spring's {link org.springframework.context.annotation.CommonAnnotationBeanPostProcessor}
 * supports the JSR-250 {@link javax.annotation.PostConstruct} and {@link javax.annotation.PreDestroy}
 * annotations out of the box, as init annotation and destroy annotation, respectively.
 * Furthermore, it also supports the {@link javax.annotation.Resource} annotation
 * for annotation-driven injection of named beans.
 *
 * @author Juergen Hoeller
 * @see #setInitAnnotationType
 * @see #setDestroyAnnotationType
 * @since 2.5
 */
@SuppressWarnings("serial")
public class InitDestroyAnnotationBeanPostProcessor
        implements DestructionAwareBeanPostProcessor, MergedBeanDefinitionPostProcessor, PriorityOrdered, Serializable {

    private final transient LifecycleMetadata emptyLifecycleMetadata =
            new LifecycleMetadata(Object.class, Collections.emptyList(), Collections.emptyList()) {
                @Override
                public void checkConfigMembers(RootBeanDefinition beanDefinition) {
                }

                @Override
                public void invokeInitMethods(Object target, String beanName) {
                }

                @Override
                public void invokeDestroyMethods(Object target, String beanName) {
                }

                @Override
                public boolean hasDestroyMethods() {
                    return false;
                }
            };


    protected transient Log logger = LogFactory.getLog(getClass());

    // CommonAnnotationBeanPostProcessor在初始化时向里面放置了@PostConstruct注解
    @Nullable
    private Class<? extends Annotation> initAnnotationType;

    // CommonAnnotationBeanPostProcessor在初始化时向里面放置了@PreDestory注解
    @Nullable
    private Class<? extends Annotation> destroyAnnotationType;

    /**
     * 排序优先级
     */
    private int order = Ordered.LOWEST_PRECEDENCE;

    // 生命周期的元数据缓存
    @Nullable
    private final transient Map<Class<?>, LifecycleMetadata> lifecycleMetadataCache = new ConcurrentHashMap<>(256);


    /**
     * Specify the init annotation to check for, indicating initialization
     * methods to call after configuration of a bean.
     * <p>Any custom annotation can be used, since there are no required
     * annotation attributes. There is no default, although a typical choice
     * is the JSR-250 {@link javax.annotation.PostConstruct} annotation.
     */
    public void setInitAnnotationType(Class<? extends Annotation> initAnnotationType) {
        this.initAnnotationType = initAnnotationType;
    }

    /**
     * Specify the destroy annotation to check for, indicating destruction
     * methods to call when the context is shutting down.
     * <p>Any custom annotation can be used, since there are no required
     * annotation attributes. There is no default, although a typical choice
     * is the JSR-250 {@link javax.annotation.PreDestroy} annotation.
     */
    public void setDestroyAnnotationType(Class<? extends Annotation> destroyAnnotationType) {
        this.destroyAnnotationType = destroyAnnotationType;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int getOrder() {
        return this.order;
    }


    /**
     * 处理@PostConstruct、@PreDestroy注解
     *
     * @param beanDefinition the merged bean definition for the bean  已合并的bean定义
     * @param beanType       the actual type of the managed bean instance   bean的类型
     * @param beanName       the name of the bean  beanName
     */
    @Override
    public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
        //获取生命周期相关的元数据对象，LifecycleMetadata是该类的内部类，内部持有当前的class以及对应的具有@PostConstruct、@PreDestroy注解的方法
        LifecycleMetadata metadata = findLifecycleMetadata(beanType);
        //检查配置信息
        metadata.checkConfigMembers(beanDefinition);
    }

    /**
     * 注册的生命周期元数据要开始调用,调用持有 @PostConstruct 注解的初始化方法
     *
     * @param bean     the new bean instance
     * @param beanName the name of the bean
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        LifecycleMetadata metadata = findLifecycleMetadata(bean.getClass());
        try {
            metadata.invokeInitMethods(bean, beanName);
        } catch (InvocationTargetException ex) {
            throw new BeanCreationException(beanName, "Invocation of init method failed", ex.getTargetException());
        } catch (Throwable ex) {
            throw new BeanCreationException(beanName, "Failed to invoke init method", ex);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
        LifecycleMetadata metadata = findLifecycleMetadata(bean.getClass());
        try {
            metadata.invokeDestroyMethods(bean, beanName);
        } catch (InvocationTargetException ex) {
            String msg = "Destroy method on bean with name '" + beanName + "' threw an exception";
            if (logger.isDebugEnabled()) {
                logger.warn(msg, ex.getTargetException());
            } else {
                logger.warn(msg + ": " + ex.getTargetException());
            }
        } catch (Throwable ex) {
            logger.warn("Failed to invoke destroy method on bean with name '" + beanName + "'", ex);
        }
    }

    /**
     * 判断给定实例所属的类中是否存在@PreDestroy注解的方法
     *
     * @param bean the bean instance to check  bean实例
     * @return  如果存在,返回true,否则返回false
     */
    @Override
    public boolean requiresDestruction(Object bean) {
        //调用findLifecycleMetadata查找回调元数据，随后调用hasDestroyMethods判断是否具有销毁回调方法
        return findLifecycleMetadata(bean.getClass()).hasDestroyMethods();
    }

    /**
     * 查找生命周期元数据。
     * <p>
     * 该方法用于查找给定类的生命周期元数据，包括PostConstruct和PreDestroy注解的方法。
     * 如果启用了缓存，则首先从缓存中查找，如果缓存中不存在，则构建新的元数据并放入缓存。
     * </p>
     *
     * @param clazz 要查找生命周期元数据的类。
     * @return 类的生命周期元数据，包括PostConstruct和PreDestroy注解的方法。
     */
    private LifecycleMetadata findLifecycleMetadata(Class<?> clazz) {
        // 检查是否启用了缓存。如果 lifecycleMetadataCache 为 null，表示未启用缓存
        if (this.lifecycleMetadataCache == null) {
            // Happens after deserialization, during destruction...
            // 未启用缓存，直接构建生命周期元数据。
            return buildLifecycleMetadata(clazz);
        }
        // Quick check on the concurrent map first, with minimal locking.
        // 从缓存中尝试获取生命周期元数据。
        LifecycleMetadata metadata = this.lifecycleMetadataCache.get(clazz);
        // 检查缓存中是否存在元数据。
        if (metadata == null) {
            // 缓存未命中，需要构建新的生命周期元数据。
            // 使用 synchronized 块保证线程安全，避免并发构建导致重复工作。
            synchronized (this.lifecycleMetadataCache) {
                // 再次检查缓存，防止在高并发情况下，其他线程已经构建了元数据并放入缓存。
                metadata = this.lifecycleMetadataCache.get(clazz);
                if (metadata == null) {
                    // 缓存仍然未命中，构建新的生命周期元数据。
                    metadata = buildLifecycleMetadata(clazz);
                    // 将构建的元数据放入缓存。
                    this.lifecycleMetadataCache.put(clazz, metadata);
                }
                // 返回从缓存中获取到的元数据。
                return metadata;
            }
        }
        // 缓存命中，直接返回缓存中的元数据。
        return metadata;
    }

    /**
     * 构建给定类的生命周期元数据。
     * <p>
     * 该方法用于扫描给定类及其父类，查找带有PostConstruct和PreDestroy注解的方法，并将它们封装成LifecycleElement对象，
     * 然后创建LifecycleMetadata对象返回。
     * </p>
     *
     * @param clazz 要构建生命周期元数据的类。
     * @return 类的生命周期元数据，包括PostConstruct和PreDestroy注解的方法，封装在LifecycleMetadata对象中。
     */
    private LifecycleMetadata buildLifecycleMetadata(final Class<?> clazz) {
        // 快速检查类是否包含任何生命周期注解（initAnnotationType 或 destroyAnnotationType）。
        //如果任何一个注解的全路径名都不是以"java."开始，并且该Class全路径名以"start."开始，或者Class的类型为Ordered.class，那么返回false，否则其他情况都返回true
        if (!AnnotationUtils.isCandidateClass(clazz, Arrays.asList(this.initAnnotationType, this.destroyAnnotationType))) {
            return this.emptyLifecycleMetadata;
        }

        // 实例化后的回调方法（@PostConstruct）
        List<LifecycleElement> initMethods = new ArrayList<>();
        // 销毁前的回调方法（@PreDestroy）
        List<LifecycleElement> destroyMethods = new ArrayList<>();
        // 获取正在处理的目标类
        Class<?> targetClass = clazz;

        /*
         *循环遍历该类及其父类，直到父类为Object
         *LifecycleElement表示了一个具有@PostConstruct、@PreDestroy等生命周期注解的方法
         */
        do {
            //当前的初始化回调方法集合
            final List<LifecycleElement> currInitMethods = new ArrayList<>();
            //当前的销毁回调方法集合
            final List<LifecycleElement> currDestroyMethods = new ArrayList<>();
            /*
             * 反射获取当前类中的所有方法并依次对其调用第二个参数的lambda表达式
             * 循环过滤所有的方法（不包括构造器），查找被初始化注解@PostConstruct和销毁注解@PreDestroy标注的方法
             * 这两个注解都是标注在方法上的，构造器上没有标注
             */
            ReflectionUtils.doWithLocalMethods(targetClass, method -> {
                //如果initAnnotationType不为null，并且存在该类型的注解（@PostConstruct）
                if (this.initAnnotationType != null && method.isAnnotationPresent(this.initAnnotationType)) {
                    //那么根据当前方法新建一个LifecycleElement，添加到currInitMethods中
                    LifecycleElement element = new LifecycleElement(method);
                    // 将创建好的元素添加到集合中
                    currInitMethods.add(element);
                    if (logger.isTraceEnabled()) {
                        logger.trace("Found init method on class [" + clazz.getName() + "]: " + method);
                    }
                }
                //如果initAnnotationType不为null，并且存在该类型的注解
                if (this.destroyAnnotationType != null && method.isAnnotationPresent(this.destroyAnnotationType)) {
                    //那么根据当前方法新建一个LifecycleElement，添加到currDestroyMethods中
                    currDestroyMethods.add(new LifecycleElement(method));
                    if (logger.isTraceEnabled()) {
                        logger.trace("Found destroy method on class [" + clazz.getName() + "]: " + method);
                    }
                }
            });

            //currInitMethods集合整体添加到initMethods集合的开头
            initMethods.addAll(0, currInitMethods);
            // 销毁方法父类晚于子类
            //currDestroyMethods集合整体添加到destroyMethods集合的开头
            destroyMethods.addAll(currDestroyMethods);
            //获取下一个目标类型，是当前类型的父类型
            targetClass = targetClass.getSuperclass();
        }
        // 如果当前类存在父类且父类不为object基类则循环对父类进行处理
        while (targetClass != null && targetClass != Object.class);

        //如果initMethods和destroyMethods都是空集合，那么返回一个空的LifecycleMetadata实例
        //否则返回一个新LifecycleMetadata，包含当前的class以及对应的找到的initMethods和destroyMethods
        return (initMethods.isEmpty() && destroyMethods.isEmpty() ? this.emptyLifecycleMetadata :
                new LifecycleMetadata(clazz, initMethods, destroyMethods));
    }


    //---------------------------------------------------------------------
    // Serialization support
    //---------------------------------------------------------------------

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        // Rely on default serialization; just initialize state after deserialization.
        ois.defaultReadObject();

        // Initialize transient fields.
        this.logger = LogFactory.getLog(getClass());
    }


    /**
     * Class representing information about annotated init and destroy methods.
     */
    private class LifecycleMetadata {

        /**
         * 目标类型
         */
        private final Class<?> targetClass;

        /**
         * 初始化回调方法集合
         */
        private final Collection<LifecycleElement> initMethods;

        /**
         * 销毁回调方法集合
         */
        private final Collection<LifecycleElement> destroyMethods;

        /**
         * 检查的初始化回调方法集合
         */
        @Nullable
        private volatile Set<LifecycleElement> checkedInitMethods;

        /**
         * 检查的销毁回调方法集合
         */
        @Nullable
        private volatile Set<LifecycleElement> checkedDestroyMethods;

        /**
         * LifecycleMetadata的构造器
         *
         * @param targetClass    目标类型
         * @param initMethods    初始化回调方法
         * @param destroyMethods 销毁回调方法
         */
        public LifecycleMetadata(Class<?> targetClass, Collection<LifecycleElement> initMethods,
                                 Collection<LifecycleElement> destroyMethods) {
            //为属性赋值
            this.targetClass = targetClass;
            this.initMethods = initMethods;
            this.destroyMethods = destroyMethods;
        }

        /**
         * 检查配置成员，并将未被外部管理的初始化和销毁方法注册到 Bean 定义中。
         * <p>
         * 该方法用于遍历 `initMethods` 和 `destroyMethods` 集合，检查其中的每个生命周期方法是否已经被外部管理。
         * 如果某个生命周期方法未被外部管理，则将其注册到 Bean 定义中，并记录日志。
         * </p>
         *
         * @param beanDefinition 要检查和注册生命周期方法的 Bean 定义。
         */
        public void checkConfigMembers(RootBeanDefinition beanDefinition) {
            // ---------------------- 处理初始化方法 ----------------------

            // 创建一个有序的集合，用于存储已检查的初始化方法。
            // 使用 LinkedHashSet 可以保证元素的添加顺序，并且不允许重复元素。
            Set<LifecycleElement> checkedInitMethods = new LinkedHashSet<>(this.initMethods.size());

            // 遍历所有的初始化方法
            for (LifecycleElement element : this.initMethods) {
                // 获取初始化方法的标识符（方法签名等唯一标识）。
                String methodIdentifier = element.getIdentifier();

                // 检查 Bean 定义是否已经外部管理了该初始化方法。
                // 如果返回 false，表示该方法尚未被外部管理。
                if (!beanDefinition.isExternallyManagedInitMethod(methodIdentifier)) {

                    // 将该初始化方法注册为外部管理的初始化方法。
                    // 这可以防止 Spring 在创建 Bean 实例时重复调用该方法。
                    beanDefinition.registerExternallyManagedInitMethod(methodIdentifier);
                    // 将该生命周期元素添加到已检查的集合中。
                    checkedInitMethods.add(element);

                    // 如果启用了 trace 级别的日志，则记录注册的初始化方法信息。
                    if (logger.isTraceEnabled()) {
                        logger.trace("Registered init method on class [" + this.targetClass.getName() + "]: " + element);
                    }
                }
            }

            // ---------------------- 处理销毁方法 ----------------------

            // 创建一个有序的集合，用于存储已检查的销毁方法。
            // 使用 LinkedHashSet 可以保证元素的添加顺序，并且不允许重复元素。
            Set<LifecycleElement> checkedDestroyMethods = new LinkedHashSet<>(this.destroyMethods.size());

            // 遍历所有的销毁方法。
            for (LifecycleElement element : this.destroyMethods) {
                // 获取销毁方法的标识符（方法签名等唯一标识）。
                String methodIdentifier = element.getIdentifier();

                // 检查 Bean 定义是否已经外部管理了该销毁方法。
                // 如果返回 false，表示该方法尚未被外部管理。
                if (!beanDefinition.isExternallyManagedDestroyMethod(methodIdentifier)) {
                    // 将该销毁方法注册为外部管理的销毁方法。
                    // 这可以防止 Spring 在销毁 Bean 实例时重复调用该方法。
                    beanDefinition.registerExternallyManagedDestroyMethod(methodIdentifier);
                    // 将该生命周期元素添加到已检查的集合中。
                    checkedDestroyMethods.add(element);

                    // 如果启用了 trace 级别的日志，则记录注册的销毁方法信息。
                    if (logger.isTraceEnabled()) {
                        logger.trace("Registered destroy method on class [" + this.targetClass.getName() + "]: " + element);
                    }
                }
            }

            // ---------------------- 更新成员变量 ----------------------

            // 将已检查的初始化方法集合赋值给成员变量 `checkedInitMethods`。
            this.checkedInitMethods = checkedInitMethods;
            // 将已检查的销毁方法集合赋值给成员变量 `checkedDestroyMethods`。
            this.checkedDestroyMethods = checkedDestroyMethods;
        }

        /**
         * 调用前面注册的初始化方法集合checkedInitMethods的每一个方法
         *
         * @param target
         * @param beanName
         * @throws Throwable
         */
        public void invokeInitMethods(Object target, String beanName) throws Throwable {
            Collection<LifecycleElement> checkedInitMethods = this.checkedInitMethods;
            Collection<LifecycleElement> initMethodsToIterate =
                    (checkedInitMethods != null ? checkedInitMethods : this.initMethods);
            if (!initMethodsToIterate.isEmpty()) {
                for (LifecycleElement element : initMethodsToIterate) {
                    if (logger.isTraceEnabled()) {
                        logger.trace("Invoking init method on bean '" + beanName + "': " + element.getMethod());
                    }
                    element.invoke(target);
                }
            }
        }

        public void invokeDestroyMethods(Object target, String beanName) throws Throwable {
            Collection<LifecycleElement> checkedDestroyMethods = this.checkedDestroyMethods;
            Collection<LifecycleElement> destroyMethodsToUse =
                    (checkedDestroyMethods != null ? checkedDestroyMethods : this.destroyMethods);
            if (!destroyMethodsToUse.isEmpty()) {
                for (LifecycleElement element : destroyMethodsToUse) {
                    if (logger.isTraceEnabled()) {
                        logger.trace("Invoking destroy method on bean '" + beanName + "': " + element.getMethod());
                    }
                    element.invoke(target);
                }
            }
        }

        /**
         * @return  是否具有@PreDestroy注解标注的销毁回调方法
         */
        public boolean hasDestroyMethods() {
            //判断这两个缓存中是否具有存在@PreDestroy注解的销毁回调方法
            Collection<LifecycleElement> checkedDestroyMethods = this.checkedDestroyMethods;
            Collection<LifecycleElement> destroyMethodsToUse =
                    (checkedDestroyMethods != null ? checkedDestroyMethods : this.destroyMethods);
            return !destroyMethodsToUse.isEmpty();
        }
    }


    /**
     * Class representing injection information about an annotated method.
     */
    private static class LifecycleElement {
        /**
         * 当前方法
         */
        private final Method method;

        /**
         * 标识符
         */
        private final String identifier;

        /**
         * 标识符
         */
        public LifecycleElement(Method method) {
            //如果方法参数不为0，那么抛出异常，从这里可知，初始化和销毁回调方法都不能有参数
            if (method.getParameterCount() != 0) {
                throw new IllegalStateException("Lifecycle method annotation requires a no-arg method: " + method);
            }
            this.method = method;
            //计算identifier
            //如果是私有方法，则标识符为：该方法的全路径名，如果是非私有方法，则标识符为该方法简单名字
            this.identifier = (Modifier.isPrivate(method.getModifiers()) ?
                    ClassUtils.getQualifiedMethodName(method) : method.getName());
        }

        public Method getMethod() {
            return this.method;
        }

        public String getIdentifier() {
            return this.identifier;
        }

        /**
         * 方法的反射调用
         *
         * @param target
         * @throws Throwable
         */
        public void invoke(Object target) throws Throwable {
            ReflectionUtils.makeAccessible(this.method);
            this.method.invoke(target, (Object[]) null);
        }

        @Override
        public boolean equals(@Nullable Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof LifecycleElement)) {
                return false;
            }
            LifecycleElement otherElement = (LifecycleElement) other;
            return (this.identifier.equals(otherElement.identifier));
        }

        @Override
        public int hashCode() {
            return this.identifier.hashCode();
        }
    }

}
