/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.support;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

import org.apache.commons.logging.Log;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyAccessorUtils;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.TypeConverter;
import org.springframework.beans.factory.Aware;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanCurrentlyInCreationException;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.UnsatisfiedDependencyException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.AutowiredPropertyMarker;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.core.NamedThreadLocal;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.ResolvableType;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.MethodCallback;
import org.springframework.util.StringUtils;

/**
 * 综合abstractBeanFactory并对接口AutowireCapableBeanFactory进行实现
 * <p>
 * Abstract bean factory superclass that implements default bean creation,
 * with the full capabilities specified by the {@link RootBeanDefinition} class.
 * Implements the {@link org.springframework.beans.factory.config.AutowireCapableBeanFactory}
 * interface in addition to AbstractBeanFactory's {@link #createBean} method.
 *
 * <p>Provides bean creation (with constructor resolution), property population,
 * wiring (including autowiring), and initialization. Handles runtime bean
 * references, resolves managed collections, calls initialization methods, etc.
 * Supports autowiring constructors, properties by name, and properties by type.
 *
 * <p>The main template method to be implemented by subclasses is
 * {@link #resolveDependency(DependencyDescriptor, String, Set, TypeConverter)},
 * used for autowiring by type. In case of a factory which is capable of searching
 * its bean definitions, matching beans will typically be implemented through such
 * a search. For other factory styles, simplified matching algorithms can be implemented.
 *
 * <p>Note that this class does <i>not</i> assume or implement bean definition
 * registry capabilities. See {@link DefaultListableBeanFactory} for an implementation
 * of the {@link org.springframework.beans.factory.ListableBeanFactory} and
 * {@link BeanDefinitionRegistry} interfaces, which represent the API and SPI
 * view of such a factory, respectively.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Rob Harrop
 * @author Mark Fisher
 * @author Costin Leau
 * @author Chris Beams
 * @author Sam Brannen
 * @author Phillip Webb
 * @see RootBeanDefinition
 * @see DefaultListableBeanFactory
 * @see BeanDefinitionRegistry
 * @since 13.02.2004
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory
        implements AutowireCapableBeanFactory {

    /**
     * bean的生成策略，默认是cglib
     * Strategy for creating bean instances.
     */
    private InstantiationStrategy instantiationStrategy = new CglibSubclassingInstantiationStrategy();

    /**
     * 解析策略的方法参数
     * Resolver strategy for method parameter names.
     */
    @Nullable
    private ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();

    /**
     * 尝试解析循环引用
     * Whether to automatically try to resolve circular references between beans.
     */
    private boolean allowCircularReferences = true;

    /**
     * 在循环引用的而情况下，是否需要注入一个原始的bean实例
     * <p>
     * Whether to resort to injecting a raw bean instance in case of circular reference,
     * even if the injected bean eventually got wrapped.
     */
    private boolean allowRawInjectionDespiteWrapping = false;

    /**
     * 依赖项检查和自动装配时忽略的依赖项类型
     * <p>
     * Dependency types to ignore on dependency check and autowire, as Set of
     * Class objects: for example, String. Default is none.
     */
    private final Set<Class<?>> ignoredDependencyTypes = new HashSet<>();

    /**
     * 依赖项检查和自动装配时忽略的依赖项接口
     * <p>
     * Dependency interfaces to ignore on dependency check and autowire, as Set of
     * Class objects. By default, only the BeanFactory interface is ignored.
     */
    private final Set<Class<?>> ignoredDependencyInterfaces = new HashSet<>();

    /**
     * 当前创建的 Bean 的名称，用于对 getBean 进行隐式依赖关系注册，以及从用户指定的 Supplier 回调触发的调用。
     * <p>
     * The name of the currently created bean, for implicit dependency registration
     * on getBean etc invocations triggered from a user-specified Supplier callback.
     */
    private final NamedThreadLocal<String> currentlyCreatedBean = new NamedThreadLocal<>("Currently created bean");

    /**
     * beanName和FactoryBean的映射
     * 缓存未完成的FactoryBean实例：FactoryBean名称到BeanWrapper
     * <p>
     * Cache of unfinished FactoryBean instances: FactoryBean name to BeanWrapper.
     */
    private final ConcurrentMap<String, BeanWrapper> factoryBeanInstanceCache = new ConcurrentHashMap<>();

    /**
     * 类和候选方法的映射
     * <p>
     * Cache of candidate factory methods per factory class.
     */
    private final ConcurrentMap<Class<?>, Method[]> factoryMethodCandidateCache = new ConcurrentHashMap<>();

    /**
     * 类和propertyDescriptor的映射
     * <p>
     * Cache of filtered PropertyDescriptors: bean Class to PropertyDescriptor array.
     */
    private final ConcurrentMap<Class<?>, PropertyDescriptor[]> filteredPropertyDescriptorsCache =
            new ConcurrentHashMap<>();


    /**
     * 构造方法，忽略BeanNameAware，BeanFactoryAware，BeanClassLoaderAware的依赖
     * <p>
     * Create a new AbstractAutowireCapableBeanFactory.
     */
    public AbstractAutowireCapableBeanFactory() {
        super();
        //忽略给定依赖接口的setter自动装配，主要是Aware接口
        //如果存在接口A，类Aimpl实现A，添加忽略A之后，Aimpl将忽略和A中相同的setter方法的自动注入
        ignoreDependencyInterface(BeanNameAware.class);
        ignoreDependencyInterface(BeanFactoryAware.class);
        ignoreDependencyInterface(BeanClassLoaderAware.class);
    }

    /**
     * Create a new AbstractAutowireCapableBeanFactory with the given parent.
     *
     * @param parentBeanFactory parent bean factory, or {@code null} if none
     */
    public AbstractAutowireCapableBeanFactory(@Nullable BeanFactory parentBeanFactory) {
        this();
        setParentBeanFactory(parentBeanFactory);
    }


    /**
     * 实例化生成策略的设置和获取
     * <p>
     * Set the instantiation strategy to use for creating bean instances.
     * Default is CglibSubclassingInstantiationStrategy.
     *
     * @see CglibSubclassingInstantiationStrategy
     */
    public void setInstantiationStrategy(InstantiationStrategy instantiationStrategy) {
        this.instantiationStrategy = instantiationStrategy;
    }

    /**
     * Return the instantiation strategy to use for creating bean instances.
     */
    protected InstantiationStrategy getInstantiationStrategy() {
        return this.instantiationStrategy;
    }

    /**
     * 解析策略的方法参数的设置和获取
     * <p>
     * Set the ParameterNameDiscoverer to use for resolving method parameter
     * names if needed (e.g. for constructor names).
     * <p>Default is a {@link DefaultParameterNameDiscoverer}.
     */
    public void setParameterNameDiscoverer(@Nullable ParameterNameDiscoverer parameterNameDiscoverer) {
        this.parameterNameDiscoverer = parameterNameDiscoverer;
    }

    /**
     * Return the ParameterNameDiscoverer to use for resolving method parameter
     * names if needed.
     */
    @Nullable
    protected ParameterNameDiscoverer getParameterNameDiscoverer() {
        return this.parameterNameDiscoverer;
    }

    /**
     * 尝试解决循环依赖的值设置
     * <p>
     * Set whether to allow circular references between beans - and automatically
     * try to resolve them.
     * <p>Note that circular reference resolution means that one of the involved beans
     * will receive a reference to another bean that is not fully initialized yet.
     * This can lead to subtle and not-so-subtle side effects on initialization;
     * it does work fine for many scenarios, though.
     * <p>Default is "true". Turn this off to throw an exception when encountering
     * a circular reference, disallowing them completely.
     * <p><b>NOTE:</b> It is generally recommended to not rely on circular references
     * between your beans. Refactor your application logic to have the two beans
     * involved delegate to a third bean that encapsulates their common logic.
     */
    public void setAllowCircularReferences(boolean allowCircularReferences) {
        this.allowCircularReferences = allowCircularReferences;
    }

    /**
     * 在循环引用的情况下，是否需要注入一个原视的bean实例
     * <p>
     * Set whether to allow the raw injection of a bean instance into some other
     * bean's property, despite the injected bean eventually getting wrapped
     * (for example, through AOP auto-proxying).
     * <p>This will only be used as a last resort in case of a circular reference
     * that cannot be resolved otherwise: essentially, preferring a raw instance
     * getting injected over a failure of the entire bean wiring process.
     * <p>Default is "false", as of Spring 2.0. Turn this on to allow for non-wrapped
     * raw beans injected into some of your references, which was Spring 1.2's
     * (arguably unclean) default behavior.
     * <p><b>NOTE:</b> It is generally recommended to not rely on circular references
     * between your beans, in particular with auto-proxying involved.
     *
     * @see #setAllowCircularReferences
     */
    public void setAllowRawInjectionDespiteWrapping(boolean allowRawInjectionDespiteWrapping) {
        this.allowRawInjectionDespiteWrapping = allowRawInjectionDespiteWrapping;
    }

    /**
     * 依赖项检查和自动装配时忽略的依赖项类型
     * <p>
     * Ignore the given dependency type for autowiring:
     * for example, String. Default is none.
     */
    public void ignoreDependencyType(Class<?> type) {
        this.ignoredDependencyTypes.add(type);
    }

    /**
     * 忽略给定依赖接口的setter自动装配，这通常被应用程序上下文用于注册。
     * <p>
     * Ignore the given dependency interface for autowiring.
     * <p>This will typically be used by application contexts to register
     * dependencies that are resolved in other ways, like BeanFactory through
     * BeanFactoryAware or ApplicationContext through ApplicationContextAware.
     * <p>By default, only the BeanFactoryAware interface is ignored.
     * For further types to ignore, invoke this method for each type.
     *
     * @see org.springframework.beans.factory.BeanFactoryAware
     * see org.springframework.context.ApplicationContextAware
     */
    public void ignoreDependencyInterface(Class<?> ifc) {
        this.ignoredDependencyInterfaces.add(ifc);
    }

    /**
     * 复制父类的几种配置
     *
     * @param otherFactory
     */
    @Override
    public void copyConfigurationFrom(ConfigurableBeanFactory otherFactory) {
        super.copyConfigurationFrom(otherFactory);
        if (otherFactory instanceof AbstractAutowireCapableBeanFactory) {
            AbstractAutowireCapableBeanFactory otherAutowireFactory =
                    (AbstractAutowireCapableBeanFactory) otherFactory;
            this.instantiationStrategy = otherAutowireFactory.instantiationStrategy;
            this.allowCircularReferences = otherAutowireFactory.allowCircularReferences;
            this.ignoredDependencyTypes.addAll(otherAutowireFactory.ignoredDependencyTypes);
            this.ignoredDependencyInterfaces.addAll(otherAutowireFactory.ignoredDependencyInterfaces);
        }
    }


    //-------------------------------------------------------------------------
    // Typical methods for creating and populating external bean instances
    //-------------------------------------------------------------------------

    /**
     * 创建bean
     *
     * @param beanClass the class of the bean to create
     * @param <T>
     * @return
     * @throws BeansException
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T createBean(Class<T> beanClass) throws BeansException {
        // Use prototype bean definition, to avoid registering bean as dependent bean.
        // 封装RootBeanDefinition
        RootBeanDefinition bd = new RootBeanDefinition(beanClass);
        // 设置bean的作用域
        bd.setScope(SCOPE_PROTOTYPE);
        // 是否允许被缓存
        bd.allowCaching = ClassUtils.isCacheSafe(beanClass, getBeanClassLoader());
        return (T) createBean(beanClass.getName(), bd, null);
    }

    @Override
    public void autowireBean(Object existingBean) {
        // Use non-singleton bean definition, to avoid registering bean as dependent bean.
        // 使用非单例的beanDefinition，防止注册bean为bean的依赖
        RootBeanDefinition bd = new RootBeanDefinition(ClassUtils.getUserClass(existingBean));
        // 设置作用域
        bd.setScope(SCOPE_PROTOTYPE);
        // 是否允许被缓存
        bd.allowCaching = ClassUtils.isCacheSafe(bd.getBeanClass(), getBeanClassLoader());
        BeanWrapper bw = new BeanWrapperImpl(existingBean);
        // 初始化beanWrapper
        initBeanWrapper(bw);
        // 给bean的属性赋值
        populateBean(bd.getBeanClass().getName(), bd, bw);
    }

    @Override
    public Object configureBean(Object existingBean, String beanName) throws BeansException {
        // 如果已经创建了bean,那么bean的定义要清除
        markBeanAsCreated(beanName);
        // 重新设置bean的定义
        BeanDefinition mbd = getMergedBeanDefinition(beanName);
        RootBeanDefinition bd = null;
        if (mbd instanceof RootBeanDefinition) {
            RootBeanDefinition rbd = (RootBeanDefinition) mbd;
            bd = (rbd.isPrototype() ? rbd : rbd.cloneBeanDefinition());
        }
        if (bd == null) {
            bd = new RootBeanDefinition(mbd);
        }
        if (!bd.isPrototype()) {
            bd.setScope(SCOPE_PROTOTYPE);
            bd.allowCaching = ClassUtils.isCacheSafe(ClassUtils.getUserClass(existingBean), getBeanClassLoader());
        }
        BeanWrapper bw = new BeanWrapperImpl(existingBean);
        // 初始化BeanWrapper
        initBeanWrapper(bw);
        // 给bean的属性赋值
        populateBean(beanName, bd, bw);
        // 调用init方法，完成初始化
        return initializeBean(beanName, existingBean, bd);
    }


    //-------------------------------------------------------------------------
    // Specialized methods for fine-grained control over the bean lifecycle
    //-------------------------------------------------------------------------

    @Override
    public Object createBean(Class<?> beanClass, int autowireMode, boolean dependencyCheck) throws BeansException {
        // Use non-singleton bean definition, to avoid registering bean as dependent bean.
        RootBeanDefinition bd = new RootBeanDefinition(beanClass, autowireMode, dependencyCheck);
        bd.setScope(SCOPE_PROTOTYPE);
        return createBean(beanClass.getName(), bd, null);
    }

    @Override
    public Object autowire(Class<?> beanClass, int autowireMode, boolean dependencyCheck) throws BeansException {
        // Use non-singleton bean definition, to avoid registering bean as dependent bean.
        RootBeanDefinition bd = new RootBeanDefinition(beanClass, autowireMode, dependencyCheck);
        bd.setScope(SCOPE_PROTOTYPE);
        // 如果是构造器注入
        if (bd.getResolvedAutowireMode() == AUTOWIRE_CONSTRUCTOR) {
            return autowireConstructor(beanClass.getName(), bd, null, null).getWrappedInstance();
        } else {
            Object bean;
            if (System.getSecurityManager() != null) {
                bean = AccessController.doPrivileged(
                        (PrivilegedAction<Object>) () -> getInstantiationStrategy().instantiate(bd, null, this),
                        getAccessControlContext());
            } else {
                // 生成bean
                bean = getInstantiationStrategy().instantiate(bd, null, this);
            }
            // 给bean的属性赋值
            populateBean(beanClass.getName(), bd, new BeanWrapperImpl(bean));
            return bean;
        }
    }

    @Override
    public void autowireBeanProperties(Object existingBean, int autowireMode, boolean dependencyCheck)
            throws BeansException {
        // 如果注入类型是构造器注入，直接抛出异常
        if (autowireMode == AUTOWIRE_CONSTRUCTOR) {
            throw new IllegalArgumentException("AUTOWIRE_CONSTRUCTOR not supported for existing bean instance");
        }
        // Use non-singleton bean definition, to avoid registering bean as dependent bean.
        RootBeanDefinition bd =
                new RootBeanDefinition(ClassUtils.getUserClass(existingBean), autowireMode, dependencyCheck);
        bd.setScope(SCOPE_PROTOTYPE);
        BeanWrapper bw = new BeanWrapperImpl(existingBean);
        initBeanWrapper(bw);
        populateBean(bd.getBeanClass().getName(), bd, bw);
    }

    @Override
    public void applyBeanPropertyValues(Object existingBean, String beanName) throws BeansException {
        markBeanAsCreated(beanName);
        BeanDefinition bd = getMergedBeanDefinition(beanName);
        BeanWrapper bw = new BeanWrapperImpl(existingBean);
        initBeanWrapper(bw);
        applyPropertyValues(beanName, bd, bw, bd.getPropertyValues());
    }

    @Override
    public Object initializeBean(Object existingBean, String beanName) {
        return initializeBean(beanName, existingBean, null);
    }

    /**
     * 初始化之前调用的方法
     * init-method调用之前回调所有BeanPostProcessor的postProcessBeforeInitialization方法
     * @param existingBean the existing bean instance  现有的 Bean 实例
     * @param beanName     the name of the bean, to be passed to it if necessary
     *                     (only passed to {@link BeanPostProcessor BeanPostProcessors};
     *                     can follow the {@link #ORIGINAL_INSTANCE_SUFFIX} convention in order to
     *                     enforce the given instance to be returned, i.e. no proxies etc)  Bean 的名称，必要时传递给它（仅传递给 BeanPostProcessor ;可以遵循 .ORIGINAL（源语言） 约定来强制返回给定的实例，即没有代理等）
     * @return 应用之后的返回值
     * @throws BeansException
     */
    @Override
    public Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName)
            throws BeansException {

        //初始化返回结果为existingBean
        Object result = existingBean;
        //遍历所有已注册的BeanPostProcessor，按照遍历顺序回调postProcessBeforeInitialization方法
        for (BeanPostProcessor processor : getBeanPostProcessors()) {
            // postProcessBeforeInitialization：在任何Bean初始化回调之前(如初始化Bean的afterPropertiesSet或自定义的init方法)
            // 将此BeanPostProcessor 应用到给定的新Bean实例。Bean已经填充了属性值。返回的Bean实例可能时原始Bean的包装器。
            // 默认实现按原样返回给定的 Bean
            Object current = processor.postProcessBeforeInitialization(result, beanName);
            //如果途中某个processor的postProcessBeforeInitialization方法返回null，那么不进行后续的回调
            //直接返回倒数第二个processor的postProcessBeforeInitialization方法的返回值
            if (current == null) {
                //直接返回result，中断其后续的BeanPostProcessor处理
                return result;
            }
            //让result引用processor的返回结果,使其经过所有BeanPostProcess对象的后置处理的层层包装
            result = current;
        }
        //返回经过所有BeanPostProcess对象的后置处理的层层包装后的result
        return result;
    }

    /**
     * initMethod调用之后应用所有已注册的BeanPostProcessor后处理器的postProcessAfterInitialization方法
     *
     * @param existingBean the existing bean instance  现有的 bean 实例
     * @param beanName     the name of the bean, to be passed to it if necessary
     *                     (only passed to {@link BeanPostProcessor BeanPostProcessors};
     *                     can follow the {@link #ORIGINAL_INSTANCE_SUFFIX} convention in order to
     *                     enforce the given instance to be returned, i.e. no proxies etc)  Bean 的名称
     * @return 要使用的 bean 实例，无论是原始实例还是包装实例
     * @throws BeansException
     */
    @Override
    public Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName)
            throws BeansException {

        //初始化结果对象为result，默认引用existingBean
        Object result = existingBean;
        //遍历所有已注册的BeanPostProcessor，按照遍历顺序回调postProcessAfterInitialization方法
        for (BeanPostProcessor processor : getBeanPostProcessors()) {
            //回调BeanPostProcessor#postProcessAfterInitialization来对现有的bean实例进行包装
            Object current = processor.postProcessAfterInitialization(result, beanName);
            //一般processor对不感兴趣的bean会回调直接返回result，使其能继续回调后续的BeanPostProcessor；
            // 但有些processor会返回null来中断其后续的BeanPostProcessor
            // 如果current为null
            if (current == null) {
                //直接返回result，中断其后续的BeanPostProcessor处理，返回上一个不为null的结果
                return result;
            }
            //让result引用processor的返回结果,使其经过所有BeanPostProcess对象的后置处理的层层包装
            result = current;
        }
        //返回经过所有BeanPostProcess对象的后置处理的层层包装后的result
        return result;
    }

    @Override
    public void destroyBean(Object existingBean) {
        new DisposableBeanAdapter(existingBean, getBeanPostProcessors(), getAccessControlContext()).destroy();
    }


    //-------------------------------------------------------------------------
    // Delegate methods for resolving injection points
    //-------------------------------------------------------------------------

    @Override
    public Object resolveBeanByName(String name, DependencyDescriptor descriptor) {
        InjectionPoint previousInjectionPoint = ConstructorResolver.setCurrentInjectionPoint(descriptor);
        try {
            // 获取bean
            return getBean(name, descriptor.getDependencyType());
        } finally {
            // 为目标工厂方法提供依赖描述符
            ConstructorResolver.setCurrentInjectionPoint(previousInjectionPoint);
        }
    }

    @Override
    @Nullable
    public Object resolveDependency(DependencyDescriptor descriptor, @Nullable String requestingBeanName) throws BeansException {
        return resolveDependency(descriptor, requestingBeanName, null, null);
    }


    //---------------------------------------------------------------------
    // Implementation of relevant AbstractBeanFactory template methods
    //---------------------------------------------------------------------

    /**
     * 使用给定合并的 bean 定义和参数创建、填充 bean 实例，并且应用后处理器等。支持自动注入
     * <p>
     * Central method of this class: creates a bean instance,
     * populates the bean instance, applies post-processors, etc.
     *
     * @param beanName beanName
     * @param mbd      已合并的bean定义
     * @param args     用于构造器或工厂方法调用的显式参数
     * @return 返回一个初始化完毕的对象
     * @see #doCreateBean
     */
    @Override
    protected Object createBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
            throws BeanCreationException {

        if (logger.isTraceEnabled()) {
            logger.trace("Creating instance of bean '" + beanName + "'");
        }
        RootBeanDefinition mbdToUse = mbd;

        // Make sure bean class is actually resolved at this point, and
        // clone the bean definition in case of a dynamically resolved Class
        // which cannot be stored in the shared merged bean definition.
        // 锁定class，根据设置的class属性或者根据className来解析class
        Class<?> resolvedClass = resolveBeanClass(mbd, beanName);
        //如果已解析的class不为null，并且beanClass属性不为null，并且beanClass属性类型不是Class类型，即存的是全路径各类名字符串
        if (resolvedClass != null && !mbd.hasBeanClass() && mbd.getBeanClassName() != null) {
            // 重新创建一个RootBeanDefinition对象
            mbdToUse = new RootBeanDefinition(mbd);
            // 设置BeanClass属性值
            mbdToUse.setBeanClass(resolvedClass);
        }

        // Prepare method overrides.
        //准备以及验证方法重写，也就是<lookup-method>、<replaced-method>这两个标签的配置，此前IoC学习的时候已经给介绍过了，一般用的不多
        //主要是校验指定名称的方法是否存在，如果不存在则抛出异常；以及设置是否存在重载方法，如果不存在重载方法，那么后续不需要参数校验。
        try {
            mbdToUse.prepareMethodOverrides();
        } catch (BeanDefinitionValidationException ex) {
            throw new BeanDefinitionStoreException(mbdToUse.getResourceDescription(),
                    beanName, "Validation of method overrides failed", ex);
        }

        try {
            // Give BeanPostProcessors a chance to return a proxy instead of the target bean instance.
            //Spring实例化bean之前的处理，给InstantiationAwareBeanPostProcessor后处理器一个返回代理对象来代替目标bean实例的机会
            //一般用不到，都是走Spring创建对象的逻辑
            Object bean = resolveBeforeInstantiation(beanName, mbdToUse);
            //如果bean不为null，那么就返回这个代理bean
            if (bean != null) {
                return bean;
            }
        } catch (Throwable ex) {
            throw new BeanCreationException(mbdToUse.getResourceDescription(), beanName,
                    "BeanPostProcessor before instantiation of bean failed", ex);
        }

        try {
            // 实际创建bean的调用
            Object beanInstance = doCreateBean(beanName, mbdToUse, args);
            if (logger.isTraceEnabled()) {
                logger.trace("Finished creating instance of bean '" + beanName + "'");
            }
            return beanInstance;
        } catch (BeanCreationException | ImplicitlyAppearedSingletonException ex) {
            // A previously detected exception with proper bean creation context already,
            // or illegal singleton state to be communicated up to DefaultSingletonBeanRegistry.
            throw ex;
        } catch (Throwable ex) {
            throw new BeanCreationException(
                    mbdToUse.getResourceDescription(), beanName, "Unexpected exception during bean creation", ex);
        }
    }

    /**
     * 在 Spring 中实际创建 Bean 的过程，强调了前置处理已经完成，例如检查 postProcessBeforeInstantiation 回调
     * 并区分了不同的实例化方式（默认实例化、工厂方法、自动装配构造函数）。
     *
     * Actually create the specified bean. Pre-creation processing has already happened
     * at this point, e.g. checking {@code postProcessBeforeInstantiation} callbacks.
     * <p>Differentiates between default bean instantiation, use of a
     * factory method, and autowiring a constructor.
     *
     * @param beanName the name of the bean  beanName
     * @param mbd      the merged bean definition for the bean   已合并bean定义
     * @param args     explicit arguments to use for constructor or factory method invocation  用于构造器或工厂方法调用的显式参数
     * @return a new instance of the bean  bean的新实例
     * @throws BeanCreationException if the bean could not be created
     * @see #instantiateBean
     * @see #instantiateUsingFactoryMethod
     * @see #autowireConstructor
     */
    protected Object doCreateBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
            throws BeanCreationException {

        /*
         * 实例化bean
         */
        // Instantiate the bean.
        // 这个beanWrapper是用来持有创建出来的bean对象的
        BeanWrapper instanceWrapper = null;
        //如果mbd是单例的
        if (mbd.isSingleton()) {
            //尝试直接从factoryBeanInstanceCache实例缓存中移除并获取正在创建中的FactoryBean的指定beanName的BeanWrapper
            instanceWrapper = this.factoryBeanInstanceCache.remove(beanName);
        }
        // 没有就创建实例
        if (instanceWrapper == null) {
            /*
             * 1 核心方法之一，使用适当的实例化策略创建Bean实例，并返回包装类BeanWrapper：
             *   1 根据生产者Supplier实例化bean
             *   2 根据工厂方法实例化bean
             *   3 如果可以自动注入，则使用构造器自动注入实例化bean
             *   4 不能自动注入，则使用默认无参构造器实例化bean
             *
             * 该方法执行完毕后，Bean 实例已经创建完成。如果存在构造器注入，相关的依赖已经在实例化过程中注入完成。
             * 接下来，Spring 会通过 populateBean 方法处理其他依赖注入（如 setter 方法、字段注入等）。
             * 依赖注入完成后，
             * Spring 会继续执行 Bean 的初始化逻辑（如调用 @PostConstruct 方法、InitializingBean 接口的 afterPropertiesSet 方法，以及自定义的 init-method）。
             */
            instanceWrapper = createBeanInstance(beanName, mbd, args);
        }
        // 从包装类中获取原始bean
        Object bean = instanceWrapper.getWrappedInstance();
        // 获取具体的bean对象的Class属性
        Class<?> beanType = instanceWrapper.getWrappedClass();
        // 如果不等于NullBean类型，那么修改目标类型
        if (beanType != NullBean.class) {
            mbd.resolvedTargetType = beanType;
        }

        // Allow post-processors to modify the merged bean definition.
        // 允许后处理器修改合并的 bean 定义
        synchronized (mbd.postProcessingLock) {
            //如果没有应用过MergedBeanDefinitionPostProcessor
            if (!mbd.postProcessed) {
                try {
                    /*
                     * 2 核心方法之二，应用MergedBeanDefinitionPostProcessor类型后处理器的postProcessMergedBeanDefinition方法，允许后处理器修改已合并的 bean 定义
                     * 最重要的是它被用来查找、解析各种注解，常见的MergedBeanDefinitionPostProcessor的实现以及对应postProcessMergedBeanDefinition方法的功能有:
                     *  1 InitDestroyAnnotationBeanPostProcessor -> 处理方法上的@PostConstruct、@PreDestroy注解，用于初始化、销毁方法回调
                     *  2 CommonAnnotationBeanPostProcessor -> 处理字段和方法上的@WebServiceRef、@EJB、@Resource注解，用于资源注入
                     *  3 AutowiredAnnotationBeanPostProcessor ->  -> 处理字段和方法上的@Autowired、@Value、@Inject注解，用于自动注入
                     *  4 ApplicationListenerDetector -> 记录ApplicationListener类型的bean是否是单例的，后面监听器过滤的时候才会用到(不必关心)
                     *
                     * 这一步仅仅是查找、简单解析Class中的各种注解，并缓存起来，后面时机到了，自然会调用这些被缓存的注解，完成它们的功能
                     */
                    applyMergedBeanDefinitionPostProcessors(mbd, beanType, beanName);
                } catch (Throwable ex) {
                    throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                            "Post-processing of merged bean definition failed", ex);
                }
                //标志位改为true
                mbd.postProcessed = true;
            }
        }

        /*
         * 3 解决setter方法和注解反射注入的单例bean循环依赖问题
         */

        // Eagerly cache singletons to be able to resolve circular references
        // even when triggered by lifecycle interfaces like BeanFactoryAware.
        //如果当前bean的"singleton"单例，并且允许循环依赖（默认允许），并且正在创建当前单例bean实例
        //那么earlySingletonExposure设置为ture，表示允许早期单例实例的暴露，用于解决循环依赖
        boolean earlySingletonExposure = (mbd.isSingleton() && this.allowCircularReferences &&
                isSingletonCurrentlyInCreation(beanName));
        //如果允许早期单例暴露
        if (earlySingletonExposure) {
            if (logger.isTraceEnabled()) {
                logger.trace("Eagerly caching bean '" + beanName +
                        "' to allow for resolving potential circular references");
            }
            /*
             *
             * 核心方法之三 addSingletonFactory：
             *  将早期单例实例工厂存入singletonFactories缓存中，用于解决循环依赖
             *  如果A依赖B、B依赖A，那么注入的时候，将会注入一个从singletonFactories中的ObjectFactory获取的早期对象
             *  为什么叫"早期"呢？
             *  因为此时该实例虽然被创建，但是可能还没有执行后续的依赖注入（setter方法、注解反射）的过程，该bean实例是不完整的
             *  但是此时能够保证获取的依赖不为null而不会抛出异常，并且和后续进行的注入对象是同一个对象，从而在不知不觉中解决循环依赖
             *
             *  以前的版本第二个参数传递的是一个ObjectFactory的匿名对象，Spring5以及Java8的之后第二个参数传递的是一个lambda对象
             *  lambda语法更加简单，采用invokedynamic指令运行时动态构建类，不会生成额外的class文件
             *
             *  这个lambda的意思就是 ObjectFactory对象的getObject方法实际上就是调用这里的getEarlyBeanReference方法
             *
             *  getEarlyBeanReference：
             *      应用SmartInstantiationAwareBeanPostProcessor后处理器的getEarlyBeanReference方法
             *      该方法可以改变要返回的提前暴露的单例bean引用对象，默认都是返回原值的，即不做改变，但是我们可以自己扩展。但是如果存在Spring AOP，则该方法可能会创建代理对象。
             */
            addSingletonFactory(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));

            //todo 修改源代码，模拟不使用三级缓存 ，解决循环依赖（非代理情况下）
            //只保留二级缓存，不向三级缓存中存放对象
//			earlySingletonObjects.put(beanName,bean);
//			registeredSingletons.add(beanName);

//			synchronized (this.singletonObjects) {
//				if (!this.singletonObjects.containsKey(beanName)) {
//					//实例化后的对象先添加到三级缓存中，三级缓存对应beanName的是一个lambda表达式(能够触发创建代理对象的机制)
//					this.singletonFactories.put(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));
//					this.registeredSingletons.add(beanName);
//				}
//			}
        }

        // Initialize the bean instance.
        /*
         * 4 初始化 bean 实例
         * 前面的createBeanInstance操作被称为"实例化"bean操作，简单的说就调用构造器创建对象
         * 下面的操作就是"初始化"bean的操作，简单的说就是对bean实例进行依赖注入以及各种回调
         */
        Object exposedObject = bean;
        try {
            /*
             * 核心方法之四 populateBean
             *
             * 填充(装配)bean实例，也就是setter方法和注解反射的属性注入过程
             * 这里又有可能由于依赖其他bean而导致其他bean的初始化
             */
            populateBean(beanName, mbd, instanceWrapper);
            /*
             * 核心方法之五 initializeBean
             * 继续初始化bean实例，也就是回调各种方法
             * 比如顺序回调所有设置的init初始化方法，以及回调Aware接口的方法，进行AOP代理等操作。
             */
            exposedObject = initializeBean(beanName, exposedObject, mbd);
        } catch (Throwable ex) {
            if (ex instanceof BeanCreationException && beanName.equals(((BeanCreationException) ex).getBeanName())) {
                throw (BeanCreationException) ex;
            } else {
                throw new BeanCreationException(
                        mbd.getResourceDescription(), beanName, "Initialization of bean failed", ex);
            }
        }

        /*
         * 5 循环依赖检查，看是否需要抛出异常
         */
        //如果允许暴露早期单例实例
        if (earlySingletonExposure) {
            /*
             * 获取当前单例实例缓存，allowEarlyReference参数为false，因此只会尝试从一级缓存（singletonObjects）和二级缓存（earlySingletonObjects）中查找 Bean 实例，没找到就返回null
             *
             * 如果不为null，说明当前 Bean 已经被提前暴露，存在循环依赖，此前另一个互相依赖的Bean通过getSingleton(beanName)获取当前bean实例时，
             * 获取的结果就是ObjectFactory#getObject的返回值（提前暴露的实例是通过 ObjectFactory#getObject 方法生成的），
             * 实际上就是getEarlyBeanReference方法返回的结果，
             * 因此，最后还会将结果存入earlySingletonObjects缓存，因此这里获取的实际上就是另一个Bean注入的实例，可能是一个代理对象，供其他 Bean 使用。
             */
            Object earlySingletonReference = getSingleton(beanName, false);
            /*
             * 如果earlySingletonReference不为null，说明存在循环依赖，并且此前已经调用了ObjectFactory#getObject方法，即getEarlyBeanReference方法。
             * getEarlyBeanReference方法中，会回调所有SmartInstantiationAwareBeanPostProcessor的getEarlyBeanReference方法
             * 因此将可能进行基于AbstractAutoProxyCreator的Spring AOP代理增强，比如自定义Spring AOP代理、Spring声明式事务等
             *
             * 这里获取的earlySingletonReference可能就是一个代理对象
             */
            if (earlySingletonReference != null) {
                /*
                 * 如果在经过populateBean和initializeBean方法之后返回的对象exposedObject还是等于原始对象bean，
                 * 即说明当前循环依赖的bean实例没有在populateBean和initializeBean这两个方法中调用的回调方法中被代理增强
                 * 或者只有基于AbstractAutoProxyCreator的Spring AOP代理增强，但是这在getEarlyBeanReference方法中已经被增强了
                 * Spring会保证基于AbstractAutoProxyCreator的增强只会进行一次，那么经过这两个填方法之后将仍然返回原始的bean
                 *
                 * 但是如果还有Spring @Async异步任务的AOP增强，由于它是通过AbstractAdvisingBeanPostProcessor来实现的，因此
                 * 经过这两个方法之后将返回一个新的代理的bean，这样exposedObject就和原来的bean不相等了
                 */
                if (exposedObject == bean) {
                    //那么exposedObject赋值为earlySingletonReference，对之前的循环引用没影响
                    //这里的earlySingletonReference可能就是被基于AbstractAutoProxyCreator代理增强的代理对象
                    exposedObject = earlySingletonReference;
                }
                //否则，说明该bean实例进行了其他的代理，比如Spring @Async异步任务
                //继续判断如果不允许注入原始bean实例，并且该beanName被其他bean依赖
                else if (!this.allowRawInjectionDespiteWrapping && hasDependentBean(beanName)) {
                    //获取依赖该bean的beanName数组
                    String[] dependentBeans = getDependentBeans(beanName);
                    Set<String> actualDependentBeans = new LinkedHashSet<>(dependentBeans.length);
                    for (String dependentBean : dependentBeans) {
                        // 返回false说明依赖还没实例化好
                        /*
                         * 尝试将dependentBean对应的仅用于类型检查的已创建实例移除，如果是真正的创建的实例则不会一会，最后还会抛出异常
                         *
                         * 如果alreadyCreated缓存集合中不包含对应的dependentBean，说明该bean实例还未被真正创建，但是可能因为类型检查而创建
                         * 那么尝试移除对象缓存中的beanName对应的缓存（无论是否真正的因为类型检查而创建了），并返回true
                         * 如果alreadyCreated中包含对应的dependentBean否则，说明该bean实例因为其他用途已被创建了或者正在创建（比如真正的初始化该bean实例），
                         * 那么返回false，表示依赖该bean的bean实例可能已被创建了，但是注入的对象可能并不是最终的代理对象
                         * 因此后续将会抛出BeanCurrentlyInCreationException异常
                         *
                         * 在前面的doGetBean方法中，如果不是为了类型检查，那么会将即将获取的实例beanName通过markBeanAsCreated方法存入alreadyCreated缓存集合中
                         */
                        if (!removeSingletonIfCreatedForTypeCheckOnly(dependentBean)) {
                            //移除失败的dependentBean加入actualDependentBeans集合中
                            actualDependentBeans.add(dependentBean);
                        }
                    }
                    // 因为bean创建后所依赖的bean一定是已经创建的
                    // actualDependentBeans不为空则表示当前bean创建后其依赖的bean却没有全部创建完，也就是说存在循环依赖
                    /*
                     * 如果actualDependentBeans集合不为空，那么表示可能有其他依赖该bean的实例注入的并不是目前最终的bean实例，那么将抛出异常。
                     *
                     * 实际上对于普通bean以及通用的AOP循环依赖注入以及事务循环依赖，Spring都可以帮我们解决循环依赖而不会抛出异常！
                     * 如果对@Async注解标注的类进行setter方法和反射字段注解的循环依赖注入（包括自己注入自己），就会抛出该异常。
                     * 而@Async类抛出异常的根本原因这个AOP代理对象不是使用通用的AbstractAutoProxyCreator的方法创建的，
                     * 而是使用AsyncAnnotationBeanPostProcessor后处理器来创建的，可以加一个@Lazy注解解决！
                     */
                    if (!actualDependentBeans.isEmpty()) {
                        throw new BeanCurrentlyInCreationException(beanName,
                                "Bean with name '" + beanName + "' has been injected into other beans [" +
                                        StringUtils.collectionToCommaDelimitedString(actualDependentBeans) +
                                        "] in its raw version as part of a circular reference, but has eventually been " +
                                        "wrapped. This means that said other beans do not use the final version of the " +
                                        "bean. This is often the result of over-eager type matching - consider using " +
                                        "'getBeanNamesForType' with the 'allowEagerInit' flag turned off, for example.");
                    }
                }
            }
        }

        /*将 bean 注册为 disposable。*/
        // Register bean as disposable.
        try {
            // 注册bean对象，方便后续在容器销毁的时候销毁对象
            /*
             * 6 尝试将当前bean实例注册为可销毁的bean，即存入disposableBeans缓存中，后续容器销毁时将会进行销毁回调
             * 注册销毁回调方法，可以来自于@PreDestroy注解、XML配置的destroy-method方法，以及DisposableBean接口，甚至AutoCloseable接口
             */
            registerDisposableBeanIfNecessary(beanName, bean, mbd);
        } catch (BeanDefinitionValidationException ex) {
            throw new BeanCreationException(
                    mbd.getResourceDescription(), beanName, "Invalid destruction signature", ex);
        }
        //返回exposedObject，该实例被创建完毕
        return exposedObject;
    }

    @Override
    @Nullable
    protected Class<?> predictBeanType(String beanName, RootBeanDefinition mbd, Class<?>... typesToMatch) {
        // 确定给定bean定义的目标类型
        Class<?> targetType = determineTargetType(beanName, mbd, typesToMatch);
        // Apply SmartInstantiationAwareBeanPostProcessors to predict the
        // eventual type after a before-instantiation shortcut.
        // 通过SmartInstantiationAwareBeanPostProcessor获取实际类型
        if (targetType != null && !mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
            boolean matchingOnlyFactoryBean = typesToMatch.length == 1 && typesToMatch[0] == FactoryBean.class;
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                if (bp instanceof SmartInstantiationAwareBeanPostProcessor) {
                    SmartInstantiationAwareBeanPostProcessor ibp = (SmartInstantiationAwareBeanPostProcessor) bp;
                    Class<?> predicted = ibp.predictBeanType(targetType, beanName);
                    if (predicted != null &&
                            (!matchingOnlyFactoryBean || FactoryBean.class.isAssignableFrom(predicted))) {
                        return predicted;
                    }
                }
            }
        }
        return targetType;
    }

    /**
     * Determine the target type for the given bean definition.
     *
     * @param beanName     the name of the bean (for error handling purposes)
     * @param mbd          the merged bean definition for the bean
     * @param typesToMatch the types to match in case of internal type matching purposes
     *                     (also signals that the returned {@code Class} will never be exposed to application code)
     * @return the type for the bean if determinable, or {@code null} otherwise
     */
    @Nullable
    protected Class<?> determineTargetType(String beanName, RootBeanDefinition mbd, Class<?>... typesToMatch) {
        Class<?> targetType = mbd.getTargetType();
        if (targetType == null) {
            targetType = (mbd.getFactoryMethodName() != null ?
                    getTypeForFactoryMethod(beanName, mbd, typesToMatch) :
                    resolveBeanClass(mbd, beanName, typesToMatch));
            if (ObjectUtils.isEmpty(typesToMatch) || getTempClassLoader() == null) {
                mbd.resolvedTargetType = targetType;
            }
        }
        return targetType;
    }

    /**
     * 工厂方法确定给定bean定义的目标类型。仅在尚未为目标bean注册单例实例时调用
     * <p>
     * Determine the target type for the given bean definition which is based on
     * a factory method. Only called if there is no singleton instance registered
     * for the target bean already.
     * <p>This implementation determines the type matching {@link #createBean}'s
     * different creation strategies. As far as possible, we'll perform static
     * type checking to avoid creation of the target bean.
     *
     * @param beanName     the name of the bean (for error handling purposes)
     * @param mbd          the merged bean definition for the bean
     * @param typesToMatch the types to match in case of internal type matching purposes
     *                     (also signals that the returned {@code Class} will never be exposed to application code)
     * @return the type for the bean if determinable, or {@code null} otherwise
     * @see #createBean
     */
    @Nullable
    protected Class<?> getTypeForFactoryMethod(String beanName, RootBeanDefinition mbd, Class<?>... typesToMatch) {
        // 尝试获取bean的合并bean定义中的缓存工厂方法返回类型
        ResolvableType cachedReturnType = mbd.factoryMethodReturnType;
        // 如果成功获取到了bean的合并bean定义中的缓存工厂方法返回类型
        if (cachedReturnType != null) {
            // ResolvableType.resolve:将ResolvableType对象解析为Class,如果无法解析，则返回null
            return cachedReturnType.resolve();
        }

        // 通用的返回类型，经过比较 AutowireUtils#resolveReturnTypeForFactoryMethod方法的返回结果
        // 和Method#getReturnType方法的返回结果所得到共同父类。
        Class<?> commonType = null;
        // 尝试获取bean的合并bean定义中的缓存用于自省的唯一工厂方法对象
        Method uniqueCandidate = mbd.factoryMethodToIntrospect;

        // 如果成功获取到了bean的合并bean定义中的缓存用于自省的唯一工厂方法对象
        if (uniqueCandidate == null) {
            Class<?> factoryClass;
            boolean isStatic = true;

            // 获取bean的合并bean定义的工厂bean名
            String factoryBeanName = mbd.getFactoryBeanName();
            // 如果成功获取到bean的合并bean定义的工厂bean名
            if (factoryBeanName != null) {
                // 如果工厂bean名与生成该bean的bean名相等
                if (factoryBeanName.equals(beanName)) {
                    throw new BeanDefinitionStoreException(mbd.getResourceDescription(), beanName,
                            "factory-bean reference points back to the same bean definition");
                }
                // Check declared factory method return type on factory class.
                // 检查工厂类上声明的工厂方法返回类型
                // 获取factoryBeanName对应的工厂类
                factoryClass = getType(factoryBeanName);
                isStatic = false;
            } else {
                // Check declared factory method return type on bean class.
                // 检查bean类上声明的工厂方法返回类型
                // 为mbd解析bean类，将bean类名解析为Class引用（如果需要）,并将解析后的Class存储在
                // mbd中以备将来使用。
                factoryClass = resolveBeanClass(mbd, beanName, typesToMatch);
            }
            // 如果mbd指定的工厂类获取失败
            if (factoryClass == null) {
                return null;
            }
            // 如果factoryClass是CGLIB生成的子类，则返回factoryClass的父类，否则直接返回factoryClass
            factoryClass = ClassUtils.getUserClass(factoryClass);

            // If all factory methods have the same return type, return that type.
            // Can't clearly figure out exact method due to type converting / autowiring!
            // 如果所有工厂方法都具有相同的返回类型，则返回该类型。
            // 由于类型转换/自动装配，无法明确找出确切的方法。
            // 如果mbd有配置构造函数参数值，就获取该构造函数参数值的数量，否则为0
            int minNrOfArgs =
                    (mbd.hasConstructorArgumentValues() ? mbd.getConstructorArgumentValues().getArgumentCount() : 0);
            // 在子类和所有超类上获取一组唯一的已声明方法，即被重写非协变返回类型的方法
            // 首先包含子类方法和然后遍历父类层次结构任何方法，将过滤出所有与已包含的方法匹配的签名方法。
            Method[] candidates = this.factoryMethodCandidateCache.computeIfAbsent(factoryClass,
                    clazz -> ReflectionUtils.getUniqueDeclaredMethods(clazz, ReflectionUtils.USER_DECLARED_METHODS));

            // 遍历候选方法
            for (Method candidate : candidates) {
                // 如果candidate是否静态的判断结果与isStatic一致且candidate有资格作为工厂方法且candidate的方法参数数量>=minNrOfArgs
                if (Modifier.isStatic(candidate.getModifiers()) == isStatic && mbd.isFactoryMethod(candidate) &&
                        candidate.getParameterCount() >= minNrOfArgs) {
                    // Declared type variables to inspect?
                    // 声明要检查的类型变量?
                    // 如果candidate的参数数量>0
                    if (candidate.getTypeParameters().length > 0) {
                        try {
                            // Fully resolve parameter names and argument values.
                            // 完全解析参数名称和参数值
                            // 获取candidate的参数类型数组
                            Class<?>[] paramTypes = candidate.getParameterTypes();
                            // 参数名数组
                            String[] paramNames = null;
                            // 获取参数名发现器
                            ParameterNameDiscoverer pnd = getParameterNameDiscoverer();
                            // 如果pnd不为null
                            if (pnd != null) {
                                // 使用pnd获取candidate的参数名
                                paramNames = pnd.getParameterNames(candidate);
                            }
                            // 获取mbd的构造函数参数值
                            ConstructorArgumentValues cav = mbd.getConstructorArgumentValues();
                            // HashSet:HashSet简单的理解就是HashSet对象中不能存储相同的数据，存储数据时是无序的。
                            // 但是HashSet存储元素的顺序并不是按照存入时的顺序（和List显然不同） 是按照哈希值来存的所以取数据也是按照哈希值取得。
                            // 定义一个存储构造函数参数值ValueHolder对象的HashSet
                            Set<ConstructorArgumentValues.ValueHolder> usedValueHolders = new HashSet<>(paramTypes.length);
                            // 定义一个用于存储参数值的数组
                            Object[] args = new Object[paramTypes.length];
                            // 遍历参数值
                            for (int i = 0; i < args.length; i++) {
                                // 获取第i个构造函数参数值ValueHolder对象
                                // 尽可能的提供位置，参数类型,参数名以最精准的方式获取获取第i个构造函数参数值ValueHolder对象，传入
                                // usedValueHolder来提示cav#getArgumentValue方法不应再次返回该usedValueHolder所出现的ValueHolder对象
                                // (如果有 多个类型的通用参数值，则允许返回下一个通用参数匹配项)
                                ConstructorArgumentValues.ValueHolder valueHolder = cav.getArgumentValue(
                                        i, paramTypes[i], (paramNames != null ? paramNames[i] : null), usedValueHolders);
                                // 如果valueHolder获取失败
                                if (valueHolder == null) {
                                    // 使用不匹配类型，不匹配参数名的方式获取除userValueHolders以外的下一个参数值valueHolder对象
                                    valueHolder = cav.getGenericArgumentValue(null, null, usedValueHolders);
                                }
                                // 如果valueHolder获取成功
                                if (valueHolder != null) {
                                    // 从valueHolder中获取值保存到第i个args元素中
                                    args[i] = valueHolder.getValue();
                                    // 将valueHolder添加到usedValueHolders缓存中，表示该valueHolder已经使用过
                                    usedValueHolders.add(valueHolder);
                                }
                            }
                            // 获取candidate的最终返回类型，该方法支持泛型情况下的目标类型获取
                            Class<?> returnType = AutowireUtils.resolveReturnTypeForFactoryMethod(
                                    candidate, args, getBeanClassLoader());
                            // 如果commonType为null且returnType等于candidate直接获取的返回类型，唯一候选方法就是candidate，否则为null
                            uniqueCandidate = (commonType == null && returnType == candidate.getReturnType() ?
                                    candidate : null);
                            // 获取returnType与commonType的共同父类，将该父类重新赋值给commonType
                            commonType = ClassUtils.determineCommonAncestor(returnType, commonType);
                            // 如果commonType为null
                            if (commonType == null) {
                                // Ambiguous return types found: return null to indicate "not determinable".
                                // 找到不明确的返回类型：返回null表示'不可确定'
                                return null;
                            }
                        }
                        // 捕捉获取commonType的所有异常
                        catch (Throwable ex) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Failed to resolve generic return type for factory method: " + ex);
                            }
                        }
                    }
                    // 如果candidate无需参数
                    else {
                        // 如果还没有找到commonType，candidate就为唯一的候选方法
                        uniqueCandidate = (commonType == null ? candidate : null);
                        // 获取candidate返回类型与commonType的共同父类，将该父类重新赋值给commonType
                        commonType = ClassUtils.determineCommonAncestor(candidate.getReturnType(), commonType);
                        // 如果commonType为null
                        if (commonType == null) {
                            // Ambiguous return types found: return null to indicate "not determinable".
                            // 找到不明确的返回类型：返回null表示'不可确定'
                            return null;
                        }
                    }
                }
            }

            // 缓存uniqueCandidate到mbd的factoryMethodToIntrospect
            mbd.factoryMethodToIntrospect = uniqueCandidate;
            // 如果commonType为null，加上这个判断能保证下面的步骤commonType肯定有值
            if (commonType == null) {
                // 找到不明确的返回类型：返回null表示'不可确定'
                return null;
            }
        }

        // Common return type found: all factory methods return same type. For a non-parameterized
        // unique candidate, cache the full type declaration context of the target factory method.
        // 找到常见的返回类型：所有工厂方法都返回相同的类型。对象非参数化的唯一候选者，缓存目标工厂方法的
        // 完整类型声明上下文
        // 如果获取到了uniqueCandidate就获取uniqueCandidate的返回类型，否则就用commonType作为返回类型
        cachedReturnType = (uniqueCandidate != null ?
                ResolvableType.forMethodReturnType(uniqueCandidate) : ResolvableType.forClass(commonType));
        // 缓存cachedReturnType到mdb的factoryMethodReturnType
        mbd.factoryMethodReturnType = cachedReturnType;
        // 返回cachedReturnType封装的Class对象
        return cachedReturnType.resolve();
    }

    /**
     * This implementation attempts to query the FactoryBean's generic parameter metadata
     * if present to determine the object type. If not present, i.e. the FactoryBean is
     * declared as a raw type, checks the FactoryBean's {@code getObjectType} method
     * on a plain instance of the FactoryBean, without bean properties applied yet.
     * If this doesn't return a type yet, and {@code allowInit} is {@code true} a
     * full creation of the FactoryBean is used as fallback (through delegation to the
     * superclass's implementation).
     * <p>The shortcut check for a FactoryBean is only applied in case of a singleton
     * FactoryBean. If the FactoryBean instance itself is not kept as singleton,
     * it will be fully created to check the type of its exposed object.
     */
    @Override
    protected ResolvableType getTypeForFactoryBean(String beanName, RootBeanDefinition mbd, boolean allowInit) {
        // Check if the bean definition itself has defined the type with an attribute
        ResolvableType result = getTypeForFactoryBeanFromAttributes(mbd);
        if (result != ResolvableType.NONE) {
            return result;
        }

        ResolvableType beanType =
                (mbd.hasBeanClass() ? ResolvableType.forClass(mbd.getBeanClass()) : ResolvableType.NONE);

        // For instance supplied beans try the target type and bean class
        if (mbd.getInstanceSupplier() != null) {
            result = getFactoryBeanGeneric(mbd.targetType);
            if (result.resolve() != null) {
                return result;
            }
            result = getFactoryBeanGeneric(beanType);
            if (result.resolve() != null) {
                return result;
            }
        }

        // Consider factory methods
        String factoryBeanName = mbd.getFactoryBeanName();
        String factoryMethodName = mbd.getFactoryMethodName();

        // Scan the factory bean methods
        if (factoryBeanName != null) {
            if (factoryMethodName != null) {
                // Try to obtain the FactoryBean's object type from its factory method
                // declaration without instantiating the containing bean at all.
                BeanDefinition factoryBeanDefinition = getBeanDefinition(factoryBeanName);
                Class<?> factoryBeanClass;
                if (factoryBeanDefinition instanceof AbstractBeanDefinition &&
                        ((AbstractBeanDefinition) factoryBeanDefinition).hasBeanClass()) {
                    factoryBeanClass = ((AbstractBeanDefinition) factoryBeanDefinition).getBeanClass();
                } else {
                    RootBeanDefinition fbmbd = getMergedBeanDefinition(factoryBeanName, factoryBeanDefinition);
                    factoryBeanClass = determineTargetType(factoryBeanName, fbmbd);
                }
                if (factoryBeanClass != null) {
                    result = getTypeForFactoryBeanFromMethod(factoryBeanClass, factoryMethodName);
                    if (result.resolve() != null) {
                        return result;
                    }
                }
            }
            // If not resolvable above and the referenced factory bean doesn't exist yet,
            // exit here - we don't want to force the creation of another bean just to
            // obtain a FactoryBean's object type...
            if (!isBeanEligibleForMetadataCaching(factoryBeanName)) {
                return ResolvableType.NONE;
            }
        }

        // If we're allowed, we can create the factory bean and call getObjectType() early
        if (allowInit) {
            FactoryBean<?> factoryBean = (mbd.isSingleton() ?
                    getSingletonFactoryBeanForTypeCheck(beanName, mbd) :
                    getNonSingletonFactoryBeanForTypeCheck(beanName, mbd));
            if (factoryBean != null) {
                // Try to obtain the FactoryBean's object type from this early stage of the instance.
                Class<?> type = getTypeForFactoryBean(factoryBean);
                if (type != null) {
                    return ResolvableType.forClass(type);
                }
                // No type found for shortcut FactoryBean instance:
                // fall back to full creation of the FactoryBean instance.
                return super.getTypeForFactoryBean(beanName, mbd, true);
            }
        }

        if (factoryBeanName == null && mbd.hasBeanClass() && factoryMethodName != null) {
            // No early bean instantiation possible: determine FactoryBean's type from
            // static factory method signature or from class inheritance hierarchy...
            return getTypeForFactoryBeanFromMethod(mbd.getBeanClass(), factoryMethodName);
        }
        result = getFactoryBeanGeneric(beanType);
        if (result.resolve() != null) {
            return result;
        }
        return ResolvableType.NONE;
    }

    private ResolvableType getFactoryBeanGeneric(@Nullable ResolvableType type) {
        if (type == null) {
            return ResolvableType.NONE;
        }
        return type.as(FactoryBean.class).getGeneric();
    }

    /**
     * Introspect the factory method signatures on the given bean class,
     * trying to find a common {@code FactoryBean} object type declared there.
     *
     * @param beanClass         the bean class to find the factory method on
     * @param factoryMethodName the name of the factory method
     * @return the common {@code FactoryBean} object type, or {@code null} if none
     */
    private ResolvableType getTypeForFactoryBeanFromMethod(Class<?> beanClass, String factoryMethodName) {
        // CGLIB subclass methods hide generic parameters; look at the original user class.
        Class<?> factoryBeanClass = ClassUtils.getUserClass(beanClass);
        FactoryBeanMethodTypeFinder finder = new FactoryBeanMethodTypeFinder(factoryMethodName);
        ReflectionUtils.doWithMethods(factoryBeanClass, finder, ReflectionUtils.USER_DECLARED_METHODS);
        return finder.getResult();
    }

    /**
     * This implementation attempts to query the FactoryBean's generic parameter metadata
     * if present to determine the object type. If not present, i.e. the FactoryBean is
     * declared as a raw type, checks the FactoryBean's {@code getObjectType} method
     * on a plain instance of the FactoryBean, without bean properties applied yet.
     * If this doesn't return a type yet, a full creation of the FactoryBean is
     * used as fallback (through delegation to the superclass's implementation).
     * <p>The shortcut check for a FactoryBean is only applied in case of a singleton
     * FactoryBean. If the FactoryBean instance itself is not kept as singleton,
     * it will be fully created to check the type of its exposed object.
     */
    @Override
    @Deprecated
    @Nullable
    protected Class<?> getTypeForFactoryBean(String beanName, RootBeanDefinition mbd) {
        return getTypeForFactoryBean(beanName, mbd, true).resolve();
    }

    /**
     * 应用SmartInstantiationAwareBeanPostProcessor后处理器的getEarlyBeanReference方法
     * 该方法可以改变要返回的提前暴露的单例bean引用对象
     * 获取用于早期访问的bean 的引用，通常用于解析循环引用，只有单例bean会调用该方法
     * <p>
     * Obtain a reference for early access to the specified bean,
     * typically for the purpose of resolving a circular reference.
     *
     * @param beanName the name of the bean (for error handling purposes)  Bean的名称（用于错误处理目的）
     * @param mbd      the merged bean definition for the bean   bean的合并bean定义
     * @param bean     the raw bean instance  原始bean实例
     * @return the object to expose as bean reference    要作为Bean引用公开的对象
     */
    protected Object getEarlyBeanReference(String beanName, RootBeanDefinition mbd, Object bean) {
        // 默认最终公开的对象是bean,通过createBeanInstance创建出来的普通对象
        Object exposedObject = bean;
        // mbd的 synthetic 属性：设置此bean定义是否是"synthetic"，一般是指只有AOP相关的pointCut配置或者Advice配置才会将 synthetic设置为true
        // 如果mdb不是synthetic且此工厂拥有InstantiationAwareBeanPostProcessor
        if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
            //获取、遍历全部注册的后处理器
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                //如果属于SmartInstantiationAwareBeanPostProcessor类型
                if (bp instanceof SmartInstantiationAwareBeanPostProcessor) {
                    //强转
                    SmartInstantiationAwareBeanPostProcessor ibp = (SmartInstantiationAwareBeanPostProcessor) bp;
                    //回调getEarlyBeanReference方法，传递的参数就是此前获取的bean实例以及beanName
                    //该方法可以改变要返回的提前暴露的单例bean引用对象，默认直接返回参数bean实例
                    exposedObject = ibp.getEarlyBeanReference(exposedObject, beanName);
                }
            }
        }
        // 返回最终经过层次包装后的对象
        return exposedObject;
    }


    //---------------------------------------------------------------------
    // Implementation methods
    //---------------------------------------------------------------------

    /**
     * Obtain a "shortcut" singleton FactoryBean instance to use for a
     * {@code getObjectType()} call, without full initialization of the FactoryBean.
     *
     * @param beanName the name of the bean
     * @param mbd      the bean definition for the bean
     * @return the FactoryBean instance, or {@code null} to indicate
     * that we couldn't obtain a shortcut FactoryBean instance
     */
    @Nullable
    private FactoryBean<?> getSingletonFactoryBeanForTypeCheck(String beanName, RootBeanDefinition mbd) {
        synchronized (getSingletonMutex()) {
            // 是否已经实例化
            BeanWrapper bw = this.factoryBeanInstanceCache.get(beanName);
            if (bw != null) {
                // 实例化直接返回
                return (FactoryBean<?>) bw.getWrappedInstance();
            }
            // factoryBeanInstanceCache没有，看看是否已经创建
            Object beanInstance = getSingleton(beanName, false);
            // 创建好的单例时FactoryBean，直接返回
            if (beanInstance instanceof FactoryBean) {
                return (FactoryBean<?>) beanInstance;
            }
            // 创建好的单例不是FactoryBean，或者已经创建或者正在创建，返回空
            if (isSingletonCurrentlyInCreation(beanName) ||
                    (mbd.getFactoryBeanName() != null && isSingletonCurrentlyInCreation(mbd.getFactoryBeanName()))) {
                return null;
            }

            Object instance;
            try {
                // Mark this bean as currently in creation, even if just partially.
                // 创建前检查
                beforeSingletonCreation(beanName);
                // Give BeanPostProcessors a chance to return a proxy instead of the target bean instance.
                // 看看代理能不能返回一个实例
                instance = resolveBeforeInstantiation(beanName, mbd);
                if (instance == null) {
                    // 代理没返回，就创建一个
                    bw = createBeanInstance(beanName, mbd, null);
                    instance = bw.getWrappedInstance();
                }
            } catch (UnsatisfiedDependencyException ex) {
                // Don't swallow, probably misconfiguration...
                throw ex;
            } catch (BeanCreationException ex) {
                // Instantiation failure, maybe too early...
                if (logger.isDebugEnabled()) {
                    logger.debug("Bean creation exception on singleton FactoryBean type check: " + ex);
                }
                onSuppressedException(ex);
                return null;
            } finally {
                // Finished partial creation of this bean.
                //  创建后检查
                afterSingletonCreation(beanName);
            }

            // 获取到的实例转换为FactoryBean
            FactoryBean<?> fb = getFactoryBean(beanName, instance);
            if (bw != null) {
                // 放入缓存
                this.factoryBeanInstanceCache.put(beanName, bw);
            }
            return fb;
        }
    }

    /**
     * Obtain a "shortcut" non-singleton FactoryBean instance to use for a
     * {@code getObjectType()} call, without full initialization of the FactoryBean.
     *
     * @param beanName the name of the bean
     * @param mbd      the bean definition for the bean
     * @return the FactoryBean instance, or {@code null} to indicate
     * that we couldn't obtain a shortcut FactoryBean instance
     */
    @Nullable
    private FactoryBean<?> getNonSingletonFactoryBeanForTypeCheck(String beanName, RootBeanDefinition mbd) {
        // 当前线程有在创建
        if (isPrototypeCurrentlyInCreation(beanName)) {
            return null;
        }

        Object instance;
        try {
            // Mark this bean as currently in creation, even if just partially.
            // 标记正在创建
            beforePrototypeCreation(beanName);
            // Give BeanPostProcessors a chance to return a proxy instead of the target bean instance.
            // 看看代理能不能返回一个实例
            instance = resolveBeforeInstantiation(beanName, mbd);
            if (instance == null) {
                BeanWrapper bw = createBeanInstance(beanName, mbd, null);
                instance = bw.getWrappedInstance();
            }
        } catch (UnsatisfiedDependencyException ex) {
            // Don't swallow, probably misconfiguration...
            throw ex;
        } catch (BeanCreationException ex) {
            // Instantiation failure, maybe too early...
            if (logger.isDebugEnabled()) {
                logger.debug("Bean creation exception on non-singleton FactoryBean type check: " + ex);
            }
            onSuppressedException(ex);
            return null;
        } finally {
            // Finished partial creation of this bean.
            // 标记创建结束
            afterPrototypeCreation(beanName);
        }

        // 直接缓缓为factoryBean
        return getFactoryBean(beanName, instance);
    }

    /**
     * 将 MergedBeanDefinitionPostProcessors 应用于指定的 bean 定义，调用其  postProcessMergedBeanDefinition 方法
     * <p>
     * Apply MergedBeanDefinitionPostProcessors to the specified bean definition,
     * invoking their {@code postProcessMergedBeanDefinition} methods.
     *
     * @param mbd      the merged bean definition for the bean  已合并的bean定义
     * @param beanType the actual type of the managed bean instance  bean的类型
     * @param beanName the name of the bean  beanName
     * @see MergedBeanDefinitionPostProcessor#postProcessMergedBeanDefinition
     */
    protected void applyMergedBeanDefinitionPostProcessors(RootBeanDefinition mbd, Class<?> beanType, String beanName) {
        //获取、遍历全部已注册的后处理器
        for (BeanPostProcessor bp : getBeanPostProcessors()) {
            //如果bp属于MergedBeanDefinitionPostProcessor
            if (bp instanceof MergedBeanDefinitionPostProcessor) {
                //进行类型强转
                MergedBeanDefinitionPostProcessor bdp = (MergedBeanDefinitionPostProcessor) bp;
                //回调postProcessMergedBeanDefinition方法，该方法可用于修改给定bean的已合并的RootBeanDefinition
                bdp.postProcessMergedBeanDefinition(mbd, beanType, beanName);
            }
        }
    }

    /**
     * 在Spring的bean实例化之前使用后处理器来实例化bean，这是一种快速实例化bean的方法
     * <p>
     * Apply before-instantiation post-processors, resolving whether there is a
     * before-instantiation shortcut for the specified bean.
     *
     * @param beanName the name of the bean  beanName
     * @param mbd      the bean definition for the bean  bean的定义
     * @return the shortcut-determined bean instance, or {@code null} if none  快捷方式实例化的bean实例，如果没有，则为 null
     */
    @Nullable
    protected Object resolveBeforeInstantiation(String beanName, RootBeanDefinition mbd) {
        Object bean = null;
        // 如果beforeInstantiationResolved值为null或者true，那么表示尚未被处理，进行后续的处理
        if (!Boolean.FALSE.equals(mbd.beforeInstantiationResolved)) {
            // Make sure bean class is actually resolved at this point.
            // 确保mbd不是合成的，只有在实现aop的时候synthetic的值才为true，并且具有InstantiationAwareBeanPostProcessor这个后处理器
            if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
                //获取这个bean的目标class
                Class<?> targetType = determineTargetType(beanName, mbd);
                if (targetType != null) {
                    //应用InstantiationAwareBeanPostProcessor后处理器的postProcessBeforeInstantiation方法
                    bean = applyBeanPostProcessorsBeforeInstantiation(targetType, beanName);
                    //如果bean不为null
                    if (bean != null) {
                        //应用所有已注册的后处理器的postProcessAfterInitialization方法
                        bean = applyBeanPostProcessorsAfterInitialization(bean, beanName);
                    }
                }
            }
            //如果bean不为空，则将RootBeanDefinition的beforeInstantiationResolved属性赋值为true，表示在Spring实例化之前已经解析
            mbd.beforeInstantiationResolved = (bean != null);
        }
        return bean;
    }

    /**
     * Spring实例化bean之前应用InstantiationAwareBeanPostProcessors后处理器的postProcessBeforeInstantiation方法
     * InstantiationAwareBeanPostProcessor类型的处理器处理，返回的是一个Object对象，
     * 也就是说此处可以做代理的事，如果发现有一个处理器返回的不是null，就直接返回了
     * <p>
     * Apply InstantiationAwareBeanPostProcessors to the specified bean definition
     * (by class and name), invoking their {@code postProcessBeforeInstantiation} methods.
     * <p>Any returned object will be used as the bean instead of actually instantiating
     * the target bean. A {@code null} return value from the post-processor will
     * result in the target bean being instantiated.
     *
     * @param beanClass the class of the bean to be instantiated  要实例化的bean的Class
     * @param beanName  the name of the bean  beanName
     * @return the bean object to use instead of a default instance of the target bean, or {@code null}   要使用的代理 bean 对象，或null
     * @see InstantiationAwareBeanPostProcessor#postProcessBeforeInstantiation
     */
    @Nullable
    protected Object applyBeanPostProcessorsBeforeInstantiation(Class<?> beanClass, String beanName) {
        for (BeanPostProcessor bp : getBeanPostProcessors()) {
            //找到类型为InstantiationAwareBeanPostProcessor的后处理器进行回调
            if (bp instanceof InstantiationAwareBeanPostProcessor) {
                InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                //回调
                Object result = ibp.postProcessBeforeInstantiation(beanClass, beanName);
                //如果结果不为null，表示创建了代理对象，那么直接返回该对象
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    /**
     *
     *  使用适当的实例化策略为指定的 bean 创建新实例
     *  采用的策略有：工厂方法实例化、自动注入带参数构造器实例化或默认无参构造器简单实例化。
     * Create a new instance for the specified bean, using an appropriate instantiation strategy:
     * factory method, constructor autowiring, or simple instantiation.
     *
     * @param beanName the name of the bean beanName
     * @param mbd      the bean definition for the bean bean定义
     * @param args     explicit arguments to use for constructor or factory method invocation 用于构造器或工厂方法调用的显式参数
     * @return a BeanWrapper for the new instance 一个新实例的 Beanwrapper对象
     * @see #obtainFromSupplier
     * @see #instantiateUsingFactoryMethod
     * @see #autowireConstructor
     * @see #instantiateBean
     */
    protected BeanWrapper createBeanInstance(String beanName, RootBeanDefinition mbd, @Nullable Object[] args) {
        // Make sure bean class is actually resolved at this point.
        /*
         * 根据bean定义中的属性解析class，如果此前已经解析过了那么直接返回beanClass属性指定的Class对象，否则解析className字符串为Class对象
         */
        Class<?> beanClass = resolveBeanClass(mbd, beanName);

        //如果beanClass不为null，并且该类的访问权限修饰符不是public的，并且不允许访问非公共构造器和方法(默认是允许的)
        if (beanClass != null && !Modifier.isPublic(beanClass.getModifiers()) && !mbd.isNonPublicAccessAllowed()) {
            throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                    "Bean class isn't public, and non-public access not allowed: " + beanClass.getName());
        }

        // 判断当前beanDefinition中是否包含实例供应器，此处相当于一个回调方法，利用回调方法来创建bean。这是Spring5以及Java8的新特性
        Supplier<?> instanceSupplier = mbd.getInstanceSupplier();
        /*
         * 1 如果存在bean实例生产者
         */
        if (instanceSupplier != null) {
            //那么从给定的生产者获取 bean 实例
            return obtainFromSupplier(instanceSupplier, beanName);
        }

        /*
         * 2 如果存在工厂方法，即XML的factory-method属性
         */
        if (mbd.getFactoryMethodName() != null) {
            //那么从工厂方法中获取 bean 实例
            return instantiateUsingFactoryMethod(beanName, mbd, args);
        }

        /*
         * 3 Spring会将此前已经解析、确定好的构造器缓存到 resolvedConstructorOrFactoryMethod 中
         * 后续再次创建对象时首先尝试从缓存中查找已经解析过的构造器或者工厂方法，避免再次创建相同bean时再次解析
         *
         * 对于原型（prototype）bean的创建来说，这非常的有用，可以快速的创建对象，称为Shortcut
         */
        // Shortcut when re-creating the same bean...
        //resolved表示是否已解析构造器或者工厂方法的标志，默认false
        boolean resolved = false;
        //autowireNecessary 表示是否构造器自动注入的标志，默认false
        boolean autowireNecessary = false;
        // 如果没有指定外部参数
        if (args == null) {
            synchronized (mbd.constructorArgumentLock) {
                //如果解析的构造器或工厂方法缓存不为null，表示已经解析过
                if (mbd.resolvedConstructorOrFactoryMethod != null) {
                    //resolved标志位设置为true
                    resolved = true;
                    //autowireNecessary标志位设置为constructorArgumentsResolved
                    //constructorArgumentsResolved属性表示是否已经解析了构造器参数，如果已经解析了，则需要通过缓存的构造器来实例化
                    autowireNecessary = mbd.constructorArgumentsResolved;
                }
            }
        }
        //如果已经解析过，则使用解析过的构造器进行初始化，设置的显示参数此时无效
        if (resolved) {
            //如果支持自动注入
            if (autowireNecessary) {
                // 构造函数自动注入
                return autowireConstructor(beanName, mbd, null, null);
            } else {
                // 使用默认构造函数构造
                return instantiateBean(beanName, mbd);
            }
        }

        /*
         * 4 一般bean第一次解析都是走下面的逻辑，使用构造器初始化
         */

        // Candidate constructors for autowiring?
        /*
         * 从bean后置处理器中为自动装配寻找构造方法, 有且仅有一个有参构造或者有且仅有@Autowired注解构造，因此需要开启注解支持
         * 利用SmartInstantiationAwareBeanPostProcessor后处理器回调，自动匹配、推测需要使用的候选构造器数组 ctors
         */
        Constructor<?>[] ctors = determineConstructorsFromBeanPostProcessors(beanClass, beanName);

        /*
         * 5 以下情况符合其一即可进入
         *  1、存在可选构造方法（如果ctors的候选构造器数组不为null（这是查找注解，比如@Autowired注解））
         *  2、自动装配模型为构造函数自动装配（自动注入模式为构造器自动注入（这是XML设置的自动注入模式））
         *  3、给BeanDefinition中设置了构造参数值 （XML定义了<constructor-arg/>标签）
         *  4、有参与构造函数参数列表的参数（外部设置的参数args不为空）
         * 四个条件满足一个，那么使用构造器自动注入
         */
        if (ctors != null || mbd.getResolvedAutowireMode() == AUTOWIRE_CONSTRUCTOR ||
                mbd.hasConstructorArgumentValues() || !ObjectUtils.isEmpty(args)) {
            //那么使用构造器自动注入
            return autowireConstructor(beanName, mbd, ctors, args);
        }

        // Preferred constructors for default construction?
        // 找出最合适的默认构造方法
        ctors = mbd.getPreferredConstructors();
        if (ctors != null) {
            // 构造函数自动注入
            return autowireConstructor(beanName, mbd, ctors, null);
        }

        // No special handling: simply use no-arg constructor.
        /*
         * 6 没有特殊处理，使用默认无参构造器初始化。
         * 使用默认无参构造函数创建对象，如果没有无参构造且存在多个有参构造且没有@AutoWired注解构造，会报错
         * 在大量使用注解的今天，一般都是通过无参构造器创建对象的，还能避免构造器的循环依赖
         * 当然无参构造器一般也是在上面的autowireConstructor方法中调用的
         */
        return instantiateBean(beanName, mbd);
    }

    /**
     *  从给定的生产者获取 bean 实例。
     * <p>
     * Obtain a bean instance from the given supplier.
     *
     * @param instanceSupplier the configured supplier 配置的bean实例生产者
     * @param beanName         the corresponding bean name 相应的beanName
     * @return a BeanWrapper for the new instance  一个新实例的 Beanwrapper对象
     * @see #getObjectForBeanInstance
     * @since 5.0
     */
    protected BeanWrapper obtainFromSupplier(Supplier<?> instanceSupplier, String beanName) {
        Object instance;

        //获取当前线程正在创建的 bean 的名称支持obtainFromSupplier方法加入的属性
        String outerBean = this.currentlyCreatedBean.get();
        //设置为给定的beanName，表示正在创建当前beanName的实例
        this.currentlyCreatedBean.set(beanName);
        try {
            // 调用supplier的方法获取实例
            instance = instanceSupplier.get();
        } finally {
            //如果outerBean不为null
            if (outerBean != null) {
                //那么还是设置为outerBean，表示正在创建outerBean的实例
                this.currentlyCreatedBean.set(outerBean);
            } else {
                this.currentlyCreatedBean.remove();
            }
        }

        //如果生产者返回null，那么返回NullBean包装bean
        if (instance == null) {
            instance = new NullBean();
        }
        // 初始化BeanWrapper并返回
        BeanWrapper bw = new BeanWrapperImpl(instance);
        //初始化BeanWrapper
        initBeanWrapper(bw);
        //返回BeanWrapper
        return bw;
    }

    /**
     * Overridden in order to implicitly register the currently created bean as
     * dependent on further beans getting programmatically retrieved during a
     * {@link Supplier} callback.
     *
     * @see #obtainFromSupplier
     * @since 5.0
     */
    @Override
    protected Object getObjectForBeanInstance(
            Object beanInstance, String name, String beanName, @Nullable RootBeanDefinition mbd) {

        // 注册当前创建的bean和给定的beanName的依赖
        String currentlyCreatedBean = this.currentlyCreatedBean.get();
        if (currentlyCreatedBean != null) {
            registerDependentBean(beanName, currentlyCreatedBean);
        }

        // 返回一个bean
        return super.getObjectForBeanInstance(beanInstance, name, beanName, mbd);
    }

    /**
     * 通过检查所有已注册的SmartInstantiationAwareBeanPostProcessor后处理器，确定要用于给定 bean 的候选构造器
     * 具体的检查逻辑是在SmartInstantiationAwareBeanPostProcessor后处理器的determineCandidateConstructors方法中
     * <p>
     * 注意这里是检查注解，比如@Autowired注解，因此需要开启注解支持，比如annotation-config或者component-scan
     * 该类型的后置处理器的实现有两个：ConfigurationClassPostProcessor$ImportAwareBeanPostProcessor，AutowiredAnnotationBeanPostProcessor
     * ConfigurationClassPostProcessor$ImportAwareBeanPostProcessor中什么也没干，因此具体的逻辑都在AutowiredAnnotationBeanPostProcessor中
     * <p>
     * 所以说这个后置处理器的determineCandidateConstructors方法执行时机是在: 对象实例化之前执行
     * <p>
     * Determine candidate constructors to use for the given bean, checking all registered
     * {@link SmartInstantiationAwareBeanPostProcessor SmartInstantiationAwareBeanPostProcessors}.
     *
     * @param beanClass the raw class of the bean  beanClass
     * @param beanName  the name of the bean  beanName
     * @return the candidate constructors, or {@code null} if none specified  候选构造器数组，如果没有指定则为null
     * @throws org.springframework.beans.BeansException in case of errors
     * @see org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor#determineCandidateConstructors
     */
    @Nullable
    protected Constructor<?>[] determineConstructorsFromBeanPostProcessors(@Nullable Class<?> beanClass, String beanName)
            throws BeansException {

        //如果beanClass不为null，并且具有InstantiationAwareBeanPostProcessor这个类型的后处理器
        //SmartInstantiationAwareBeanPostProcessor继承了InstantiationAwareBeanPostProcessor
        if (beanClass != null && hasInstantiationAwareBeanPostProcessors()) {
            //遍历所有注册的BeanPostProcessor后处理器
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                //如果属于SmartInstantiationAwareBeanPostProcessor类型
                if (bp instanceof SmartInstantiationAwareBeanPostProcessor) {
                    // 从SmartInstantiationAwareBeanPostProcessor判断
                    SmartInstantiationAwareBeanPostProcessor ibp = (SmartInstantiationAwareBeanPostProcessor) bp;
                    //回调determineCandidateConstructors方法，返回候选构造器数组
                    //比如使用@Autowired注解标注的构造器都被当作候选构造器，加入到数组中，留待后续继续筛选
                    Constructor<?>[] ctors = ibp.determineCandidateConstructors(beanClass, beanName);
                    //如果返回值不为null，那么直接返回结果，表明解析到了候选构造器
                    if (ctors != null) {
                        return ctors;
                    }
                }
            }
        }
        return null;
    }

    /**
     * 使用其默认无参构造器实例化给定 bean
     *
     * Instantiate the given bean using its default constructor.
     *
     * @param beanName the name of the bean  beanName
     * @param mbd      the bean definition for the bean    bean的BeanDefinition
     * @return a BeanWrapper for the new instance  新实例的 Beanwrapper
     */
    protected BeanWrapper instantiateBean(String beanName, RootBeanDefinition mbd) {
        try {
            Object beanInstance;
            //如果存在安全管理器，一般不存在
            if (System.getSecurityManager() != null) {
                beanInstance = AccessController.doPrivileged(
                        (PrivilegedAction<Object>) () -> getInstantiationStrategy().instantiate(mbd, beanName, this),
                        getAccessControlContext());
            } else {
                //通用逻辑
                //getInstantiationStrategy，返回用于创建 bean 实例的实例化策略，就是instantiationStrategy属性
                //默认是CglibSubclassingInstantiationStrategy类型的实例，实现了SimpleInstantiationStrategy
                beanInstance = getInstantiationStrategy().instantiate(mbd, beanName, this);
            }
            //新建BeanWrapperImpl，设置到内部属性中
            BeanWrapper bw = new BeanWrapperImpl(beanInstance);
            //初始化BeanWrapper，此前讲过了
            //主要是为当前的BeanWrapperImpl实例设置转换服务ConversionService以及注册自定义的属性编辑器PropertyEditor。
            initBeanWrapper(bw);
            return bw;
        } catch (Throwable ex) {
            //抛出异常
            throw new BeanCreationException(
                    mbd.getResourceDescription(), beanName, "Instantiation of bean failed", ex);
        }
    }

    /**
     * 通过工厂方法实例化，先获取构造器解析器，然后用工厂方法进行实例化
     * <p>
     * Instantiate the bean using a named factory method. The method may be static, if the
     * mbd parameter specifies a class, rather than a factoryBean, or an instance variable
     * on a factory object itself configured using Dependency Injection.
     *
     * @param beanName     the name of the bean
     * @param mbd          the bean definition for the bean
     * @param explicitArgs argument values passed in programmatically via the getBean method,
     *                     or {@code null} if none (-> use constructor argument values from bean definition)
     * @return a BeanWrapper for the new instance
     * @see #getBean(String, Object[])
     */
    protected BeanWrapper instantiateUsingFactoryMethod(
            String beanName, RootBeanDefinition mbd, @Nullable Object[] explicitArgs) {
        //  创建构造器处理器并使用 factoryMethod 进行实例化操作
        return new ConstructorResolver(this).instantiateUsingFactoryMethod(beanName, mbd, explicitArgs);
    }

    /**
     * 选择合适的构造器实例化bean，并且进行构造器自动注入，返回被包装后的bean实例——BeanWrapper对象。
     * <p>
     * "autowire constructor" (with constructor arguments by type) behavior.
     * Also applied if explicit constructor argument values are specified,
     * matching all remaining arguments with beans from the bean factory.
     * <p>This corresponds to constructor injection: In this mode, a Spring
     * bean factory is able to host components that expect constructor-based
     * dependency resolution.
     *
     * @param beanName     the name of the bean    beanName
     * @param mbd          the bean definition for the bean  Bean 的 Bean 定义
     * @param ctors        the chosen candidate constructors  候选构造器数组，可能为null
     * @param explicitArgs argument values passed in programmatically via the getBean method,
     *                     or {@code null} if none (-> use constructor argument values from bean definition)  外部传递进来的构造器所需参数，如果没有传递，则为null
     * @return a BeanWrapper for the new instance   新实例的 BeanWrapper 对象
     */
    protected BeanWrapper autowireConstructor(
            String beanName, RootBeanDefinition mbd, @Nullable Constructor<?>[] ctors, @Nullable Object[] explicitArgs) {
        // ConstructorResolver 是 Spring 中用于解析构造器的工具类
        // 它负责处理构造器选择和参数注入的逻辑
        return new ConstructorResolver(this).autowireConstructor(beanName, mbd, ctors, explicitArgs);
    }

    /**
     * 用来自 BeanDefinition的属性值填充给定的BeanWrapper中的bean实例
     * 简单来说就是 setter 方法和注解反射的方式的依赖注入，有可能由于依赖其他bean而导致其他bean的初始化
     * <p>
     * Populate the bean instance in the given BeanWrapper with the property values
     * from the bean definition.
     *
     * @param beanName the name of the bean  beanName
     * @param mbd      the bean definition for the bean   bean的Bean 定义
     * @param bw       the BeanWrapper with bean instance   具有 Bean 实例的BeanWrapper对象
     */
    @SuppressWarnings("deprecation")  // for postProcessPropertyValues
    protected void populateBean(String beanName, RootBeanDefinition mbd, @Nullable BeanWrapper bw) {
        /*
         * 1 校验bw为null的情况
         * 如果此bean定义中定义了<property>标签，那么抛出异常，其他情况则直接返回
         */

        // 如果beanWrapper为空
        if (bw == null) {
            //如果mbd存在propertyValues属性，即定义了<property>标签
            //因为BeanWrapper都为null了，不能进行依赖注入，那么抛出异常
            if (mbd.hasPropertyValues()) {
                // 抛出bean创建异常
                throw new BeanCreationException(
                        mbd.getResourceDescription(), beanName, "Cannot apply property values to null instance");
            } else {
                // Skip property population phase for null instance.
                // 没有可填充的属性，直接跳过
                return;
            }
        }

        // Give any InstantiationAwareBeanPostProcessors the opportunity to modify the
        // state of the bean before properties are set. This can be used, for example,
        // to support styles of field injection.

        /*
         * 2 在bean实例化之后，属性填充（初始化）之前，回调InstantiationAwareBeanPostProcessor后处理器的
         * postProcessAfterInstantiation方法，可用于修改bean实例的状态
         *
         * Spring在这个扩展点方法中没有任何额外操作，但是我们可以自定义后处理器，重写该方法定义自己的逻辑。
         */

        // 是否是"synthetic"。一般是指只有AOP相关的pointCut配置或者Advice配置才会将 synthetic设置为true
        // 如果mdb不是 'synthetic' 不是合成的，且工厂拥有 InstantiationAwareBeanPostProcessor这个后处理器
        if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
            //遍历所有注册的BeanPostProcessor后处理器
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                //如果 bp 是 InstantiationAwareBeanPostProcessor 实例
                if (bp instanceof InstantiationAwareBeanPostProcessor) {
                    //强制转型
                    InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                    //回调postProcessAfterInstantiation方法，一般用于设置属性，传递的参数就是此前获取的bean实例以及beanName
                    //该方法可用于在bean实例化之后，初始化之前，修改bean实例的状态
                    //如果返回false，则不会继续应用后续的处理同时也会结束后续的属性填充流程，该方法结束，否则继续向后调用
                    if (!ibp.postProcessAfterInstantiation(bw.getWrappedInstance(), beanName)) {
                        return;
                    }
                }
            }
        }

        //获取bean定义的PropertyValues集合，在此前的parsePropertyElement方法中，我们说过
        //所有的<property>标签都被解析为PropertyValue对象并存入PropertyValues集合中了
        //PropertyValues：包含以一个或多个PropertyValue对象的容器，通常包括针对特定目标Bean的一次更新
        //如果mdb有PropertyValues就获取其PropertyValues
        PropertyValues pvs = (mbd.hasPropertyValues() ? mbd.getPropertyValues() : null);

        /*
         * 3 根据名称或者类型进行setter自动注入，适用于基于XML的配置autowire自动注入，现在很少基于XML配置了
         * 并没有真正的注入，而是将可以自动注入的属性名和bean实例存入新建的newPvs中，后面会统一注入
         */
        //获取已解析的自动注入模式，默认就是0，即不自动注入，可以设置，对应XML的autowire属性
        int resolvedAutowireMode = mbd.getResolvedAutowireMode();
        //如果是1（就是byName）按名称自动装配bean属性 ，或者如果是2（就是byType）按类型自动装配bean属性
        if (resolvedAutowireMode == AUTOWIRE_BY_NAME || resolvedAutowireMode == AUTOWIRE_BY_TYPE) {
            //MutablePropertyValues：PropertyValues接口的默认实现。允许对属性进行简单操作，并提供构造函数来支持从映射 进行深度复制和构造
            MutablePropertyValues newPvs = new MutablePropertyValues(pvs);
            // Add property values based on autowire by name if applicable.
            // 基于byName的setter自动注入
            //并没有真正的注入，而是将可以自动注入的属性名和bean实例存入新建的newPvs中，后面会统一注入
            if (resolvedAutowireMode == AUTOWIRE_BY_NAME) {
                //通过bw的PropertyDescriptor属性名，查找出对应的Bean对象，将其添加到newPvs中
                autowireByName(beanName, mbd, bw, newPvs);
            }
            // Add property values based on autowire by type if applicable.
            // 基于byType的setter自动注入
            //并没有真正的注入，而是将可以自动注入的属性名和bean实例存入新建的newPvs中，后面会统一注入
            if (resolvedAutowireMode == AUTOWIRE_BY_TYPE) {
                //通过bw的PropertyDescriptor属性类型，查找出对应的Bean对象，将其添加到newPvs中
                autowireByType(beanName, mbd, bw, newPvs);
            }
            //让pvs重新引用newPvs,newPvs此时已经包含了pvs的属性值以及通过AUTOWIRE_BY_NAME，AUTOWIRE_BY_TYPE自动装配所得到的属性值
            pvs = newPvs;
        }

        //工厂是否拥有 InstantiationAwareBeanPostProcessor这个后处理器
        boolean hasInstAwareBpps = hasInstantiationAwareBeanPostProcessors();

        //是否需要进行依赖检查，即是否设置dependencyCheck属性，表示属性强制检查，就是XML的dependency-check属性，默认返回 DEPENDENCY_CHECK_NONE，表示 不检查
        //然而这个属性早在spring3.0的时候就被废弃了，代替它的就是构造器注入或者@Required，默认就是0，不进行强制检查，因此为false
        boolean needsDepCheck = (mbd.getDependencyCheck() != AbstractBeanDefinition.DEPENDENCY_CHECK_NONE);

        //经过筛选的 PropertyDescriptor 数组,存放着排除忽略的依赖项或忽略项上的定义的属性
        PropertyDescriptor[] filteredPds = null;

        /*
         * 4 查找全部InstantiationAwareBeanPostProcessor后处理器，回调postProcessProperties方法
         * 解析此前通过applyMergedBeanDefinitionPostProcessors方法找到的自动注入注解，
         * 这里进行了注解的真正的注入 CommonAnnotationBeanPostProcessor,AutowiredAnnotationBeanPostProcessor 这几个类
         */

        if (hasInstAwareBpps) {
            //如果为null，初始化一个空的MutablePropertyValues对象
            if (pvs == null) {
                //尝试获取mbd的PropertyValues，如果为null，初始化一个空的MutablePropertyValues对象
                pvs = mbd.getPropertyValues();
            }
            //遍历工厂内的所有后置处理器
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                //如果 bp 是 InstantiationAwareBeanPostProcessor 的实例
                if (bp instanceof InstantiationAwareBeanPostProcessor) {
                    //将bp 强转成 InstantiationAwareBeanPostProcessor 对象
                    InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                    /*
                     * 回调postProcessProperties方法，传递的参数就是目前的pvs、bean实例、beanName，
                     * 通过 applyMergedBeanDefinitionPostProcessors方法找到的自动注入注解，返回PropertyValues
                     *
                     * CommonAnnotationBeanPostProcessor -> 解析注入@WebServiceRef、@EJB、@Resource注解，默认直接返回参数pvs
                     * AutowiredAnnotationBeanPostProcessor -> 解析注入@Autowired、@Value、@Inject注解，默认直接返回参数pvs
                     *
                     * 在Spring 5.1 之前使用的是postProcessPropertyValues回调方法，Spring 5.1开始该方法被不推荐使用，推荐使用postProcessProperties来替代
                     */
                    PropertyValues pvsToUse = ibp.postProcessProperties(pvs, bw.getWrappedInstance(), beanName);
                    //如果pvsToUse为null，那么调用此前的已被放弃的postProcessPropertyValues方法继续尝试
                    if (pvsToUse == null) {
                        //如果filteredPds为null
                        if (filteredPds == null) {
                            //mbd.allowCaching:是否允许缓存，默认时允许的。缓存除了可以提高效率以外，还可以保证在并发的情况下，返回的 PropertyDescriptor[]永远都是同一份
                            //从bw提取一组经过筛选的 PropertyDescriptor,排除忽略的依赖项或忽略项上的定义的属性
                            filteredPds = filterPropertyDescriptorsForDependencyCheck(bw, mbd.allowCaching);
                        }

                        //postProcessPropertyValues:一般进行检查是否所有依赖项都满足，例如基于"Require"注释在 bean属性 setter，
                        // 	-- 替换要应用的属性值，通常是通过基于原始的PropertyValues创建一个新的MutablePropertyValue实例， 添加或删除特定的值
                        // 	-- 返回的PropertyValues 将应用于bw包装的bean实例 的实际属性值（添加PropertyValues实例到pvs 或者 设置为null以跳过属性填充）
                        //回到ipd的postProcessPropertyValues方法
                        //调用postProcessPropertyValues方法，该方法已被丢弃
                        //实际上CommonAnnotationBeanPostProcessor和AutowiredAnnotationBeanPostProcessor的方法内部就是直接调用的postProcessProperties方法
                        pvsToUse = ibp.postProcessPropertyValues(pvs, filteredPds, bw.getWrappedInstance(), beanName);
                        //如果pvsToUse为null，将终止该方法精致，以跳过属性填充
                        if (pvsToUse == null) {
                            return;
                        }
                    }
                    //重新设置pvs，对于CommonAnnotationBeanPostProcessor和AutowiredAnnotationBeanPostProcessor来说具有同一批数据
                    pvs = pvsToUse;
                }
            }
        }
        //如果需要进行依赖检查，默认不需要，Spring3.0之后没法设置
        if (needsDepCheck) {
            //如果filteredPds为null
            if (filteredPds == null) {
                //从bw提取一组经过筛选的 PropertyDescriptor,排除忽略的依赖项或忽略项上的定义的属性
                filteredPds = filterPropertyDescriptorsForDependencyCheck(bw, mbd.allowCaching);
            }
            //检查依赖项：主要检查pd的setter方法需要赋值时,pvs中有没有满足其pd的需求的属性值可供其赋值
            checkDependencies(beanName, mbd, filteredPds, pvs);
        }

        /*
         * 5 如果pvs不为null，这里默认的pvs就是<property>标签和byName或者byType找到的属性值的汇总
         * 这里将所有PropertyValues中的属性继续填充到bean实例中
         */
        if (pvs != null) {
            //应用给定的属性值，解决任何在这个bean工厂运行时其他bean的引用。必须使用深拷贝，所以我们 不会永久地修改这个属性
            applyPropertyValues(beanName, mbd, bw, pvs);
        }
    }

    /**
     * 使用 "byName" 自动装配模式，用工厂中其他 bean 的引用填充任何缺失的属性值。
     * 将属性名和bean实例存入newPvs集合中，并没有真正的注入依赖，类似于预先查找
     * <p>
     * Fill in any missing property values with references to
     * other beans in this factory if autowire is set to "byName".
     *
     * @param beanName the name of the bean we're wiring up.
     *                 Useful for debugging messages; not used functionally.   正在装配的 bean 的名称。
     * @param mbd      bean definition to update through autowiring  要通过自动装配更新的 bean 定义
     * @param bw       the BeanWrapper from which we can obtain information about the bean   可以从中获取有关 bean 信息的 BeanWrapper
     * @param pvs      the PropertyValues to register wired objects with    用于注册已装配对象的 PropertyValues (可变的属性值)
     */
    protected void autowireByName(
            String beanName, AbstractBeanDefinition mbd, BeanWrapper bw, MutablePropertyValues pvs) {

        //获取bw中有setter方法 && 非简单类型属性 && mbd的PropertyValues中没有该pd的属性名的 PropertyDescriptor 属性名数组
        String[] propertyNames = unsatisfiedNonSimpleProperties(mbd, bw);

        // 遍历所有需要自动装配的属性名称
        for (String propertyName : propertyNames) {
            // 如果此 Bean 工厂及其父工厂包含具有给定名称的 bean 定义或bean实例
            if (containsBean(propertyName)) {
                //调用getBean通过propertyName获取bean实例，这里就有可能初始化该beanName的实例
                Object bean = getBean(propertyName);
                //将propertyName,bean添加到pvs中
                pvs.add(propertyName, bean);
                //那么将propertyName和beanName的依赖关系注册到dependentBeanMap和dependenciesForBeanMap缓存中
                //表示beanName的实例依赖propertyName的实例，用于依赖关系的管理和销毁时的顺序控制
                registerDependentBean(propertyName, beanName);
                //打印跟踪日志
                if (logger.isTraceEnabled()) {
                    logger.trace("Added autowiring by name from bean name '" + beanName +
                            "' via property '" + propertyName + "' to bean named '" + propertyName + "'");
                }
            } else {
                //打印跟踪日志
                if (logger.isTraceEnabled()) {
                    logger.trace("Not autowiring property '" + propertyName + "' of bean '" + beanName +
                            "' by name: no matching bean found");
                }
            }
        }
    }

    /**
     * 通过"byType"引用此工厂中的其他 bean 实例填充缺失的属性，将属性名和bean实例存入newPvs集合中
     * 并没有真正的注入依赖，类似于预先查找
     * <p>
     * Abstract method defining "autowire by type" (bean properties by type) behavior.
     * <p>This is like PicoContainer default, in which there must be exactly one bean
     * of the property type in the bean factory. This makes bean factories simple to
     * configure for small namespaces, but doesn't work as well as standard Spring
     * behavior for bigger applications.
     *
     * @param beanName the name of the bean to autowire by type  需要进行自动注入的beanName
     * @param mbd      the merged bean definition to update through autowiring   bean定义
     * @param bw       the BeanWrapper from which we can obtain information about the bean  BeanWrapper
     * @param pvs      the PropertyValues to register wired objects with  已找到的property属性
     */
    protected void autowireByType(
            String beanName, AbstractBeanDefinition mbd, BeanWrapper bw, MutablePropertyValues pvs) {

        //获取AbstractBeanFactory中的typeConverter自定义类型转换器属性，该属性用来覆盖默认属性编辑器PropertyEditor
        //并没有提供getCustomTypeConverter方法的默认返回，因此customConverter默认返回null，我们同样可以自己扩展
        TypeConverter converter = getCustomTypeConverter();
        //如果没有配置自定义类型转换器
        if (converter == null) {
            //使用bw作为类型转换器
            converter = bw;
        }
        //存放所有自动注入BeanName的集合
        Set<String> autowiredBeanNames = new LinkedHashSet<>(4);
        //获取bw中有setter方法 && 非简单类型属性 && mbd的PropertyValues中没有该pd的属性名的 PropertyDescriptor 属性名数组
        String[] propertyNames = unsatisfiedNonSimpleProperties(mbd, bw);
        //遍历属性名数组
        for (String propertyName : propertyNames) {
            try {
                //PropertyDescriptor:表示JavaBean类通过存储器导出一个属性
                //从bw中获取propertyName对应的PropertyDescriptor
                PropertyDescriptor pd = bw.getPropertyDescriptor(propertyName);
                // Don't try autowiring by type for type Object: never makes sense,
                // even if it technically is a unsatisfied, non-simple property.
                //如果参数类型不是Object类型，永远不会按照类型自动注入任何属于Object类型的
                if (Object.class != pd.getPropertyType()) {
                    //获取方法参数对象，必须是setter方法
                    MethodParameter methodParam = BeanUtils.getWriteMethodParameter(pd);
                    // Do not allow eager init for type matching in case of a prioritized post-processor.
                    //判断bean对象是否是PriorityOrder实例，如果不是就允许急于初始化来进行类型匹配。
                    //eager为true时会导致初始化lazy-init单例和由FactoryBeans(或带有"factory-bean"引用的工厂方法)创建 的对象以进行类型检查
                    boolean eager = !(bw.getWrappedInstance() instanceof PriorityOrdered);
                    //AutowireByTypeDependencyDescriptor:根据类型依赖自动注入的描述符，重写了 getDependencyName() 方法，使其永远返回null
                    //将 methodParam 封装包装成AutowireByTypeDependencyDescriptor对象
                    DependencyDescriptor desc = new AutowireByTypeDependencyDescriptor(methodParam, eager);
                    //调用resolveDependency方法根据类型解析依赖，这个方法在前面已经讲过了
                    //如果没找到任何依赖就返回null，如果找到多个同类型依赖项并且没找到最合适的依赖则会抛出异常（除非是集合类型依赖）
                    Object autowiredArgument = resolveDependency(desc, beanName, autowiredBeanNames, converter);
                    //如果不为null，将propertyName和bean实例加入到pvs集合中，如果为null，那么放弃注入
                    if (autowiredArgument != null) {
                        //将propertyName.autowireArgument作为键值添加到pvs中
                        pvs.add(propertyName, autowiredArgument);
                    }
                    /*
                     * 遍历自动注入的beanName，这些bean也算作当前bean定义依赖的bean
                     * 这就类似于createArgumentArray方法最后的逻辑了
                     */
                    for (String autowiredBeanName : autowiredBeanNames) {
                        //那么将autowiredBeanName和beanName的依赖关系注册到dependentBeanMap和dependenciesForBeanMap缓存中
                        //用于依赖关系的管理和销毁时的顺序控制，这个方法我们在前面就讲过了
                        registerDependentBean(autowiredBeanName, beanName);
                        //打印跟踪日志
                        if (logger.isTraceEnabled()) {
                            logger.trace("Autowiring by type from bean name '" + beanName + "' via property '" +
                                    propertyName + "' to bean named '" + autowiredBeanName + "'");
                        }
                    }
                    //清空autowiredBeanNames集合
                    autowiredBeanNames.clear();
                }
            } catch (BeansException ex) {
                //捕捉自动装配时抛出的Bean异常，重新抛出 不满足依赖异常
                throw new UnsatisfiedDependencyException(mbd.getResourceDescription(), beanName, propertyName, ex);
            }
        }
    }


    /**
     * 返回一个不满足要求的非简单bean属性数组。这些可能是对工厂中其他bean的不满意的引用。不包括简单属性，如原始或字符串
     * <p>
     * 获取bw中有setter方法 && 非简单类型属性 && mbd的PropertyValues中没有该pd的属性名的 PropertyDescriptor 属性名数组
     * <p>
     * Return an array of non-simple bean properties that are unsatisfied.
     * These are probably unsatisfied references to other beans in the
     * factory. Does not include simple properties like primitives or Strings.
     *
     * @param mbd the merged bean definition the bean was created with  创建 Bean 时使用的合并 Bean 定义
     * @param bw  the BeanWrapper the bean was created with   创建 Bean 时使用的 BeanWrapper
     * @return an array of bean property names   Bean属性名称的数组
     * @see org.springframework.beans.BeanUtils#isSimpleProperty
     */
    protected String[] unsatisfiedNonSimpleProperties(AbstractBeanDefinition mbd, BeanWrapper bw) {
        //TreeSet:TreeSet底层是二叉树，可以对对象元素进行排序，但是自定义类需要实现comparable接口，重写 comparableTo() 方法。
        Set<String> result = new TreeSet<>();
        //获取自己定义的property的集合
        PropertyValues pvs = mbd.getPropertyValues();
        //获取BeanWrapper中的属性描述符数组。
        //请注意，这里的一个数组元素实际上是一个具有一个参数的setter方法转换之后的属性描述符
        //PropertyDescriptor的name属性是截取的"set"之后的部分，并进行了处理：如果至少开头两个字符是大写，那么就返回原截取的值，否则返回开头为小写的截取的值
        //PropertyDescriptor的propertyType属性是方法的参数类型
        PropertyDescriptor[] pds = bw.getPropertyDescriptors();
        //遍历属性描述对象
        for (PropertyDescriptor pd : pds) {
            //1 如果该描述符存在用于写属性的方法(即"set"开头的方法)
            //2 并且没有排除依赖类型检查（该类型没有在ignoredDependencyTypes和ignoredDependencyInterfaces两个忽略注入的集合中
            //  此前讲的忽略setter自动注入的扩展点就是这两个集合的控制的，可以通过ignoreDependencyType和ignoreDependencyInterface方法设置）
            //3 并且自己定义的property的集合没有该"属性名"，即所有的<property>标签的name属性不包括该"属性"的name
            //4 并且propertyType不是简单类型属性：基本类型及其包装类、Enum、String、CharSequence、Number、Date、Temporal、URI、URL、Locale、Class
            //  这些类型的单个对象或者数组都被称为简单类型。
            if (pd.getWriteMethod() != null && !isExcludedFromDependencyCheck(pd) && !pvs.contains(pd.getName()) &&
                    !BeanUtils.isSimpleProperty(pd.getPropertyType())) {
                //将name添加到返回值集合中
                result.add(pd.getName());
            }
        }
        //将result装换成数组
        return StringUtils.toStringArray(result);
    }

    /**
     * 过滤出需要依赖检查的属性
     * <p>
     * Extract a filtered set of PropertyDescriptors from the given BeanWrapper,
     * excluding ignored dependency types or properties defined on ignored dependency interfaces.
     *
     * @param bw    the BeanWrapper the bean was created with
     * @param cache whether to cache filtered PropertyDescriptors for the given bean Class
     * @return the filtered PropertyDescriptors
     * @see #isExcludedFromDependencyCheck
     * @see #filterPropertyDescriptorsForDependencyCheck(org.springframework.beans.BeanWrapper)
     */
    protected PropertyDescriptor[] filterPropertyDescriptorsForDependencyCheck(BeanWrapper bw, boolean cache) {
        PropertyDescriptor[] filtered = this.filteredPropertyDescriptorsCache.get(bw.getWrappedClass());
        if (filtered == null) {
            filtered = filterPropertyDescriptorsForDependencyCheck(bw);
            // 缓存
            if (cache) {
                PropertyDescriptor[] existing =
                        this.filteredPropertyDescriptorsCache.putIfAbsent(bw.getWrappedClass(), filtered);
                if (existing != null) {
                    filtered = existing;
                }
            }
        }
        return filtered;
    }

    /**
     * Extract a filtered set of PropertyDescriptors from the given BeanWrapper,
     * excluding ignored dependency types or properties defined on ignored dependency interfaces.
     *
     * @param bw the BeanWrapper the bean was created with
     * @return the filtered PropertyDescriptors
     * @see #isExcludedFromDependencyCheck
     */
    protected PropertyDescriptor[] filterPropertyDescriptorsForDependencyCheck(BeanWrapper bw) {
        List<PropertyDescriptor> pds = new ArrayList<>(Arrays.asList(bw.getPropertyDescriptors()));
        pds.removeIf(this::isExcludedFromDependencyCheck);
        return pds.toArray(new PropertyDescriptor[0]);
    }

    /**
     * 确定给定bean属性是否被排除在依赖项检查之外
     * <p>
     * Determine whether the given bean property is excluded from dependency checks.
     * <p>This implementation excludes properties defined by CGLIB and
     * properties whose type matches an ignored dependency type or which
     * are defined by an ignored dependency interface.
     *
     * @param pd the PropertyDescriptor of the bean property
     * @return whether the bean property is excluded
     * @see #ignoreDependencyType(Class)
     * @see #ignoreDependencyInterface(Class)
     */
    protected boolean isExcludedFromDependencyCheck(PropertyDescriptor pd) {
        //pd的属性是CGLIB定义的属性 || 该工厂的忽略依赖类型列表中包含该pd的属性类型 || pd的属性是ignoredDependencyInterfaces里面的接口定义的方法
        return (AutowireUtils.isExcludedFromDependencyCheck(pd) ||
                this.ignoredDependencyTypes.contains(pd.getPropertyType()) ||
                AutowireUtils.isSetterDefinedInInterface(pd, this.ignoredDependencyInterfaces));
    }

    /**
     * 执行依赖检查所有暴露的属性已经被设置
     * <p>
     * Perform a dependency check that all properties exposed have been set,
     * if desired. Dependency checks can be objects (collaborating beans),
     * simple (primitives and String), or all (both).
     *
     * @param beanName the name of the bean
     * @param mbd      the merged bean definition the bean was created with
     * @param pds      the relevant property descriptors for the target bean
     * @param pvs      the property values to be applied to the bean
     * @see #isExcludedFromDependencyCheck(java.beans.PropertyDescriptor)
     */
    protected void checkDependencies(
            String beanName, AbstractBeanDefinition mbd, PropertyDescriptor[] pds, @Nullable PropertyValues pvs)
            throws UnsatisfiedDependencyException {

        int dependencyCheck = mbd.getDependencyCheck();
        for (PropertyDescriptor pd : pds) {
            if (pd.getWriteMethod() != null && (pvs == null || !pvs.contains(pd.getName()))) {
                boolean isSimple = BeanUtils.isSimpleProperty(pd.getPropertyType());
                boolean unsatisfied = (dependencyCheck == AbstractBeanDefinition.DEPENDENCY_CHECK_ALL) ||
                        (isSimple && dependencyCheck == AbstractBeanDefinition.DEPENDENCY_CHECK_SIMPLE) ||
                        (!isSimple && dependencyCheck == AbstractBeanDefinition.DEPENDENCY_CHECK_OBJECTS);
                if (unsatisfied) {
                    throw new UnsatisfiedDependencyException(mbd.getResourceDescription(), beanName, pd.getName(),
                            "Set this property value or disable dependency checking for this bean.");
                }
            }
        }
    }

    /**
     * 应用给定的属性值，解决任何在这个bean工厂运行时其他bean的引用。必须使用深拷贝，所以我们不会永久地修改这个属性
     * 应用从<property>标签和byName或者byType找到的属性值，在此前已经应用过某些注解注入了
     * 这个方法可以说是为基于XML的setter属性注入准备的，纯粹的注解注入在前一步postProcessProperties已经完成了
     * <p>
     * Apply the given property values, resolving any runtime references
     * to other beans in this bean factory. Must use deep copy, so we
     * don't permanently modify this property.
     *
     * @param beanName the bean name passed for better exception information  主要用于异常信息输出
     * @param mbd      the merged bean definition  合并的bean定义
     * @param bw       the BeanWrapper wrapping the target object    BeanWrapper，包装了目标对象
     * @param pvs      the new property values  属性值集合
     */
    protected void applyPropertyValues(String beanName, BeanDefinition mbd, BeanWrapper bw, PropertyValues pvs) {
        //如果是属性值集合为空，直接返回
        if (pvs.isEmpty()) {
            // 直接结束方法
            return;
        }

        // 如果有安全管理器，且bw是BeanWrapperImpl的实例，暂不考虑
        if (System.getSecurityManager() != null && bw instanceof BeanWrapperImpl) {
            // 设置bw的安全上下文为工厂的访问控制上下文
            ((BeanWrapperImpl) bw).setSecurityContext(getAccessControlContext());
        }

        //MutablePropertyValues：PropertyValues接口的默认实现。允许对属性进行简单操作，并提供构造函数来支持从映射 进行深度复制和构造
        MutablePropertyValues mpvs = null;
        // 原始属性列表
        List<PropertyValue> original;

        // 如果pvs是MutablePropertyValues，一般都是这个类型
        if (pvs instanceof MutablePropertyValues) {
            // 类型强制转换
            mpvs = (MutablePropertyValues) pvs;
            //isConverted:返回该holder是否只包含转换后的值(true),或者是否仍然需要转换这些值
            //如果mpvs只包含转换后的值，这个第一次创建bean的时候一般是false，后续可能为true
            //第一次解析之后会存储起来，后续走这个判断，直接使用"捷径"
            if (mpvs.isConverted()) {
                // Shortcut: use the pre-converted values as-is.
                try {
                    // 已完成，通过bw调用setPropertyValues注入属性值，直接返回
                    bw.setPropertyValues(mpvs);
                    return;
                } catch (BeansException ex) {
                    //捕捉Bean异常，重新抛出Bean创佳异常：错误设置属性值。
                    throw new BeanCreationException(
                            mbd.getResourceDescription(), beanName, "Error setting property values", ex);
                }
            }
            //如果没转换，那么获取内部的原始PropertyValue属性值集合
            original = mpvs.getPropertyValueList();
        } else {
            //如果不是MutablePropertyValues类型，那么原始属性值数组转换为集合
            original = Arrays.asList(pvs.getPropertyValues());
        }

        /*
         *  获取用户自定义类型转换器
         * 获取AbstractBeanFactory中的typeConverter自定义类型转换器属性，该属性用来覆盖默认属性编辑器PropertyEditor
         * 并没有提供getCustomTypeConverter方法的默认返回，因此customConverter默认返回null，我们同样可以自己扩展
         * 如果自定义类型转换器customConverter不为null，那么就使用customConverter
         * 否则使用bw对象本身，因为BeanWrapper也能实现类型转换，一般都是使用bw本身
         */
        TypeConverter converter = getCustomTypeConverter();
        // 如果转换器为空，则直接把包装类赋值给converter
        if (converter == null) {
            converter = bw;
        }
        //初始化一个BeanDefinitionValueResolver对象，用于将 bean 定义对象中包含的值解析为应用于目标 bean 实例的实际值
        //BeanDefinitionValueResolver:在bean工厂实现中使用Helper类，它将beanDefinition对象中包含的值解析为应用于 目标bean实例的实际值
        BeanDefinitionValueResolver valueResolver = new BeanDefinitionValueResolver(this, beanName, mbd, converter);

        // Create a deep copy, resolving any references for values.
        // 创建一个深拷贝，存放解析后的属性值
        List<PropertyValue> deepCopy = new ArrayList<>(original.size());
        //resolveNecessary表示是否需要解析，默认为false
        boolean resolveNecessary = false;
        //遍历原始值属性集合的每一个属性，转换类型
        for (PropertyValue pv : original) {
            //如果单个属性已解析，直接加入解析值集合中
            if (pv.isConverted()) {
                //将pv添加到deepCopy中
                deepCopy.add(pv);
            }
            // 如果没有转换过，那么需要转换
            else {
                // 获取属性的名字
                String propertyName = pv.getName();
                // 获取未经类型转换的值（属性值）
                Object originalValue = pv.getValue();
                // AutowiredPropertyMarker.INSTANCE：自动生成标记的规范实例，一般都不是AutowiredPropertyMarker实例
                if (originalValue == AutowiredPropertyMarker.INSTANCE) {
                    //获取propertyName在bw中的setter方法
                    Method writeMethod = bw.getPropertyDescriptor(propertyName).getWriteMethod();
                    //如果setter方法为null
                    if (writeMethod == null) {
                        //抛出非法参数异常：自动装配标记属性没有写方法。
                        throw new IllegalArgumentException("Autowire marker for property without write method: " + pv);
                    }
                    //将writerMethod封装到DependencyDescriptor对象
                    originalValue = new DependencyDescriptor(new MethodParameter(writeMethod, 0), true);
                }
                /*
                 * 第一次转换
                 * 这里的resolveValueIfNecessary方法，则是将之前的值包装类转换为对应的实例Java类型，比如ManagedArray转换为数组、ManagedList转换为list集合
                 * 如果是引用其他bean或者指定了另一个bean定义，比如RuntimeBeanReference，则在这里会先初始化该引用的bean实例并返回
                 * 相当于反解回来，这里的resolvedValue就是转换后的实际Java类型，这就是所谓的转换，这也是前面所说的运行时解析，就是这个逻辑
                 *
                 * 之前就讲过了，在resolveConstructorArguments方法解析构造器参数的时候也用的是这个方法转换类型
                 */
                //交由valueResolver根据pv解析出originalValue所封装的对象
                Object resolvedValue = valueResolver.resolveValueIfNecessary(pv, originalValue);
                //默认转换后的值是刚解析出来的值
                Object convertedValue = resolvedValue;
                //可转换标记: propertyName是否bw中的可写属性 && propertyName不是表示索引属性或嵌套属性（如果propertyName中有'.'||'['就认为是索引属性或嵌套属性）
                boolean convertible = bw.isWritableProperty(propertyName) &&
                        !PropertyAccessorUtils.isNestedOrIndexedProperty(propertyName);
                //如果可转换
                if (convertible) {
                    //将resolvedValue转换为指定的目标属性对象
                    convertedValue = convertForProperty(resolvedValue, propertyName, bw, converter);
                }
                // Possibly store converted value in merged bean definition,
                // in order to avoid re-conversion for every created bean instance.
                /*
                 * 在合并的BeanDefinition中存储转换后的值，以避免为每个创建的bean实例重新转换
                 */
                //如果原值等于转换后的值，原值一般都是RuntimeBeanReference、ManagedArray、TypedStringValue等包包装类一般都不相等，
                if (resolvedValue == originalValue) {
                    //设置当前PropertyValue的converted属性为true，convertedValue属性为当前已转换的convertedValue值
                    if (convertible) {
                        //将convertedValue设置到pv中
                        pv.setConvertedValue(convertedValue);
                    }
                    //加入解析值集合中
                    deepCopy.add(pv);
                }
                //TypedStringValue:类型字符串的Holder,这个holder将只存储字符串值和目标类型。实际得转换将由Bean工厂执行
                //如果可转换 && originalValue是TypedStringValue的实例 && orginalValue 不是标记为动态【即不是一个表达式】&&
                // 	convertedValue不是Collection对象 或 数组
                else if (convertible && originalValue instanceof TypedStringValue &&
                        !((TypedStringValue) originalValue).isDynamic() &&
                        !(convertedValue instanceof Collection || ObjectUtils.isArray(convertedValue))) {
                    //设置当前PropertyValue的converted属性为true，convertedValue属性为当前已转换的convertedValue值
                    pv.setConvertedValue(convertedValue);
                    //加入解析值集合中
                    deepCopy.add(pv);
                } else {
                    //resolveNecessary设置为true，表示还需要继续解析
                    resolveNecessary = true;
                    //根据pv,convertedValue构建PropertyValue对象，并添加到deepCopy中
                    deepCopy.add(new PropertyValue(pv, convertedValue));
                }
            }
        }
        //如果是MutablePropertyValues类型，并且不需要继续解析了
        if (mpvs != null && !resolveNecessary) {
            //那么当前MutablePropertyValues的converted标记为true
            mpvs.setConverted();
        }

        // Set our (possibly massaged) deep copy.
        try {
            //根据转换之后的属性值deepCopy新建一个MutablePropertyValues，通过bw调用setPropertyValues注入属性值
            // 这个方法的调用链比较长，后续解析
            bw.setPropertyValues(new MutablePropertyValues(deepCopy));
        } catch (BeansException ex) {
            throw new BeanCreationException(
                    mbd.getResourceDescription(), beanName, "Error setting property values", ex);
        }
    }

    /**
     * 给定的值转换为指定的目标属性对象
     * <p>
     * Convert the given value for the specified target property.
     */
    @Nullable
    private Object convertForProperty(
            @Nullable Object value, String propertyName, BeanWrapper bw, TypeConverter converter) {

        //如果coverter是BeanWrapperImpl实例
        if (converter instanceof BeanWrapperImpl) {
            return ((BeanWrapperImpl) converter).convertForProperty(value, propertyName);
        } else {
            //获取 propertyName的属性描述符对象
            PropertyDescriptor pd = bw.getPropertyDescriptor(propertyName);
            //获取pd的setter方法参数
            MethodParameter methodParam = BeanUtils.getWriteMethodParameter(pd);
            //将value转换为pd要求的属性类型对象
            return converter.convertIfNecessary(value, pd.getPropertyType(), methodParam);
        }
    }


    /**
     * 初始化给定的 bean 实例，主要是应用各种回调方法：
     * 1 回调一些特殊的Aware接口的方法，包括BeanNameAware、BeanClassLoaderAware、BeanFactoryAware
     * 2 回调所有BeanPostProcessor的postProcessBeforeInitialization方法，包括@PostConstruct注解标注的初始化方法
     * 3 回调所有配置的init-method方法，包括InitializingBean.afterPropertiesSet()和init-method
     * 4 回调所有BeanPostProcessor的postProcessAfterInitialization方法
     * <p>
     * Initialize the given bean instance, applying factory callbacks
     * as well as init methods and bean post processors.
     * <p>Called from {@link #createBean} for traditionally defined beans,
     * and from {@link #initializeBean} for existing bean instances.
     *
     * @param beanName the bean name in the factory (for debugging purposes)  工厂中的bean名称（用于调试目的）
     * @param bean     the new bean instance we may need to initialize  可能需要初始化的新 bean 实例
     * @param mbd      the bean definition that the bean was created with
     *                 (can also be {@code null}, if given an existing bean instance)   创建 bean 的 bean 定义（如果给定现有 bean 实例，也可以为 null）
     * @return the initialized bean instance (potentially wrapped)  初始化 bean 实例（可能已包装）
     * @see BeanNameAware
     * @see BeanClassLoaderAware
     * @see BeanFactoryAware
     * @see #applyBeanPostProcessorsBeforeInitialization
     * @see #invokeInitMethods
     * @see #applyBeanPostProcessorsAfterInitialization
     */
    protected Object initializeBean(String beanName, Object bean, @Nullable RootBeanDefinition mbd) {
        /*
         * 1 一些特殊的Aware接口的回调，BeanNameAware、BeanClassLoaderAware、BeanFactoryAware
         */

        //如果存在安全管理器，一般不存在
        if (System.getSecurityManager() != null) {
            // 以特权的方式执行回调bean中的Aware接口方法
            AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                invokeAwareMethods(beanName, bean);
                return null;
            }, getAccessControlContext());
        } else {
            // Aware接口处理器，调用BeanNameAware、BeanClassLoaderAware、beanFactoryAware
            invokeAwareMethods(beanName, bean);
        }

        /*
         * 2 initMethod调用之前回调所有BeanPostProcessor的postProcessBeforeInitialization方法
         *
         * 其中就包括@PostConstruct注解标注的初始化方法的调用，在applyMergedBeanDefinitionPostProcessors方法中已经解析了该注解
         */
        Object wrappedBean = bean;
        //如果mdb为null || mbd不是"synthetic"，不是合成的。一般是指只有AOP相关的 prointCut 配置或者Advice配置才会将 synthetic设置为true
        if (mbd == null || !mbd.isSynthetic()) {
            // 将BeanPostProcessors应用到给定的现有Bean实例，调用它们的postProcessBeforeInitialization初始化方法。
            // 返回的Bean实例可能是原始Bean包装器
            // 其中就包括@PostConstruct注解标注的初始化方法的调用，在applyMergedBeanDefinitionPostProcessors方法中已经解析了该注解
            wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);
        }

        /*
         * 3 调用初始化方法，先调用bean的InitializingBean接口方法，后调用bean的自定义初始化方法
         * 包括InitializingBean.afterPropertiesSet()，以及XML配置的init-method属性指定的方法
         */
        try {
            invokeInitMethods(beanName, wrappedBean, mbd);
        } catch (Throwable ex) {
            //捕捉调用初始化方法时抛出的异常，重新抛出Bean创建异常：调用初始化方法失败
            throw new BeanCreationException(
                    (mbd != null ? mbd.getResourceDescription() : null),
                    beanName, "Invocation of init method failed", ex);
        }
        /*
         * 4 initMethod调用之后回调所有BeanPostProcessor的postProcessAfterInitialization方法
         */

        //如果mbd为null || mbd不是"synthetic"，不是合成的。
        if (mbd == null || !mbd.isSynthetic()) {
            // 将BeanPostProcessors应用到给定的现有Bean实例，调用它们的postProcessAfterInitialization方法。
            // 返回的Bean实例可能是原始Bean包装器
            wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);
        }

        //返回包装后的Bean
        return wrappedBean;
    }

    /**
     * 回调 bean 中 Aware接口 方法
     * 一些特殊的Aware接口的回调，顺序为（如果存在）：
     *  1 BeanNameAware.setBeanName(name)
     *  2 BeanClassLoaderAware.setBeanClassLoader(classLoader)
     *  3 BeanFactoryAware.setBeanFactory(beanFactory)
     *  这些Aware接口在此前的createBeanFactory方法中已经被加入到了忽略setter方法的自动装配的集合ignoredDependencyInterfaces中
     *
     * @param beanName beanName
     * @param bean  bean实例
     */
    private void invokeAwareMethods(String beanName, Object bean) {
        //如果 bean 是 Aware 实例
        if (bean instanceof Aware) {
            //如果bean是BeanNameAware实例
            if (bean instanceof BeanNameAware) {
                //调用 bean 的setBeanName方法
                ((BeanNameAware) bean).setBeanName(beanName);
            }
            //如果bean是 BeanClassLoaderAware 实例
            if (bean instanceof BeanClassLoaderAware) {
                //获取此工厂的类加载器以加载Bean类(即使无法使用系统ClassLoader,也只能为null)
                ClassLoader bcl = getBeanClassLoader();
                if (bcl != null) {
                    //调用 bean 的 setBeanClassLoader 方法
                    ((BeanClassLoaderAware) bean).setBeanClassLoader(bcl);
                }
            }
            //如果bean是 BeanFactoryAware 实例
            if (bean instanceof BeanFactoryAware) {
                // //调用 bean 的 setBeanFactory 方法
                ((BeanFactoryAware) bean).setBeanFactory(AbstractAutowireCapableBeanFactory.this);
            }
        }
    }

    /**
     * 回调自定义的initMethod初始化方法，顺序为：
     *  1 InitializingBean.afterPropertiesSet()方法
     *  2 XML配置的init-method方法
     * <p>
     * Give a bean a chance to react now all its properties are set,
     * and a chance to know about its owning bean factory (this object).
     * This means checking whether the bean implements InitializingBean or defines
     * a custom init method, and invoking the necessary callback(s) if it does.
     *
     * @param beanName the bean name in the factory (for debugging purposes)  工厂中的bean名称（用于调试目的）
     * @param bean     the new bean instance we may need to initialize   可能需要初始化的新 bean 实例
     * @param mbd      the merged bean definition that the bean was created with
     *                 (can also be {@code null}, if given an existing bean instance)  创建 bean 的 bean 定义（如果给定现有 bean 实例，也可以为 null）
     * @throws Throwable if thrown by init methods or by the invocation process
     * @see #invokeCustomInitMethod
     */
    protected void invokeInitMethods(String beanName, Object bean, @Nullable RootBeanDefinition mbd)
            throws Throwable {

        //bean实例是否属于InitializingBean
        boolean isInitializingBean = (bean instanceof InitializingBean);
        /*
         * 1 如果属于InitializingBean
         * 2 并且externallyManagedInitMethods集合中不存在afterPropertiesSet方法
         *   前面的在LifecycleMetadata的checkedInitMethods方法中我们就知道，通过@PostConstruct标注的方法会被存入externallyManagedInitMethods中
         *   如果此前@PostConstruct注解标注了afterPropertiesSet方法，那么这个方法不会被再次调用，这就是externallyManagedInitMethods防止重复调用的逻辑
         */
        if (isInitializingBean && (mbd == null || !mbd.isExternallyManagedInitMethod("afterPropertiesSet"))) {
            // 如果是日志级别为跟踪模式
            if (logger.isTraceEnabled()) {
                logger.trace("Invoking afterPropertiesSet() on bean with name '" + beanName + "'");
            }
            // 如果安全管理器不为null
            if (System.getSecurityManager() != null) {
                try {
                    // 以特权方式调用 bean的 afterPropertiesSet 方法
                    AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () -> {
                        ((InitializingBean) bean).afterPropertiesSet();
                        return null;
                    }, getAccessControlContext());
                } catch (PrivilegedActionException pae) {
                    throw pae.getException();
                }
            } else {
                // 调用bean的afterPropertiesSet方法
                ((InitializingBean) bean).afterPropertiesSet();
            }
        }

        // 如果mbd不为null&&bean不是NullBean类
        if (mbd != null && bean.getClass() != NullBean.class) {
            //获取initMethodName属性，就是XML的init-method属性
            String initMethodName = mbd.getInitMethodName();
            /*
             * 1 如果存在init-method方法
             * 2 并且不是InitializingBean类型或者是InitializingBean类型但是initMethodName不是"afterPropertiesSet"方法
             *   这里也是防止重复调用同一个方法的逻辑，因为在上面会调用afterPropertiesSet方法，这里不必再次调用
             * 3 并且externallyManagedInitMethods集合中不存在该方法
             *   在LifecycleMetadata的checkedInitMethods方法中我们就知道，通过@PostConstruct标注的方法会被存入externallyManagedInitMethods中
             *   如果此前@PostConstruct注解标注了afterPropertiesSet方法，那么这个方法不会被再次调用，这就是externallyManagedInitMethods防止重复调用的逻辑
             */
            if (StringUtils.hasLength(initMethodName) &&
                    !(isInitializingBean && "afterPropertiesSet".equals(initMethodName)) &&
                    !mbd.isExternallyManagedInitMethod(initMethodName)) {
                // 在bean上调用指定的自定义init（init-method）方法
                invokeCustomInitMethod(beanName, bean, mbd);
            }
        }
    }

    /**
     * 获取bean的自定义初始化方法，如果自身或者父类是接口类型的话，就反射出接口方法来，最后调用
     * <p>
     * Invoke the specified custom init method on the given bean.
     * Called by invokeInitMethods.
     * <p>Can be overridden in subclasses for custom resolution of init
     * methods with arguments.
     *
     * @see #invokeInitMethods
     */
    protected void invokeCustomInitMethod(String beanName, Object bean, RootBeanDefinition mbd)
            throws Throwable {

        // 获取初始化方法名称
        String initMethodName = mbd.getInitMethodName();
        Assert.state(initMethodName != null, "No init method set");
        // 获取初始化方法
        Method initMethod = (mbd.isNonPublicAccessAllowed() ?
                BeanUtils.findMethod(bean.getClass(), initMethodName) :
                ClassUtils.getMethodIfAvailable(bean.getClass(), initMethodName));

        if (initMethod == null) {
            if (mbd.isEnforceInitMethod()) {
                throw new BeanDefinitionValidationException("Could not find an init method named '" +
                        initMethodName + "' on bean with name '" + beanName + "'");
            } else {
                if (logger.isTraceEnabled()) {
                    logger.trace("No default init method named '" + initMethodName +
                            "' found on bean with name '" + beanName + "'");
                }
                // Ignore non-existent default lifecycle methods.
                return;
            }
        }

        if (logger.isTraceEnabled()) {
            logger.trace("Invoking init method  '" + initMethodName + "' on bean with name '" + beanName + "'");
        }
        Method methodToInvoke = ClassUtils.getInterfaceMethodIfPossible(initMethod);

        if (System.getSecurityManager() != null) {
            AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                ReflectionUtils.makeAccessible(methodToInvoke);
                return null;
            });
            try {
                AccessController.doPrivileged((PrivilegedExceptionAction<Object>)
                        () -> methodToInvoke.invoke(bean), getAccessControlContext());
            } catch (PrivilegedActionException pae) {
                InvocationTargetException ex = (InvocationTargetException) pae.getException();
                throw ex.getTargetException();
            }
        } else {
            try {
                ReflectionUtils.makeAccessible(methodToInvoke);
                // 反射执行
                methodToInvoke.invoke(bean);
            } catch (InvocationTargetException ex) {
                throw ex.getTargetException();
            }
        }
    }


    /**
     * 应用所有已注册BeanPostProcessor的postProcessAfterInitalization回调,使它们有机会对FactoryBeans获得的对象进行后处理(例如,自动代理它们)
     * <p>
     * Applies the {@code postProcessAfterInitialization} callback of all
     * registered BeanPostProcessors, giving them a chance to post-process the
     * object obtained from FactoryBeans (for example, to auto-proxy them).
     *
     * @param object   需要进行后处理的bean实例
     * @param beanName beanName
     * @return 最后一个BeanPostProcessor的postProcessAfterInitialization方法的返回结果
     * @see #applyBeanPostProcessorsAfterInitialization
     */
    @Override
    protected Object postProcessObjectFromFactoryBean(Object object, String beanName) {
        //调用applyBeanPostProcessorsAfterInitialization方法
        return applyBeanPostProcessorsAfterInitialization(object, beanName);
    }

    /**
     * 移除单例，并且从factoryBeanInstanceCache中移除指定beanName
     * <p>
     * Overridden to clear FactoryBean instance cache as well.
     */
    @Override
    protected void removeSingleton(String beanName) {
        synchronized (getSingletonMutex()) {
            super.removeSingleton(beanName);
            this.factoryBeanInstanceCache.remove(beanName);
        }
    }

    /**
     * 清空单例
     * <p>
     * Overridden to clear FactoryBean instance cache as well.
     */
    @Override
    protected void clearSingletonCache() {
        synchronized (getSingletonMutex()) {
            super.clearSingletonCache();
            this.factoryBeanInstanceCache.clear();
        }
    }

    /**
     * Expose the logger to collaborating delegates.
     *
     * @since 5.0.7
     */
    Log getLogger() {
        return logger;
    }


    /**
     * Special DependencyDescriptor variant for Spring's good old autowire="byType" mode.
     * Always optional; never considering the parameter name for choosing a primary candidate.
     */
    @SuppressWarnings("serial")
    private static class AutowireByTypeDependencyDescriptor extends DependencyDescriptor {

        public AutowireByTypeDependencyDescriptor(MethodParameter methodParameter, boolean eager) {
            super(methodParameter, false, eager);
        }

        @Override
        public String getDependencyName() {
            return null;
        }
    }


    /**
     * {@link MethodCallback} used to find {@link FactoryBean} type information.
     */
    private static class FactoryBeanMethodTypeFinder implements MethodCallback {

        private final String factoryMethodName;

        private ResolvableType result = ResolvableType.NONE;

        FactoryBeanMethodTypeFinder(String factoryMethodName) {
            this.factoryMethodName = factoryMethodName;
        }

        @Override
        public void doWith(Method method) throws IllegalArgumentException, IllegalAccessException {
            if (isFactoryBeanMethod(method)) {
                ResolvableType returnType = ResolvableType.forMethodReturnType(method);
                ResolvableType candidate = returnType.as(FactoryBean.class).getGeneric();
                if (this.result == ResolvableType.NONE) {
                    this.result = candidate;
                } else {
                    Class<?> resolvedResult = this.result.resolve();
                    Class<?> commonAncestor = ClassUtils.determineCommonAncestor(candidate.resolve(), resolvedResult);
                    if (!ObjectUtils.nullSafeEquals(resolvedResult, commonAncestor)) {
                        this.result = ResolvableType.forClass(commonAncestor);
                    }
                }
            }
        }

        private boolean isFactoryBeanMethod(Method method) {
            return (method.getName().equals(this.factoryMethodName) &&
                    FactoryBean.class.isAssignableFrom(method.getReturnType()));
        }

        ResolvableType getResult() {
            Class<?> resolved = this.result.resolve();
            boolean foundResult = resolved != null && resolved != Object.class;
            return (foundResult ? this.result : ResolvableType.NONE);
        }
    }

}
