/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.xml;

import org.w3c.dom.Element;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.lang.Nullable;

/**
 * Base class for those {@link BeanDefinitionParser} implementations that
 * need to parse and define just a <i>single</i> {@code BeanDefinition}.
 *
 * <p>Extend this parser class when you want to create a single bean definition
 * from an arbitrarily complex XML element. You may wish to consider extending
 * the {@link AbstractSimpleBeanDefinitionParser} when you want to create a
 * single bean definition from a relatively simple custom XML element.
 *
 * <p>The resulting {@code BeanDefinition} will be automatically registered
 * with the {@link org.springframework.beans.factory.support.BeanDefinitionRegistry}.
 * Your job simply is to {@link #doParse parse} the custom XML {@link Element}
 * into a single {@code BeanDefinition}.
 *
 * @author Rob Harrop
 * @author Juergen Hoeller
 * @author Rick Evans
 * @since 2.0
 * @see #getBeanClass
 * @see #getBeanClassName
 * @see #doParse
 */
public abstract class AbstractSingleBeanDefinitionParser extends AbstractBeanDefinitionParser {

	/**
	 * parseInternal 方法负责初始化 Bean 定义构建过程，并将具体的属性配置工作委托给子类实现的 doParse 策略方法。
	 * 它定义了一个骨架流程，而具体的实现则由子类根据自身的需求来完成。
	 *
	 * Creates a {@link BeanDefinitionBuilder} instance for the
	 * {@link #getBeanClass bean Class} and passes it to the
	 * {@link #doParse} strategy method. s
	 * +
	 * @param element the element that is to be parsed into a single BeanDefinition  要解析为单个BeanDefinition的元素
	 * @param parserContext the object encapsulating the current state of the parsing process 解析上下文
	 * @return the BeanDefinition resulting from the parsing of the supplied {@link Element}  解析所提供的 Element 所产生的BeanDefinition
	 * @throws IllegalStateException if the bean {@link Class} returned from
	 * {@link #getBeanClass(org.w3c.dom.Element)} is {@code null}  如果从 getBeanClass（org.w3c.dom.Element）返回的beanClass为 null，则出现IllegalStateException
	 * @see #doParse
	 */
	@Override
	protected final AbstractBeanDefinition parseInternal(Element element, ParserContext parserContext) {
		// BeanDefinitionBuilder实例构建BeanDefinition对象。
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition();
		//获取element的parentName属性，默认返回null，子类PropertyPlaceholderBeanDefinitionParser没有重写
		String parentName = getParentName(element);
		if (parentName != null) {
			// 设置 父级名称
			builder.getRawBeanDefinition().setParentName(parentName);
		}
		/*
		 * 获取element的Class，默认返回null，子类PropertyPlaceholderBeanDefinitionParser重写了该方法
		 * 默认返回PropertySourcesPlaceholderConfigurer.class
		 */
		Class<?> beanClass = getBeanClass(element);
		if (beanClass != null) {
			// 设置 beanClass
			builder.getRawBeanDefinition().setBeanClass(beanClass);
		}
		else {
			// 若子类没有重写getBeanClass方法则尝试检查子类是否重写getBeanClassName方法
			String beanClassName = getBeanClassName(element);
			if (beanClassName != null) {
				// 设置 beanClassName
				builder.getRawBeanDefinition().setBeanClassName(beanClassName);
			}
		}
		// 将当前解析的元素作为源信息，设置到BeanDefinitionBuilder中，以便追踪配置来源。
		builder.getRawBeanDefinition().setSource(parserContext.extractSource(element));
		//获取 包含BeanDefinition（例如，内部bean）
		//内部bean设置为和外部bean的相同的scope作用域属性
		BeanDefinition containingBd = parserContext.getContainingBeanDefinition();
		// / 如果存在包含BeanDefinition（例如，内部bean）
		if (containingBd != null) {
			// Inner bean definition must receive same scope as containing bean.
			// 若存在父类则使用父类的scope属性
			builder.setScope(containingBd.getScope());
		}
		// 如果parserContext设置了默认延迟加载
		//判断< beans/>的default-lazy-init属性，即是否延迟初始化
		if (parserContext.isDefaultLazyInit()) {
			// Default-lazy-init applies to custom bean definitions as well.
			// 配置延迟加载
			builder.setLazyInit(true);
		}
		/*
		 * 调用doParse方法继续解析
		 * 不同标签的自有属性的解析是通过内部的doParse方法解析的，这个方法由具体的解析器子类实现
		 */
		doParse(element, parserContext, builder);
		//构建一个GenericBeanDefinition
		return builder.getBeanDefinition();
	}

	/**
	 * 该函数是一个protected函数，用于确定当前解析的bean的父bean的名称。
	 * 如果当前bean被定义为子bean，则会调用该函数。默认实现是返回null，表示根bean定义。
	 * 函数的参数是一个Element对象，表示当前正在解析的元素。函数返回一个String对象，
	 * 表示当前解析bean的父bean的名称，如果没有父bean，则返回null。
	 * <p>
	 * Determine the name for the parent of the currently parsed bean,
	 * in case of the current bean being defined as a child bean.
	 * <p>The default implementation returns {@code null},
	 * indicating a root bean definition.
	 * @param element the {@code Element} that is being parsed
	 * @return the name of the parent bean for the currently parsed bean,
	 * or {@code null} if none
	 */
	@Nullable
	protected String getParentName(Element element) {
		return null;
	}

	/**
	 * 该函数是一个protected方法，用于获取与给定的Element对应的bean类。
	 * 注意，为了使应用程序类不直接依赖bean的实现类，避免耦合，推荐在应用程序类中重写getBeanClassName方法
	 * 这样，即使应用程序类不在插件的类路径上，也可以在IDE插件中使用BeanDefinitionParser和NamespaceHandler进行解析。
	 * 该方法的参数是一个Element对象，返回值是通过解析该Element定义的bean的类，如果不存在，则返回null
	 *  <p>
	 * Determine the bean class corresponding to the supplied {@link Element}.
	 * <p>Note that, for application classes, it is generally preferable to
	 * override {@link #getBeanClassName} instead, in order to avoid a direct
	 * dependence on the bean implementation class. The BeanDefinitionParser
	 * and its NamespaceHandler can be used within an IDE plugin then, even
	 * if the application classes are not available on the plugin's classpath.
	 * @param element the {@code Element} that is being parsed
	 * @return the {@link Class} of the bean that is being defined via parsing
	 * the supplied {@code Element}, or {@code null} if none
	 * @see #getBeanClassName
	 */
	@Nullable
	protected Class<?> getBeanClass(Element element) {
		return null;
	}

	/**
	 * 接受一个Element参数并返回一个String类型值。函数的主要功能是确定与给定Element对应的bean类名。
	 * 具体来说，它通过解析给定的Element来定义一个bean，并返回该bean的类名。如果无法确定类名，则返回null。
	 * <p>
	 * Determine the bean class name corresponding to the supplied {@link Element}.
	 * @param element the {@code Element} that is being parsed
	 * @return the class name of the bean that is being defined via parsing
	 * the supplied {@code Element}, or {@code null} if none
	 * @see #getBeanClass
	 */
	@Nullable
	protected String getBeanClassName(Element element) {
		return null;
	}

	/**
	 * Parse the supplied {@link Element} and populate the supplied
	 * {@link BeanDefinitionBuilder} as required.
	 * <p>The default implementation delegates to the {@code doParse}
	 * version without ParserContext argument.
	 * @param element the XML element being parsed
	 * @param parserContext the object encapsulating the current state of the parsing process
	 * @param builder used to define the {@code BeanDefinition}
	 * @see #doParse(Element, BeanDefinitionBuilder)
	 */
	protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {
		doParse(element, builder);
	}

	/**
	 * Parse the supplied {@link Element} and populate the supplied
	 * {@link BeanDefinitionBuilder} as required.
	 * <p>The default implementation does nothing.
	 * @param element the XML element being parsed
	 * @param builder used to define the {@code BeanDefinition}
	 */
	protected void doParse(Element element, BeanDefinitionBuilder builder) {
	}

}
