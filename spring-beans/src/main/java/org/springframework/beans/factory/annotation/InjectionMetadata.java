/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.annotation;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.lang.Nullable;
import org.springframework.util.ReflectionUtils;

/**
 * 注入元数据
 *
 * Internal class for managing injection metadata.
 * Not intended for direct use in applications.
 *
 * <p>Used by {@link AutowiredAnnotationBeanPostProcessor},
 * {link org.springframework.context.annotation.CommonAnnotationBeanPostProcessor} and
 * {link org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor}.
 *
 * @author Juergen Hoeller
 * @since 2.5
 */
public class InjectionMetadata {

	/**
	 * An empty {@code InjectionMetadata} instance with no-op callbacks.
	 * @since 5.2
	 */
	public static final InjectionMetadata EMPTY = new InjectionMetadata(Object.class, Collections.emptyList()) {
		@Override
		protected boolean needsRefresh(Class<?> clazz) {
			return false;
		}
		@Override
		public void checkConfigMembers(RootBeanDefinition beanDefinition) {
		}
		@Override
		public void inject(Object target, @Nullable String beanName, @Nullable PropertyValues pvs) {
		}
		@Override
		public void clear(@Nullable PropertyValues pvs) {
		}
	};


	private static final Log logger = LogFactory.getLog(InjectionMetadata.class);
	/**
	 * 目标类型
	 */
	private final Class<?> targetClass;

	/**
	 * 找到的注入点资源集合
	 */
	private final Collection<InjectedElement> injectedElements;

	/**
	 * 检查的注入点资源集合
	 */
	@Nullable
	private volatile Set<InjectedElement> checkedElements;


	/**
	 * Create a new {@code InjectionMetadata instance}.
	 * <p>Preferably use {@link #forElements} for reusing the {@link #EMPTY}
	 * instance in case of no elements.
	 * @param targetClass the target class
	 * @param elements the associated elements to inject
	 * @see #forElements
	 */
	public InjectionMetadata(Class<?> targetClass, Collection<InjectedElement> elements) {
		this.targetClass = targetClass;
		this.injectedElements = elements;
	}


	/**
	 * Determine whether this metadata instance needs to be refreshed.
	 * @param clazz the current target class
	 * @return {@code true} indicating a refresh, {@code false} otherwise
	 * @since 5.2.4
	 */
	protected boolean needsRefresh(Class<?> clazz) {
		return this.targetClass != clazz;
	}

	/**
	 * 检查配置成员，并将未被外部管理的注入元素注册到 Bean 定义中。
	 * <p>
	 * 该方法用于遍历 `injectedElements` 集合，检查其中的每个注入元素是否已经被外部管理。
	 * 如果某个注入元素未被外部管理，则将其注册到 Bean 定义中，并记录日志。
	 * </p>
	 *
	 * @param beanDefinition  要检查和注册注入元素的 Bean 定义。
	 */
	public void checkConfigMembers(RootBeanDefinition beanDefinition) {
		// 创建一个有序的集合，用于存储已检查的注入元素。
		// 使用 LinkedHashSet 可以保证元素的添加顺序，并且不允许重复元素。
		Set<InjectedElement> checkedElements = new LinkedHashSet<>(this.injectedElements.size());
		// 遍历所有的注入元素。
		for (InjectedElement element : this.injectedElements) {
			// 获取注入元素的成员（Field 或 Method）。
			Member member = element.getMember();
			// 检查 Bean 定义是否已经外部管理了该成员。
			// 如果返回 false，表示该成员尚未被外部管理。
			if (!beanDefinition.isExternallyManagedConfigMember(member)) {
				// 将该成员注册为外部管理的配置成员。
				// 这可以防止 Spring 在创建 Bean 实例时重复注入该成员。
				beanDefinition.registerExternallyManagedConfigMember(member);
				// 将该注入元素添加到已检查的集合中。
				checkedElements.add(element);
				if (logger.isTraceEnabled()) {
					logger.trace("Registered injected element on class [" + this.targetClass.getName() + "]: " + element);
				}
			}
		}
		// 将已检查的元素集合赋值给成员变量 `checkedElements`。
		this.checkedElements = checkedElements;
	}

	/**
	 *  对于Metadata内部的全部InjectedElement执行注解注入
	 * @param target 需要进行注入的目标实例
	 * @param beanName 当前beanName
	 * @param pvs   已找到的属性值数组，用于防止重复注入
	 * @throws Throwable
	 */
	public void inject(Object target, @Nullable String beanName, @Nullable PropertyValues pvs) throws Throwable {
		//如果checkedElements不为null，那么就使用checkedElements，即已检查的注解，前面就说过了，这用于防止注解重复注入
		Collection<InjectedElement> checkedElements = this.checkedElements;
		Collection<InjectedElement> elementsToIterate =
				(checkedElements != null ? checkedElements : this.injectedElements);
		if (!elementsToIterate.isEmpty()) {
			//遍历每一个InjectedElement，一个InjectedElement就表示一个注入点，可能是字段或者方法
			for (InjectedElement element : elementsToIterate) {
				if (logger.isTraceEnabled()) {
					logger.trace("Processing injected element of bean '" + beanName + "': " + element);
				}
				//调用各种类型的element的inject方法完成每一个注入点的注入操作
				//AutowiredFieldElement、AutowiredMethodElement这两个类型均重写了该方法，其它类型则使用父类InjectedElement本身的方法
				element.inject(target, beanName, pvs);
			}
		}
	}

	/**
	 * Clear property skipping for the contained elements.
	 * @since 3.2.13
	 */
	public void clear(@Nullable PropertyValues pvs) {
		Collection<InjectedElement> checkedElements = this.checkedElements;
		Collection<InjectedElement> elementsToIterate =
				(checkedElements != null ? checkedElements : this.injectedElements);
		if (!elementsToIterate.isEmpty()) {
			for (InjectedElement element : elementsToIterate) {
				element.clearPropertySkipping(pvs);
			}
		}
	}


	/**
	 * 根据是否有属性和方法有@Autowired和@Value注解，没有就返回InjectionMetadata.EMPTY,有的话就封装成InjectionMetadata返回
	 *
	 * Return an {@code InjectionMetadata} instance, possibly for empty elements.
	 * @param elements the elements to inject (possibly empty)
	 * @param clazz the target class
	 * @return a new {@link #InjectionMetadata(Class, Collection)} instance,
	 * or {@link #EMPTY} in case of no elements
	 * @since 5.2
	 */
	public static InjectionMetadata forElements(Collection<InjectedElement> elements, Class<?> clazz) {
		return (elements.isEmpty() ? InjectionMetadata.EMPTY : new InjectionMetadata(clazz, elements));
	}

	/**
	 * 判断给定的注入元数据是否需要被刷新
	 *
	 * Check whether the given injection metadata needs to be refreshed.
	 * @param metadata the existing metadata instance
	 * @param clazz the current target class
	 * @return {@code true} indicating a refresh, {@code false} otherwise
	 * @see #needsRefresh(Class)
	 */
	public static boolean needsRefresh(@Nullable InjectionMetadata metadata, Class<?> clazz) {
		return (metadata == null || metadata.needsRefresh(clazz));
	}


	/**
	 * A single injected element.
	 * 单个注入元素
	 */
	public abstract static class InjectedElement {

		protected final Member member;
		/**
		 * 是否是字段
		 */
		protected final boolean isField;
		/**
		 * 可以为 null 的属性扫描
		 */
		@Nullable
		protected final PropertyDescriptor pd;
		/**
		 *  该字段用于缓存是否跳过此注入元素的决策结果，避免重复计算。
		 *  初始值为 null，表示尚未进行检查。
		 */
		@Nullable
		protected volatile Boolean skip;

		protected InjectedElement(Member member, @Nullable PropertyDescriptor pd) {
			this.member = member;
			this.isField = (member instanceof Field);
			this.pd = pd;
		}

		public final Member getMember() {
			return this.member;
		}

		protected final Class<?> getResourceType() {
			if (this.isField) {
				return ((Field) this.member).getType();
			}
			else if (this.pd != null) {
				return this.pd.getPropertyType();
			}
			else {
				return ((Method) this.member).getParameterTypes()[0];
			}
		}

		protected final void checkResourceType(Class<?> resourceType) {
			if (this.isField) {
				Class<?> fieldType = ((Field) this.member).getType();
				if (!(resourceType.isAssignableFrom(fieldType) || fieldType.isAssignableFrom(resourceType))) {
					throw new IllegalStateException("Specified field type [" + fieldType +
							"] is incompatible with resource type [" + resourceType.getName() + "]");
				}
			}
			else {
				Class<?> paramType =
						(this.pd != null ? this.pd.getPropertyType() : ((Method) this.member).getParameterTypes()[0]);
				if (!(resourceType.isAssignableFrom(paramType) || paramType.isAssignableFrom(resourceType))) {
					throw new IllegalStateException("Specified parameter type [" + paramType +
							"] is incompatible with resource type [" + resourceType.getName() + "]");
				}
			}
		}

		/**
		 * InjectionMetadata的内部类InjectedElement本身的方法
		 * 完成每一个注入点的注入操作，包括@WebServiceRef、@EJB、@Resource注解的解析
		 * 进行属性或者方法注入，但是方法注入前会判断是否已经有设置值了，有设置就不会注入，直接返回
		 *
		 * Either this or {@link #getResourceToInject} needs to be overridden.
		 * @param target             需要进行注入的目标实例
		 * @param requestingBeanName beanName
		 * @param pvs                已找到的属性值数组，用于防止重复注入
		 */
		protected void inject(Object target, @Nullable String requestingBeanName, @Nullable PropertyValues pvs)
				throws Throwable {

			/*
			 * 如果是字段属性注入点
			 */
			if (this.isField) {
				//获取字段
				Field field = (Field) this.member;
				//设置字段的可访问属性，即field.setAccessible(true)
				ReflectionUtils.makeAccessible(field);
				/*
				 * 1 反射注入该字段属性的值，getResourceToInject方法默认返回null，
				 * 该方法被WebServiceRefElement、EjbRefElement、ResourceElement
				 * 这几个子类重写，用于获取要注入的属性值，我们主要看ResourceElement重写的方法
				 */
				field.set(target, getResourceToInject(target, requestingBeanName));
			}
			//否则，表示一个方法注入点
			else {
				/*
				 * 2 检查是否可以跳过该"属性"注入，也就是看此前找到的pvs中是否存在该名字的属性，如果存在就跳过，不存在就不跳过
				 * 这里可以发现：
				 *  setter方法的注入流程的优先级为：XML手动设置的property > XML设置自动注入的找到的property -> 注解设置的property
				 *  前面的流程中没找到指定名称的property时，当前流程才会查找property
				 */
				if (checkPropertySkipping(pvs)) {
					return;
				}
				try {
					//获取方法
					Method method = (Method) this.member;
					//设置方法的可访问属性，即field.setAccessible(true) 支持私有方法
					ReflectionUtils.makeAccessible(method);
					/*
					 * 3 反射调用该方法，设置参数的值，getResourceToInject方法默认返回null，
					 * 该方法被WebServiceRefElement、EjbRefElement、ResourceElement
					 * 这几个子类重写，用于获取要注入的属性值，我们主要看ResourceElement重写的方法
					 */
					method.invoke(target, getResourceToInject(target, requestingBeanName));
				}
				catch (InvocationTargetException ex) {
					throw ex.getTargetException();
				}
			}
		}

		/**
		 * 判断是否要略过属性注入，如果是定义的时候给定值了就忽略
		 *
		 * Check whether this injector's property needs to be skipped due to
		 * an explicit property value having been specified. Also marks the
		 * affected property as processed for other processors to ignore it.
		 * @param pvs  PropertyValues对象，包含了已经设置的属性值。 如果为 null，表示没有属性值被设置。
		 * @return   true如果应该跳过此注入元素的属性； false如果不应该跳过。
		 */
		protected boolean checkPropertySkipping(@Nullable PropertyValues pvs) {
			// 1. 检查是否已经确定了是否跳过
			Boolean skip = this.skip;
			if (skip != null) {
				// 如果 skip 已经有值，直接返回，避免重复计算
				return skip;
			}
			// 2. 如果 PropertyValues 为 null，表示没有属性值被设置，则不跳过
			if (pvs == null) {
				// 设置 skip 为 false，表示不跳过
				this.skip = false;
				// 返回 false，表示不跳过
				return false;
			}
			// 3. 使用同步块来保证线程安全，因为 PropertyValues 可能是共享资源
			synchronized (pvs) {
				// 再次检查是否已经确定了是否跳过 (双重检查锁模式)
				skip = this.skip;
				if (skip != null) {
					// 如果 skip 已经有值，直接返回，避免重复计算
					return skip;
				}
				// 4. 检查是否已经显式设置了该属性的值
				if (this.pd != null) {
					// 如果 PropertyValues 包含当前属性的名称
					if (pvs.contains(this.pd.getName())) {
						// Explicit value provided as part of the bean definition.
						// 设置 skip 为 true，表示跳过
						this.skip = true;
						// 返回 true，表示跳过
						return true;
					}
					// 如果 PropertyValues 是 MutablePropertyValues 的实例，
					// 并且 PropertyValues 中没有包含该属性的名称，
					// 则将该属性标记为已处理，以便其他处理器忽略它。
					else if (pvs instanceof MutablePropertyValues) {
						((MutablePropertyValues) pvs).registerProcessedProperty(this.pd.getName());
					}
				}
				// 5. 如果以上条件都不满足，则不跳过
				// 设置 skip 为 false，表示不跳过
				this.skip = false;
				// 返回 false，表示不跳过
				return false;
			}
		}

		/**
		 * Clear property skipping for this element.
		 * @since 3.2.13
		 */
		protected void clearPropertySkipping(@Nullable PropertyValues pvs) {
			if (pvs == null) {
				return;
			}
			synchronized (pvs) {
				if (Boolean.FALSE.equals(this.skip) && this.pd != null && pvs instanceof MutablePropertyValues) {
					((MutablePropertyValues) pvs).clearProcessedProperty(this.pd.getName());
				}
			}
		}

		/**
		 * Either this or {@link #inject} needs to be overridden.
		 */
		@Nullable
		protected Object getResourceToInject(Object target, @Nullable String requestingBeanName) {
			return null;
		}

		@Override
		public boolean equals(@Nullable Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof InjectedElement)) {
				return false;
			}
			InjectedElement otherElement = (InjectedElement) other;
			return this.member.equals(otherElement.member);
		}

		@Override
		public int hashCode() {
			return this.member.getClass().hashCode() * 29 + this.member.getName().hashCode();
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + " for " + this.member;
		}
	}

}
