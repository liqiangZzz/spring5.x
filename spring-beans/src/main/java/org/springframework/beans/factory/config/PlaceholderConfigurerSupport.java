/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.config;

import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.lang.Nullable;
import org.springframework.util.StringValueResolver;

/**
 * spring提供的一个工具类，用于解析bean定义中属性值里面的占位符，此类不能被直接实例化使用
 *
 * Abstract base class for property resource configurers that resolve placeholders
 * in bean definition property values. Implementations <em>pull</em> values from a
 * properties file or other {@linkplain org.springframework.core.env.PropertySource
 * property source} into bean definitions.
 *
 * <p>The default placeholder syntax follows the Ant / Log4J / JSP EL style:
 *
 * <pre class="code">${...}</pre>
 *
 * Example XML bean definition:
 *
 * <pre class="code">
 * &lt;bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource"/&gt;
 *   &lt;property name="driverClassName" value="${driver}"/&gt;
 *   &lt;property name="url" value="jdbc:${dbname}"/&gt;
 * &lt;/bean&gt;
 * </pre>
 *
 * Example properties file:
 *
 * <pre class="code">driver=com.mysql.jdbc.Driver
 * dbname=mysql:mydb</pre>
 *
 * Annotated bean definitions may take advantage of property replacement using
 * the {@link org.springframework.beans.factory.annotation.Value @Value} annotation:
 *
 * <pre class="code">@Value("${person.age}")</pre>
 *
 * Implementations check simple property values, lists, maps, props, and bean names
 * in bean references. Furthermore, placeholder values can also cross-reference
 * other placeholders, like:
 *
 * <pre class="code">rootPath=myrootdir
 * subPath=${rootPath}/subdir</pre>
 *
 * In contrast to {@link PropertyOverrideConfigurer}, subclasses of this type allow
 * filling in of explicit placeholders in bean definitions.
 *
 * <p>If a configurer cannot resolve a placeholder, a {@link BeanDefinitionStoreException}
 * will be thrown. If you want to check against multiple properties files, specify multiple
 * resources via the {@link #setLocations locations} property. You can also define multiple
 * configurers, each with its <em>own</em> placeholder syntax. Use {@link
 * #ignoreUnresolvablePlaceholders} to intentionally suppress throwing an exception if a
 * placeholder cannot be resolved.
 *
 * <p>Default property values can be defined globally for each configurer instance
 * via the {@link #setProperties properties} property, or on a property-by-property basis
 * using the default value separator which is {@code ":"} by default and
 * customizable via {@link #setValueSeparator(String)}.
 *
 * <p>Example XML property with default value:
 *
 * <pre class="code">
 *   <property name="url" value="jdbc:${dbname:defaultdb}"/>
 * </pre>
 *
 * @author Chris Beams
 * @author Juergen Hoeller
 * @since 3.1
 * @see PropertyPlaceholderConfigurer
 * see org.springframework.context.support.PropertySourcesPlaceholderConfigurer
 */
public abstract class PlaceholderConfigurerSupport extends PropertyResourceConfigurer
		implements BeanNameAware, BeanFactoryAware {

	/**
	 * 默认的占位符前缀
	 * Default placeholder prefix: {@value}. */
	public static final String DEFAULT_PLACEHOLDER_PREFIX = "${";

	/**
	 * 默认的占位符后缀
	 * Default placeholder suffix: {@value}. */
	public static final String DEFAULT_PLACEHOLDER_SUFFIX = "}";

	/**
	 * 默认的值分隔符
	 * Default value separator: {@value}. */
	public static final String DEFAULT_VALUE_SEPARATOR = ":";


	/**
	 * 默认的占位符前缀 ${
	 * Defaults to {@value #DEFAULT_PLACEHOLDER_PREFIX}.
	 */
	protected String placeholderPrefix = DEFAULT_PLACEHOLDER_PREFIX;

	/**
	 *  默认的占位符后缀 }
	 *  Defaults to {@value #DEFAULT_PLACEHOLDER_SUFFIX}.
	 */
	protected String placeholderSuffix = DEFAULT_PLACEHOLDER_SUFFIX;

	/**
	 * 默认的值分隔符 :
	 * Defaults to {@value #DEFAULT_VALUE_SEPARATOR}.
	 */
	@Nullable
	protected String valueSeparator = DEFAULT_VALUE_SEPARATOR;

	//  在进行占位符解析之前是否去除原始值的前后空白字符
	protected boolean trimValues = false;

	// 遇到占位符对应属性值为""或者null时的替代属性值,默认 null
	@Nullable
	protected String nullValue;

	// 不能解析的占位符是否抛出异常，false表示抛出异常，true表示不抛出异常
	protected boolean ignoreUnresolvablePlaceholders = false;

	// 用于记录当前bean的名称
	@Nullable
	private String beanName;

	// 用于记录当前bean的所在容器
	@Nullable
	private BeanFactory beanFactory;


	/**
	 * Set the prefix that a placeholder string starts with.
	 * The default is {@value #DEFAULT_PLACEHOLDER_PREFIX}.
	 */
	public void setPlaceholderPrefix(String placeholderPrefix) {
		this.placeholderPrefix = placeholderPrefix;
	}

	/**
	 * Set the suffix that a placeholder string ends with.
	 * The default is {@value #DEFAULT_PLACEHOLDER_SUFFIX}.
	 */
	public void setPlaceholderSuffix(String placeholderSuffix) {
		this.placeholderSuffix = placeholderSuffix;
	}

	/**
	 * Specify the separating character between the placeholder variable
	 * and the associated default value, or {@code null} if no such
	 * special character should be processed as a value separator.
	 * The default is {@value #DEFAULT_VALUE_SEPARATOR}.
	 */
	public void setValueSeparator(@Nullable String valueSeparator) {
		this.valueSeparator = valueSeparator;
	}

	/**
	 * Specify whether to trim resolved values before applying them,
	 * removing superfluous whitespace from the beginning and end.
	 * <p>Default is {@code false}.
	 * @since 4.3
	 */
	public void setTrimValues(boolean trimValues) {
		this.trimValues = trimValues;
	}

	/**
	 * Set a value that should be treated as {@code null} when resolved
	 * as a placeholder value: e.g. "" (empty String) or "null".
	 * <p>Note that this will only apply to full property values,
	 * not to parts of concatenated values.
	 * <p>By default, no such null value is defined. This means that
	 * there is no way to express {@code null} as a property value
	 * unless you explicitly map a corresponding value here.
	 */
	public void setNullValue(String nullValue) {
		this.nullValue = nullValue;
	}

	/**
	 * Set whether to ignore unresolvable placeholders.
	 * <p>Default is "false": An exception will be thrown if a placeholder fails
	 * to resolve. Switch this flag to "true" in order to preserve the placeholder
	 * String as-is in such a case, leaving it up to other placeholder configurers
	 * to resolve it.
	 */
	public void setIgnoreUnresolvablePlaceholders(boolean ignoreUnresolvablePlaceholders) {
		this.ignoreUnresolvablePlaceholders = ignoreUnresolvablePlaceholders;
	}

	/**
	 * 创建PropertySourcesPlaceholderConfigurer实例时回调
	 *
	 * Only necessary to check that we're not parsing our own bean definition,
	 * to avoid failing on unresolvable placeholders in properties file locations.
	 * The latter case can happen with placeholders for system properties in
	 * resource locations.
	 *
	 * @param beanName 当前实例beanName
	 * @see #setLocations
	 * @see org.springframework.core.io.ResourceEditor
	 */
	@Override
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	/**
	 * 创建PropertySourcesPlaceholderConfigurer实例时回调
	 *
	 * Only necessary to check that we're not parsing our own bean definition,
	 * to avoid failing on unresolvable placeholders in properties file locations.
	 * The latter case can happen with placeholders for system properties in
	 * resource locations.
	 *
	 * @param beanFactory 当前工厂
	 * @see #setLocations
	 * @see org.springframework.core.io.ResourceEditor
	 */
	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}


	/**
	 * 检查所有bean定义，替换占位符
	 *
	 * @param beanFactoryToProcess 要处理的bean定义所属的容器
	 * @param valueResolver 属性值解析器
	 */
	protected void doProcessProperties(ConfigurableListableBeanFactory beanFactoryToProcess,
			StringValueResolver valueResolver) {
		/*
		 * 创建一个用于遍历BeanDefinition对象的访问者类，可以访问给定bean定义的属性
		 * 特别是其中包含的属性值和构造函数参数值,双亲bean名称，bean类名，bean工厂bean名称，bean工厂方法名称，作用域，在遍历的同时解析值中的占位符
		 */
		BeanDefinitionVisitor visitor = new BeanDefinitionVisitor(valueResolver);

		// 获取容器中所有bean的名称
		String[] beanNames = beanFactoryToProcess.getBeanDefinitionNames();
		/*
		 * 1 遍历beanName数组，获取bean定义，解析替换内部属性值中的占位符
		 */
		for (String curName : beanNames) {
			// Check that we're not parsing our own bean definition,
			// to avoid failing on unresolvable placeholders in properties file locations.
			//如果当前遍历的beanName不等于当前注册的PropertySourcesPlaceholderConfigurer的beanName
			//并且是同一个beanFactory，那么可以解析其中的占位符
			if (!(curName.equals(this.beanName) && beanFactoryToProcess.equals(this.beanFactory))) {
				//获取bena定义
				BeanDefinition bd = beanFactoryToProcess.getBeanDefinition(curName);
				try {
					/*
					 * 遍历给定的 Bean 定义对象属性，包括MutablePropertyValues属性值和ConstructorArgumentValues构造器值
					 * 对遍历的每一个值使用valueResolver替换其中的占位符
					 * 支持占位符的属性有:
					 * 1 <bean/>的标签的parent属性、class属性、factory-bean属性、factory-method属性
					 * 2 <property/>标签的value、<constructor-arg/>标签的value
					 */
					visitor.visitBeanDefinition(bd);
				}
				catch (Exception ex) {
					throw new BeanDefinitionStoreException(bd.getResourceDescription(), curName, ex.getMessage(), ex);
				}
			}
		}

		// New in Spring 2.5: resolve placeholders in alias target names and aliases as well.
		/*
		 * 2 Spring 2.5 中的新功能：也解析别名映射缓存aliasMap中的 value-目标名称和key-别名 中的占位符。
		 */
		beanFactoryToProcess.resolveAliases(valueResolver);

		// New in Spring 3.0: resolve placeholders in embedded values such as annotation attributes.
		/*
		 * 3 Spring 3.0 中的新功能：将当前的valueResolver加入embeddedValueResolvers中，用于后续比如@Value、@Resource注解中的占位符解析，
		 * 注意这里并没有立即解析，因为invokeBeanFactoryPostProcessors方法调用的时候，bean的相关注解还没有被解析，
		 * 在后面的finishBeanFactoryInitialization方法中才会用到
		 */
		beanFactoryToProcess.addEmbeddedValueResolver(valueResolver);
	}

}
