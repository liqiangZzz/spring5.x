/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.annotation;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.TypeConverter;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.UnsatisfiedDependencyException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.beans.factory.support.LookupOverride;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

/**
 * {@link org.springframework.beans.factory.config.BeanPostProcessor BeanPostProcessor}
 * implementation that autowires annotated fields, setter methods, and arbitrary
 * config methods. Such members to be injected are detected through annotations:
 * by default, Spring's {@link Autowired @Autowired} and {@link Value @Value}
 * annotations.
 *
 * <p>Also supports JSR-330's {@link javax.inject.Inject @Inject} annotation,
 * if available, as a direct alternative to Spring's own {@code @Autowired}.
 *
 * <h3>Autowired Constructors</h3>
 * <p>Only one constructor of any given bean class may declare this annotation with
 * the 'required' attribute set to {@code true}, indicating <i>the</i> constructor
 * to autowire when used as a Spring bean. Furthermore, if the 'required' attribute
 * is set to {@code true}, only a single constructor may be annotated with
 * {@code @Autowired}. If multiple <i>non-required</i> constructors declare the
 * annotation, they will be considered as candidates for autowiring. The constructor
 * with the greatest number of dependencies that can be satisfied by matching beans
 * in the Spring container will be chosen. If none of the candidates can be satisfied,
 * then a primary/default constructor (if present) will be used. If a class only
 * declares a single constructor to begin with, it will always be used, even if not
 * annotated. An annotated constructor does not have to be public.
 *
 * <h3>Autowired Fields</h3>
 * <p>Fields are injected right after construction of a bean, before any
 * config methods are invoked. Such a config field does not have to be public.
 *
 * <h3>Autowired Methods</h3>
 * <p>Config methods may have an arbitrary name and any number of arguments; each of
 * those arguments will be autowired with a matching bean in the Spring container.
 * Bean property setter methods are effectively just a special case of such a
 * general config method. Config methods do not have to be public.
 *
 * <h3>Annotation Config vs. XML Config</h3>
 * <p>A default {@code AutowiredAnnotationBeanPostProcessor} will be registered
 * by the "context:annotation-config" and "context:component-scan" XML tags.
 * Remove or turn off the default annotation configuration there if you intend
 * to specify a custom {@code AutowiredAnnotationBeanPostProcessor} bean definition.
 *
 * <p><b>NOTE:</b> Annotation injection will be performed <i>before</i> XML injection;
 * thus the latter configuration will override the former for properties wired through
 * both approaches.
 *
 * <h3>{@literal @}Lookup Methods</h3>
 * <p>In addition to regular injection points as discussed above, this post-processor
 * also handles Spring's {@link Lookup @Lookup} annotation which identifies lookup
 * methods to be replaced by the container at runtime. This is essentially a type-safe
 * version of {@code getBean(Class, args)} and {@code getBean(String, args)}.
 * See {@link Lookup @Lookup's javadoc} for details.
 *
 * @author Juergen Hoeller
 * @author Mark Fisher
 * @author Stephane Nicoll
 * @author Sebastien Deleuze
 * @author Sam Brannen
 * @since 2.5
 * @see #setAutowiredAnnotationType
 * @see Autowired
 * @see Value
 */
public class AutowiredAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
		implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware {

	protected final Log logger = LogFactory.getLog(getClass());

	/**
	 * 存储自动注入的类型，默认支持的是3个
	 * 自动注入的注解类型集合
	 */
	private final Set<Class<? extends Annotation>> autowiredAnnotationTypes = new LinkedHashSet<>(4);

	// @Autowired(required = false)这个注解的属性值名称
	private String requiredParameterName = "required";

	// 这个值一般请不要改变（若改成false，效果required = false的作用是相反的了）
	private boolean requiredParameterValue = true;

	private int order = Ordered.LOWEST_PRECEDENCE - 2;

	@Nullable
	private ConfigurableListableBeanFactory beanFactory;

	// 对@Lookup方法的支持
	private final Set<String> lookupMethodsChecked = Collections.newSetFromMap(new ConcurrentHashMap<>(256));

	// 构造函数候选器缓存
	private final Map<Class<?>, Constructor<?>[]> candidateConstructorsCache = new ConcurrentHashMap<>(256);

	// 方法注入、字段filed注入
	// 此处InjectionMetadata这个类非常重要，到了此处@Autowired注解含义已经没有了，完全被准备成这个元数据了
	// InjectionMetadata持有targetClass、Collection<InjectedElement> injectedElements等两个重要属性
	// 其中InjectedElement这个抽象类最重要的两个实现为：AutowiredFieldElement和AutowiredMethodElement
	private final Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);


	/**
	 * 将@Autowired和@Value注解的类型存入autowiredAnnotationTypes集合，同时支持JSR-330的@Inject（如果存在）
	 *
	 * Create a new {@code AutowiredAnnotationBeanPostProcessor} for Spring's
	 * standard {@link Autowired @Autowired} and {@link Value @Value} annotations.
	 * <p>Also supports JSR-330's {@link javax.inject.Inject @Inject} annotation,
	 * if available.
	 */
	@SuppressWarnings("unchecked")
	public AutowiredAnnotationBeanPostProcessor() {
		//首先将 Spring 的 @Autowired 注解添加到自动装配注解类型集合中。
		this.autowiredAnnotationTypes.add(Autowired.class);
		//然后将 Spring 的 @Value 注解添加到自动装配注解类型集合中。
		this.autowiredAnnotationTypes.add(Value.class);

		// 尝试加载 JSR-330 规范中的 @Inject 注解。如果存在（必须引入支持JSR-330 API的依赖）
		try {
			// 使用 ClassUtils.forName 方法加载 javax.inject.Inject 类。
			// AutowiredAnnotationBeanPostProcessor.class.getClassLoader() 用于获取当前类的类加载器，
			// 确保能够正确加载 @Inject 注解类。
			this.autowiredAnnotationTypes.add((Class<? extends Annotation>)
					ClassUtils.forName("javax.inject.Inject", AutowiredAnnotationBeanPostProcessor.class.getClassLoader()));
			logger.trace("JSR-330 'javax.inject.Inject' annotation found and supported for autowiring");
		}
		catch (ClassNotFoundException ex) {
			// JSR-330 API not available - simply skip.
		}
	}


	/**
	 * 自定义支持的依赖注入注解类型
	 *
	 * Set the 'autowired' annotation type, to be used on constructors, fields,
	 * setter methods, and arbitrary config methods.
	 * <p>The default autowired annotation types are the Spring-provided
	 * {@link Autowired @Autowired} and {@link Value @Value} annotations as well
	 * as JSR-330's {@link javax.inject.Inject @Inject} annotation, if available.
	 * <p>This setter property exists so that developers can provide their own
	 * (non-Spring-specific) annotation type to indicate that a member is supposed
	 * to be autowired.
	 */
	public void setAutowiredAnnotationType(Class<? extends Annotation> autowiredAnnotationType) {
		Assert.notNull(autowiredAnnotationType, "'autowiredAnnotationType' must not be null");
		this.autowiredAnnotationTypes.clear();
		this.autowiredAnnotationTypes.add(autowiredAnnotationType);
	}

	/**
	 * Set the 'autowired' annotation types, to be used on constructors, fields,
	 * setter methods, and arbitrary config methods.
	 * <p>The default autowired annotation types are the Spring-provided
	 * {@link Autowired @Autowired} and {@link Value @Value} annotations as well
	 * as JSR-330's {@link javax.inject.Inject @Inject} annotation, if available.
	 * <p>This setter property exists so that developers can provide their own
	 * (non-Spring-specific) annotation types to indicate that a member is supposed
	 * to be autowired.
	 */
	public void setAutowiredAnnotationTypes(Set<Class<? extends Annotation>> autowiredAnnotationTypes) {
		Assert.notEmpty(autowiredAnnotationTypes, "'autowiredAnnotationTypes' must not be empty");
		this.autowiredAnnotationTypes.clear();
		this.autowiredAnnotationTypes.addAll(autowiredAnnotationTypes);
	}

	/**
	 * Set the name of an attribute of the annotation that specifies whether it is required.
	 * @see #setRequiredParameterValue(boolean)
	 */
	public void setRequiredParameterName(String requiredParameterName) {
		this.requiredParameterName = requiredParameterName;
	}

	/**
	 * Set the boolean value that marks a dependency as required.
	 * <p>For example if using 'required=true' (the default), this value should be
	 * {@code true}; but if using 'optional=false', this value should be {@code false}.
	 * @see #setRequiredParameterName(String)
	 */
	public void setRequiredParameterValue(boolean requiredParameterValue) {
		this.requiredParameterValue = requiredParameterValue;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return this.order;
	}

	// bean工厂必须是ConfigurableListableBeanFactory的
	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		if (!(beanFactory instanceof ConfigurableListableBeanFactory)) {
			throw new IllegalArgumentException(
					"AutowiredAnnotationBeanPostProcessor requires a ConfigurableListableBeanFactory: " + beanFactory);
		}
		this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
	}


	/**
	 * {@link org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor} 接口的实现，
	 * 在 Bean 定义合并后执行后处理操作。
	 * <p>
	 * 此方法用于查找指定 Bean 的自动装配元数据，解析和处理 @Autowired、@Value 和 @Inject，并调用
	 * {@link InjectionMetadata#checkConfigMembers(RootBeanDefinition)} 方法来验证和注册
	 * 自动装配相关的配置成员。
	 * </p>
	 * @param beanDefinition the merged bean definition for the bean   已合并的 Bean 定义
	 * @param beanType the actual type of the managed bean instance   Bean 的类型
	 * @param beanName the name of the bean   Bean 的名称
	 */
	@Override
	public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
		// 查找指定 Bean 的自动装配元数据。
		// 自动装配元数据包含了 Bean 中需要进行自动装配的字段和方法的信息并缓存，例如被 @Autowired、@Value 和 @Inject等注解标记的字段和方法。
		InjectionMetadata metadata = findAutowiringMetadata(beanName, beanType, null);
		//和CommonAnnotationBeanPostProcessor的同名方法具有一样的逻辑
		// 检查配置成员。
		// 该方法会遍历自动装配元数据中包含的注入点，确保这些注入点已经被正确地配置，
		// 并且没有与 Bean 定义中的其他配置产生冲突。
		// 如果某个注入点尚未被外部管理，则将其注册到 Bean 定义中，以避免重复注入。
		metadata.checkConfigMembers(beanDefinition);
	}

	@Override
	public void resetBeanDefinition(String beanName) {
		this.lookupMethodsChecked.remove(beanName);
		this.injectionMetadataCache.remove(beanName);
	}

	/**
	 * 解析@Autowired、@Value、@Inject注解，确定要用于给定 bean 的候选构造器。
	 * 该方法仅被AbstractAutowireCapableBeanFactory#determineConstructorsFromBeanPostProcessors方法调用
	 *
	 * @param beanClass  bean的Class，永远不为null
	 * @param beanName  beanName
	 * @return   候选构造器数组，如果没有就返回null
	 * @throws BeanCreationException
	 */
	@Override
	@Nullable
	public Constructor<?>[] determineCandidateConstructors(Class<?> beanClass, final String beanName)
			throws BeanCreationException {

		// Let's check for lookup methods here...
		// 处理包含@Loopup注解的方法，如果集合中没有beanName，则走一遍bean中的所有方法，过滤是否含有lookup方法
		if (!this.lookupMethodsChecked.contains(beanName)) {
			//确定给定类是否是承载指定注释的候选项（在类型、方法或字段级别）。
			//如果任何一个注解的全路径名都不是以"java."开始，并且该Class全路径名以"start."开始，或者Class的类型为Ordered.class，那么返回false，否则其他情况都返回true
			if (AnnotationUtils.isCandidateClass(beanClass, Lookup.class)) {
				try {
					Class<?> targetClass = beanClass;
					do {
						// 遍历当前类以及所有父类，找出lookup注解的方法进行处理
						ReflectionUtils.doWithLocalMethods(targetClass, method -> {
							// 获取method上的Lookup注解
							Lookup lookup = method.getAnnotation(Lookup.class);
							// 存在此注解的话，就将方法和注解中的内容构建LookupOverride对象，设置进BeanDefinition中
							if (lookup != null) {
								Assert.state(this.beanFactory != null, "No BeanFactory available");
								LookupOverride override = new LookupOverride(method, lookup.value());
								try {
									RootBeanDefinition mbd = (RootBeanDefinition)
											this.beanFactory.getMergedBeanDefinition(beanName);
									//加入methodOverrides内部的overrides集合中
									mbd.getMethodOverrides().addOverride(override);
								}
								catch (NoSuchBeanDefinitionException ex) {
									throw new BeanCreationException(beanName,
											"Cannot apply @Lookup to beans without corresponding bean definition");
								}
							}
						});
						//获取父类Class
						targetClass = targetClass.getSuperclass();
					}
					//遍历父类，直到Object
					while (targetClass != null && targetClass != Object.class);

				}
				catch (IllegalStateException ex) {
					throw new BeanCreationException(beanName, "Lookup method resolution failed", ex);
				}
			}
			// 无论对象中是否含有@Lookup方法，过滤完成后都会放到集合中，证明此bean已经检查完@Lookup注解
			this.lookupMethodsChecked.add(beanName);
		}

		/*
		 * 2 查找候选构造器
		 */
		// Quick check on the concurrent map first, with minimal locking.
		// 从缓存中拿构造函数，不存在的话就进入代码块中再拿一遍，还不存在的话就进行下方的逻辑，
		Constructor<?>[] candidateConstructors = this.candidateConstructorsCache.get(beanClass);
		/*
		 * 如果缓存为null，说明没解析过，那么需要解析
		 * 解析之后会将该类型Class以及对应的结果加入candidateConstructorsCache缓存，后续同类型再来时不会再次解析
		 */
		if (candidateConstructors == null) {
			// Fully synchronized resolution now...
			// 加锁防止并发
			synchronized (this.candidateConstructorsCache) {
				//再检测一遍，双重检测
				candidateConstructors = this.candidateConstructorsCache.get(beanClass);
				/*
				 * 如果缓存为null，说明没解析过，那么需要解析
				 * 如果不为null，那么这一段逻辑就跳过了
				 */
				if (candidateConstructors == null) {
					Constructor<?>[] rawCandidates;
					try {
						//反射获取当前类型的全部构造器数组
						rawCandidates = beanClass.getDeclaredConstructors();
					}
					catch (Throwable ex) {
						throw new BeanCreationException(beanName,
								"Resolution of declared constructors on bean Class [" + beanClass.getName() +
								"] from ClassLoader [" + beanClass.getClassLoader() + "] failed", ex);
					}
					//candidates用于保存满足条件的候选构造器（存在@Inject注解或者@Autowired注解）
					List<Constructor<?>> candidates = new ArrayList<>(rawCandidates.length);
					//必须的构造器（存在@Inject注解，或者存在@Autowired注解并且required属性为true）
					Constructor<?> requiredConstructor = null;
					//默认的构造器（无参构造器）
					Constructor<?> defaultConstructor = null;
					//Kotlin专用，Java用不上
					Constructor<?> primaryConstructor = BeanUtils.findPrimaryConstructor(beanClass);
					// 标识，表示不是合成构造函数的数量
					// 合成构造函数->有方法参数并对实例进行赋值的构造函数
					int nonSyntheticConstructors = 0;
					/*
					 * 循环全部构造器进行解析
					 */
					for (Constructor<?> candidate : rawCandidates) {
						// 构造函数不是合成构造函数，标识累加
						if (!candidate.isSynthetic()) {
							nonSyntheticConstructors++;
						}
						else if (primaryConstructor != null) {
							continue;
						}
						//获取构造器上的自动注入注解的MergedAnnotation，一般是按顺序查找@Autowired、@Value、@Inject注解，找到某一个就返回
						//@Value注解不能标注在构造器上，@Inject很少使用，因此构造器大部分都返回@Autowired的MergedAnnotation（如果标注了@Autowired注解）
						MergedAnnotation<?> ann = findAutowiredAnnotation(candidate);
						//如果不存在@Autowired、@Inject注解
						if (ann == null) {
							//返回给定类的用户定义的类：通常只是给定类即返回原始类，但在 CGLIB 生成的子类的情况下返回原始类。
							Class<?> userClass = ClassUtils.getUserClass(beanClass);
							//如果确实是CGLIB代理的子类，那么解析原始类，看原始类上面有没有自动注入的注解
							if (userClass != beanClass) {
								try {
									// 获取构造方法
									Constructor<?> superCtor =
											userClass.getDeclaredConstructor(candidate.getParameterTypes());
									//继续按顺序查找@Autowired、@Value注解
									ann = findAutowiredAnnotation(superCtor);
								}
								catch (NoSuchMethodException ex) {
									// Simply proceed, no equivalent superclass constructor found...
								}
							}
						}
						/*
						 * 如果存在@Autowired或者@Inject自动注入注解
						 */
						if (ann != null) {
							//如果requiredConstructor不为null，这是不允许的，抛出异常
							//如果存在使用了@Autowired(required = true)或者@Inject注解的构造器，那么就不允许其他构造器上出现任何的自动注入注解
							if (requiredConstructor != null) {
								throw new BeanCreationException(beanName,
										"Invalid autowire-marked constructor: " + candidate +
										". Found constructor with 'required' Autowired annotation already: " +
										requiredConstructor);
							}
							//判断是否是@Inject注解，或者是@Autowired注解并且required属性为true，即判断是否是必须的构造器
							boolean required = determineRequiredStatus(ann);
							// 如果是必须的构造器，为true则将这个构造函数设置为带有依赖项的构造函数并进行判断，不可存在多个带有依赖项的构造函数
							if (required) {
								//这说明如果存在@Autowired(required = true)或者@Inject注解，那么只能在一个构造器上，且其他构造器上不能出现自动注入注解
								if (!candidates.isEmpty()) {
									throw new BeanCreationException(beanName,
											"Invalid autowire-marked constructors: " + candidates +
											". Found constructor with 'required' Autowired annotation: " +
											candidate);
								}
								//那么requiredConstructor等于candidate，即使用了@Autowired(required = true)或者@Inject注解的构造器
								requiredConstructor = candidate;
							}
							//只要存在@Autowired或者@Inject自动注入注解，就会被加入candidates集合
							candidates.add(candidate);
						}
						/*
						 * 否则，表示还是不存在@Autowired或者@Inject自动注入注解
						 *
						 * 判断当前构造器是否是无参构造器，如果是，那么默认构造器就是无参构造器
						 */
						else if (candidate.getParameterCount() == 0) {
							defaultConstructor = candidate;
						}
					}
					/*
					 * 获取最终的candidateConstructors，即候选构造器数组
					 */

					/*
					 * 如果candidates不为空，说明存在具有@Autowired或者@Inject注解的构造器
					 */

					if (!candidates.isEmpty()) {
						// Add default constructor to list of optional constructors, as fallback.
						//如果requiredConstructor为null，即全部都是@Autowired(required = false)的注解
						if (requiredConstructor == null) {
							//如果defaultConstructor不为null，即存在无参构造器
							if (defaultConstructor != null) {
								//那么candidates还要加上无参构造器
								candidates.add(defaultConstructor);
							}
							//否则，如果candidates只有一个元素，即只有一个构造器，那么输出日志
							else if (candidates.size() == 1 && logger.isInfoEnabled()) {
								logger.info("Inconsistent constructor declaration on bean with name '" + beanName +
										"': single autowire-marked constructor flagged as optional - " +
										"this constructor is effectively required since there is no " +
										"default constructor to fall back to: " + candidates.get(0));
							}
						}
						//candidates转换为数组，赋给candidateConstructors
						candidateConstructors = candidates.toArray(new Constructor<?>[0]);
					}
					// 如果只存在一个构造函数，且这个构造函数有参数列表，则使用这个构造函数
					else if (rawCandidates.length == 1 && rawCandidates[0].getParameterCount() > 0) {
						//新建长度为1的数组，加入该构造器，赋给candidateConstructors
						candidateConstructors = new Constructor<?>[] {rawCandidates[0]};
					}
					// 如果非合成构造存在两个且有主构造和默认构造，且主构造和默认构造不相等，则这两个一块使用
					else if (nonSyntheticConstructors == 2 && primaryConstructor != null &&
							defaultConstructor != null && !primaryConstructor.equals(defaultConstructor)) {
						//有两个非合成方法，有优先方法和默认方法，且不相同
						candidateConstructors = new Constructor<?>[] {primaryConstructor, defaultConstructor};
					}
					// 如果只有一个非合成构造且有主构造，使用主构造
					else if (nonSyntheticConstructors == 1 && primaryConstructor != null) {
						//只有一个优先的
						candidateConstructors = new Constructor<?>[] {primaryConstructor};
					}
					// 否则没有能够直接使用的构造
					else {
						//大于2个没注解的构造方法就不知道要用什么了，所以就返回null
						candidateConstructors = new Constructor<?>[0];
					}
					/*
					 * 将对该类型Class及其解析之后的candidateConstructors，存入candidateConstructorsCache缓存，value一定不为null
					 * 后续同类型的Class再来时，不会再次解析
					 */
					this.candidateConstructorsCache.put(beanClass, candidateConstructors);
				}
			}
		}
		//如果缓存不为null则直接到这一步，或者解析了构造器之后，也会到这一步
		//如果candidateConstructors没有数据，就返回null
		return (candidateConstructors.length > 0 ? candidateConstructors : null);
	}

	/**
	 * 完成bean中@Autowired，@Inject，@Value注解的解析并注入的功能
	 * @param pvs      已找到的属性值数组
	 * @param bean     bean实例
	 * @param beanName beanName
	 * @return 要应用于给定 bean 的实际属性值（可以是传递的pvs），或为null（为了兼容原方法，随后会调用postProcessPropertyValues查找）
	 */
	@Override
	public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) {
		//熟悉的findResourceMetadata方法，用于处理方法或者字段上的@Autowired、@Value、@Inject注解
		//前面的applyMergedBeanDefinitionPostProcessors中已被解析了，因此这里直接从缓存中获取
		InjectionMetadata metadata = findAutowiringMetadata(beanName, bean.getClass(), pvs);
		try {
			//调用metadata的inject方法完成注解注入
			metadata.inject(bean, beanName, pvs);
		}
		catch (BeanCreationException ex) {
			throw ex;
		}
		catch (Throwable ex) {
			throw new BeanCreationException(beanName, "Injection of autowired dependencies failed", ex);
		}
		//返回pvs，即原参数
		return pvs;
	}

	@Deprecated
	@Override
	public PropertyValues postProcessPropertyValues(
			PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) {

		return postProcessProperties(pvs, bean, beanName);
	}

	/**
	 * 'Native' processing method for direct calls with an arbitrary target instance,
	 * resolving all of its fields and methods which are annotated with one of the
	 * configured 'autowired' annotation types.
	 * @param bean the target instance to process
	 * @throws BeanCreationException if autowiring failed
	 * @see #setAutowiredAnnotationTypes(Set)
	 */
	public void processInjection(Object bean) throws BeanCreationException {
		Class<?> clazz = bean.getClass();
		InjectionMetadata metadata = findAutowiringMetadata(clazz.getName(), clazz, null);
		try {
			metadata.inject(bean, null, null);
		}
		catch (BeanCreationException ex) {
			throw ex;
		}
		catch (Throwable ex) {
			throw new BeanCreationException(
					"Injection of autowired dependencies failed for class [" + clazz + "]", ex);
		}
	}


	/**
	 * 查找给定 Bean 的自动装配元数据。
	 * <p>
	 * 该方法用于查找给定 Bean 的自动装配元数据，包括带有 {@link org.springframework.beans.factory.annotation.Autowired}、
	 * {@link org.springframework.beans.factory.annotation.Value} 和 {@link javax.inject.Inject} 注解的字段和方法。
	 * 如果启用了缓存，则首先从缓存中查找，如果缓存中不存在，则构建新的元数据并放入缓存。
	 * </p>
	 *
	 * @param beanName Bean 的名称。
	 * @param clazz    Bean 的类型。
	 * @param pvs      Bean 的属性值集合，可能为 null。
	 * @return Bean 的自动装配元数据，包括带有自动装配注解的字段和方法，封装在 {@link InjectionMetadata} 对象中。
	 */
	private InjectionMetadata findAutowiringMetadata(String beanName, Class<?> clazz, @Nullable PropertyValues pvs) {
		// Fall back to class name as cache key, for backwards compatibility with custom callers.
		// 为了向后兼容自定义调用者，回退到使用类名作为缓存 Key。
		String cacheKey = (StringUtils.hasLength(beanName) ? beanName : clazz.getName());
		// Quick check on the concurrent map first, with minimal locking.
		// 从缓存中获取该类的信息
		InjectionMetadata metadata = this.injectionMetadataCache.get(cacheKey);

		// 检查是否需要刷新缓存。如果 metadata 为 null，或者类的类型发生了变化，则需要刷新缓存。
		if (InjectionMetadata.needsRefresh(metadata, clazz)) {

			// 使用 synchronized 块保证线程安全，避免并发构建导致重复工作。
			synchronized (this.injectionMetadataCache) {
				// 再次检查缓存，防止在高并发情况下，其他线程已经构建了元数据并放入缓存。
				metadata = this.injectionMetadataCache.get(cacheKey);
				if (InjectionMetadata.needsRefresh(metadata, clazz)) {
					// 如果 metadata 不为 null，则清除之前的 metadata 信息，为重新构建做准备。
					if (metadata != null) {
						metadata.clear(pvs);
					}
					// 构建新的自动装配元数据。
					metadata = buildAutowiringMetadata(clazz);
					// 将构建的元数据放入缓存。
					this.injectionMetadataCache.put(cacheKey, metadata);
				}
			}
		}
		// 返回从缓存中获取到的元数据。
		return metadata;
	}

	/**
	 *  创建自动装配元数据AutowiringMetadata，实际类型为InjectionMetadata。
	 *  该方法用于扫描指定类及其父类中的字段和方法，查找被 @Autowired、@Value、@Inject标注的注入点注解标记的元素，并将它们封装成 InjectionMetadata 对象。
	 * @param clazz 要扫描的类
	 * @return 包含自动装配信息的 InjectionMetadata 对象。如果类及其父类中没有找到任何 @Autowired、@Value、@Inject标注的注解，则返回 InjectionMetadata.EMPTY。
	 */
	private InjectionMetadata buildAutowiringMetadata(final Class<?> clazz) {
		//确定给定类是否是承载指定注释的候选项（在类型、方法或字段级别）。
		//如果任何一个注解的全路径名都不是以"java."开始，并且该Class全路径名以"start."开始，或者Class的类型为Ordered.class，那么返回false，否则其他情况都返回true
		if (!AnnotationUtils.isCandidateClass(clazz, this.autowiredAnnotationTypes)) {
			//如果不满足，那么直接返回空InjectionMetadata对象
			return InjectionMetadata.EMPTY;
		}

		// 用于存储找到的注入元素 (字段或方法)
		List<InjectionMetadata.InjectedElement> elements = new ArrayList<>();
		//当前正在处理的类
		Class<?> targetClass = clazz;

		// 循环处理当前类及其所有父类，直到到达 Object 类为止。
		do {
			// 用于存储当前类中找到的注入元素
			final List<InjectionMetadata.InjectedElement> currElements = new ArrayList<>();

			// 1. 扫描当前类中的字段
			// 遍历类中的每个属性，判断属性是否包含指定的属性(通过 findAutowiredAnnotation 方法)
			// 如果存在则保存，这里注意，属性保存的类型是 AutowiredFieldElement
			ReflectionUtils.doWithLocalFields(targetClass, field -> {
				//findAutowiredAnnotation用于查找自动注入注解，在前在determineCandidateConstructors部分就讲过了
				//按照顺序查找，只要找到一个自动注入注解就返回，因此优先级@Autowired > @Value > @Inject
				MergedAnnotation<?> ann = findAutowiredAnnotation(field);
				//如果存在自动注入注解
				if (ann != null) {
					// 如果字段是静态的，则不支持 Autowired 注解。
					if (Modifier.isStatic(field.getModifiers())) {
						if (logger.isInfoEnabled()) {
							logger.info("Autowired annotation is not supported on static fields: " + field);
						}
						return;
					}
					//判断是否是必须的，即是否是@Inject、@Value注解，或者是@Autowired注解并且required属性为true
					boolean required = determineRequiredStatus(ann);
					//那么根据当前字段field和required新建一个AutowiredFieldElement，添加到currElements中
					//AutowiredFieldElement表示了一个具有自动注入注解的字段
					currElements.add(new AutowiredFieldElement(field, required));
				}
			});

			// 2. 扫描当前类中的方法
			// 遍历类中的每个方法，判断属性是否包含指定的属性(通过 findAutowiredAnnotation 方法)
			// 如果存在则保存，这里注意，方法保存的类型是 AutowiredMethodElement
			ReflectionUtils.doWithLocalMethods(targetClass, method -> {
				// 处理桥接方法，查找原始方法而非编译器为我们生成的方法，方法不可见就直接返回
				Method bridgedMethod = BridgeMethodResolver.findBridgedMethod(method);
				if (!BridgeMethodResolver.isVisibilityBridgeMethodPair(method, bridgedMethod)) {
					return;
				}
				//findAutowiredAnnotation用于查找自动注入注解，在前在determineCandidateConstructors部分就讲过了
				//按照顺序查找，只要找到一个自动注入注解就返回，因此优先级@Autowired > @Value > @Inject
				MergedAnnotation<?> ann = findAutowiredAnnotation(bridgedMethod);
				// 确保只处理当前类中定义的方法，避免处理父类中的方法两次。
				if (ann != null && method.equals(ClassUtils.getMostSpecificMethod(method, clazz))) {
					//如果方法是静态的，抛出异常
					if (Modifier.isStatic(method.getModifiers())) {
						if (logger.isInfoEnabled()) {
							logger.info("Autowired annotation is not supported on static methods: " + method);
						}
						return;
					}
					// 如果方法没有入参，输出日志，不做任何处理
					if (method.getParameterCount() == 0) {
						if (logger.isInfoEnabled()) {
							logger.info("Autowired annotation should only be used on methods with parameters: " +
									method);
						}
					}
					//判断是否是必须的，即是否是@Inject、@Value注解，或者是@Autowired注解并且required属性为true
					boolean required = determineRequiredStatus(ann);
					// 查找与该方法关联的 PropertyDescriptor (用于 setter 方法)
					PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
					// 创建一个 AutowiredMethodElement 对象，表示要注入的方法
					currElements.add(new AutowiredMethodElement(method, required, pd));
				}
			});

			//currElements集合整体添加到elements集合的开头，即父类的自动注入注解的注入点在前面
			elements.addAll(0, currElements);
			//获取下一个目标类型，是当前类型的父类型
			targetClass = targetClass.getSuperclass();
		}
		//如果目标类型不为null并且不是Object.class类型，那么继续循环，否则结束循环
		while (targetClass != null && targetClass != Object.class);

		//根据找到的elements和Class创建InjectionMetadata对象，如果没有任何注入点元素，那么返回一个空的InjectionMetadata
		return InjectionMetadata.forElements(elements, clazz);
	}

	/**
	 * 获取构造器、方法、字段上的@Autowired、@Value、@Inject注解的MergedAnnotation
	 * 按照顺序查找，因此优先级@Autowired > @Value > @Inject
	 * @param ao   指定元素，可能是构造器、方法、字段
	 * @return   @Autowired、@Value、@Inject注解的MergedAnnotation，没有就返回null
	 */
	@Nullable
	private MergedAnnotation<?> findAutowiredAnnotation(AccessibleObject ao) {
		//创建一个新的MergedAnnotations实例，其中包含指定元素中的所有注解和元注解
		MergedAnnotations annotations = MergedAnnotations.from(ao);
		//遍历是否存在autowiredAnnotationTypes中的类型的注解：按照顺序为@Autowired、@Value、@Inject
		//autowiredAnnotationTypes中的元素在AutowiredAnnotationBeanPostProcessor初始化时就添加进去了
		for (Class<? extends Annotation> type : this.autowiredAnnotationTypes) {
			//尝试获取该类型的注解
			MergedAnnotation<?> annotation = annotations.get(type);
			//如果存在，那么直接返回该注解的MergedAnnotation
			if (annotation.isPresent()) {
				return annotation;
			}
		}
		//返回null
		return null;
	}

	/**
	 * Determine if the annotated field or method requires its dependency.
	 * <p>A 'required' dependency means that autowiring should fail when no beans
	 * are found. Otherwise, the autowiring process will simply bypass the field
	 * or method when no beans are found.
	 * @param ann the Autowired annotation
	 * @return whether the annotation indicates that a dependency is required
	 */
	@SuppressWarnings({"deprecation", "cast"})
	protected boolean determineRequiredStatus(MergedAnnotation<?> ann) {
		// The following (AnnotationAttributes) cast is required on JDK 9+.
		return determineRequiredStatus((AnnotationAttributes)
				ann.asMap(mergedAnnotation -> new AnnotationAttributes(mergedAnnotation.getType())));
	}

	/**
	 * Determine if the annotated field or method requires its dependency.
	 * <p>A 'required' dependency means that autowiring should fail when no beans
	 * are found. Otherwise, the autowiring process will simply bypass the field
	 * or method when no beans are found.
	 * @param ann the Autowired annotation
	 * @return whether the annotation indicates that a dependency is required
	 * @deprecated since 5.2, in favor of {@link #determineRequiredStatus(MergedAnnotation)}
	 */
	@Deprecated
	protected boolean determineRequiredStatus(AnnotationAttributes ann) {
		return (!ann.containsKey(this.requiredParameterName) ||
				this.requiredParameterValue == ann.getBoolean(this.requiredParameterName));
	}

	/**
	 * Obtain all beans of the given type as autowire candidates.
	 * @param type the type of the bean
	 * @return the target beans, or an empty Collection if no bean of this type is found
	 * @throws BeansException if bean retrieval failed
	 */
	protected <T> Map<String, T> findAutowireCandidates(Class<T> type) throws BeansException {
		if (this.beanFactory == null) {
			throw new IllegalStateException("No BeanFactory configured - " +
					"override the getBeanOfType method or specify the 'beanFactory' property");
		}
		return BeanFactoryUtils.beansOfTypeIncludingAncestors(this.beanFactory, type);
	}

	/**
	 * Register the specified bean as dependent on the autowired beans.
	 */
	private void registerDependentBeans(@Nullable String beanName, Set<String> autowiredBeanNames) {
		if (beanName != null) {
			for (String autowiredBeanName : autowiredBeanNames) {
				if (this.beanFactory != null && this.beanFactory.containsBean(autowiredBeanName)) {
					this.beanFactory.registerDependentBean(autowiredBeanName, beanName);
				}
				if (logger.isTraceEnabled()) {
					logger.trace("Autowiring by type from bean name '" + beanName +
							"' to bean named '" + autowiredBeanName + "'");
				}
			}
		}
	}

	/**
	 * 解析指定的缓存方法参数或字段值
	 *
	 * Resolve the specified cached method argument or field value.
	 *
	 * @param beanName       beanName
	 * @param cachedArgument 缓存的cachedFieldValue
	 */
	@Nullable
	private Object resolvedCachedArgument(@Nullable String beanName, @Nullable Object cachedArgument) {
		//如果cachedArgument不为null
		if (cachedArgument instanceof DependencyDescriptor) {
			DependencyDescriptor descriptor = (DependencyDescriptor) cachedArgument;
			Assert.state(this.beanFactory != null, "No BeanFactory available");
			/*
			 * 调用resolveDependency方法根据类型解析依赖，返回找到的依赖
			 *
			 * 这里的descriptor是此前cachedFieldValue缓存起来的，如果是ShortcutDependencyDescriptor类型，那么
			 * 在resolveDependency方法内部的doResolveDependency方法开头就会尝试调用resolveShortcut方法快速获取依赖，
			 * 而这个方法在DependencyDescriptor中默认返回null，而ShortcutDependencyDescriptor则重写了该方法，
			 * 从给定工厂中快速获取具有指定beanName和type的bean实例。因此，上面的ShortcutDependencyDescriptor用于快速获取依赖项
			 */
			return this.beanFactory.resolveDependency(descriptor, beanName, null, null);
		}
		else {
			//直接返回cachedArgument，当第一次调用的value为null并且不是必须依赖，那么缓存的cachedFieldValue也置空null
			//没有找到依赖又不是必须的，那么直接返回null就行了，同样是提升效率
			return cachedArgument;
		}
	}


	/**
	 * 表示自动注入注解（@Autowired、@Value、@Inject）字段的注入信息的类。
	 * Class representing injection information about an annotated field.
	 */
	private class AutowiredFieldElement extends InjectionMetadata.InjectedElement {

		/**
		 * 是否是必须依赖
		 */
		private final boolean required;

		/**
		 * 是否以缓存，即该注入点是否已被解析过
		 */
		private volatile boolean cached = false;

		/**
		 * 缓存的已被解析的字段值，可能是DependencyDescriptor或者ShortcutDependencyDescriptor或者null
		 */
		@Nullable
		private volatile Object cachedFieldValue;

		/**
		 *
		 * @param field 字段
		 * @param required 是否是必须依赖
		 */
		public AutowiredFieldElement(Field field, boolean required) {
			super(field, null);
			this.required = required;
		}

		/**
		 *完成字段注入点的注入操作，包括@Autowired、@Value、@Inject注解的解析
		 * @param bean  需要进行注入的目标实例
		 * @param beanName  beanName
		 * @param pvs  已找到的属性值数组，用于防止重复注入
		 * @throws Throwable
		 */
		@Override
		protected void inject(Object bean, @Nullable String beanName, @Nullable PropertyValues pvs) throws Throwable {
			//获取字段
			Field field = (Field) this.member;
			Object value;
			//如果已被缓存过，只要调用过一次该方法，那么cached将被设置为true，后续都走resolvedCachedArgument
			if (this.cached) {
				//从缓存中获取需要注入的依赖
				// 如果  cachedFieldValue instanceof DependencyDescriptor。则调用 resolveDependency 方法重新加载。
				value = resolvedCachedArgument(beanName, this.cachedFieldValue);
			}
			// 否则调用了 resolveDependency 方法。这个在前篇讲过，在 populateBean 方法中按照类型注入的时候就是通过此方法，
			// 也就是说明了 @Autowired 和 @Inject默认是 按照类型注入的
			else {
				//获取字段描述符，这里的required属性就是@Autowired注解的required属性值，没有就设置默认true
				DependencyDescriptor desc = new DependencyDescriptor(field, this.required);
				desc.setContainingClass(bean.getClass());
				//自动注入的beanName
				Set<String> autowiredBeanNames = new LinkedHashSet<>(1);
				Assert.state(beanFactory != null, "No BeanFactory available");
				// 转换器使用的bean工厂的转换器
				TypeConverter typeConverter = beanFactory.getTypeConverter();
				try {
					/*
					 * 调用resolveDependency方法根据类型解析依赖，返回找到的依赖，查找规则在之前讲过，注意这里的required是可以设置的
					 * 如果required设置为false，那么没找到依赖将不会抛出异常
					 * 如果找到多个依赖，那么会尝试查找最合适的依赖，就掉调用determineAutowireCandidate方法，此前就讲过了
					 * 在最后一步会尝试根据name进行匹配，如果还是不能筛选出合适的依赖，那么抛出异常
					 * 这就是byType优先于byName的原理，实际上一个resolveDependency方法就完成了
					 * 这个接口方法由AutowireCapableBeanFactory提供，它提供了从bean工厂里获取依赖值的能力
					 */
					value = beanFactory.resolveDependency(desc, beanName, autowiredBeanNames, typeConverter);
				}
				catch (BeansException ex) {
					throw new UnsatisfiedDependencyException(null, beanName, new InjectionPoint(field), ex);
				}
				/*同步，设置缓存*/
				synchronized (this) {
					//如果没有被缓存，那么尝试将解析结果加入缓存
					if (!this.cached) {
						// 可以看到value！=null并且required=true才会进行缓存的处理
						if (value != null || this.required) {
							// 这里先缓存一下 desc，如果下面 autowiredBeanNames.size() > 1。则在上面从缓存中获取的时候会重新获取。
							this.cachedFieldValue = desc;
							// 注册依赖bean
							//那么将每一个autowiredBeanName和beanName的依赖关系注册到dependentBeanMap和dependenciesForBeanMap缓存中
							//表示beanName的实例依赖autowiredBeanName的实例
							registerDependentBeans(beanName, autowiredBeanNames);
							//如果只有一个bean，如果字段属性是集合则可能有多个
							// autowiredBeanNames里可能会有别名的名称,所以size可能大于1
							if (autowiredBeanNames.size() == 1) {
								//获取name
								String autowiredBeanName = autowiredBeanNames.iterator().next();
								//如果工厂中包含该bean并且类型匹配
								if (beanFactory.containsBean(autowiredBeanName) &&
										beanFactory.isTypeMatch(autowiredBeanName, field.getType())) {
									/*
									 * 将当前desc描述符、beanName、字段类型存入一个ShortcutDependencyDescriptor对象中，随后赋给cachedFieldValue
									 * 后续查找时将直接获取就有该beanName和字段类型的依赖实例返回
									 */
									this.cachedFieldValue = new ShortcutDependencyDescriptor(
											desc, autowiredBeanName, field.getType());
								}
							}
						}
						else {
							//如果value为null并且不是必须依赖，那么清空缓存
							//下一次走resolvedCachedArgument方法时，直接返回null
							this.cachedFieldValue = null;
						}
						//cached设置为true，表示已解析过，并且设置了缓存
						this.cached = true;
					}
				}
			}
			//如果value不为null
			if (value != null) {
				//设置字段的可访问属性，即field.setAccessible(true)
				// 通过反射，给属性赋值
				ReflectionUtils.makeAccessible(field);
				/*
				 * 反射注入该字段属性的值
				 */
				field.set(bean, value);
			}
		}
	}


	/**
	 * 表示自动注入注解（@Autowired、@Value、@Inject）方法的注入信息的类。
	 * Class representing injection information about an annotated method.
	 */
	private class AutowiredMethodElement extends InjectionMetadata.InjectedElement {
		/**
		 * 是否是必须依赖
		 */
		private final boolean required;

		/**
		 * 是否以缓存，即该注入点是否已被解析过
		 */
		private volatile boolean cached = false;


		/**
		 * 缓存的已被解析的方法参数值数组，单个元素可能是DependencyDescriptor或者ShortcutDependencyDescriptor
		 * 或者cachedMethodArguments就是null
		 */
		@Nullable
		private volatile Object[] cachedMethodArguments;

		/**
		 * 构造方法
		 *
		 * @param method    需要注入的方法
		 * @param required  是否是必须依赖（通常由 @Autowired 的 required 属性决定）
		 * @param pd        属性描述符（PropertyDescriptor），可能为 null
		 */
		public AutowiredMethodElement(Method method, boolean required, @Nullable PropertyDescriptor pd) {
			// 调用父类 InjectedElement 的构造方法，传入方法和属性描述符
			super(method, pd);
			// 设置 required 属性
			this.required = required;
		}

		/**
		 * 完成方法的注入,包括@Autowired、@Value、@Inject注解的解析
		 *
		 * @param bean     需要进行注入的目标实例
		 * @param beanName beanName
		 * @param pvs      已找到的属性值数组，用于防止重复注入
		 * @throws Throwable
		 */
		@Override
		protected void inject(Object bean, @Nullable String beanName, @Nullable PropertyValues pvs) throws Throwable {
			/*
			 * 检查是否可以跳过该"属性"注入，也就是看此前找到的pvs中是否存在该名字的属性，如果存在就跳过，不存在就不跳过
			 * 这里可以发现：
			 *  setter方法的注入流程的优先级为：XML手动设置的property > XML设置自动注入的找到的property -> 注解设置的property
			 *  前面的流程中没找到指定名称的property时，当前流程才会查找property
			 */
			if (checkPropertySkipping(pvs)) {
				return;
			}
			// 获取方法
			Method method = (Method) this.member;
			Object[] arguments;
			//如果已被缓存过，只要调用过一次该方法，那么cached将被设置为true，后续都走resolveCachedArguments
			if (this.cached) {
				// Shortcut for avoiding synchronization...
				// 走缓存的逻辑，获取需要注入的依赖项数组
				arguments = resolveCachedArguments(beanName);
			}
			else {
				// 获取方法的参数的数量，从Spring 容器中获取(缓存中没有则尝试创建)
				int argumentCount = method.getParameterCount();
				//需要注入的参数值数组
				arguments = new Object[argumentCount];
				//新建依赖描述符集合
				DependencyDescriptor[] descriptors = new DependencyDescriptor[argumentCount];
				//自动注入的beanName集合
				Set<String> autowiredBeans = new LinkedHashSet<>(argumentCount);
				Assert.state(beanFactory != null, "No BeanFactory available");
				//获取转换器
				TypeConverter typeConverter = beanFactory.getTypeConverter();
				/*遍历每一个属性，进行注入*/
				for (int i = 0; i < arguments.length; i++) {
					//新建方法参数对象
					MethodParameter methodParam = new MethodParameter(method, i);
					//新建当前参数的描述符对象，这里的required属性就是@Autowired注解的required属性值，没有就设置默认true
					DependencyDescriptor currDesc = new DependencyDescriptor(methodParam, this.required);
					currDesc.setContainingClass(bean.getClass());
					//加入到descriptors集合中
					descriptors[i] = currDesc;
					try {
						/*
						 * 根据类型从容器中获取
						 * 调用resolveDependency方法根据类型解析依赖，返回找到的依赖，查找规则在之前讲过，注意这里的required是可以设置的
						 * 如果required设置为false，那么没找到依赖将不会抛出异常
						 * 如果找到多个依赖，那么会尝试查找最合适的依赖，就掉调用determineAutowireCandidate方法，此前就讲过了
						 * 在最后一步会尝试根据name进行匹配，如果还是不能筛选出合适的依赖，那么抛出异常
						 * 这就是byType优先于byName的原理，实际上一个resolveDependency方法就完成了
						 */
						Object arg = beanFactory.resolveDependency(currDesc, beanName, autowiredBeans, typeConverter);
						//如果返回null并且是非必须依赖
						if (arg == null && !this.required) {
							//参数值数组置为null
							arguments = null;
							//直接结束循环
							break;
						}
						//否则，对应位置存入找到的依赖项
						arguments[i] = arg;
					}
					catch (BeansException ex) {
						throw new UnsatisfiedDependencyException(null, beanName, new InjectionPoint(methodParam), ex);
					}
				}
				/*同步，设置缓存，类似于AutowiredFieldElement中的步骤*/
				synchronized (this) {
					//如果没有被缓存，那么尝试将解析结果加入缓存
					if (!this.cached) {
						//如果arguments数组不为null
						if (arguments != null) {
							//拷贝描述符数组
							DependencyDescriptor[] cachedMethodArguments = Arrays.copyOf(descriptors, arguments.length);
							///将每一个autowiredBeanName和beanName的依赖关系注册到dependentBeanMap和dependenciesForBeanMap缓存中
							//表示beanName的实例依赖autowiredBeanName的实例，这个方法我们在前面就讲过了
							registerDependentBeans(beanName, autowiredBeans);
							//如果依赖bean等于参数个数，即一个参数只会注入一个依赖
							if (autowiredBeans.size() == argumentCount) {
								Iterator<String> it = autowiredBeans.iterator();
								Class<?>[] paramTypes = method.getParameterTypes();
								for (int i = 0; i < paramTypes.length; i++) {
									//获取依赖beanName
									String autowiredBeanName = it.next();
									//如果工厂中包含该bean并且类型匹配
									if (beanFactory.containsBean(autowiredBeanName) &&
											beanFactory.isTypeMatch(autowiredBeanName, paramTypes[i])) {
										/*
										 * 将当前desc描述符、beanName、字段类型存入一个ShortcutDependencyDescriptor对象中，随后赋给cachedMethodArguments
										 * 的对应索引位置，后续查找时将直接获取就有该beanName和字段类型的依赖实例返回
										 */
										cachedMethodArguments[i] = new ShortcutDependencyDescriptor(
												descriptors[i], autowiredBeanName, paramTypes[i]);
									}
								}
							}
							//将cachedMethodArguments赋值给cachedMethodArguments对象变量，方便后续查找
							this.cachedMethodArguments = cachedMethodArguments;
						}
						else {
							//如果arguments为null并且不是必须依赖，那么清空缓存
							//下一次走resolveCachedArguments方法时，直接返回null
							this.cachedMethodArguments = null;
						}
						this.cached = true;
					}
				}
			}
			//如果arguments不为null
			if (arguments != null) {
				try {
					//设置方法的可访问属性，即method.setAccessible(true)
					ReflectionUtils.makeAccessible(method);
					/*
					 * 反射回调方法，采用获取的依赖项作为参数
					 */
					method.invoke(bean, arguments);
				}
				catch (InvocationTargetException ex) {
					throw ex.getTargetException();
				}
			}
		}

		/**
		 * 解析指定的已缓存的方法参数，快速可获取需要注入的依赖项数组
		 *
		 * @param beanName beanName
		 * @return 需要注入的依赖项数组
		 */
		@Nullable
		private Object[] resolveCachedArguments(@Nullable String beanName) {
			//获取缓存
			Object[] cachedMethodArguments = this.cachedMethodArguments;
			//如果为null，这是第一次解析依赖返回null并且是非必须依赖的情况，这种情况直接返回null
			if (cachedMethodArguments == null) {
				return null;
			}
			//存储找到的依赖项
			Object[] arguments = new Object[cachedMethodArguments.length];
			//遍历
			for (int i = 0; i < arguments.length; i++) {
				//调用resolvedCachedArgument方法，查找每一个cachedMethodArguments的描述符，将返回的依赖项存入对应的arguments的索引位置
				arguments[i] = resolvedCachedArgument(beanName, cachedMethodArguments[i]);
			}
			return arguments;
		}
	}


	/**
	 * DependencyDescriptor variant with a pre-resolved target bean name.
	 */
	@SuppressWarnings("serial")
	private static class ShortcutDependencyDescriptor extends DependencyDescriptor {
		/**
		 * 缓存的beanName
		 */
		private final String shortcut;

		/**
		 * 缓存的type
		 */
		private final Class<?> requiredType;

		/**
		 * 构造器
		 */
		public ShortcutDependencyDescriptor(DependencyDescriptor original, String shortcut, Class<?> requiredType) {
			super(original);
			this.shortcut = shortcut;
			this.requiredType = requiredType;
		}

		/**
		 *  从给定工厂中快速获取具有指定beanName和type的bean实例
		 * @param beanFactory the associated factory  联合工厂
		 * @return
		 */
		@Override
		public Object resolveShortcut(BeanFactory beanFactory) {
			return beanFactory.getBean(this.shortcut, this.requiredType);
		}
	}

}
