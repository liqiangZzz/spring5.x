/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.xml;

import org.w3c.dom.Element;

import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

/**
 * Abstract {@link BeanDefinitionParser} implementation providing
 * a number of convenience methods and a
 * {@link AbstractBeanDefinitionParser#parseInternal template method}
 * that subclasses must override to provide the actual parsing logic.
 *
 * <p>Use this {@link BeanDefinitionParser} implementation when you want
 * to parse some arbitrarily complex XML into one or more
 * {@link BeanDefinition BeanDefinitions}. If you just want to parse some
 * XML into a single {@code BeanDefinition}, you may wish to consider
 * the simpler convenience extensions of this class, namely
 * {@link AbstractSingleBeanDefinitionParser} and
 * {@link AbstractSimpleBeanDefinitionParser}.
 *
 * @author Rob Harrop
 * @author Juergen Hoeller
 * @author Rick Evans
 * @author Dave Syer
 * @since 2.0
 */
public abstract class AbstractBeanDefinitionParser implements BeanDefinitionParser {

	/**
	 * "id"属性的常量
	 * Constant for the "id" attribute.
	 */
	public static final String ID_ATTRIBUTE = "id";

	/**
	 * “name”属性的常量
	 * Constant for the "name" attribute.
	 */
	public static final String NAME_ATTRIBUTE = "name";

	/**
	 * 基于模版方法模式，BeanDefinitionParser的parse方法（扩展标签的解析方法）的抽象骨干实现
	 *
	 * @param element the element that is to be parsed into one or more {@link BeanDefinition BeanDefinitions}  标签元素
	 * @param parserContext the object encapsulating the current state of the parsing process;
	 * provides access to a {@link org.springframework.beans.factory.support.BeanDefinitionRegistry}  解析上下文
	 * @return  解析后的bean定义
	 */
	@Override
	@Nullable
	public final BeanDefinition parse(Element element, ParserContext parserContext) {
		/*
		 * 1 调用parseInternal方法解析标签获取bean定义，这个方法是子类实现的
		 */
		AbstractBeanDefinition definition = parseInternal(element, parserContext);
		//判断解析得到的BeanDefinition对象是否为空，且当前解析的元素是否为顶层元素
		if (definition != null && !parserContext.isNested()) {
			try {
				/*
				 * 2 解析id属性，如果需要Spring自动生成，那么使用DefaultBeanNameGenerator生成器
				 */
				String id = resolveId(element, definition, parserContext);
				if (!StringUtils.hasText(id)) {
					parserContext.getReaderContext().error(
							"Id is required for element '" + parserContext.getDelegate().getLocalName(element)
									+ "' when used as a top-level tag", element);
				}
				/*
				 * 3 别名处理
				 */
				String[] aliases = null;
				//判断是否需要将name属性解析为别名，默认true
				if (shouldParseNameAsAliases()) {
					// 从element的 name属性中获取 name 。
					String name = element.getAttribute(NAME_ATTRIBUTE);
					//如果具有该属性
					if (StringUtils.hasLength(name)) {
						//将name属性值按照","分隔成为一个数组，去除前后空白，作为别名
						aliases = StringUtils.trimArrayElements(StringUtils.commaDelimitedListToStringArray(name));
					}
				}
				// 将AbstractBeanDefinition转换为BeanDefinitionHolder并注册
				BeanDefinitionHolder holder = new BeanDefinitionHolder(definition, id, aliases);
				/*
				 * 4 注册bean定义，registerBeanDefinition方法
				 */
				registerBeanDefinition(holder, parserContext.getRegistry());
				/*
				 * 5 发布组件注册事件
				 */
				// 判断是否需要触发事件，若是则创建BeanComponentDefinition对象，进行后处理，并注册到监听器中。
				if (shouldFireEvents()) {
					// 通知监听器进行处理
					BeanComponentDefinition componentDefinition = new BeanComponentDefinition(holder);
					postProcessComponentDefinition(componentDefinition);
					//发布组件注册事件
					parserContext.registerComponent(componentDefinition);
				}
			}
			catch (BeanDefinitionStoreException ex) {
				String msg = ex.getMessage();
				parserContext.getReaderContext().error((msg != null ? msg : ex.toString()), element);
				return null;
			}
		}
		//返回bean定义
		return definition;
	}

	/**
	 * 该函数用于解析给定BeanDefinition的ID。
	 * 首先检查是否应该自动生成ID，如果是，则使用parserContext生成一个bean名称并返回。
	 * 如果不应该自动生成ID，则从element的"id"属性中获取ID。
	 * 如果ID为空并且允许使用回退生成ID，则再次使用parserContext生成一个bean名称并返回。
	 * <p>
	 * Resolve the ID for the supplied {@link BeanDefinition}.
	 * <p>When using {@link #shouldGenerateId generation}, a name is generated automatically.
	 * Otherwise, the ID is extracted from the "id" attribute, potentially with a
	 * {@link #shouldGenerateIdAsFallback() fallback} to a generated id.
	 * @param element the element that the bean definition has been built from  构建bean定义的元素
	 * @param definition the bean definition to be registered    要注册的bean定义
	 * @param parserContext the object encapsulating the current state of the parsing process; 包含了解析过程的状态信息，提供了访问BeanDefinitionRegistry的能力，用于注册bean定义。
	 * provides access to a {@link org.springframework.beans.factory.support.BeanDefinitionRegistry}
	 * @return the resolved id 已解析的id
	 * @throws BeanDefinitionStoreException if no unique name could be generated
	 * for the given bean definition
	 */
	protected String resolveId(Element element, AbstractBeanDefinition definition, ParserContext parserContext)
			throws BeanDefinitionStoreException {
		// 检查是否应该自动生成ID
		if (shouldGenerateId()) {
			// 使用parserContext生成一个bean名称，并返回。
			return parserContext.getReaderContext().generateBeanName(definition);
		}
		// 如果不应该自动生成ID
		else {
			// 从element的id属性中获取ID。
			String id = element.getAttribute(ID_ATTRIBUTE);
			//检查ID是否为空，如果为空并且允许使用回退生成ID
			if (!StringUtils.hasText(id) && shouldGenerateIdAsFallback()) {
				// 如果允许，再次使用parserContext生成一个bean名称，并返回。
				id = parserContext.getReaderContext().generateBeanName(definition);
			}
			return id;
		}
	}

	/**
	 * Register the supplied {@link BeanDefinitionHolder bean} with the supplied
	 * {@link BeanDefinitionRegistry registry}.
	 * <p>Subclasses can override this method to control whether or not the supplied
	 * {@link BeanDefinitionHolder bean} is actually even registered, or to
	 * register even more beans.
	 * <p>The default implementation registers the supplied {@link BeanDefinitionHolder bean}
	 * with the supplied {@link BeanDefinitionRegistry registry} only if the {@code isNested}
	 * parameter is {@code false}, because one typically does not want inner beans
	 * to be registered as top level beans.
	 * @param definition the bean definition to be registered
	 * @param registry the registry that the bean is to be registered with
	 * @see BeanDefinitionReaderUtils#registerBeanDefinition(BeanDefinitionHolder, BeanDefinitionRegistry)
	 */
	protected void registerBeanDefinition(BeanDefinitionHolder definition, BeanDefinitionRegistry registry) {
		BeanDefinitionReaderUtils.registerBeanDefinition(definition, registry);
	}


	/**
	 * Central template method to actually parse the supplied {@link Element}
	 * into one or more {@link BeanDefinition BeanDefinitions}.
	 * @param element the element that is to be parsed into one or more {@link BeanDefinition BeanDefinitions}
	 * @param parserContext the object encapsulating the current state of the parsing process;
	 * provides access to a {@link org.springframework.beans.factory.support.BeanDefinitionRegistry}
	 * @return the primary {@link BeanDefinition} resulting from the parsing of the supplied {@link Element}
	 * @see #parse(org.w3c.dom.Element, ParserContext)
	 * @see #postProcessComponentDefinition(org.springframework.beans.factory.parsing.BeanComponentDefinition)
	 */
	@Nullable
	protected abstract AbstractBeanDefinition parseInternal(Element element, ParserContext parserContext);

	/**
	 * 该函数是一个protected类型的函数，名为shouldGenerateId，返回值为boolean类型。函数的默认实现是返回false。
	 * 子类可以重写该函数来启用ID生成。当该函数返回true时，解析器将始终生成ID，而不会检查元素中的"id"属性。
	 * <p>
	 * Should an ID be generated instead of read from the passed in {@link Element}?
	 * <p>Disabled by default; subclasses can override this to enable ID generation.
	 * Note that this flag is about <i>always</i> generating an ID; the parser
	 * won't even check for an "id" attribute in this case.
	 * @return whether the parser should always generate an id
	 */
	protected boolean shouldGenerateId() {
		return false;
	}

	/**
	 * 该函数是一个protected函数，其功能是判断当传入的Element对象没有明确指定"id"属性时，
	 * 是否应该生成一个id作为后备。默认情况下，该函数返回false，表示不生成id作为后备。
	 * 子类可以重写该函数来启用id生成作为后备。在启用的情况下，解析器首先会检查Element对象是否指定了"id"属性，
	 * 如果没有指定，则会回退到生成一个id。该函数返回一个布尔值，表示解析器是否应该生成一个id作为后备。
	 * <p>
	 * Should an ID be generated instead if the passed in {@link Element} does not
	 * specify an "id" attribute explicitly?
	 * <p>Disabled by default; subclasses can override this to enable ID generation
	 * as fallback: The parser will first check for an "id" attribute in this case,
	 * only falling back to a generated ID if no value was specified.
	 * @return whether the parser should generate an id if no id was specified
	 */
	protected boolean shouldGenerateIdAsFallback() {
		return false;
	}

	/**
	 * 该函数是一个protected函数，其功能是判断元素的"name"属性是否应被解析为bean定义别名，
	 * 即替代的bean定义名称。默认情况下，该函数返回true。该函数是在4.1.5版本之后加入的
	 * <p>
	 * Determine whether the element's "name" attribute should get parsed as
	 * bean definition aliases, i.e. alternative bean definition names.
	 * <p>The default implementation returns {@code true}.
	 * @return whether the parser should evaluate the "name" attribute as aliases
	 * @since 4.1.5
	 */
	protected boolean shouldParseNameAsAliases() {
		return true;
	}

	/**
	 * 该函数是一个protected函数，返回一个布尔值。
	 * 其作用是确定在解析完bean定义后，是否应该触发一个{@link org.springframework.beans.factory.parsing.BeanComponentDefinition}事件。
	 * 默认情况下，该函数返回true，即会触发事件。如果想要抑制事件的触发，可以在子类中重写该函数，返回false。
	 * 该函数主要与 postProcessComponentDefinition函数 和 ReaderContext.fireComponentRegistered函数 配合使用。
	 * <p>
	 * Determine whether this parser is supposed to fire a
	 * {@link org.springframework.beans.factory.parsing.BeanComponentDefinition}
	 * event after parsing the bean definition.
	 * <p>This implementation returns {@code true} by default; that is,
	 * an event will be fired when a bean definition has been completely parsed.
	 * Override this to return {@code false} in order to suppress the event.
	 * @return {@code true} in order to fire a component registration event
	 * after parsing the bean definition; {@code false} to suppress the event
	 * @see #postProcessComponentDefinition
	 * @see org.springframework.beans.factory.parsing.ReaderContext#fireComponentRegistered
	 */
	protected boolean shouldFireEvents() {
		return true;
	}

	/**
	 * Hook method called after the primary parsing of a
	 * {@link BeanComponentDefinition} but before the
	 * {@link BeanComponentDefinition} has been registered with a
	 * {@link org.springframework.beans.factory.support.BeanDefinitionRegistry}.
	 * <p>Derived classes can override this method to supply any custom logic that
	 * is to be executed after all the parsing is finished.
	 * <p>The default implementation is a no-op.
	 * @param componentDefinition the {@link BeanComponentDefinition} that is to be processed
	 */
	protected void postProcessComponentDefinition(BeanComponentDefinition componentDefinition) {
	}

}
