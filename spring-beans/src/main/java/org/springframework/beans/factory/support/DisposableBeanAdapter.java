/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.support;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

/**
 * 实际一次性Bean和可运行接口适配器，对给定Bean实例执行各种销毁步骤
 *
 * Adapter that implements the {@link DisposableBean} and {@link Runnable}
 * interfaces performing various destruction steps on a given bean instance:
 * <ul>
 * <li>DestructionAwareBeanPostProcessors;
 * <li>the bean implementing DisposableBean itself;
 * <li>a custom destroy method specified on the bean definition.
 * </ul>
 *
 * @author Juergen Hoeller
 * @author Costin Leau
 * @author Stephane Nicoll
 * @since 2.0
 * @see AbstractBeanFactory
 * @see org.springframework.beans.factory.DisposableBean
 * @see org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor
 * @see AbstractBeanDefinition#getDestroyMethodName()
 */
@SuppressWarnings("serial")
class DisposableBeanAdapter implements DisposableBean, Runnable, Serializable {

	/**
	 * 表示close方法名
	 */
	private static final String CLOSE_METHOD_NAME = "close";

	/**
	 * 表示shutdown方法名
	 */
	private static final String SHUTDOWN_METHOD_NAME = "shutdown";

	private static final Log logger = LogFactory.getLog(DisposableBeanAdapter.class);


	/**
	 * 保存bean实例
	 */
	private final Object bean;

	/**
	 * 保存beanName
	 */
	private final String beanName;

	/**
	 * bean是否是DisposableBean实例&&'destroy'没有受外部管理的销毁方法的标记
	 */
	private final boolean invokeDisposableBean;

	/**
	 * beanDefinition 是否允许访问非公共构造函数和方法标记
	 */
	private final boolean nonPublicAccessAllowed;

	@Nullable
	private final AccessControlContext acc;

	/**
	 * 销毁方法名，来自于自动推断或者设置的destroy-method属性
	 */
	@Nullable
	private String destroyMethodName;

	/**
	 * 根据方法名获取的销毁方法
	 */
	@Nullable
	private transient Method destroyMethod;

	/**
	 * 处理器列表中的所有DestructionAwareBeanPostProcessors类型的并且支持处理当前bean实例的处理器列表
	 */
	@Nullable
	private List<DestructionAwareBeanPostProcessor> beanPostProcessors;


	/**
	 * 使用给定的bean实例创建新的DisposableBeanAdapter
	 *
	 * Create a new DisposableBeanAdapter for the given bean.
	 * @param bean the bean instance (never {@code null})    bean 实例（从不为null ）
	 * @param beanName the name of the bean  beanName
	 * @param beanDefinition the merged bean definition  已合并的bean定义
	 * @param postProcessors the List of BeanPostProcessors
	 * (potentially DestructionAwareBeanPostProcessor), if any  已注册的BeanPostProcessor集合，可能包含DestructionAwareBeanPostProcessor
	 */
	public DisposableBeanAdapter(Object bean, String beanName, RootBeanDefinition beanDefinition,
			List<BeanPostProcessor> postProcessors, @Nullable AccessControlContext acc) {

		Assert.notNull(bean, "Disposable bean must not be null");
		this.bean = bean;
		this.beanName = beanName;
		/*
		 * 如果bean实例属于DisposableBean类型，并且@PreDestroy注解的标注的方法中没有名为"destroy"的方法
		 * 那么表示需要回调DisposableBean的destroy方法，invokeDisposableBean设置为true，否则设置为false
		 * 这里检查externallyManagedDestroyMethods集合的目的同样是防止同一个销毁方法的重复调用
		 */
		this.invokeDisposableBean =
				(this.bean instanceof DisposableBean && !beanDefinition.isExternallyManagedDestroyMethod("destroy"));
		// beanDefinition是否允许访问非公共构造函数和方法
		this.nonPublicAccessAllowed = beanDefinition.isNonPublicAccessAllowed();
		this.acc = acc;
		//推断destroyMethod方法名
		String destroyMethodName = inferDestroyMethodIfNecessary(bean, beanDefinition);
		//如果找到了destroyMethod方法
		//并且（不需要回调DisposableBean的destroy方法，或者destroyMethodName不等于"destroy"），防止同一个销毁方法的重复调用
		//并且@PreDestroy注解的标注的方法中没有名为"destroy"的方法，用于防止同一个销毁方法的重复调用
		if (destroyMethodName != null && !(this.invokeDisposableBean && "destroy".equals(destroyMethodName)) &&
				!beanDefinition.isExternallyManagedDestroyMethod(destroyMethodName)) {
			//设置destroyMethodName属性
			this.destroyMethodName = destroyMethodName;
			//获取destroyMethod方法对象，根据beanDefinition是否允许访问非公共构造函数和方法的情况来查找最小参数(最好是none)的销毁方法对象
			Method destroyMethod = determineDestroyMethod(destroyMethodName);
			//没找到该方法，并且没有默认的销毁方法，即没有default-destroy-method属性，那么抛出异常
			if (destroyMethod == null) {
				// 如果beanDefinition配置的destroy方法为默认方法
				if (beanDefinition.isEnforceDestroyMethod()) {
					throw new BeanDefinitionValidationException("Could not find a destroy method named '" +
							destroyMethodName + "' on bean with name '" + beanName + "'");
				}
			}
			else {
				// 获取destroyMethod的参数类型数组
				Class<?>[] paramTypes = destroyMethod.getParameterTypes();
				//如果参数个数大于1，那么抛出异常
				if (paramTypes.length > 1) {
					throw new BeanDefinitionValidationException("Method '" + destroyMethodName + "' of bean '" +
							beanName + "' has more than one parameter - not supported as destroy method");
				}
				//如果参数个数为1，并且不是boolean类型，那么同样抛出异常
				else if (paramTypes.length == 1 && boolean.class != paramTypes[0]) {
					throw new BeanDefinitionValidationException("Method '" + destroyMethodName + "' of bean '" +
							beanName + "' has a non-boolean parameter - not supported as destroy method");
				}
				// 获取destroyMethod相应的接口方法对象，如果找不到，则返回原始方法
				destroyMethod = ClassUtils.getInterfaceMethodIfPossible(destroyMethod);
			}
			//设置方法
			this.destroyMethod = destroyMethod;
		}
		/*
		 * 过滤PostProcessor
		 * 搜索后处理器列表中的所有DestructionAwareBeanPostProcessors类型的并且支持处理当前bean实例的处理器列表
		 */
		this.beanPostProcessors = filterPostProcessors(postProcessors, bean);
	}

	/**
	 * Create a new DisposableBeanAdapter for the given bean.
	 * @param bean the bean instance (never {@code null})
	 * @param postProcessors the List of BeanPostProcessors
	 * (potentially DestructionAwareBeanPostProcessor), if any
	 */
	public DisposableBeanAdapter(Object bean, List<BeanPostProcessor> postProcessors, AccessControlContext acc) {
		Assert.notNull(bean, "Disposable bean must not be null");
		this.bean = bean;
		this.beanName = bean.getClass().getName();
		this.invokeDisposableBean = (this.bean instanceof DisposableBean);
		this.nonPublicAccessAllowed = true;
		this.acc = acc;
		this.beanPostProcessors = filterPostProcessors(postProcessors, bean);
	}

	/**
	 * Create a new DisposableBeanAdapter for the given bean.
	 */
	private DisposableBeanAdapter(Object bean, String beanName, boolean invokeDisposableBean,
			boolean nonPublicAccessAllowed, @Nullable String destroyMethodName,
			@Nullable List<DestructionAwareBeanPostProcessor> postProcessors) {

		this.bean = bean;
		this.beanName = beanName;
		this.invokeDisposableBean = invokeDisposableBean;
		this.nonPublicAccessAllowed = nonPublicAccessAllowed;
		this.acc = null;
		this.destroyMethodName = destroyMethodName;
		this.beanPostProcessors = postProcessors;
	}


	/**
	 * 推断destroyMethod方法名
	 * 1 如果没设置destroy-method属性（值为null或者""）那么返回null
	 * 2 如果设置了destroy-method属性：
	 * 2.1 如果值为"(inferred)"，那么按照顺序先后尝试获取"close"以及"shutdown"方法名返回，前提是这两个方法至少存在一个，无法推断则返回null
	 * 2.2 如果是其他之值，那么直接返回设置的值
	 *
	 * If the current value of the given beanDefinition's "destroyMethodName" property is
	 * {@link AbstractBeanDefinition#INFER_METHOD}, then attempt to infer a destroy method.
	 * Candidate methods are currently limited to public, no-arg methods named "close" or
	 * "shutdown" (whether declared locally or inherited). The given BeanDefinition's
	 * "destroyMethodName" is updated to be null if no such method is found, otherwise set
	 * to the name of the inferred method. This constant serves as the default for the
	 * {@code @Bean#destroyMethod} attribute and the value of the constant may also be
	 * used in XML within the {@code <bean destroy-method="">} or {@code
	 * <beans default-destroy-method="">} attributes.
	 * <p>Also processes the {@link java.io.Closeable} and {@link java.lang.AutoCloseable}
	 * interfaces, reflectively calling the "close" method on implementing beans as well.
	 *
	 * @param bean           bean实例
	 * @param beanDefinition bean定义
	 * @return destroyMethodName
	 */
	@Nullable
	private String inferDestroyMethodIfNecessary(Object bean, RootBeanDefinition beanDefinition) {
		//获取destroyMethodName属性，就是XML的destroy-method属性
		String destroyMethodName = beanDefinition.getDestroyMethodName();
		//如果destroyMethodName值为"(inferred)"，表示需要Spring进行销毁方法的自动推断
		//或者destroyMethodName值为null，并且bean属于AutoCloseable，表示存在close方法（来自AutoCloseable接口）
		if (AbstractBeanDefinition.INFER_METHOD.equals(destroyMethodName) ||
				(destroyMethodName == null && bean instanceof AutoCloseable)) {
			// Only perform destroy method inference or Closeable detection
			// in case of the bean not explicitly implementing DisposableBean
			//如果bean不属于DisposableBean类型，那么推断销毁方法以及进行Closeable的检测
			if (!(bean instanceof DisposableBean)) {
				try {
					//首先尝试值获取close方法的方法名（简单名字），作为销毁回调方法的名字
					//如果不存在该方法，会抛出异常
					return bean.getClass().getMethod(CLOSE_METHOD_NAME).getName();
				}
				// 如果没有找到
				catch (NoSuchMethodException ex) {
					try {
						//如果抛出了异常
						//继续尝试值获取shutdown方法的方法名（简单名字），作为销毁回调方法的名字
						return bean.getClass().getMethod(SHUTDOWN_METHOD_NAME).getName();
					}
					catch (NoSuchMethodException ex2) {
						// no candidate destroy method found
						//如果还是抛出异常，那么表示没有推断出候选销毁方法，但是该异常不会抛出
					}
				}
			}
			// 没有找到候选的销毁方法时，返回null
			return null;
		}
		//判断destroyMethodName属性是否不为null以及""，即是否设置了destroy-method属性，如果是那么返回设置的方法名，否则返回null
		return (StringUtils.hasLength(destroyMethodName) ? destroyMethodName : null);
	}

	/**
	 * 搜索后处理器列表中的所有DestructionAwareBeanPostProcessors类型的并且支持处理当前bean实例的处理器列表并返回
	 *
	 * Search for all DestructionAwareBeanPostProcessors in the List.
	 * @param processors the List to search  要搜索的列表
	 * @return the filtered List of DestructionAwareBeanPostProcessors   过滤的DestructionAwareBeanPostProcessors列表
	 */
	@Nullable
	private List<DestructionAwareBeanPostProcessor> filterPostProcessors(List<BeanPostProcessor> processors, Object bean) {
		// 定义已过滤出DestructionAwareBeanPostProcessors的列表
		List<DestructionAwareBeanPostProcessor> filteredPostProcessors = null;
		// 如果processors不为空列表
		if (!CollectionUtils.isEmpty(processors)) {
			// 实例化filteredPostProcessors
			filteredPostProcessors = new ArrayList<>(processors.size());
			// 遍历processors
			for (BeanPostProcessor processor : processors) {
				//如果是DestructionAwareBeanPostProcessor类型，并且当前后处理器支持处理该bean实例，那么加入filteredPostProcessors集合中
				if (processor instanceof DestructionAwareBeanPostProcessor) {
					DestructionAwareBeanPostProcessor dabpp = (DestructionAwareBeanPostProcessor) processor;
					// 如果bean是否需要由这个后处理其销毁
					if (dabpp.requiresDestruction(bean)) {
						// 将dabpp添加到filteredPostProcessors中
						filteredPostProcessors.add(dabpp);
					}
				}
			}
		}
		return filteredPostProcessors;
	}


	@Override
	public void run() {
		destroy();
	}

	@Override
	public void destroy() {
		if (!CollectionUtils.isEmpty(this.beanPostProcessors)) {
			for (DestructionAwareBeanPostProcessor processor : this.beanPostProcessors) {
				processor.postProcessBeforeDestruction(this.bean, this.beanName);
			}
		}

		if (this.invokeDisposableBean) {
			if (logger.isTraceEnabled()) {
				logger.trace("Invoking destroy() on bean with name '" + this.beanName + "'");
			}
			try {
				if (System.getSecurityManager() != null) {
					AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () -> {
						((DisposableBean) this.bean).destroy();
						return null;
					}, this.acc);
				}
				else {
					((DisposableBean) this.bean).destroy();
				}
			}
			catch (Throwable ex) {
				String msg = "Invocation of destroy method failed on bean with name '" + this.beanName + "'";
				if (logger.isDebugEnabled()) {
					logger.warn(msg, ex);
				}
				else {
					logger.warn(msg + ": " + ex);
				}
			}
		}

		if (this.destroyMethod != null) {
			invokeCustomDestroyMethod(this.destroyMethod);
		}
		else if (this.destroyMethodName != null) {
			Method methodToInvoke = determineDestroyMethod(this.destroyMethodName);
			if (methodToInvoke != null) {
				invokeCustomDestroyMethod(ClassUtils.getInterfaceMethodIfPossible(methodToInvoke));
			}
		}
	}


	/**
	 *根据beanDefinition是否允许访问非公共构造函数和方法的情况来查找最小参数(最好是none)的销毁方法对象
	 * @param name 销毁方法名
	 */
	@Nullable
	private Method determineDestroyMethod(String name) {
		try {
			// 如果安全管理器不为null
			if (System.getSecurityManager() != null) {
				// 以特权方式根据beanDefinition是否允许访问非公共构造函数和方法的情况来查找最小参数(最好是none)的销毁方法对象
				return AccessController.doPrivileged((PrivilegedAction<Method>) () -> findDestroyMethod(name));
			}
			else {
				// 根据beanDefinition是否允许访问非公共构造函数和方法的情况来查找最小参数(最好是none)的销毁方法对象
				return findDestroyMethod(name);
			}
		}
		catch (IllegalArgumentException ex) {
			// 捕捉查找方法对象时抛出的非法参数异常
			// 重新抛出BeanDefinition验证异常：不能找到唯一销毁方法在名为'beanName'的Bean对象中：ex异常信息
			throw new BeanDefinitionValidationException("Could not find unique destroy method on bean with name '" +
					this.beanName + ": " + ex.getMessage());
		}
	}

	/**
	 * 根据beanDefinition是否允许访问非公共构造函数和方法的情况来查找最小参数(最好是none)的销毁方法对象
	 * @param name 销毁方法名
	 * @return 销毁方法对象
	 */
	@Nullable
	private Method findDestroyMethod(String name) {
		// findMethodWithMinimalParameters(Class<?> clazz,String methodName)：找到一个具有给定方法名和最小参数(最好是none)的
		// 方法，在给定的类或它的超类中声明。 首选公共方法，但也将返回受保护的包访问或私有访问
		// BeanUtils.findMethodWithMinimalParameters(Method[] methods, String methodName)：在给定方法列表中找到一个具有给定
		// 方法名和最小参数(最好是无)的方法
		// 根据beanDefinition是否允许访问非公共构造函数和方法的情况来查找最小参数(最好是none)的销毁方法对象
		return (this.nonPublicAccessAllowed ?
				BeanUtils.findMethodWithMinimalParameters(this.bean.getClass(), name) :
				BeanUtils.findMethodWithMinimalParameters(this.bean.getClass().getMethods(), name));
	}

	/**
	 * Invoke the specified custom destroy method on the given bean.
	 * <p>This implementation invokes a no-arg method if found, else checking
	 * for a method with a single boolean argument (passing in "true",
	 * assuming a "force" parameter), else logging an error.
	 */
	private void invokeCustomDestroyMethod(final Method destroyMethod) {
		int paramCount = destroyMethod.getParameterCount();
		final Object[] args = new Object[paramCount];
		if (paramCount == 1) {
			args[0] = Boolean.TRUE;
		}
		if (logger.isTraceEnabled()) {
			logger.trace("Invoking destroy method '" + this.destroyMethodName +
					"' on bean with name '" + this.beanName + "'");
		}
		try {
			if (System.getSecurityManager() != null) {
				AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
					ReflectionUtils.makeAccessible(destroyMethod);
					return null;
				});
				try {
					AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () ->
						destroyMethod.invoke(this.bean, args), this.acc);
				}
				catch (PrivilegedActionException pax) {
					throw (InvocationTargetException) pax.getException();
				}
			}
			else {
				ReflectionUtils.makeAccessible(destroyMethod);
				destroyMethod.invoke(this.bean, args);
			}
		}
		catch (InvocationTargetException ex) {
			String msg = "Destroy method '" + this.destroyMethodName + "' on bean with name '" +
					this.beanName + "' threw an exception";
			if (logger.isDebugEnabled()) {
				logger.warn(msg, ex.getTargetException());
			}
			else {
				logger.warn(msg + ": " + ex.getTargetException());
			}
		}
		catch (Throwable ex) {
			logger.warn("Failed to invoke destroy method '" + this.destroyMethodName +
					"' on bean with name '" + this.beanName + "'", ex);
		}
	}


	/**
	 * Serializes a copy of the state of this class,
	 * filtering out non-serializable BeanPostProcessors.
	 */
	protected Object writeReplace() {
		List<DestructionAwareBeanPostProcessor> serializablePostProcessors = null;
		if (this.beanPostProcessors != null) {
			serializablePostProcessors = new ArrayList<>();
			for (DestructionAwareBeanPostProcessor postProcessor : this.beanPostProcessors) {
				if (postProcessor instanceof Serializable) {
					serializablePostProcessors.add(postProcessor);
				}
			}
		}
		return new DisposableBeanAdapter(this.bean, this.beanName, this.invokeDisposableBean,
				this.nonPublicAccessAllowed, this.destroyMethodName, serializablePostProcessors);
	}


	/**
	 * 检查bean是否有destroy方法
	 *
	 * Check whether the given bean has any kind of destroy method to call.
	 * @param bean the bean instance  bean实例
	 * @param beanDefinition the corresponding bean definition  相应的bean定义
	 */
	public static boolean hasDestroyMethod(Object bean, RootBeanDefinition beanDefinition) {
		// DisposableBean:要在销毁时释放资源的bean所实现的接口
		// 如果bean时DisposableBean实例||bean是AutoClosable实例
		if (bean instanceof DisposableBean || bean instanceof AutoCloseable) {
			// 返回true
			return true;
		}
		//获取destroyMethodName属性，就是XML的destroy-method属性
		String destroyMethodName = beanDefinition.getDestroyMethodName();
		// AbstractBeanDefinition.INFER_METHOD：常量，指示容器应该尝试推断Bean的销毁方法名，而不是显示地指定方法名。
		// 值'(inferred)'是专门设计 用来在方法名中包含非法字符的，以确保不会与合法命名的同名方法发生冲突。目前，
		// 在销毁方法推断过程中检测到方法名'close'和'shutdown'(如果存在特点Bean类上的话)
		// 如果destroyMethodName是'(inferred)'
		if (AbstractBeanDefinition.INFER_METHOD.equals(destroyMethodName)) {
			//目前Spring推断规则是：如果该类中存在名为"close" 或者 "shutdown"的方法，就返回true否则返回false
			return (ClassUtils.hasMethod(bean.getClass(), CLOSE_METHOD_NAME) ||
					ClassUtils.hasMethod(bean.getClass(), SHUTDOWN_METHOD_NAME));
		}
		//否则，判断destroyMethodName属性是否不为null以及""，即是否设置了destroy-method属性，如果是那么返回true，否则返回false
		return StringUtils.hasLength(destroyMethodName);
	}

	/**
	 检查给定的 bean 是否应用了某个具有销毁感知的后处理器
	 * 实际上默认就是查找该实例所属的类是否具有标注了@PreDestroy注解的方法
	 *
	 * Check whether the given bean has destruction-aware post-processors applying to it.
	 * @param bean the bean instance   bean 实例
	 * @param postProcessors the post-processor candidates  注册的bean后处理器集合
	 */
	public static boolean hasApplicableProcessors(Object bean, List<BeanPostProcessor> postProcessors) {
		// 如果postProcessors不是空数组
		if (!CollectionUtils.isEmpty(postProcessors)) {
			// 遍历postProcessors
			for (BeanPostProcessor processor : postProcessors) {
				// 如果processor是DestructionAwareBeanPostProcessor实例
				if (processor instanceof DestructionAwareBeanPostProcessor) {
					DestructionAwareBeanPostProcessor dabpp = (DestructionAwareBeanPostProcessor) processor;
					//调用requiresDestruction方法，用于判断确定给定的 bean 实例是否需要此后处理器进行销毁回调
					//常见的实现就是InitDestroyAnnotationBeanPostProcessor，它会判断该实例所属的类中是否存在@PreDestroy注解的方法
					//如果存在就返回true，不存在就返回false，当bean销毁的时候就会调用postProcessBeforeDestruction方法回调注解方法
					if (dabpp.requiresDestruction(bean)) {
						// 是就返回true
						return true;
					}
				}
			}
		}
		// 默认返回false
		return false;
	}

}
