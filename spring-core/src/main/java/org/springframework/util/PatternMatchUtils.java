/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.util;

import org.springframework.lang.Nullable;

/**
 * Utility methods for simple pattern matching, in particular for
 * Spring's typical "xxx*", "*xxx" and "*xxx*" pattern styles.
 *
 * @author Juergen Hoeller
 * @since 2.0
 */
public abstract class PatternMatchUtils {

	/**
	 * 匹配一个字符串是否符合给定的模式。
	 * 支持以下简单的模式样式：
	 * - "xxx*"：匹配以 "xxx" 开头的字符串。
	 * - "*xxx"：匹配以 "xxx" 结尾的字符串。
	 * - "*xxx*"：匹配包含 "xxx" 的字符串。
	 * - "xxx*yyy"：匹配以 "xxx" 开头并以 "yyy" 结尾的字符串。
	 * 还支持直接相等匹配。
	 *
	 * Match a String against the given pattern, supporting the following simple
	 * pattern styles: "xxx*", "*xxx", "*xxx*" and "xxx*yyy" matches (with an
	 * arbitrary number of pattern parts), as well as direct equality.
	 * @param pattern the pattern to match against 要匹配的模式
	 * @param str the String to match 要匹配的字符串
	 * @return whether the String matches the given pattern  字符串是否匹配给定的模式
	 */
	public static boolean simpleMatch(@Nullable String pattern, @Nullable String str) {
		// 如果模式或字符串为 null，直接返回 false
		if (pattern == null || str == null) {
			return false;
		}

		// 查找模式中第一个 '*' 的位置
		int firstIndex = pattern.indexOf('*');
		// 如果模式中没有 '*'，则直接比较字符串是否相等
		if (firstIndex == -1) {
			return pattern.equals(str);
		}

		// 如果 '*' 在模式的开头
		if (firstIndex == 0) {
			// 如果模式只有一个 '*'，则匹配任意字符串
			if (pattern.length() == 1) {
				return true;
			}
			// 查找下一个 '*' 的位置
			int nextIndex = pattern.indexOf('*', 1);
			// 如果模式中只有一个 '*'，则检查字符串是否以模式中 '*' 后面的部分结尾
			if (nextIndex == -1) {
				return str.endsWith(pattern.substring(1));
			}
			// 获取两个 '*' 之间的部分
			String part = pattern.substring(1, nextIndex);
			// 如果这部分为空，则递归匹配剩余的模式和字符串
			if (part.isEmpty()) {
				return simpleMatch(pattern.substring(nextIndex), str);
			}
			// 在字符串中查找这部分内容
			int partIndex = str.indexOf(part);
			// 遍历所有匹配的部分
			while (partIndex != -1) {
				// 递归匹配剩余的模式和字符串
				if (simpleMatch(pattern.substring(nextIndex), str.substring(partIndex + part.length()))) {
					return true;
				}
				// 继续查找下一个匹配的部分
				partIndex = str.indexOf(part, partIndex + 1);
			}
			// 如果没有匹配的部分，返回 false
			return false;
		}

		// 如果 '*' 不在模式的开头，则检查字符串的前缀是否匹配模式的前缀
		// 然后递归匹配剩余的模式和字符串
		return (str.length() >= firstIndex &&
				pattern.substring(0, firstIndex).equals(str.substring(0, firstIndex)) &&
				simpleMatch(pattern.substring(firstIndex), str.substring(firstIndex)));
	}

	/**
	 * 匹配一个字符串是否符合给定的多个模式。
	 * 支持以下简单的模式样式：
	 * - "xxx*"：匹配以 "xxx" 开头的字符串。
	 * - "*xxx"：匹配以 "xxx" 结尾的字符串。
	 * - "*xxx*"：匹配包含 "xxx" 的字符串。
	 * - "xxx*yyy"：匹配以 "xxx" 开头并以 "yyy" 结尾的字符串。
	 * 还支持直接相等匹配。
	 *
	 * Match a String against the given patterns, supporting the following simple
	 * pattern styles: "xxx*", "*xxx", "*xxx*" and "xxx*yyy" matches (with an
	 * arbitrary number of pattern parts), as well as direct equality.
	 * @param patterns the patterns to match against  要匹配的模式数组
	 * @param str the String to match 要匹配的字符串
	 * @return whether the String matches any of the given patterns 字符串是否匹配任意一个给定的模式
	 */
	public static boolean simpleMatch(@Nullable String[] patterns, String str) {
		// 如果模式数组不为空，遍历每个模式
		if (patterns != null) {
			for (String pattern : patterns) {
				// 如果字符串匹配当前模式，返回 true
				if (simpleMatch(pattern, str)) {
					return true;
				}
			}
		}
		// 如果没有匹配的模式，返回 false
		return false;
	}

}
