/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.support;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.EmbeddedValueResolver;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.lang.Nullable;
import org.springframework.util.StringValueResolver;

/**
 * 此类用来完成以下功能，向某些实现了Aware的接口的bean设置ApplicationContext中的相应属性
 * 	EnvironmentAware
 * 	EmbeddedValueResolverAware
 * 	ResourceLoaderAware
 * 	ApplicationEventPublisherAware
 *	MessageSourceAware
 *	ApplicationContextAware
 *
 *
 *
 * {@link BeanPostProcessor} implementation that supplies the {@code ApplicationContext},
 * {@link org.springframework.core.env.Environment Environment}, or
 * {@link StringValueResolver} for the {@code ApplicationContext} to beans that
 * implement the {@link EnvironmentAware}, {@link EmbeddedValueResolverAware},
 * {@link ResourceLoaderAware}, {@link ApplicationEventPublisherAware},
 * {@link MessageSourceAware}, and/or {@link ApplicationContextAware} interfaces.
 *
 * <p>Implemented interfaces are satisfied in the order in which they are
 * mentioned above.
 *
 * <p>Application contexts will automatically register this with their
 * underlying bean factory. Applications do not use this directly.
 *
 * @author Juergen Hoeller
 * @author Costin Leau
 * @author Chris Beams
 * @since 10.10.2003
 * @see org.springframework.context.EnvironmentAware
 * @see org.springframework.context.EmbeddedValueResolverAware
 * @see org.springframework.context.ResourceLoaderAware
 * @see org.springframework.context.ApplicationEventPublisherAware
 * @see org.springframework.context.MessageSourceAware
 * @see org.springframework.context.ApplicationContextAware
 * @see org.springframework.context.support.AbstractApplicationContext#refresh()
 */
class ApplicationContextAwareProcessor implements BeanPostProcessor {

	private final ConfigurableApplicationContext applicationContext;

	private final StringValueResolver embeddedValueResolver;


	/**
	 * Create a new ApplicationContextAwareProcessor for the given context.
	 */
	public ApplicationContextAwareProcessor(ConfigurableApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
		this.embeddedValueResolver = new EmbeddedValueResolver(applicationContext.getBeanFactory());
	}


	/**
	 * 接口 beanPostProcessor规定的方法，会在bean创建时，实例化后，初始化前，对bean对象应用
	 * @param bean the new bean instance 新的 Bean 实例
	 * @param beanName the name of the bean  Bean的名称
	 * @return
	 * @throws BeansException
	 */
	@Override
	@Nullable
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		/*1 如果bean不属于这几个接口的实例，那么返回bean，相当于什么都不做*/
		if (!(bean instanceof EnvironmentAware || bean instanceof EmbeddedValueResolverAware ||
				bean instanceof ResourceLoaderAware || bean instanceof ApplicationEventPublisherAware ||
				bean instanceof MessageSourceAware || bean instanceof ApplicationContextAware)){
			return bean;
		}

		/*
		 * 2. 处理安全上下文
		 * 如果启用了安全管理器（SecurityManager），则获取当前的安全上下文（AccessControlContext）。
		 * 安全管理器用于在 Java 应用程序中实施安全策略。
		 */
		AccessControlContext acc = null;

		if (System.getSecurityManager() != null) {
			acc = this.applicationContext.getBeanFactory().getAccessControlContext();
		}

		/*
		 * 3. 调用 Aware 接口的方法
		 * 如果启用了安全管理器，则在安全上下文中执行权限检查。
		 * 否则，直接调用 invokeAwareInterfaces 方法。
		 */
		if (acc != null) {
			// 在安全上下文中执行权限检查
			AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
				// 检测bean上是否实现了某个aware接口，有的话进行相关的调用
				invokeAwareInterfaces(bean);
				return null;
			}, acc);
		}
		else {
			// 检测bean上是否实现了某个aware接口，有的话进行相关的调用
			invokeAwareInterfaces(bean);
		}

		/*
		 * 4. 返回处理后的 bean
		 * 无论是否调用了 Aware 接口的方法，最终都会返回原始的 bean 实例。
		 */
		return bean;
	}

	/**
	 * 如果某个bean实现了某个aware接口，给指定的bean设置相应的属性值
	 *
	 * @param bean
	 */
	private void invokeAwareInterfaces(Object bean) {
		/*
		 * 1. 如果 bean 实现了 EnvironmentAware 接口，则调用其 setEnvironment 方法，
		 * 将 Spring 容器的 Environment 对象注入到 bean 中。
		 * Environment 对象包含应用程序的环境配置（如配置文件中的属性）。
		 */
		if (bean instanceof EnvironmentAware) {
			((EnvironmentAware) bean).setEnvironment(this.applicationContext.getEnvironment());
		}
		/*
		 * 2. 如果 bean 实现了 EmbeddedValueResolverAware 接口，则调用其 setEmbeddedValueResolver 方法，
		 * 将 embeddedValueResolver 注入到 bean 中。
		 * embeddedValueResolver 用于解析 Spring 表达式语言（SpEL）中的占位符（如 ${...}）。
		 */
		if (bean instanceof EmbeddedValueResolverAware) {
			((EmbeddedValueResolverAware) bean).setEmbeddedValueResolver(this.embeddedValueResolver);
		}
		/*
		 * 3.  如果 bean 实现了 ResourceLoaderAware 接口，则调用其 setResourceLoader 方法，
		 * 将 applicationContext 注入到 bean 中。
		 * ResourceLoader 用于加载资源（如类路径下的文件）。
		 */
		if (bean instanceof ResourceLoaderAware) {
			((ResourceLoaderAware) bean).setResourceLoader(this.applicationContext);
		}
		/*
		 * 4. 如果 bean 实现了 ApplicationEventPublisherAware 接口，则调用其 setApplicationEventPublisher 方法，
		 * 将 applicationContext 注入到 bean 中。
		 * ApplicationEventPublisher 用于发布应用事件。
		 */
		if (bean instanceof ApplicationEventPublisherAware) {
			((ApplicationEventPublisherAware) bean).setApplicationEventPublisher(this.applicationContext);
		}
		/*
		 * 5.  如果 bean 实现了 MessageSourceAware 接口，则调用其 setMessageSource 方法，
		 * 将 applicationContext 注入到 bean 中。
		 * MessageSource 用于国际化（i18n）和消息解析。
		 */
		if (bean instanceof MessageSourceAware) {
			((MessageSourceAware) bean).setMessageSource(this.applicationContext);
		}
		/*
		 * 6. 如果 bean 实现了 ApplicationContextAware 接口，则调用其 setApplicationContext 方法，
		 * 将 applicationContext 注入到 bean 中。
		 * ApplicationContext 是 Spring 容器的核心接口，提供了访问容器功能的能力。
		 */
		if (bean instanceof ApplicationContextAware) {
			((ApplicationContextAware) bean).setApplicationContext(this.applicationContext);
		}
	}

}
