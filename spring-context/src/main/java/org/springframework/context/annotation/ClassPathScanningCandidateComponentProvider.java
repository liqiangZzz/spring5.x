/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.annotation;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.index.CandidateComponentsIndex;
import org.springframework.context.index.CandidateComponentsIndexLoader;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.core.env.EnvironmentCapable;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Indexed;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

/**
 * A component provider that provides candidate components from a base package. Can
 * use {@link CandidateComponentsIndex the index} if it is available of scans the
 * classpath otherwise. Candidate components are identified by applying exclude and
 * include filters. {@link AnnotationTypeFilter}, {@link AssignableTypeFilter} include
 * filters on an annotation/superclass that are annotated with {@link Indexed} are
 * supported: if any other include filter is specified, the index is ignored and
 * classpath scanning is used instead.
 *
 * <p>This implementation is based on Spring's
 * {@link org.springframework.core.type.classreading.MetadataReader MetadataReader}
 * facility, backed by an ASM {@link org.springframework.asm.ClassReader ClassReader}.
 *
 * @author Mark Fisher
 * @author Juergen Hoeller
 * @author Ramnivas Laddad
 * @author Chris Beams
 * @author Stephane Nicoll
 * @since 2.5
 * @see org.springframework.core.type.classreading.MetadataReaderFactory
 * @see org.springframework.core.type.AnnotationMetadata
 * @see ScannedGenericBeanDefinition
 * @see CandidateComponentsIndex
 */
public class ClassPathScanningCandidateComponentProvider implements EnvironmentCapable, ResourceLoaderAware {

	static final String DEFAULT_RESOURCE_PATTERN = "**/*.class";


	protected final Log logger = LogFactory.getLog(getClass());

	private String resourcePattern = DEFAULT_RESOURCE_PATTERN;

	/**
	 * 包括过滤器
	 */
	private final List<TypeFilter> includeFilters = new LinkedList<>();

	/**
	 * 排除过滤器
	 */
	private final List<TypeFilter> excludeFilters = new LinkedList<>();

	@Nullable
	private Environment environment;

	@Nullable
	private ConditionEvaluator conditionEvaluator;

	@Nullable
	private ResourcePatternResolver resourcePatternResolver;

	@Nullable
	private MetadataReaderFactory metadataReaderFactory;

	@Nullable
	private CandidateComponentsIndex componentsIndex;


	/**
	 * Protected constructor for flexible subclass initialization.
	 * @since 4.3.6
	 */
	protected ClassPathScanningCandidateComponentProvider() {
	}

	/**
	 * Create a ClassPathScanningCandidateComponentProvider with a {@link StandardEnvironment}.
	 * @param useDefaultFilters whether to register the default filters for the
	 * {@link Component @Component}, {@link Repository @Repository},
	 * {@link Service @Service}, and {@link Controller @Controller}
	 * stereotype annotations
	 * @see #registerDefaultFilters()
	 */
	public ClassPathScanningCandidateComponentProvider(boolean useDefaultFilters) {
		this(useDefaultFilters, new StandardEnvironment());
	}

	/**
	 * Create a ClassPathScanningCandidateComponentProvider with the given {@link Environment}.
	 * @param useDefaultFilters whether to register the default filters for the
	 * {@link Component @Component}, {@link Repository @Repository},
	 * {@link Service @Service}, and {@link Controller @Controller}
	 * stereotype annotations
	 * @param environment the Environment to use
	 * @see #registerDefaultFilters()
	 */
	public ClassPathScanningCandidateComponentProvider(boolean useDefaultFilters, Environment environment) {
		if (useDefaultFilters) {
			registerDefaultFilters();
		}
		setEnvironment(environment);
		setResourceLoader(null);
	}


	/**
	 * Set the resource pattern to use when scanning the classpath.
	 * This value will be appended to each base package name.
	 * @see #findCandidateComponents(String)
	 * @see #DEFAULT_RESOURCE_PATTERN
	 */
	public void setResourcePattern(String resourcePattern) {
		Assert.notNull(resourcePattern, "'resourcePattern' must not be null");
		this.resourcePattern = resourcePattern;
	}

	/**
	 * 将包含类型过滤器添加到includeFilters列表尾部
	 *
	 * Add an include type filter to the <i>end</i> of the inclusion list.
	 */
	public void addIncludeFilter(TypeFilter includeFilter) {
		this.includeFilters.add(includeFilter);
	}

	/**
	 * 将排除类型过滤器添加到excludeFilters列表头部
	 *
	 * Add an exclude type filter to the <i>front</i> of the exclusion list.
	 */
	public void addExcludeFilter(TypeFilter excludeFilter) {
		this.excludeFilters.add(0, excludeFilter);
	}

	/**
	 * Reset the configured type filters.
	 * @param useDefaultFilters whether to re-register the default filters for
	 * the {@link Component @Component}, {@link Repository @Repository},
	 * {@link Service @Service}, and {@link Controller @Controller}
	 * stereotype annotations
	 * @see #registerDefaultFilters()
	 */
	public void resetFilters(boolean useDefaultFilters) {
		this.includeFilters.clear();
		this.excludeFilters.clear();
		if (useDefaultFilters) {
			registerDefaultFilters();
		}
	}

	/**
	 * 尝试添加@Component、@ManagedBean、@Named这三个注解类型过滤器到includeFilters缓存集合中！
	 * 隐式的注册所有具有@Component元注解的注解标志的类，比如@Component、@Repository、@Service、@Controller、@Configuration
	 * 还支持扫描注册还支持 Java EE 6 的注解，比如@ManagedBean，以及JSR-330的注解，比如@Named
	 *
	 * Register the default filter for {@link Component @Component}.
	 * <p>This will implicitly register all annotations that have the
	 * {@link Component @Component} meta-annotation including the
	 * {@link Repository @Repository}, {@link Service @Service}, and
	 * {@link Controller @Controller} stereotype annotations.
	 * <p>Also supports Java EE 6's {@link javax.annotation.ManagedBean} and
	 * JSR-330's {@link javax.inject.Named} annotations, if available.
	 *
	 */
	@SuppressWarnings("unchecked")
	protected void registerDefaultFilters() {
		//添加@Component注解类型过滤器，过滤器将不会匹配接口
		this.includeFilters.add(new AnnotationTypeFilter(Component.class));
		ClassLoader cl = ClassPathScanningCandidateComponentProvider.class.getClassLoader();
		try {
			//添加@ManagedBean注解类型过滤器，过滤器将不会匹配接口
			this.includeFilters.add(new AnnotationTypeFilter(
					((Class<? extends Annotation>) ClassUtils.forName("javax.annotation.ManagedBean", cl)), false));
			logger.trace("JSR-250 'javax.annotation.ManagedBean' found and supported for component scanning");
		}
		catch (ClassNotFoundException ex) {
			// JSR-250 1.1 API (as included in Java EE 6) not available - simply skip.
		}
		try {
			//添加@Named注解类型过滤器，过滤器将不会匹配接口
			this.includeFilters.add(new AnnotationTypeFilter(
					((Class<? extends Annotation>) ClassUtils.forName("javax.inject.Named", cl)), false));
			logger.trace("JSR-330 'javax.inject.Named' annotation found and supported for component scanning");
		}
		catch (ClassNotFoundException ex) {
			// JSR-330 API not available - simply skip.
		}
	}

	/**
	 * Set the Environment to use when resolving placeholders and evaluating
	 * {@link Conditional @Conditional}-annotated component classes.
	 * <p>The default is a {@link StandardEnvironment}.
	 * @param environment the Environment to use
	 */
	public void setEnvironment(Environment environment) {
		Assert.notNull(environment, "Environment must not be null");
		this.environment = environment;
		this.conditionEvaluator = null;
	}

	@Override
	public final Environment getEnvironment() {
		if (this.environment == null) {
			this.environment = new StandardEnvironment();
		}
		return this.environment;
	}

	/**
	 * Return the {@link BeanDefinitionRegistry} used by this scanner, if any.
	 */
	@Nullable
	protected BeanDefinitionRegistry getRegistry() {
		return null;
	}

	/**
	 * 创建了ResourcePatternResolver,用来解析URL资源，
	 *
	 * Set the {@link ResourceLoader} to use for resource locations.
	 * This will typically be a {@link ResourcePatternResolver} implementation.
	 * <p>Default is a {@code PathMatchingResourcePatternResolver}, also capable of
	 * resource pattern resolving through the {@code ResourcePatternResolver} interface.
	 * @see org.springframework.core.io.support.ResourcePatternResolver
	 * @see org.springframework.core.io.support.PathMatchingResourcePatternResolver
	 */
	@Override
	public void setResourceLoader(@Nullable ResourceLoader resourceLoader) {
		this.resourcePatternResolver = ResourcePatternUtils.getResourcePatternResolver(resourceLoader);
		// 创建CachingMetadataReaderFactory，用来做字节码文件元数据的缓存
		this.metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
		// 创建CandidateComponentsIndexLoader，spring内部定义的组件，读取META-INF/spring.components下的信息
		this.componentsIndex = CandidateComponentsIndexLoader.loadIndex(this.resourcePatternResolver.getClassLoader());
	}

	/**
	 * Return the ResourceLoader that this component provider uses.
	 */
	public final ResourceLoader getResourceLoader() {
		return getResourcePatternResolver();
	}

	private ResourcePatternResolver getResourcePatternResolver() {
		if (this.resourcePatternResolver == null) {
			this.resourcePatternResolver = new PathMatchingResourcePatternResolver();
		}
		return this.resourcePatternResolver;
	}

	/**
	 * Set the {@link MetadataReaderFactory} to use.
	 * <p>Default is a {@link CachingMetadataReaderFactory} for the specified
	 * {@linkplain #setResourceLoader resource loader}.
	 * <p>Call this setter method <i>after</i> {@link #setResourceLoader} in order
	 * for the given MetadataReaderFactory to override the default factory.
	 */
	public void setMetadataReaderFactory(MetadataReaderFactory metadataReaderFactory) {
		this.metadataReaderFactory = metadataReaderFactory;
	}

	/**
	 * Return the MetadataReaderFactory used by this component provider.
	 */
	public final MetadataReaderFactory getMetadataReaderFactory() {
		if (this.metadataReaderFactory == null) {
			this.metadataReaderFactory = new CachingMetadataReaderFactory();
		}
		return this.metadataReaderFactory;
	}


	/**
	 * 扫描类路径查找候 component 注解。
	 * 扫描组建索引或者候选组件的路径basePackage，返回扫描到的BeanDefinition集合
	 *
	 * Scan the class path for candidate components.
	 * @param basePackage the package to check for annotated classes 要扫描的包路径
	 * @return a corresponding Set of autodetected bean definitions 一组对应的BeanDefinition集合
	 */
	public Set<BeanDefinition> findCandidateComponents(String basePackage) {
		/*
		 * componentsIndex：
		 * 该属性存储了通过"META-INF/spring.components"组件索引文件获取到的需要注册的bean定义
		 * 如果没有"META-INF/spring.components"文件，则componentsIndex为null，一般都是为null
		 * 这是Spring5的新特性，用来直接定义需要注册的bean，用于提升应用启动速度
		 *
		 * indexSupportsIncludeFilters：通过过滤器判断是否可以使用组件索引，因为Spring5的组件索引存在限制
		 */
		if (this.componentsIndex != null && indexSupportsIncludeFilters()) {
			// 如果存在组件索引文件并且可以使用组件索引，那么直接处理componentsIndex中的符合条件的bean定义，不会扫描包
			return addCandidateComponentsFromIndex(this.componentsIndex, basePackage);
		}
		else {
		   //扫描 basePackage 下符合条件的class（核心扫描方法）
			return scanCandidateComponents(basePackage);
		}
	}

	/**
	 * 确定是否可以使用组件索引
	 *
	 * Determine if the index can be used by this instance.
	 * @return {@code true} if the index is available and the configuration of this
	 * instance is supported by it, {@code false} otherwise 如果索引可用且当前实例的配置支持索引返回ture，否则返回 false
	 * @since 5.0
	 */
	private boolean indexSupportsIncludeFilters() {
		//遍历includeFilters
		for (TypeFilter includeFilter : this.includeFilters) {
			//校验每一个includeFilter，只要有一个不符合要求就返回false
			if (!indexSupportsIncludeFilter(includeFilter)) {
				return false;
			}
		}
		// 如果所有 includeFilter 都通过校验，则返回 true
		return true;
	}

	/**
	 * 确定指定是否是AnnotationTypeFilter或者AssignableTypeFilter的类型过滤器，以及是否支持组件索引
	 *
	 * Determine if the specified include {@link TypeFilter} is supported by the index.
	 * @param filter the filter to check 要检查的过滤器
	 * @return whether the index supports this include filter 索引是否支持此包括过滤器
	 * @since 5.0
	 * @see #extractStereotype(TypeFilter)
	 */
	private boolean indexSupportsIncludeFilter(TypeFilter filter) {
		// 如果过滤器是 AnnotationTypeFilter 类型
		if (filter instanceof AnnotationTypeFilter) {
			// 获取注解类型
			Class<? extends Annotation> annotation = ((AnnotationTypeFilter) filter).getAnnotationType();
			//确定该注解类型是否包含@Indexed元注解 或者 该注解类型的名称是否以javax.开头，即ManagedBean或者Named注解
			//如果满足一个，则支持组件索引，返回true，否则返回false
			return (AnnotationUtils.isAnnotationDeclaredLocally(Indexed.class, annotation) ||
					annotation.getName().startsWith("javax."));
		}
		// 如果过滤器是 AssignableTypeFilter 类型
		if (filter instanceof AssignableTypeFilter) {
			// 获取目标类型（类或接口）
			Class<?> target = ((AssignableTypeFilter) filter).getTargetType();
			//确定该类（接口）的类型上是否具有@Indexed元注解，如果具有，则支持组件索引，返回true，否则返回false
			return AnnotationUtils.isAnnotationDeclaredLocally(Indexed.class, target);
		}
		//其它类型的TypeFilter，直接返回false
		return false;
	}

	/**
	 * 提取AnnotationTypeFilter和AssignableTypeFilter类型的过滤器指定的注解名和类（接口名）字符串
	 * 如果是其它类型的TypeFilter，则返回null
	 *
	 * Extract the stereotype to use for the specified compatible filter.
	 * @param filter the filter to handle 要处理的过滤器
	 * @return the stereotype in the index matching this filter 配此筛选器的类型
	 * @since 5.0
	 * @see #indexSupportsIncludeFilter(TypeFilter)
	 */
	@Nullable
	private String extractStereotype(TypeFilter filter) {
		//如果是AnnotationTypeFilter类型
		if (filter instanceof AnnotationTypeFilter) {
			//那么返回注解的名字
			return ((AnnotationTypeFilter) filter).getAnnotationType().getName();
		}
		//如果是AnnotationTypeFilter类型
		if (filter instanceof AssignableTypeFilter) {
			//那么返回类或者接口的名字
			return ((AssignableTypeFilter) filter).getTargetType().getName();
		}
		//其它类型，返回null
		return null;
	}

	/**
	 * 基于组件索引componentsIndex加载bean定义
	 *
	 * @param index 组件索引
	 * @param basePackage 要扫描的包路径
	 * @return
	 */
	private Set<BeanDefinition> addCandidateComponentsFromIndex(CandidateComponentsIndex index, String basePackage) {
		//该集合用于保存注册的BeanDefinition
		Set<BeanDefinition> candidates = new LinkedHashSet<>();
		try {
			Set<String> types = new HashSet<>();
			// 遍历includeFilters过滤器，获取满足条件的组件索引文件中的bean类型（全路径类名）
			for (TypeFilter filter : this.includeFilters) {
				//提取AnnotationTypeFilter和AssignableTypeFilter类型的过滤器指定的注解名和类（接口名）字符串。
				//比如是Component注解的Filter，那么返回org.springframework.stereotype.Component
				//如果是其它类型的TypeFilter，则返回null
				String stereotype = extractStereotype(filter);
				//如果stereotype为null，那么抛出异常，因为组件索引文件中只能指定注解或者类的全路径名，这也是一个局限
				if (stereotype == null) {
					throw new IllegalArgumentException("Failed to extract stereotype from " + filter);
				}
				//返回满足包含Filters过滤器以及basePackage包路径的所有类型
				types.addAll(index.getCandidateTypes(basePackage, stereotype));
			}
			boolean traceEnabled = logger.isTraceEnabled();
			boolean debugEnabled = logger.isDebugEnabled();
			// 遍历所有bean类型（全路径类名），进行校验，以及创建BeanDefinition
			// 这一部分就类似于scanCandidateComponents方法的逻辑
			for (String type : types) {
				// 使用metadataReader读取资源，metadataReader是专门用来访问元数据的类
				MetadataReader metadataReader = getMetadataReaderFactory().getMetadataReader(type);
				// 使用过滤器检查给定的类是否我候选类（候选类，与excludeFilters的所有Filter不匹配，并且与includeFilters的至少一个Filter匹配）
				if (isCandidateComponent(metadataReader)) {
					//如果符合，那么基于metadataReader先创建ScannedGenericBeanDefinition，
					ScannedGenericBeanDefinition sbd = new ScannedGenericBeanDefinition(metadataReader);
					sbd.setSource(metadataReader.getResource());
					//继续检查给定bean定义是否可以作为候选组件，即是否符合bean定义
					if (isCandidateComponent(sbd)) {
						if (debugEnabled) {
							logger.debug("Using candidate component class from index: " + type);
						}
						// 确定为候选类，则添加到candidates
						candidates.add(sbd);
					}
					else {
						if (debugEnabled) {
							logger.debug("Ignored because not a concrete top-level class: " + type);
						}
					}
				}
				else {
					if (traceEnabled) {
						logger.trace("Ignored because matching an exclude filter: " + type);
					}
				}
			}
		}
		catch (IOException ex) {
			throw new BeanDefinitionStoreException("I/O failure during classpath scanning", ex);
		}
		return candidates;
	}

	/**
	 * 扫描路径下面的所有file对象，然后读取注解信息，判断与excludeFilters、includeFilters是否匹配以及Conditional判断，
	 * 最后判断是否为接口、抽象类、独立类、以及lookup注解，最终返回BeanDefinition
	 * @param basePackage 包路径
	 * @return 符合条件的BeanDefinition集合
	 */
	private Set<BeanDefinition> scanCandidateComponents(String basePackage) {
		Set<BeanDefinition> candidates = new LinkedHashSet<>();
		try {
			// 获取basePackage下所有的文件资源，并处理占位符情况${...},转换为ClassLoader资源(.class)搜索路径，当前案例是 classpath*:com/lq/**/*.class
			String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
					resolveBasePackage(basePackage) + '/' + this.resourcePattern;
			/*
			 * 加载所有路径下的资源，我们看到前缀是"classpath*"，因此项目依赖的jar包中的相同路径下资源都会被加载进来
			 * Spring会将每一个定义的类(不是源文件)加载成为一个Resource资源（包括内部类都是一个Resource资源）
			 */
			Resource[] resources = getResourcePatternResolver().getResources(packageSearchPath);
			boolean traceEnabled = logger.isTraceEnabled();
			boolean debugEnabled = logger.isDebugEnabled();
			//resource迭代执行,当资源可读取时,获取该资源的MetadataReader对象
			for (Resource resource : resources) {
				if (traceEnabled) {
					logger.trace("Scanning " + resource);
				}
				if (resource.isReadable()) {
					try {
						/*
						 *MetadataReaderFactory是CachingMetadataReaderFactory类型，能够缓存Resource -> MetadataReader的映射
                    	 * MetadataReader是一个通过ASM字节码框架读取class资源组装访问元数据的接口，简单的说用于获取类的元数据
                     	 * Spring抽象出了包括ClassMetadata、MethodMetadata、AnnotationMetadata等类元数据接口
                     	 * ClassMetadata用于访问类的信息，MethodMetadata用于访问类方法的信息，AnnotationMetadata用于访问类注解的信息
                     	 */
						//获取.class对应的元信息，例如 注解的信息，类的信息，接口抽象类,父类等
						MetadataReader metadataReader = getMetadataReaderFactory().getMetadataReader(resource);
						//判断是否是一个bean，进行过滤判断excludeFilters、includeFilters判断，以及Conditional判断，判断是否是一个候选组件
						//如果是@Conditional注解则不是一个bean
						if (isCandidateComponent(metadataReader)) {
							/*如果符合，那么基于metadataReader先创建ScannedGenericBeanDefinition*/
							ScannedGenericBeanDefinition sbd = new ScannedGenericBeanDefinition(metadataReader);
							sbd.setSource(resource);
							//判断是否为接口、抽象类、独立类、以及lookup注解
							if (isCandidateComponent(sbd)) {
								if (debugEnabled) {
									logger.debug("Identified candidate component class: " + resource);
								}
								//如果符合，那么添加该BeanDefinition
								candidates.add(sbd);
							}
							else {
								if (debugEnabled) {
									logger.debug("Ignored because not a concrete top-level class: " + resource);
								}
							}
						}
						else {
							if (traceEnabled) {
								logger.trace("Ignored because not matching any filter: " + resource);
							}
						}
					}
					catch (Throwable ex) {
						throw new BeanDefinitionStoreException(
								"Failed to read candidate component class: " + resource, ex);
					}
				}
				else {
					if (traceEnabled) {
						logger.trace("Ignored because not readable: " + resource);
					}
				}
			}
		}
		catch (IOException ex) {
			throw new BeanDefinitionStoreException("I/O failure during classpath scanning", ex);
		}
		return candidates;
	}


	/**
	 * Resolve the specified base package into a pattern specification for
	 * the package search path.
	 * <p>The default implementation resolves placeholders against system properties,
	 * and converts a "."-based package path to a "/"-based resource path.
	 * @param basePackage the base package as specified by the user
	 * @return the pattern specification to be used for package searching
	 */
	protected String resolveBasePackage(String basePackage) {
		return ClassUtils.convertClassNameToResourcePath(getEnvironment().resolveRequiredPlaceholders(basePackage));
	}

	/**
	 * 确定给定的类是否不匹配任何排除过滤器，并且匹配至少一个包含过滤器
	 * Determine whether the given class does not match any exclude filter
	 * and does match at least one include filter.
	 * @param metadataReader the ASM ClassReader for the class
	 * @return whether the class qualifies as a candidate component
	 */
	protected boolean isCandidateComponent(MetadataReader metadataReader) throws IOException {
		//判断有没有excludeFilters注解（这种注解的bean不被注入）
		for (TypeFilter tf : this.excludeFilters) {
			//如果匹配任何一个TypeFilter，则返回false，表示没资格
			if (tf.match(metadataReader, getMetadataReaderFactory())) {
				return false;
			}
		}
		// 符合includeFilters的会进行条件匹配，通过了才是Bean，也就是先看有没有@Component，再看是否符合@Conditional
		for (TypeFilter tf : this.includeFilters) {
			//如果匹配任何一个TypeFilter，则返回true，表示有资格
			if (tf.match(metadataReader, getMetadataReaderFactory())) {
				return isConditionMatch(metadataReader);
			}
		}
		return false;
	}

	/**
	 * 根据类上面的@Conditional注解的确定该类是否有资格作为候选组件
	 *
	 * Determine whether the given class is a candidate component based on any
	 * {@code @Conditional} annotations.
	 * @param metadataReader the ASM ClassReader for the class 类的 Asm 阅读器
	 * @return whether the class qualifies as a candidate component 该类是否有资格作为候选组件
	 */
	private boolean isConditionMatch(MetadataReader metadataReader) {
		if (this.conditionEvaluator == null) {
			this.conditionEvaluator =
					new ConditionEvaluator(getRegistry(), this.environment, this.resourcePatternResolver);
		}
		//调用conditionEvaluator的shouldSkip方法判断
		return !this.conditionEvaluator.shouldSkip(metadataReader.getAnnotationMetadata());
	}

	/**
	 * 判断是否为接口、抽象类、独立类、以及lookup注解
	 * Determine whether the given bean definition qualifies as candidate.
	 * <p>The default implementation checks whether the class is not an interface
	 * and not dependent on an enclosing class.
	 * <p>Can be overridden in subclasses.
	 * @param beanDefinition the bean definition to check  要检查的bean定义
	 * @return whether the bean definition qualifies as a candidate component  bean 定义是否有资格作为候选组件
	 */
	protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
		AnnotationMetadata metadata = beanDefinition.getMetadata();
		/**
		 * isIndependent:确定底层类是否是独立的，即它是否是顶级类或者嵌套类，他可以独立于封闭类构造
		 * isConcrete：判断底层类是否表示具体类，即：既不是接口也不是抽象类
		 * isAbstract：是否被标记为抽象类
		 * hasAnnotatedMethods：确定基础类是否具有使用给定注解@Lookup类型进行数据的任何方法
		 */
		return (metadata.isIndependent() && (metadata.isConcrete() ||
				(metadata.isAbstract() && metadata.hasAnnotatedMethods(Lookup.class.getName()))));
	}


	/**
	 * Clear the local metadata cache, if any, removing all cached class metadata.
	 */
	public void clearCache() {
		if (this.metadataReaderFactory instanceof CachingMetadataReaderFactory) {
			// Clear cache in externally provided MetadataReaderFactory; this is a no-op
			// for a shared cache since it'll be cleared by the ApplicationContext.
			((CachingMetadataReaderFactory) this.metadataReaderFactory).clearCache();
		}
	}

}
