/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.annotation;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceRef;

import org.springframework.aop.TargetSource;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.InitDestroyAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.config.EmbeddedValueResolver;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.jndi.support.SimpleJndiBeanFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.util.StringValueResolver;

/**
 * 负责解析@Resource、@WebServiceRef、@EJB三个注解，这三个注解都是定义再javax.*包下的注解，属于java中的注解
 *
 * {@link org.springframework.beans.factory.config.BeanPostProcessor} implementation
 * that supports common Java annotations out of the box, in particular the JSR-250
 * annotations in the {@code javax.annotation} package. These common Java
 * annotations are supported in many Java EE 5 technologies (e.g. JSF 1.2),
 * as well as in Java 6's JAX-WS.
 *
 * <p>This post-processor includes support for the {@link javax.annotation.PostConstruct}
 * and {@link javax.annotation.PreDestroy} annotations - as init annotation
 * and destroy annotation, respectively - through inheriting from
 * {@link InitDestroyAnnotationBeanPostProcessor} with pre-configured annotation types.
 *
 * <p>The central element is the {@link javax.annotation.Resource} annotation
 * for annotation-driven injection of named beans, by default from the containing
 * Spring BeanFactory, with only {@code mappedName} references resolved in JNDI.
 * The {@link #setAlwaysUseJndiLookup "alwaysUseJndiLookup" flag} enforces JNDI lookups
 * equivalent to standard Java EE 5 resource injection for {@code name} references
 * and default names as well. The target beans can be simple POJOs, with no special
 * requirements other than the type having to match.
 *
 * <p>The JAX-WS {@link javax.xml.ws.WebServiceRef} annotation is supported too,
 * analogous to {@link javax.annotation.Resource} but with the capability of creating
 * specific JAX-WS service endpoints. This may either point to an explicitly defined
 * resource by name or operate on a locally specified JAX-WS service class. Finally,
 * this post-processor also supports the EJB 3 {@link javax.ejb.EJB} annotation,
 * analogous to {@link javax.annotation.Resource} as well, with the capability to
 * specify both a local bean name and a global JNDI name for fallback retrieval.
 * The target beans can be plain POJOs as well as EJB 3 Session Beans in this case.
 *
 * <p>The common annotations supported by this post-processor are available in
 * Java 6 (JDK 1.6) as well as in Java EE 5/6 (which provides a standalone jar for
 * its common annotations as well, allowing for use in any Java 5 based application).
 *
 * <p>For default usage, resolving resource names as Spring bean names,
 * simply define the following in your application context:
 *
 * <pre class="code">
 * &lt;bean class="org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"/&gt;</pre>
 *
 * For direct JNDI access, resolving resource names as JNDI resource references
 * within the Java EE application's "java:comp/env/" namespace, use the following:
 *
 * <pre class="code">
 * &lt;bean class="org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"&gt;
 *   &lt;property name="alwaysUseJndiLookup" value="true"/&gt;
 * &lt;/bean&gt;</pre>
 *
 * {@code mappedName} references will always be resolved in JNDI,
 * allowing for global JNDI names (including "java:" prefix) as well. The
 * "alwaysUseJndiLookup" flag just affects {@code name} references and
 * default names (inferred from the field name / property name).
 *
 * <p><b>NOTE:</b> A default CommonAnnotationBeanPostProcessor will be registered
 * by the "context:annotation-config" and "context:component-scan" XML tags.
 * Remove or turn off the default annotation configuration there if you intend
 * to specify a custom CommonAnnotationBeanPostProcessor bean definition!
 * <p><b>NOTE:</b> Annotation injection will be performed <i>before</i> XML injection; thus
 * the latter configuration will override the former for properties wired through
 * both approaches.
 *
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @since 2.5
 * @see #setAlwaysUseJndiLookup
 * @see #setResourceFactory
 * @see org.springframework.beans.factory.annotation.InitDestroyAnnotationBeanPostProcessor
 * @see org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor
 */
@SuppressWarnings("serial")
public class CommonAnnotationBeanPostProcessor extends InitDestroyAnnotationBeanPostProcessor
		implements InstantiationAwareBeanPostProcessor, BeanFactoryAware, Serializable {

	/**
	 * WebServiceRef注解类型
	 */
	@Nullable
	private static final Class<? extends Annotation> webServiceRefClass;

	/**
	 * EJB注解类型
	 */
	@Nullable
	private static final Class<? extends Annotation> ejbClass;

	/**
	 * 资源注释类型集合
	 */
	private static final Set<Class<? extends Annotation>> resourceAnnotationTypes = new LinkedHashSet<>(4);

	/*
	 * 静态块，添加资源注解类型
	 */
	static {
		//尝试加载@WebServiceRef和@EJB资源注解的Class
		webServiceRefClass = loadAnnotationType("javax.xml.ws.WebServiceRef");
		ejbClass = loadAnnotationType("javax.ejb.EJB");

		// 添加@Resource注解
		resourceAnnotationTypes.add(Resource.class);
		if (webServiceRefClass != null) {
			// 添加@WebServiceRef注解
			resourceAnnotationTypes.add(webServiceRefClass);
		}
		if (ejbClass != null) {
			// 添加@EJB注解
			resourceAnnotationTypes.add(ejbClass);
		}
	}

	/**
	 * 忽略资源类型的集合，注意是名字的集合
	 */
	private final Set<String> ignoredResourceTypes = new HashSet<>(1);

	private boolean fallbackToDefaultTypeMatch = true;

	private boolean alwaysUseJndiLookup = false;

	/**
	 * 用于 JNDI 查找的 {@link BeanFactory}。 当 {@link javax.annotation.Resource}
	 * 注解的 `mappedName` 属性被设置，或者 {@link #setAlwaysUseJndiLookup} 为 true 时，会使用此工厂。
	 *
	 * <p>`transient` 关键字表示此字段不应该被序列化。 这是因为在反序列化时会创建一个新的 JNDI {@link BeanFactory}。
	 *
	 * <p>默认为 {@link SimpleJndiBeanFactory} 以提供简单的 JNDI 访问。
	 */
	private transient BeanFactory jndiFactory = new SimpleJndiBeanFactory();

	/**
	 * 用于资源查找的 {@link BeanFactory}。 这是通过 {@link #setBeanFactory} 传递给此后置处理器的
	 * {@link BeanFactory}。 当没有执行 JNDI 查找时，会使用它来自动装配资源。
	 *
	 * <p>`transient` 关键字表示此字段不应该被序列化。 这是因为 {@link BeanFactory} 是不可序列化的。
	 *
	 * <p>如果始终使用 JNDI 查找，或者没有指定 {@link #setResourceFactory}，则可以为 `null`。
	 * 如果为 `null` 并且需要自动装配资源，则会抛出 {@link org.springframework.beans.factory.NoSuchBeanDefinitionException} 异常。
	 */
	@Nullable
	private transient BeanFactory resourceFactory;

	@Nullable
	private transient BeanFactory beanFactory;

	@Nullable
	private transient StringValueResolver embeddedValueResolver;

	/**
	 * 注入点元数据缓存
	 */
	private final transient Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);


	/**
	 * 设置父类的init 和 destroy 注解类型为javax.annotation.PostConstruct和javax.annotation.PreDestroy
	 * 因此我们也可以自定义初始化和销毁注解
	 *
	 * Create a new CommonAnnotationBeanPostProcessor,
	 * with the init and destroy annotation types set to
	 * {@link javax.annotation.PostConstruct} and {@link javax.annotation.PreDestroy},
	 * respectively.
	 */
	public CommonAnnotationBeanPostProcessor() {
		//设置父类InitDestroyAnnotationBeanPostProcessor的order属性
		//用于排序
		setOrder(Ordered.LOWEST_PRECEDENCE - 3);
		//设置父类InitDestroyAnnotationBeanPostProcessor的initAnnotationType属性
		//表示初始化回调注解的类型
		setInitAnnotationType(PostConstruct.class);
		//设置父类InitDestroyAnnotationBeanPostProcessor的destroyAnnotationType属性
		//表示销毁回调注解的类型
		setDestroyAnnotationType(PreDestroy.class);
		//设置自己的ignoredResourceTypes属性
		//表示在解析@Resource注解时忽略给定的资源类型。
		ignoreResourceType("javax.xml.ws.WebServiceContext");
	}


	/**
	 * Ignore the given resource type when resolving {@code @Resource}
	 * annotations.
	 * <p>By default, the {@code javax.xml.ws.WebServiceContext} interface
	 * will be ignored, since it will be resolved by the JAX-WS runtime.
	 * @param resourceType the resource type to ignore
	 */
	public void ignoreResourceType(String resourceType) {
		Assert.notNull(resourceType, "Ignored resource type must not be null");
		this.ignoredResourceTypes.add(resourceType);
	}

	/**
	 * Set whether to allow a fallback to a type match if no explicit name has been
	 * specified. The default name (i.e. the field name or bean property name) will
	 * still be checked first; if a bean of that name exists, it will be taken.
	 * However, if no bean of that name exists, a by-type resolution of the
	 * dependency will be attempted if this flag is "true".
	 * <p>Default is "true". Switch this flag to "false" in order to enforce a
	 * by-name lookup in all cases, throwing an exception in case of no name match.
	 * @see org.springframework.beans.factory.config.AutowireCapableBeanFactory#resolveDependency
	 */
	public void setFallbackToDefaultTypeMatch(boolean fallbackToDefaultTypeMatch) {
		this.fallbackToDefaultTypeMatch = fallbackToDefaultTypeMatch;
	}

	/**
	 * Set whether to always use JNDI lookups equivalent to standard Java EE 5 resource
	 * injection, <b>even for {@code name} attributes and default names</b>.
	 * <p>Default is "false": Resource names are used for Spring bean lookups in the
	 * containing BeanFactory; only {@code mappedName} attributes point directly
	 * into JNDI. Switch this flag to "true" for enforcing Java EE style JNDI lookups
	 * in any case, even for {@code name} attributes and default names.
	 * @see #setJndiFactory
	 * @see #setResourceFactory
	 */
	public void setAlwaysUseJndiLookup(boolean alwaysUseJndiLookup) {
		this.alwaysUseJndiLookup = alwaysUseJndiLookup;
	}

	/**
	 * Specify the factory for objects to be injected into {@code @Resource} /
	 * {@code @WebServiceRef} / {@code @EJB} annotated fields and setter methods,
	 * <b>for {@code mappedName} attributes that point directly into JNDI</b>.
	 * This factory will also be used if "alwaysUseJndiLookup" is set to "true" in order
	 * to enforce JNDI lookups even for {@code name} attributes and default names.
	 * <p>The default is a {@link org.springframework.jndi.support.SimpleJndiBeanFactory}
	 * for JNDI lookup behavior equivalent to standard Java EE 5 resource injection.
	 * @see #setResourceFactory
	 * @see #setAlwaysUseJndiLookup
	 */
	public void setJndiFactory(BeanFactory jndiFactory) {
		Assert.notNull(jndiFactory, "BeanFactory must not be null");
		this.jndiFactory = jndiFactory;
	}

	/**
	 * Specify the factory for objects to be injected into {@code @Resource} /
	 * {@code @WebServiceRef} / {@code @EJB} annotated fields and setter methods,
	 * <b>for {@code name} attributes and default names</b>.
	 * <p>The default is the BeanFactory that this post-processor is defined in,
	 * if any, looking up resource names as Spring bean names. Specify the resource
	 * factory explicitly for programmatic usage of this post-processor.
	 * <p>Specifying Spring's {@link org.springframework.jndi.support.SimpleJndiBeanFactory}
	 * leads to JNDI lookup behavior equivalent to standard Java EE 5 resource injection,
	 * even for {@code name} attributes and default names. This is the same behavior
	 * that the "alwaysUseJndiLookup" flag enables.
	 * @see #setAlwaysUseJndiLookup
	 */
	public void setResourceFactory(BeanFactory resourceFactory) {
		Assert.notNull(resourceFactory, "BeanFactory must not be null");
		this.resourceFactory = resourceFactory;
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		Assert.notNull(beanFactory, "BeanFactory must not be null");
		this.beanFactory = beanFactory;
		if (this.resourceFactory == null) {
			this.resourceFactory = beanFactory;
		}
		if (beanFactory instanceof ConfigurableBeanFactory) {
			this.embeddedValueResolver = new EmbeddedValueResolver((ConfigurableBeanFactory) beanFactory);
		}
	}

	/**
	 * CommonAnnotationBeanPostProcessor的方法
	 * postProcessMergedBeanDefinition` 方法是 `BeanPostProcessor` 接口中的一个回调方法，
	 * 用于在 Bean 定义合并后进行处理。
	 * <p>
	 * 调用父类InitDestroyAnnotationBeanPostProcessor的同名方法用于处理：PostConstruct、PreDestroy注解。
	 * 随后自己处理：@WebServiceRef、@EJB、@Resource注解。
	 *
	 * @param beanDefinition      已合并的bean定义。包含 Bean 的元数据信息，例如 Bean 的类型、作用域等。
	 * @param beanType bean的类型
	 * @param beanName beanName
	 */
	@Override
	public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
		//调用父类的InitDestroyAnnotationBeanPostProcessor的同名方法用于处理@PostConstruct、@PreDestroy注解
		super.postProcessMergedBeanDefinition(beanDefinition, beanType, beanName);
		// 查找并构建 Bean 中带有 @Resource、@WebServiceRef 和 @EJB 注解的成员变量和方法对应的 `InjectionMetadata`。
		// `findResourceMetadata` 方法会扫描 Bean 的类，找到所有需要进行资源注入或建立服务引用的成员变量和方法。
		// `InjectionMetadata` 对象包含了这些注入或引用信息。
		InjectionMetadata metadata = findResourceMetadata(beanName, beanType, null);
		// 检查配置成员。
		// 确保 Bean 定义中的配置信息与需要注入的资源或服务引用之间没有冲突。
		// 例如，如果使用了 `@Resource` 注解，但是没有对应的资源可以注入，或者 `@WebServiceRef` 注解对应的 Web Service 不可用，则会抛出异常。
		metadata.checkConfigMembers(beanDefinition);
	}

	@Override
	public void resetBeanDefinition(String beanName) {
		this.injectionMetadataCache.remove(beanName);
	}

	/**
	 * 此方法的返回值为null，即不会在bean实例化之前产生一个代理对象
	 * @param beanClass the class of the bean to be instantiated
	 * @param beanName the name of the bean
	 * @return
	 */
	@Override
	public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) {
		return null;
	}

	/**
	 * 此方法的返回值为true，也就是说该类不会阻止属性的注入
	 * @param bean the bean instance created, with properties not having been set yet
	 * @param beanName the name of the bean
	 * @return
	 */
	@Override
	public boolean postProcessAfterInstantiation(Object bean, String beanName) {
		return true;
	}

	/**
	 * 进一步解析@WebServiceRef、@EJB、@Resource注解，执行自动注入
	 * @param pvs the property values that the factory is about to apply (never {@code null})  已找到的属性值数组
	 * @param bean the bean instance created, but whose properties have not yet been set  bean实例
	 * @param beanName the name of the bean  beanName
	 * @return  要应用于给定 bean 的实际属性值（可以是传递的pvs），或为null（为了兼容原方法，随后会调用postProcessPropertyValues查找）
	 */
	@Override
	public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) {
		//用于处理方法或者字段上的@WebServiceRef、@EJB、@Resource注解
		//前面的applyMergedBeanDefinitionPostProcessors中已被解析了，因此这里直接从缓存中获取
		InjectionMetadata metadata = findResourceMetadata(beanName, bean.getClass(), pvs);
		try {
			//调用metadata的inject方法完成注解注入
			metadata.inject(bean, beanName, pvs);
		}
		catch (Throwable ex) {
			throw new BeanCreationException(beanName, "Injection of resource dependencies failed", ex);
		}
		//返回pvs，即原参数
		return pvs;
	}

	@Deprecated
	@Override
	public PropertyValues postProcessPropertyValues(
			PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) {

		return postProcessProperties(pvs, bean, beanName);
	}


	/**
	 * 查找给定 Bean 的资源元数据。
	 * <p>
	 * 该方法用于查找给定 Bean 的资源元数据，包括带有 @Resource、@WebServiceRef 和 @EJB 注解的字段和方法。
	 * 如果启用了缓存，则首先从缓存中查找，如果缓存中不存在，则构建新的元数据并放入缓存。
	 * </p>
	 *
	 * @param beanName Bean 的名称。
	 * @param clazz    Bean 的类型。
	 * @param pvs      Bean 的属性值集合，可能为 null。
	 * @return Bean 的资源元数据，包括带有 @Resource、@WebServiceRef 和 @EJB 注解的字段和方法，封装在 InjectionMetadata 对象中。
	 */
	private InjectionMetadata findResourceMetadata(String beanName, final Class<?> clazz, @Nullable PropertyValues pvs) {
		// Fall back to class name as cache key, for backwards compatibility with custom callers.
		// 1. 生成缓存 Key。如果 beanName 存在则使用 beanName，否则使用类名。
		String cacheKey = (StringUtils.hasLength(beanName) ? beanName : clazz.getName());
		// Quick check on the concurrent map first, with minimal locking.
		// 2. 尝试从缓存中获取 InjectionMetadata 对象：
		InjectionMetadata metadata = this.injectionMetadataCache.get(cacheKey);
		// 3. 检查是否需要刷新缓存。如果 metadata 为 null，或者类的类型发生了变化，则需要刷新缓存
		if (InjectionMetadata.needsRefresh(metadata, clazz)) {
			// 4. 使用 synchronized 块保证线程安全，避免并发构建导致重复工作。
			synchronized (this.injectionMetadataCache) {
				// 5. 再次检查缓存，防止在高并发情况下，其他线程已经构建了元数据并放入缓存。
				metadata = this.injectionMetadataCache.get(cacheKey);
				// 6. 再次检查是否需要刷新缓存：与步骤3 作用相同，再次校验
				if (InjectionMetadata.needsRefresh(metadata, clazz)) {
					// 7. 如果 metadata 不为 null，则清除之前的 metadata 信息，为重新构建做准备。
					if (metadata != null) {
						metadata.clear(pvs);
					}
					// 8. 构建新的 Resource 元数据。
					metadata = buildResourceMetadata(clazz);
					// 9.  将构建的元数据放入缓存,以便下次使用。
					this.injectionMetadataCache.put(cacheKey, metadata);
				}
			}
		}
		// 10. 返回从缓存中获取到的元数据。
		return metadata;
	}

	/**
	 * 构建给定类的资源元数据。
	 * <p>
	 * 该方法用于扫描给定类及其父类，查找带有 @Resource、@WebServiceRef 和 @EJB 注解的字段和方法，
	 * 并将它们封装成 InjectedElement 对象，然后创建 InjectionMetadata 对象返回。
	 * </p>
	 *
	 * @param clazz 要构建资源元数据的类。
	 * @return 类的资源元数据，包括带有 @Resource、@WebServiceRef 和 @EJB 注解的字段和方法，封装在 InjectionMetadata 对象中。
	 */
	private InjectionMetadata buildResourceMetadata(final Class<?> clazz) {
		// 快速检查类是否包含任何资源注解（@Resource、@WebServiceRef 或 @EJB）。
		// 如果没有，直接返回一个空的 InjectionMetadata 对象，避免不必要的扫描。
		if (!AnnotationUtils.isCandidateClass(clazz, resourceAnnotationTypes)) {
			return InjectionMetadata.EMPTY;
		}

		// 用于存储 InjectedElement 对象的列表，每个 InjectedElement 对象代表一个需要注入的字段或方法。
		List<InjectionMetadata.InjectedElement> elements = new ArrayList<>();
		// 从当前类开始，向上遍历父类。
		Class<?> targetClass = clazz;

		// 循环遍历类及其父类，直到到达 Object 类或没有父类为止。
		do {
			// 存储当前类中找到的 InjectedElement 对象的临时列表。
			final List<InjectionMetadata.InjectedElement> currElements = new ArrayList<>();

			// ------ 扫描字段 ------

			// 使用反射工具类 ReflectionUtils 查找当前类中的所有字段。
			// 查询是否有webService,ejb,Resource的属性注解，但是不支持静态属性
			ReflectionUtils.doWithLocalFields(targetClass, field -> {
				// 检查字段是否带有 @WebServiceRef 注解。
				if (webServiceRefClass != null && field.isAnnotationPresent(webServiceRefClass)) {
					// 检查字段是否是静态的，静态字段不支持 @WebServiceRef 注解。
					if (Modifier.isStatic(field.getModifiers())) {
						throw new IllegalStateException("@WebServiceRef annotation is not supported on static fields");
					}
					// 创建 WebServiceRefElement 对象，封装 @WebServiceRef 注解字段的信息。
					currElements.add(new WebServiceRefElement(field, field, null));
				}
				// 检查字段是否带有 @EJB 注解。
				else if (ejbClass != null && field.isAnnotationPresent(ejbClass)) {
					// 检查字段是否是静态的，静态字段不支持 @EJB 注解。
					if (Modifier.isStatic(field.getModifiers())) {
						throw new IllegalStateException("@EJB annotation is not supported on static fields");
					}
					// 创建 EjbRefElement 对象，封装 @EJB 注解字段的信息。
					currElements.add(new EjbRefElement(field, field, null));
				}
				// 检查字段是否带有 @Resource 注解。
				else if (field.isAnnotationPresent(Resource.class)) {
					// 检查字段是否是静态的，静态字段不支持 @Resource 注解。
					if (Modifier.isStatic(field.getModifiers())) {
						throw new IllegalStateException("@Resource annotation is not supported on static fields");
					}
					// 检查是否忽略该类型的资源注入。如果不想注入某一类型对象 可以将其加入ignoredResourceTypes中
					if (!this.ignoredResourceTypes.contains(field.getType().getName())) {
						// 创建 ResourceElement 对象，封装 @Resource 注解字段的信息。
						currElements.add(new ResourceElement(field, field, null));
					}
				}
			});

			// ------ 扫描方法 ------

			// 使用反射工具类 ReflectionUtils 查找当前类中的所有方法。
			ReflectionUtils.doWithLocalMethods(targetClass, method -> {
				//解析桥接方法，获取原始方法。找出我们在代码中定义的方法而非编译器为我们生成的方法
				Method bridgedMethod = BridgeMethodResolver.findBridgedMethod(method);
				// 检查方法的可访问性，如果方法是桥接方法，并且桥接方法和原始方法的可见性不一致，则忽略该方法。
				if (!BridgeMethodResolver.isVisibilityBridgeMethodPair(method, bridgedMethod)) {
					return;
				}
				// 确保处理的是最具体的方法（避免重复处理）。
				if (method.equals(ClassUtils.getMostSpecificMethod(method, clazz))) {
					// 检查桥接方法是否带有 @WebServiceRef 注解。
					if (webServiceRefClass != null && bridgedMethod.isAnnotationPresent(webServiceRefClass)) {
						// 检查方法是否是静态的，静态方法不支持 @WebServiceRef 注解。
						if (Modifier.isStatic(method.getModifiers())) {
							throw new IllegalStateException("@WebServiceRef annotation is not supported on static methods");
						}
						// 检查方法参数数量是否为 1，@WebServiceRef 注解的方法必须有一个参数。
						if (method.getParameterCount() != 1) {
							throw new IllegalStateException("@WebServiceRef annotation requires a single-arg method: " + method);
						}
						// 查找方法的属性描述器，用于后续的属性注入。
						PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
						// 创建 WebServiceRefElement 对象，封装 @WebServiceRef 注解方法的信息。
						currElements.add(new WebServiceRefElement(method, bridgedMethod, pd));
					}
					// 检查桥接方法是否带有 @EJB 注解。
					else if (ejbClass != null && bridgedMethod.isAnnotationPresent(ejbClass)) {
						// 检查方法是否是静态的，静态方法不支持 @EJB 注解。
						if (Modifier.isStatic(method.getModifiers())) {
							throw new IllegalStateException("@EJB annotation is not supported on static methods");
						}
						// 检查方法参数数量是否为 1，@EJB 注解的方法必须有一个参数。
						if (method.getParameterCount() != 1) {
							throw new IllegalStateException("@EJB annotation requires a single-arg method: " + method);
						}
						// 查找方法的属性描述器，用于后续的属性注入。
						PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
						// 创建 EjbRefElement 对象，封装 @EJB 注解方法的信息。
						currElements.add(new EjbRefElement(method, bridgedMethod, pd));
					}
					// 检查桥接方法是否带有 @Resource 注解。
					else if (bridgedMethod.isAnnotationPresent(Resource.class)) {
						// 检查方法是否是静态的，静态方法不支持 @Resource 注解。
						if (Modifier.isStatic(method.getModifiers())) {
							throw new IllegalStateException("@Resource annotation is not supported on static methods");
						}
						// 获取方法的参数类型。
						Class<?>[] paramTypes = method.getParameterTypes();
						// 检查方法参数数量是否为 1，@Resource 注解的方法必须有一个参数。
						if (paramTypes.length != 1) {
							throw new IllegalStateException("@Resource annotation requires a single-arg method: " + method);
						}
						// 检查是否忽略该类型的资源注入。如果不想注入某一类型对象 可以将其加入ignoredResourceTypes中
						if (!this.ignoredResourceTypes.contains(paramTypes[0].getName())) {
							// 查找方法的属性描述器，用于后续的属性注入。
							PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
							// 创建 ResourceElement 对象，封装 @Resource 注解方法的信息。
							currElements.add(new ResourceElement(method, bridgedMethod, pd));
						}
					}
				}
			});
			// 将当前类中找到的 InjectedElement 对象添加到总的 InjectedElement 对象列表的头部，保证父类的字段和方法在子类之前注入。
			elements.addAll(0, currElements);
			// 移动到父类，继续扫描。
			targetClass = targetClass.getSuperclass();
		}
		while (targetClass != null && targetClass != Object.class);
		// 创建 InjectionMetadata 对象，封装找到的 InjectedElement 对象，并返回。
		return InjectionMetadata.forElements(elements, clazz);
	}

	/**
	 * Obtain a lazily resolving resource proxy for the given name and type,
	 * delegating to {@link #getResource} on demand once a method call comes in.
	 * @param element the descriptor for the annotated field/method
	 * @param requestingBeanName the name of the requesting bean
	 * @return the resource object (never {@code null})
	 * @since 4.2
	 * @see #getResource
	 * @see Lazy
	 */
	protected Object buildLazyResourceProxy(final LookupElement element, final @Nullable String requestingBeanName) {
		TargetSource ts = new TargetSource() {
			@Override
			public Class<?> getTargetClass() {
				return element.lookupType;
			}
			@Override
			public boolean isStatic() {
				return false;
			}
			@Override
			public Object getTarget() {
				return getResource(element, requestingBeanName);
			}
			@Override
			public void releaseTarget(Object target) {
			}
		};
		// 代理对象工厂
		ProxyFactory pf = new ProxyFactory();
		pf.setTargetSource(ts);
		if (element.lookupType.isInterface()) {
			pf.addInterface(element.lookupType);
		}
		ClassLoader classLoader = (this.beanFactory instanceof ConfigurableBeanFactory ?
				((ConfigurableBeanFactory) this.beanFactory).getBeanClassLoader() : null);
		return pf.getProxy(classLoader);
	}

	/**
	 * 获取给定名称和类型的资源对象。
	 * Obtain the resource object for the given name and type.
	 * @param element the descriptor for the annotated field/method    LookupElement实例，包含了关于要查找的资源的信息 (例如，名称，类型)。
	 * @param requestingBeanName the name of the requesting bean  发起注入请求的 bean 的名称。 用于记录日志和提供更清晰的错误信息。
	 * @return the resource object (never {@code null})  查找到的资源对象。（从不｛@code null｝）
	 * @throws NoSuchBeanDefinitionException if no corresponding target resource found
	 */
	protected Object getResource(LookupElement element, @Nullable String requestingBeanName)
			throws NoSuchBeanDefinitionException {

		// 如果 mappedName 不为空，优先使用 JNDI 查找
		if (StringUtils.hasLength(element.mappedName)) {
			return this.jndiFactory.getBean(element.mappedName, element.lookupType);
		}

		// 如果配置了 alwaysUseJndiLookup 为 true，则使用 JNDI 查找
		if (this.alwaysUseJndiLookup) {
			return this.jndiFactory.getBean(element.name, element.lookupType);
		}

		// 如果 resourceFactory 为空，抛出异常
		// resourceFactory一般不为null，就是当前的工厂DefaultListableBeanFactory
		if (this.resourceFactory == null) {
			throw new NoSuchBeanDefinitionException(element.lookupType,
					"No resource factory configured - specify the 'resourceFactory' property");
		}
		// 使用 resourceFactory 进行自动装配
		return autowireResource(this.resourceFactory, element, requestingBeanName);
	}

	/**
	 * 根据给定工厂获取自动注入的资源对象。
	 * <p>该方法是 {@code @Resource} 注解进行资源查找的核心方法，用于处理字段或方法上的依赖注入。
	 *
	 * Obtain a resource object for the given name and type through autowiring
	 * based on the given factory.
	 * @param factory the factory to autowire against   Bean 工厂，用于查找和创建 Bean 实例
	 * @param element the descriptor for the annotated field/method   包含依赖信息的查找元素 (LookupElement)，例如依赖的名称和类型
	 * @param requestingBeanName the name of the requesting bean  请求注入的 Bean 的名称 (可选)，用于注册依赖关系
	 * @return the resource object (never {@code null})   注入的资源对象
	 * @throws NoSuchBeanDefinitionException if no corresponding target resource found
	 */
	protected Object autowireResource(BeanFactory factory, LookupElement element, @Nullable String requestingBeanName)
			throws NoSuchBeanDefinitionException {

		// 用于存储找到的资源对象
		Object resource;
		// 用于存储自动注入的 Bean 的名称集合
		Set<String> autowiredBeanNames;
		// 获取要查找的依赖的名称，此名称来源于@Resource的name属性或者字段名/setter方法名
		String name = element.name;

		// 如果 Bean 工厂是 AutowireCapableBeanFactory，则使用更高级的自动装配功能
		if (factory instanceof AutowireCapableBeanFactory) {
			AutowireCapableBeanFactory beanFactory = (AutowireCapableBeanFactory) factory;
			//获取依赖描述符（可能是字段或者方法），实际类型为LookupDependencyDescriptor，它的required属性为true
			DependencyDescriptor descriptor = element.getDependencyDescriptor();

			// fallbackToDefaultTypeMatch 为 true（默认情况），element.isDefaultName 为 true（未指定 @Resource 的 name 属性），
			// 并且 Bean 工厂不包含具有给定名称的 Bean 定义或实例，则根据类型查找
			if (this.fallbackToDefaultTypeMatch && element.isDefaultName && !factory.containsBean(name)) {
				//如果容器中还没有此bean，则会使用resolveDependency()方法将符合bean type的bean definition调用一次getBean()
				// 从这些bean选出符合requestingBeanName的bean
				autowiredBeanNames = new LinkedHashSet<>();
				resource = beanFactory.resolveDependency(descriptor, requestingBeanName, autowiredBeanNames, null);
				if (resource == null) {
					throw new NoSuchBeanDefinitionException(element.getLookupType(), "No resolvable resource object");
				}
			}
			// 否则，根据名称查找 Bean
			else {
				//根据name查找，如果容器中有此bean则取出这个bean对象作为属性值
				resource = beanFactory.resolveBeanByName(name, descriptor);
				// 创建一个包含 Bean 名称的单例集合
				autowiredBeanNames = Collections.singleton(name);
			}
		}
		// 如果 Bean 工厂不是 AutowireCapableBeanFactory，则只能根据名称和类型获取 Bean
		else {
			// 根据名称和类型获取 Bean
			resource = factory.getBean(name, element.lookupType);
			// 创建一个包含 Bean 名称的单例集合
			autowiredBeanNames = Collections.singleton(name);
		}

		// 如果 Bean 工厂是 ConfigurableBeanFactory，则注册依赖 Bean，以便在销毁 Bean 时可以正确处理依赖关系
		if (factory instanceof ConfigurableBeanFactory) {
			ConfigurableBeanFactory beanFactory = (ConfigurableBeanFactory) factory;
			for (String autowiredBeanName : autowiredBeanNames) {
				// 如果请求注入的 Bean 存在，则注册依赖 Bean
				if (requestingBeanName != null && beanFactory.containsBean(autowiredBeanName)) {
					// 将 autowiredBeanName 注册为 requestingBeanName 的依赖 Bean
					beanFactory.registerDependentBean(autowiredBeanName, requestingBeanName);
				}
			}
		}
		// 返回找到的资源对象
		return resource;
	}


	@SuppressWarnings("unchecked")
	@Nullable
	private static Class<? extends Annotation> loadAnnotationType(String name) {
		try {
			return (Class<? extends Annotation>)
					ClassUtils.forName(name, CommonAnnotationBeanPostProcessor.class.getClassLoader());
		}
		catch (ClassNotFoundException ex) {
			return null;
		}
	}


	/**
	 * 表示有关带注释字段或setter方法的泛型注入信息，支持@WebServiceRef、@EJB、@Resource注解。
	 * Class representing generic injection information about an annotated field
	 * or setter method, supporting @Resource and related annotations.
	 */
	protected abstract static class LookupElement extends InjectionMetadata.InjectedElement {

		/**
		 * 依赖的属性 name
		 */
		protected String name = "";
		/**
		 * 是否没有设置name属性
		 */
		protected boolean isDefaultName = false;
		/**
		 * 依赖类型
		 */
		protected Class<?> lookupType = Object.class;
		/**
		 * 映射的名称
		 */
		@Nullable
		protected String mappedName;

		public LookupElement(Member member, @Nullable PropertyDescriptor pd) {
			//调用父类InjectionMetadata的构造器
			super(member, pd);
		}

		/**
		 * Return the resource name for the lookup.
		 */
		public final String getName() {
			return this.name;
		}

		/**
		 * Return the desired type for the lookup.
		 */
		public final Class<?> getLookupType() {
			return this.lookupType;
		}

		/**
		 * Build a DependencyDescriptor for the underlying field/method.
		 * 基础字段方法生成 DependencyDescriptor
		 * lookupType首先取指定的type属性指定的类型，没有则取属性或者参数的类型
		 * LookupDependencyDescriptor的required属性为true
		 */
		public final DependencyDescriptor getDependencyDescriptor() {
			if (this.isField) {
				return new LookupDependencyDescriptor((Field) this.member, this.lookupType);
			}
			else {
				return new LookupDependencyDescriptor((Method) this.member, this.lookupType);
			}
		}
	}


	/**
	 * 表示有关带注释字段或setter方法的注入信息，支持@Resource注释。
	 * Class representing injection information about an annotated field
	 * or setter method, supporting the @Resource annotation.
	 */
	private class ResourceElement extends LookupElement {
		/**
		 * 是否需要返回代理依赖对象
		 */
		private final boolean lazyLookup;

		/**
		 *  构造器，在applyMergedBeanDefinitionPostProcessors方法中被调用，用来创建@Resource注解相关对象
		 *
		 * @param member 字段或者方法
		 * @param ae     字段或者方法
		 * @param pd     构造器传递null
		 */
		public ResourceElement(Member member, AnnotatedElement ae, @Nullable PropertyDescriptor pd) {
			//调用父类LookupElement的构造器
			super(member, pd);
			//获取@Resource注解
			Resource resource = ae.getAnnotation(Resource.class);
			//获取name属性值，如果没设置，那么默认返回""
			String resourceName = resource.name();
			//获取type属性，如果没设置，那么默认返回Object.class
			Class<?> resourceType = resource.type();
			//是否没有设置name属性，如果没设置，那么isDefaultName=true
			this.isDefaultName = !StringUtils.hasLength(resourceName);
			/*如果没设置了name属性，使用自己的规则作为name*/
			if (this.isDefaultName) {
				//获取属性名或者方法名
				resourceName = this.member.getName();
				//如果是setter方法，并且长度大于3个字符
				if (this.member instanceof Method && resourceName.startsWith("set") && resourceName.length() > 3) {
					//那么截取setter方法名的"set"之后的部分，并进行处理：如果至少开头两个字符是大写，那么就返回原截取的值，否则返回开头为小写的截取的值
					resourceName = Introspector.decapitalize(resourceName.substring(3));
				}
			}
			/*如果设置了name属性，那么解析name属性值*/
			else if (embeddedValueResolver != null) {
				//这里就是解析name值中的占位符的逻辑，将占位符替换为属性值
				//因此设置的name属性支持占位符，即${.. : ..}，占位符的语法和解析之前就学过了，这里的占位符支持普通方式从外部配置文件中加载进来的属性以及environment的属性
				// 如果设置了@Resource name的属性，则使用EmbeddedValueResolver对象先做一次SpringEL解析得到真正的bean name
				resourceName = embeddedValueResolver.resolveStringValue(resourceName);
			}
			/*如果type不是Object类型，那么检查指定的type*/
			if (Object.class != resourceType) {
				//如果指定type和字段类型或者方法参数类型不兼容，那么抛出异常
				checkResourceType(resourceType);
			}
			else {
				// No resource type specified... check field/method.
				//如果是，那么获取字段类型或者方法参数类型作为type
				resourceType = getResourceType();
			}
			//设置为name属性为resourceName
			this.name = (resourceName != null ? resourceName : "");
			//设置lookupType属性为resourceType
			this.lookupType = resourceType;
			//获取注解上的lookup属性值，如果没设置，那么默认返回""，一般没人设置
			String lookupValue = resource.lookup();
			// 使用jndi查找名字，如果没有设置，那么设置mappedName属性值为mappedName属性值，否则设置为lookupValue的值
			this.mappedName = (StringUtils.hasLength(lookupValue) ? lookupValue : resource.mappedName());
			// 尝试获取@Lazy注解
			Lazy lazy = ae.getAnnotation(Lazy.class);
			// 是否延迟注入，如果存在@Lazy注解并且值为true，那么lazyLookup为true，否则就是false，这表示是否返回一个代理的依赖
			this.lazyLookup = (lazy != null && lazy.value());
		}

		/**
		 * 获取要注入的资源。
		 * 如果 `lazyLookup` 为 true，则返回一个代理对象；否则，直接返回查找到的资源。
		 *
		 * @param target    需要进行注入的目标实例 (未使用，可能是为了保持接口一致性)
		 * @param requestingBeanName   发起注入请求的 bean 的名称。 用于记录日志和提供更清晰的错误信息。
		 * @return  要注入的资源或代理对象。
		 */
		@Override
		protected Object getResourceToInject(Object target, @Nullable String requestingBeanName) {
			// 根据 lazyLookup 标志决定是创建延迟加载的代理对象（内部是用AOP代理工厂做的）还是直接获取资源
			return (this.lazyLookup ? buildLazyResourceProxy(this, requestingBeanName) :
					getResource(this, requestingBeanName));
		}
	}


	/**
	 * Class representing injection information about an annotated field
	 * or setter method, supporting the @WebServiceRef annotation.
	 */
	private class WebServiceRefElement extends LookupElement {

		private final Class<?> elementType;

		private final String wsdlLocation;

		public WebServiceRefElement(Member member, AnnotatedElement ae, @Nullable PropertyDescriptor pd) {
			super(member, pd);
			WebServiceRef resource = ae.getAnnotation(WebServiceRef.class);
			String resourceName = resource.name();
			Class<?> resourceType = resource.type();
			this.isDefaultName = !StringUtils.hasLength(resourceName);
			if (this.isDefaultName) {
				resourceName = this.member.getName();
				if (this.member instanceof Method && resourceName.startsWith("set") && resourceName.length() > 3) {
					resourceName = Introspector.decapitalize(resourceName.substring(3));
				}
			}
			if (Object.class != resourceType) {
				checkResourceType(resourceType);
			}
			else {
				// No resource type specified... check field/method.
				resourceType = getResourceType();
			}
			this.name = resourceName;
			this.elementType = resourceType;
			if (Service.class.isAssignableFrom(resourceType)) {
				this.lookupType = resourceType;
			}
			else {
				this.lookupType = resource.value();
			}
			this.mappedName = resource.mappedName();
			this.wsdlLocation = resource.wsdlLocation();
		}

		@Override
		protected Object getResourceToInject(Object target, @Nullable String requestingBeanName) {
			Service service;
			try {
				service = (Service) getResource(this, requestingBeanName);
			}
			catch (NoSuchBeanDefinitionException notFound) {
				// Service to be created through generated class.
				if (Service.class == this.lookupType) {
					throw new IllegalStateException("No resource with name '" + this.name + "' found in context, " +
							"and no specific JAX-WS Service subclass specified. The typical solution is to either specify " +
							"a LocalJaxWsServiceFactoryBean with the given name or to specify the (generated) Service " +
							"subclass as @WebServiceRef(...) value.");
				}
				if (StringUtils.hasLength(this.wsdlLocation)) {
					try {
						Constructor<?> ctor = this.lookupType.getConstructor(URL.class, QName.class);
						WebServiceClient clientAnn = this.lookupType.getAnnotation(WebServiceClient.class);
						if (clientAnn == null) {
							throw new IllegalStateException("JAX-WS Service class [" + this.lookupType.getName() +
									"] does not carry a WebServiceClient annotation");
						}
						service = (Service) BeanUtils.instantiateClass(ctor,
								new URL(this.wsdlLocation), new QName(clientAnn.targetNamespace(), clientAnn.name()));
					}
					catch (NoSuchMethodException ex) {
						throw new IllegalStateException("JAX-WS Service class [" + this.lookupType.getName() +
								"] does not have a (URL, QName) constructor. Cannot apply specified WSDL location [" +
								this.wsdlLocation + "].");
					}
					catch (MalformedURLException ex) {
						throw new IllegalArgumentException(
								"Specified WSDL location [" + this.wsdlLocation + "] isn't a valid URL");
					}
				}
				else {
					service = (Service) BeanUtils.instantiateClass(this.lookupType);
				}
			}
			return service.getPort(this.elementType);
		}
	}


	/**
	 * Class representing injection information about an annotated field
	 * or setter method, supporting the @EJB annotation.
	 */
	private class EjbRefElement extends LookupElement {

		private final String beanName;

		public EjbRefElement(Member member, AnnotatedElement ae, @Nullable PropertyDescriptor pd) {
			super(member, pd);
			EJB resource = ae.getAnnotation(EJB.class);
			String resourceBeanName = resource.beanName();
			String resourceName = resource.name();
			this.isDefaultName = !StringUtils.hasLength(resourceName);
			if (this.isDefaultName) {
				resourceName = this.member.getName();
				if (this.member instanceof Method && resourceName.startsWith("set") && resourceName.length() > 3) {
					resourceName = Introspector.decapitalize(resourceName.substring(3));
				}
			}
			Class<?> resourceType = resource.beanInterface();
			if (Object.class != resourceType) {
				checkResourceType(resourceType);
			}
			else {
				// No resource type specified... check field/method.
				resourceType = getResourceType();
			}
			this.beanName = resourceBeanName;
			this.name = resourceName;
			this.lookupType = resourceType;
			this.mappedName = resource.mappedName();
		}

		@Override
		protected Object getResourceToInject(Object target, @Nullable String requestingBeanName) {
			if (StringUtils.hasLength(this.beanName)) {
				if (beanFactory != null && beanFactory.containsBean(this.beanName)) {
					// Local match found for explicitly specified local bean name.
					Object bean = beanFactory.getBean(this.beanName, this.lookupType);
					if (requestingBeanName != null && beanFactory instanceof ConfigurableBeanFactory) {
						((ConfigurableBeanFactory) beanFactory).registerDependentBean(this.beanName, requestingBeanName);
					}
					return bean;
				}
				else if (this.isDefaultName && !StringUtils.hasLength(this.mappedName)) {
					throw new NoSuchBeanDefinitionException(this.beanName,
							"Cannot resolve 'beanName' in local BeanFactory. Consider specifying a general 'name' value instead.");
				}
			}
			// JNDI name lookup - may still go to a local BeanFactory.
			return getResource(this, requestingBeanName);
		}
	}


	/**
	 * Extension of the DependencyDescriptor class,
	 * overriding the dependency type with the specified resource type.
	 */
	private static class LookupDependencyDescriptor extends DependencyDescriptor {

		private final Class<?> lookupType;

		public LookupDependencyDescriptor(Field field, Class<?> lookupType) {
			super(field, true);
			this.lookupType = lookupType;
		}

		public LookupDependencyDescriptor(Method method, Class<?> lookupType) {
			super(new MethodParameter(method, 0), true);
			this.lookupType = lookupType;
		}

		@Override
		public Class<?> getDependencyType() {
			return this.lookupType;
		}
	}

}
