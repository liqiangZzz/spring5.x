/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.annotation;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.event.DefaultEventListenerFactory;
import org.springframework.context.event.EventListenerMethodProcessor;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;

/**
 * Utility class that allows for convenient registration of common
 * {@link org.springframework.beans.factory.config.BeanPostProcessor} and
 * {@link org.springframework.beans.factory.config.BeanFactoryPostProcessor}
 * definitions for annotation-based configuration. Also registers a common
 * {@link org.springframework.beans.factory.support.AutowireCandidateResolver}.
 *
 * @author Mark Fisher
 * @author Juergen Hoeller
 * @author Chris Beams
 * @author Phillip Webb
 * @author Stephane Nicoll
 * @since 2.5
 * @see ContextAnnotationAutowireCandidateResolver
 * @see ConfigurationClassPostProcessor
 * @see CommonAnnotationBeanPostProcessor
 * @see org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor
 * see org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor
 */
public abstract class AnnotationConfigUtils {

	/**
	 *  ConfigurationClassPostProcessor的bean name常量
	 *
	 * The bean name of the internally managed Configuration annotation processor.
	 */
	public static final String CONFIGURATION_ANNOTATION_PROCESSOR_BEAN_NAME =
			"org.springframework.context.annotation.internalConfigurationAnnotationProcessor";

	/**
	 * The bean name of the internally managed BeanNameGenerator for use when processing
	 * {@link Configuration} classes. Set by {@link AnnotationConfigApplicationContext}
	 * and {@code AnnotationConfigWebApplicationContext} during bootstrap in order to make
	 * any custom name generation strategy available to the underlying
	 * {@link ConfigurationClassPostProcessor}.
	 * @since 3.1.1
	 */
	public static final String CONFIGURATION_BEAN_NAME_GENERATOR =
			"org.springframework.context.annotation.internalConfigurationBeanNameGenerator";

	/**
	 * The bean name of the internally managed Autowired annotation processor.
	 */
	public static final String AUTOWIRED_ANNOTATION_PROCESSOR_BEAN_NAME =
			"org.springframework.context.annotation.internalAutowiredAnnotationProcessor";

	/**
	 *  AutowiredAnnotationBeanPostProcessor的bean name常量
	 *
	 * The bean name of the internally managed Required annotation processor.
	 * @deprecated as of 5.1, since no Required processor is registered by default anymore
	 */
	@Deprecated
	public static final String REQUIRED_ANNOTATION_PROCESSOR_BEAN_NAME =
			"org.springframework.context.annotation.internalRequiredAnnotationProcessor";

	/**
	 * CommonAnnotationBeanPostProcessor的bean name常量
	 *
	 * The bean name of the internally managed JSR-250 annotation processor.
	 */
	public static final String COMMON_ANNOTATION_PROCESSOR_BEAN_NAME =
			"org.springframework.context.annotation.internalCommonAnnotationProcessor";

	/**
	 * PersistenceAnnotationBeanPostProcessor的bean name常量，用于JPA 注解支持
	 *
	 * The bean name of the internally managed JPA annotation processor.
	 */
	public static final String PERSISTENCE_ANNOTATION_PROCESSOR_BEAN_NAME =
			"org.springframework.context.annotation.internalPersistenceAnnotationProcessor";

	/**
	 * PersistenceAnnotationBeanPostProcessor的bean class常量，用于JPA 注解支持
	 */
	private static final String PERSISTENCE_ANNOTATION_PROCESSOR_CLASS_NAME =
			"org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor";

	/**
	 * EventListenerMethodProcessor的bean name常量
	 *
	 * The bean name of the internally managed @EventListener annotation processor.
	 */
	public static final String EVENT_LISTENER_PROCESSOR_BEAN_NAME =
			"org.springframework.context.event.internalEventListenerProcessor";

	/**
	 * DefaultEventListenerFactory的bean name常量
	 *
	 * The bean name of the internally managed EventListenerFactory.
	 */
	public static final String EVENT_LISTENER_FACTORY_BEAN_NAME =
			"org.springframework.context.event.internalEventListenerFactory";

	private static final boolean jsr250Present;

	private static final boolean jpaPresent;

	static {
		ClassLoader classLoader = AnnotationConfigUtils.class.getClassLoader();
		jsr250Present = ClassUtils.isPresent("javax.annotation.Resource", classLoader);
		jpaPresent = ClassUtils.isPresent("javax.persistence.EntityManagerFactory", classLoader) &&
				ClassUtils.isPresent(PERSISTENCE_ANNOTATION_PROCESSOR_CLASS_NAME, classLoader);
	}


	/**
	 * Register all relevant annotation post processors in the given registry.
	 * @param registry the registry to operate on
	 */
	public static void registerAnnotationConfigProcessors(BeanDefinitionRegistry registry) {
		registerAnnotationConfigProcessors(registry, null);
	}

	/**
	 * 在给定注册表中注册所有注解配置处理器的bean定义，用于后续步骤中对相关注解的处理
	 * <p>
	 * ConfigurationClassPostProcessor -> 用于处理@Configuration注解 -> 一个BeanDefinitionRegistryPostProcessor
	 * AutowiredAnnotationBeanPostProcessor -> 用于处理@Autowired、@Value、@Inject、@Lookup注解 -> 一个BeanPostProcessor
	 * CommonAnnotationBeanPostProcessor(要求支持JSR 250，默认支持) -> 用于处理@Resource、@PostConstruct、@PreDestroy、@WebServiceRef、@EJB等注解 -> 一个BeanPostProcessor
	 * PersistenceAnnotationBeanPostProcessor(要求支持JPA，默认不支持，需要引入JPA依赖) -> 用于处理JPA的相关注解，比如@PersistenceContext和@PersistenceUnit -> 一个BeanPostProcessor
	 * EventListenerMethodProcessor ->  用于处理@EventListener注解 -> 一个 SmartInitializingSingleton
	 * DefaultEventListenerFactory ->用于注册为解析@EventListener注解的方法而创建ApplicationListener的策略接口对应 -> 一个EventListenerFactory
	 * <p>
	 * 注意，自Spring5.1开始，RequiredAnnotationBeanPostProcessor后处理器不被自动注册，因为Spring不赞成使用@Required注解，对于必须的依赖推荐使用构造器注入
	 *
	 * Register all relevant annotation post processors in the given registry.
	 * @param registry the registry to operate on 要操作的注册表
	 * @param source the configuration source element (already extracted)
	 * that this registration was triggered from. May be {@code null}. 触发此注册的配置源
	 * @return a Set of BeanDefinitionHolders, containing all bean definitions
	 * that have actually been registered by this call  此方法实际注册的所有注解后处理器的 Bean 定义
	 */
	public static Set<BeanDefinitionHolder> registerAnnotationConfigProcessors(
			BeanDefinitionRegistry registry, @Nullable Object source) {

		// 获取beanFactory，就是registry自身
		DefaultListableBeanFactory beanFactory = unwrapDefaultListableBeanFactory(registry);
		if (beanFactory != null) {
			if (!(beanFactory.getDependencyComparator() instanceof AnnotationAwareOrderComparator)) {
				//设置比较器，可以看到设置的是AnnotationAwareOrderComparator比较器，可以支持PriorityOrdered接口、Ordered接口、@Ordered注解、@Priority注解的排序，比较优先级为PriorityOrdered>Ordered>@Ordered>@Priority
				//后面会学习的OrderComparator 比较器只支持PriorityOrdered、Ordered接口的排序，比较优先级为PriorityOrdered>Ordered
				//如果最终没找到设置的排序值，那么返回Integer.MAX_VALUE，即最低优先级。
				beanFactory.setDependencyComparator(AnnotationAwareOrderComparator.INSTANCE);
			}
			//设置一个ContextAnnotationAutowireCandidateResolver，以决定是否应将 Bean 定义视为主动注入的候选项
			if (!(beanFactory.getAutowireCandidateResolver() instanceof ContextAnnotationAutowireCandidateResolver)) {
				// 设置自动装配解析器
				beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
			}
		}

		// 创建BeanDefinitionHolder集合
		Set<BeanDefinitionHolder> beanDefs = new LinkedHashSet<>(8);

		/*
		 * 如果注册表的beanDefinitionMap缓存中没有"org.springframework.context.annotation.internalConfigurationAnnotationProcessor"为name的缓存
		 * 那么创建并且注册一个BeanDefinitionRegistryPostProcessor后处理器的bean定义，用于处理@Configuration注解的后置处理器bean
		 */
		if (!registry.containsBeanDefinition(CONFIGURATION_ANNOTATION_PROCESSOR_BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition(ConfigurationClassPostProcessor.class);
			def.setSource(source);
			// 注册BeanDefinition到注册表中
			beanDefs.add(registerPostProcessor(registry, def, CONFIGURATION_ANNOTATION_PROCESSOR_BEAN_NAME));
		}

		/*
		 * 如果注册表的beanDefinitionMap缓存中没有"org.springframework.context.annotation.internalAutowiredAnnotationProcessor"为name的缓存
		 * 那么创建并且注册一个AutowiredAnnotationBeanPostProcessor后处理器的bean定义，用于处理@Autowired、@Value、@Inject、@Lookup注解的后置处理器bean
		 */
		if (!registry.containsBeanDefinition(AUTOWIRED_ANNOTATION_PROCESSOR_BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition(AutowiredAnnotationBeanPostProcessor.class);
			def.setSource(source);
			beanDefs.add(registerPostProcessor(registry, def, AUTOWIRED_ANNOTATION_PROCESSOR_BEAN_NAME));
		}

		// Check for JSR-250 support, and if present add the CommonAnnotationBeanPostProcessor.
		/*
		 * 检查 JSR-250 支持，如果存在JSR-250的依赖，并且注册表的beanDefinitionMap缓存中没有"org.springframework.context.annotation.internalCommonAnnotationProcessor"为name的缓存
		 * 那么创建并且注册一个CommonAnnotationBeanPostProcessor后处理器的bean定义，用于处理@Resource、@PostConstruct、@PreDestroy、@WebServiceRef、@EJB等注解的后置处理器bean
		 */
		if (jsr250Present && !registry.containsBeanDefinition(COMMON_ANNOTATION_PROCESSOR_BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition(CommonAnnotationBeanPostProcessor.class);
			def.setSource(source);
			beanDefs.add(registerPostProcessor(registry, def, COMMON_ANNOTATION_PROCESSOR_BEAN_NAME));
		}

		// Check for JPA support, and if present add the PersistenceAnnotationBeanPostProcessor.
		/*
		 * 检查 JPA 支持，如果存在JPA的依赖，并且注册表的beanDefinitionMap缓存中没有"org.springframework.context.annotation.internalPersistenceAnnotationProcessor"为name的缓存
		 * 那么创建并且注册一个PersistenceAnnotationBeanPostProcessor后处理器的bean定义，用于处理JPA的相关注解，比如@PersistenceContext和@PersistenceUnit
		 */
		if (jpaPresent && !registry.containsBeanDefinition(PERSISTENCE_ANNOTATION_PROCESSOR_BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition();
			try {
				def.setBeanClass(ClassUtils.forName(PERSISTENCE_ANNOTATION_PROCESSOR_CLASS_NAME,
						AnnotationConfigUtils.class.getClassLoader()));
			}
			catch (ClassNotFoundException ex) {
				throw new IllegalStateException(
						"Cannot load optional framework class: " + PERSISTENCE_ANNOTATION_PROCESSOR_CLASS_NAME, ex);
			}
			def.setSource(source);
			beanDefs.add(registerPostProcessor(registry, def, PERSISTENCE_ANNOTATION_PROCESSOR_BEAN_NAME));
		}

		/*
		 * 如果注册表的beanDefinitionMap缓存中没有"org.springframework.context.event.internalEventListenerProcessor"为name的缓存
		 * 那么创建并且注册一个EventListenerMethodProcessor后处理器的bean定义，用于处理@EventListener注解的后置处理器的bean
		 */
		if (!registry.containsBeanDefinition(EVENT_LISTENER_PROCESSOR_BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition(EventListenerMethodProcessor.class);
			def.setSource(source);
			beanDefs.add(registerPostProcessor(registry, def, EVENT_LISTENER_PROCESSOR_BEAN_NAME));
		}

		/*
		 * 如果注册表的beanDefinitionMap缓存中没有"org.springframework.context.event.internalEventListenerFactory"为name的缓存
		 * 那么创建并且注册一个DefaultEventListenerFactory后处理器的bean定义，用于为使用@EventListener注解的方法而创建ApplicationListener的策略接口
		 */
		if (!registry.containsBeanDefinition(EVENT_LISTENER_FACTORY_BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition(DefaultEventListenerFactory.class);
			def.setSource(source);
			beanDefs.add(registerPostProcessor(registry, def, EVENT_LISTENER_FACTORY_BEAN_NAME));
		}

		return beanDefs;
	}

	/**
	 * 注册相关注解后处理器的bean定义到注册表缓存中
	 * @param registry 注册表
	 * @param definition  注解后处理器的bean定义
	 * @param beanName
	 * @return  当前bean定义的BeanDefinitionHolder
	 */
	private static BeanDefinitionHolder registerPostProcessor(
			BeanDefinitionRegistry registry, RootBeanDefinition definition, String beanName) {

		// 设置role
		definition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		// 注册BeanDefinition到registry
		registry.registerBeanDefinition(beanName, definition);
		// 封装成BeanDefinitionHolder并返回
		return new BeanDefinitionHolder(definition, beanName);
	}

	@Nullable
	private static DefaultListableBeanFactory unwrapDefaultListableBeanFactory(BeanDefinitionRegistry registry) {
		if (registry instanceof DefaultListableBeanFactory) {
			return (DefaultListableBeanFactory) registry;
		}
		else if (registry instanceof GenericApplicationContext) {
			return ((GenericApplicationContext) registry).getDefaultListableBeanFactory();
		}
		else {
			return null;
		}
	}

	/**
	 * 解析其他类上的注解：@Lazy, @Primary, @DependsOn, @Role, @Description
	 */
	public static void processCommonDefinitionAnnotations(AnnotatedBeanDefinition abd) {
		processCommonDefinitionAnnotations(abd, abd.getMetadata());
	}

	/**
	 * 解析其他类上的注解：@Lazy, @Primary, @DependsOn, @Role, @Description
	 * @param abd
	 * @param metadata
	 */
	static void processCommonDefinitionAnnotations(AnnotatedBeanDefinition abd, AnnotatedTypeMetadata metadata) {
		/*
		 * 尝试获取@Lazy注解，设置lazyInit属性的值为value的值，设置是否延迟加载，即XML的lazy-init属性
		 */
		AnnotationAttributes lazy = attributesFor(metadata, Lazy.class);
		if (lazy != null) {
			abd.setLazyInit(lazy.getBoolean("value"));
		}
		else if (abd.getMetadata() != metadata) {
			lazy = attributesFor(abd.getMetadata(), Lazy.class);
			if (lazy != null) {
				abd.setLazyInit(lazy.getBoolean("value"));
			}
		}

		// 解析@Primary注解，自动装配时当出现多个bean都匹配时，被注解为@Primary的bean作为首选项，即XML的primary属性
		if (metadata.isAnnotated(Primary.class.getName())) {
			abd.setPrimary(true);
		}
		/*
		 * 尝试获取@DependsOn注解，设置dependsOn属性的值为value的数组值，即XML的depends-on属性
		 * 容器会确保在实例化该bean之前实例化所有依赖的bean
		 */
		AnnotationAttributes dependsOn = attributesFor(metadata, DependsOn.class);
		if (dependsOn != null) {
			abd.setDependsOn(dependsOn.getStringArray("value"));
		}

		/*
		 * 尝试获取@Role注解，设置role属性的值为value的值
		 */
		AnnotationAttributes role = attributesFor(metadata, Role.class);
		if (role != null) {
			abd.setRole(role.getNumber("value").intValue());
		}

		/*
		 * 尝试获取@Description注解，设置description属性的值为value的值，即XML的description标签
		 */
		AnnotationAttributes description = attributesFor(metadata, Description.class);
		if (description != null) {
			abd.setDescription(description.getString("value"));
		}
	}

	static BeanDefinitionHolder applyScopedProxyMode(
			ScopeMetadata metadata, BeanDefinitionHolder definition, BeanDefinitionRegistry registry) {

		ScopedProxyMode scopedProxyMode = metadata.getScopedProxyMode();
		// 如果不需要创建代理，则直接返回bean定义
		if (scopedProxyMode.equals(ScopedProxyMode.NO)) {
			return definition;
		}
		// 判断是使用基于类的代理还是基于接口的代理，
		boolean proxyTargetClass = scopedProxyMode.equals(ScopedProxyMode.TARGET_CLASS);
		// 使用相应的代理模式，创建一个scope代理
		return ScopedProxyCreator.createScopedProxy(definition, registry, proxyTargetClass);
	}

	@Nullable
	static AnnotationAttributes attributesFor(AnnotatedTypeMetadata metadata, Class<?> annotationClass) {
		return attributesFor(metadata, annotationClass.getName());
	}

	@Nullable
	static AnnotationAttributes attributesFor(AnnotatedTypeMetadata metadata, String annotationClassName) {
		return AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(annotationClassName, false));
	}

	static Set<AnnotationAttributes> attributesForRepeatable(AnnotationMetadata metadata,
			Class<?> containerClass, Class<?> annotationClass) {

		return attributesForRepeatable(metadata, containerClass.getName(), annotationClass.getName());
	}

	@SuppressWarnings("unchecked")
	static Set<AnnotationAttributes> attributesForRepeatable(
			AnnotationMetadata metadata, String containerClassName, String annotationClassName) {

		Set<AnnotationAttributes> result = new LinkedHashSet<>();

		// Direct annotation present?
		addAttributesIfNotNull(result, metadata.getAnnotationAttributes(annotationClassName, false));

		// Container annotation present?
		Map<String, Object> container = metadata.getAnnotationAttributes(containerClassName, false);
		if (container != null && container.containsKey("value")) {
			for (Map<String, Object> containedAttributes : (Map<String, Object>[]) container.get("value")) {
				addAttributesIfNotNull(result, containedAttributes);
			}
		}

		// Return merged result
		return Collections.unmodifiableSet(result);
	}

	private static void addAttributesIfNotNull(
			Set<AnnotationAttributes> result, @Nullable Map<String, Object> attributes) {

		if (attributes != null) {
			result.add(AnnotationAttributes.fromMap(attributes));
		}
	}

}
