/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.annotation;

import java.lang.annotation.Annotation;
import java.util.Set;
import java.util.regex.Pattern;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.parsing.CompositeComponentDefinition;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.XmlReaderContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AspectJTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

/**
 * Parser for the {@code <context:component-scan/>} element.
 *
 * @author Mark Fisher
 * @author Ramnivas Laddad
 * @author Juergen Hoeller
 * @since 2.5
 */
public class ComponentScanBeanDefinitionParser implements BeanDefinitionParser {

	/**
	 * base-package属性名称常量
	 */
	private static final String BASE_PACKAGE_ATTRIBUTE = "base-package";

	private static final String RESOURCE_PATTERN_ATTRIBUTE = "resource-pattern";

	private static final String USE_DEFAULT_FILTERS_ATTRIBUTE = "use-default-filters";

	/**
	 * annotation-config属性名称常量
	 */
	private static final String ANNOTATION_CONFIG_ATTRIBUTE = "annotation-config";

	private static final String NAME_GENERATOR_ATTRIBUTE = "name-generator";

	private static final String SCOPE_RESOLVER_ATTRIBUTE = "scope-resolver";

	private static final String SCOPED_PROXY_ATTRIBUTE = "scoped-proxy";

	private static final String EXCLUDE_FILTER_ELEMENT = "exclude-filter";

	private static final String INCLUDE_FILTER_ELEMENT = "include-filter";

	/**
	 * <include-filter/>和<exclude-filter/>标签的type属性名称常量
	 * 用于指定filter的类型
	 */
	private static final String FILTER_TYPE_ATTRIBUTE = "type";

	/**
	 * <include-filter/>和<exclude-filter/>标签的expression属性名称常量
	 * 用于指定filter的class
	 */
	private static final String FILTER_EXPRESSION_ATTRIBUTE = "expression";


	/**
	 *
	 * @param element the element that is to be parsed into one or more {@link BeanDefinition BeanDefinitions}  标签元素节点
	 * @param parserContext the object encapsulating the current state of the parsing process;
	 * provides access to a {@link org.springframework.beans.factory.support.BeanDefinitionRegistry} 解析上下文
	 */
	@Override
	@Nullable
	public BeanDefinition parse(Element element, ParserContext parserContext) {
		// 获取<context:component-scan>节点的base-package属性值
		String basePackage = element.getAttribute(BASE_PACKAGE_ATTRIBUTE);
		// 解析占位符
		basePackage = parserContext.getReaderContext().getEnvironment().resolvePlaceholders(basePackage);
		// 解析base-package(允许通过,;\t\n中的任一符号填写多个)
		String[] basePackages = StringUtils.tokenizeToStringArray(basePackage,
				ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);

		// Actually scan for bean definitions and register them.
		// 构建和配置ClassPathBeanDefinitionScanner
		ClassPathBeanDefinitionScanner scanner = configureScanner(parserContext, element);
		// 使用scanner在执行的basePackages包中执行扫描，返回已注册的bean定义
		Set<BeanDefinitionHolder> beanDefinitions = scanner.doScan(basePackages);
		// 组件注册（包括注册一些内部的注解后置处理器，触发注册事件）
		//主要用于解析对应的注解，对Spring容器里已注册的bean进行装配、依赖注入，甚至添加新的bean（比如@Bean注解）
		registerComponents(parserContext.getReaderContext(), beanDefinitions, element);

		return null;
	}

	/**
	 * 构建和配置ClassPathBeanDefinitionScanner
	 * @param parserContext 解析上下文
	 * @param element 标签元素节点
	 * @return 返回一个配置扫描器ClassPathBeanDefinitionScanner
	 */
	protected ClassPathBeanDefinitionScanner configureScanner(ParserContext parserContext, Element element) {
		// 解析use-default-filters属性，默认是true，用于指示是否使用默认的filter
		boolean useDefaultFilters = true;
		//如果设置了USE_DEFAULT_FILTERS_ATTRIBUTE属性，这个属性是非必需的
		if (element.hasAttribute(USE_DEFAULT_FILTERS_ATTRIBUTE)) {
			//设置useDefaultFilters的值
			useDefaultFilters = Boolean.parseBoolean(element.getAttribute(USE_DEFAULT_FILTERS_ATTRIBUTE));
		}

		// Delegate bean definition registration to scanner class.
		// 构建ClassPathBeanDefinitionScanner,将bean定义注册委托给scanner类
		ClassPathBeanDefinitionScanner scanner = createScanner(parserContext.getReaderContext(), useDefaultFilters);
		scanner.setBeanDefinitionDefaults(parserContext.getDelegate().getBeanDefinitionDefaults());
		scanner.setAutowireCandidatePatterns(parserContext.getDelegate().getAutowireCandidatePatterns());

		// 解析resource-pattern属性，这个属性是非必需的
		if (element.hasAttribute(RESOURCE_PATTERN_ATTRIBUTE)) {
			//设置该属性的值
			scanner.setResourcePattern(element.getAttribute(RESOURCE_PATTERN_ATTRIBUTE));
		}

		try {
			// 解析name-generator属性
			parseBeanNameGenerator(element, scanner);
		}
		catch (Exception ex) {
			parserContext.getReaderContext().error(ex.getMessage(), parserContext.extractSource(element), ex.getCause());
		}

		try {
			// 解析scope-resolver、scoped-proxy属性
			parseScope(element, scanner);
		}
		catch (Exception ex) {
			parserContext.getReaderContext().error(ex.getMessage(), parserContext.extractSource(element), ex.getCause());
		}

		// 解析类型过滤器，解析include-filter和exclude-filter子标签
		parseTypeFilters(element, scanner, parserContext);
		//返回scanner
		return scanner;
	}

	/**
	 * 创建ClassPathBeanDefinitionScanner
	 * @param readerContext  reader上下文
	 * @param useDefaultFilters 是否使用DefaultFilters，默认传递true
	 * @return 一个ClassPathBeanDefinitionScanner对象
	 */
	protected ClassPathBeanDefinitionScanner createScanner(XmlReaderContext readerContext, boolean useDefaultFilters) {
		return new ClassPathBeanDefinitionScanner(readerContext.getRegistry(), useDefaultFilters,
				readerContext.getEnvironment(), readerContext.getResourceLoader());
	}

	/**
	 * 注册一些组件，主要就是注册一些注解后处理器
	 * 用于通过注解对spring容器里已注册的bean进行装配、依赖注入，甚至添加新的bean（比如@Bean注解）
	 * @param readerContext   reader上下文
	 * @param beanDefinitions  已注册的beanDefinition集合
	 * @param element   标签元素节点
	 */
	protected void registerComponents(
			XmlReaderContext readerContext, Set<BeanDefinitionHolder> beanDefinitions, Element element) {
		//使用标签的名称和source构建一个CompositeComponentDefinition
		Object source = readerContext.extractSource(element);
		CompositeComponentDefinition compositeDef = new CompositeComponentDefinition(element.getTagName(), source);

		//将每一个注册的beanDefHolder封装成为BeanComponentDefinition，并添加到compositeDef的nestedComponents集合属性中
		for (BeanDefinitionHolder beanDefHolder : beanDefinitions) {
			compositeDef.addNestedComponent(new BeanComponentDefinition(beanDefHolder));
		}

		/*
		 * 如有必要，注册注解处理器
		 * annotationConfig表示是否开启class内部的注解配置支持的标志位，用于通过注解对Spring容器里已注册的bean进行装配、依赖注入，默认true
		 *
		 * 这就是<context:annotation-config/> 标签的功能，在<context:component-scan/>标签中的annotation-config属性也有这个功能
		 * 因此<context:component-scan/>标签具有<context:annotation-config/> 标签的全部功能，所以建议直接配置<context:component-scan/>标签就行了
		 */

		// Register annotation config processors, if necessary.
		boolean annotationConfig = true;
		//如果<context:component-scan/>标签设置了annotation-config属性，默认设置了值为true
		if (element.hasAttribute(ANNOTATION_CONFIG_ATTRIBUTE)) {
			// 获取component-scan标签的annotation-config属性值，默认为true
			annotationConfig = Boolean.parseBoolean(element.getAttribute(ANNOTATION_CONFIG_ATTRIBUTE));
		}
		//如果开启class内部的注解配置支持
		if (annotationConfig) {
			// 如果annotation-config属性值为true，在给定的注册表中注册所有用于注解的bean后置处理器
			Set<BeanDefinitionHolder> processorDefinitions =
					AnnotationConfigUtils.registerAnnotationConfigProcessors(readerContext.getRegistry(), source);
			/*将每一个注册的注解配置后处理器的BeanDefinition添加到compositeDef的nestedComponents属性中*/
			for (BeanDefinitionHolder processorDefinition : processorDefinitions) {
				// 将注册的注解后置处理器的BeanDefinition添加到compositeDef的nestedComponents属性中
				compositeDef.addNestedComponent(new BeanComponentDefinition(processorDefinition));
			}
		}

		/*触发组件注册事件，通知相关的监听器，默认实现为EmptyReaderEventListener，这是一个空实现，留给子类扩展*/
		readerContext.fireComponentRegistered(compositeDef);
	}

	protected void parseBeanNameGenerator(Element element, ClassPathBeanDefinitionScanner scanner) {
		if (element.hasAttribute(NAME_GENERATOR_ATTRIBUTE)) {
			BeanNameGenerator beanNameGenerator = (BeanNameGenerator) instantiateUserDefinedStrategy(
					element.getAttribute(NAME_GENERATOR_ATTRIBUTE), BeanNameGenerator.class,
					scanner.getResourceLoader().getClassLoader());
			scanner.setBeanNameGenerator(beanNameGenerator);
		}
	}

	protected void parseScope(Element element, ClassPathBeanDefinitionScanner scanner) {
		// Register ScopeMetadataResolver if class name provided.
		if (element.hasAttribute(SCOPE_RESOLVER_ATTRIBUTE)) {
			if (element.hasAttribute(SCOPED_PROXY_ATTRIBUTE)) {
				throw new IllegalArgumentException(
						"Cannot define both 'scope-resolver' and 'scoped-proxy' on <component-scan> tag");
			}
			ScopeMetadataResolver scopeMetadataResolver = (ScopeMetadataResolver) instantiateUserDefinedStrategy(
					element.getAttribute(SCOPE_RESOLVER_ATTRIBUTE), ScopeMetadataResolver.class,
					scanner.getResourceLoader().getClassLoader());
			scanner.setScopeMetadataResolver(scopeMetadataResolver);
		}

		if (element.hasAttribute(SCOPED_PROXY_ATTRIBUTE)) {
			String mode = element.getAttribute(SCOPED_PROXY_ATTRIBUTE);
			if ("targetClass".equals(mode)) {
				scanner.setScopedProxyMode(ScopedProxyMode.TARGET_CLASS);
			}
			else if ("interfaces".equals(mode)) {
				scanner.setScopedProxyMode(ScopedProxyMode.INTERFACES);
			}
			else if ("no".equals(mode)) {
				scanner.setScopedProxyMode(ScopedProxyMode.NO);
			}
			else {
				throw new IllegalArgumentException("scoped-proxy only supports 'no', 'interfaces' and 'targetClass'");
			}
		}
	}

	/**
	 *  解析<include-filter/>和<exclude-filter/>类型过滤器标签
	 *
	 * @param element 标签元素节点
	 * @param scanner scanner
	 * @param parserContext 解析上下文
	 */
	protected void parseTypeFilters(Element element, ClassPathBeanDefinitionScanner scanner, ParserContext parserContext) {
		// Parse exclude and include filter elements.
		//解析、排除和包含过滤器元素。
		ClassLoader classLoader = scanner.getResourceLoader().getClassLoader();
		//获取element的所有子节点
		NodeList nodeList = element.getChildNodes();
		// 遍历解析element下的所有节点
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			//如果该节点属性Element标签节点
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				// 拿到节点的localName
				String localName = parserContext.getDelegate().getLocalName(node);
				try {
					// 解析include-filter子节点
					if (INCLUDE_FILTER_ELEMENT.equals(localName)) {
						//解析<include-filter/>标签为一个TypeFilter
						TypeFilter typeFilter = createTypeFilter((Element) node, classLoader, parserContext);
						// 添加到scanner的includeFilters属性
						scanner.addIncludeFilter(typeFilter);
					}
					// 解析exclude-filter子节点
					else if (EXCLUDE_FILTER_ELEMENT.equals(localName)) {
						// 构建TypeFilter
						TypeFilter typeFilter = createTypeFilter((Element) node, classLoader, parserContext);
						// 添加到scanner的excludeFilters集合头部
						scanner.addExcludeFilter(typeFilter);
					}
				}
				catch (ClassNotFoundException ex) {
					parserContext.getReaderContext().warning(
							"Ignoring non-present type filter class: " + ex, parserContext.extractSource(element));
				}
				catch (Exception ex) {
					parserContext.getReaderContext().error(
							ex.getMessage(), parserContext.extractSource(element), ex.getCause());
				}
			}
		}
	}

	/**
	 * 创建一个TypeFilter
	 *
	 * @param element   <include-filter/>和<exclude-filter/>标签元素节点
	 * @param classLoader 类加载器
	 * @param parserContext 解析上下文
	 * @return  一个TypeFilter
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	protected TypeFilter createTypeFilter(Element element, @Nullable ClassLoader classLoader,
			ParserContext parserContext) throws ClassNotFoundException {

		// 获取type、expression 属性值
		String filterType = element.getAttribute(FILTER_TYPE_ATTRIBUTE);
		String expression = element.getAttribute(FILTER_EXPRESSION_ATTRIBUTE);
		//使用环境变量对象的 resolvePlaceholders 方法来解析 expression 字符串
		//这说明 expression 也可以使用占位符${..:..}，这一步就是解析替换占位符，并且只会使用environment中的属性。
		expression = parserContext.getReaderContext().getEnvironment().resolvePlaceholders(expression);

		/*
		 * 根据filterType的值创建指定类型的类型过滤器
		 */

		//如果filterType等于annotation，表明是注解的类型过滤器
		if ("annotation".equals(filterType)) {
			// expression应该是注解的全路径名，通过该注解匹配类或者接口（及其子类、子接口）
			//之前讲的默认类型过滤器：@Component、@ManagedBean、@Named过滤器都是属于AnnotationTypeFilter
			return new AnnotationTypeFilter((Class<Annotation>) ClassUtils.forName(expression, classLoader));
		}
		//如果filterType等于assignable，表明是类或者接口的类型过滤器
		else if ("assignable".equals(filterType)) {
			// 指定过滤的类或接口，包括子类和子接口，expression为完全限定名
			return new AssignableTypeFilter(ClassUtils.forName(expression, classLoader));
		}
		//如果filterType等于aspectj，表明是类或者接口的类型过滤器
		else if ("aspectj".equals(filterType)) {
			// 指定aspectj表达式来过滤类，expression为aspectj表达式字符串
			return new AspectJTypeFilter(expression, classLoader);
		}
		//如果filterType等于regex，表明是类或者接口的类型过滤器
		else if ("regex".equals(filterType)) {
			// 通过正则表达式来过滤雷，expression为正则表达式字符串
			return new RegexPatternTypeFilter(Pattern.compile(expression));
		}
		//如果filterType等于custom，表明是自定义的类型过滤器
		else if ("custom".equals(filterType)) {
			// 用户自定义过滤器类型，expression为自定义过滤器的完全限定名
			Class<?> filterClass = ClassUtils.forName(expression, classLoader);
			// 自定义的过滤器必须实现TypeFilter接口，否则抛异常
			if (!TypeFilter.class.isAssignableFrom(filterClass)) {
				throw new IllegalArgumentException(
						"Class is not assignable to [" + TypeFilter.class.getName() + "]: " + expression);
			}
			//创建一个自定义类型实例
			return (TypeFilter) BeanUtils.instantiateClass(filterClass);
		}
		else {
			throw new IllegalArgumentException("Unsupported filter type: " + filterType);
		}
	}

	@SuppressWarnings("unchecked")
	private Object instantiateUserDefinedStrategy(
			String className, Class<?> strategyType, @Nullable ClassLoader classLoader) {

		Object result;
		try {
			result = ReflectionUtils.accessibleConstructor(ClassUtils.forName(className, classLoader)).newInstance();
		}
		catch (ClassNotFoundException ex) {
			throw new IllegalArgumentException("Class [" + className + "] for strategy [" +
					strategyType.getName() + "] not found", ex);
		}
		catch (Throwable ex) {
			throw new IllegalArgumentException("Unable to instantiate class [" + className + "] for strategy [" +
					strategyType.getName() + "]: a zero-argument constructor is required", ex);
		}

		if (!strategyType.isAssignableFrom(result.getClass())) {
			throw new IllegalArgumentException("Provided class name must be an implementation of " + strategyType);
		}
		return result;
	}

}
