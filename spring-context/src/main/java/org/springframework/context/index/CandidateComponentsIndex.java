/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.index;

import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.ClassUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Provide access to the candidates that are defined in {@code META-INF/spring.components}.
 *
 * <p>An arbitrary number of stereotypes can be registered (and queried) on the index: a
 * typical example is the fully qualified name of an annotation that flags the class for
 * a certain use case. The following call returns all the {@code @Component}
 * <b>candidate</b> types for the {@code com.example} package (and its sub-packages):
 * <pre class="code">
 * Set&lt;String&gt; candidates = index.getCandidateTypes(
 *         "com.example", "org.springframework.stereotype.Component");
 * </pre>
 *
 * <p>The {@code type} is usually the fully qualified name of a class, though this is
 * not a rule. Similarly, the {@code stereotype} is usually the fully qualified name of
 * a target type but it can be any marker really.
 *
 * @author Stephane Nicoll
 * @since 5.0
 */
public class CandidateComponentsIndex {

	/**
	 * Ant路径匹配器，用来支持basePackage的模式匹配
	 */
	private static final AntPathMatcher pathMatcher = new AntPathMatcher(".");

	/**
	 * 存放格式化之后的bean组件信息
	 */
	private final MultiValueMap<String, Entry> index;


	/**
	 * doLoadIndex方法调用的构造器
	 *
	 * @param content Properties的list集合，有几个文件就有几个Properties
	 */
	CandidateComponentsIndex(List<Properties> content) {
		//调用parseIndex方法
		this.index = parseIndex(content);
	}

	private static MultiValueMap<String, Entry> parseIndex(List<Properties> content) {
		MultiValueMap<String, Entry> index = new LinkedMultiValueMap<>();
		//遍历Properties
		for (Properties entry : content) {
			entry.forEach((type, values) -> {
				//按照","拆分value为stereotypes数组
				String[] stereotypes = ((String) values).split(",");
				//遍历stereotypes数组
				for (String stereotype : stereotypes) {
					//将stereotype作为key，一个新Entry作为value加入到index映射中，这里的Entry是CandidateComponentsIndex的一个内部类
					//注意，由于是LinkedMultiValueMap类型的映射，它非常特别，对于相同的key，它的value不会被替换，而是采用一个LinkedList将value都保存起来
					//比如，如果有两个键值对，key都为a，value分别为b、c，那么添加两个键值对之后，map中仍然只有一个键值对，key为a，但是value是一个LinkedList，内部有两个值，即b和c
					index.add(stereotype, new Entry((String) type));
				}
			});
		}
		return index;
	}


	/**
	 * 返回满足条件的bean类型（全路径类名）
	 *
	 * Return the candidate types that are associated with the specified stereotype.
	 * @param basePackage the package to check for candidates 要检查的包路径
	 * @param stereotype the stereotype to use 要使用的类型
	 * @return the candidate types associated with the specified {@code stereotype}
	 * or an empty set if none has been found for the specified {@code basePackage}
	 */
	public Set<String> getCandidateTypes(String basePackage, String stereotype) {
		//获取使用指定注解或者类(接口)的全路径名的作为key的value集合
		List<Entry> candidates = this.index.get(stereotype);
		//如果candidates不为null
		if (candidates != null) {
			//使用lambda的并行流处理，如果当前bean类型属于指定的包路径中，则表示满足条件，并且收集到set集合中
			return candidates.parallelStream()
					//匹配包路径
					.filter(t -> t.match(basePackage))
					//获取type，实际上就是文件的key，即bean组件的类的全路径类名
					.map(t -> t.type)
					.collect(Collectors.toSet());
		}
		//返回空集合
		return Collections.emptySet();
	}


	private static class Entry {

		/**
		 * type就是spring.components文件中的key，即要注册的类的全限定名
		 */
		private final String type;

		private final String packageName;

		/**
		 * 构造器
		 *
		 * @param type
		 */
		Entry(String type) {
			//为type赋值
			this.type = type;
			//获取包路径名，即出去类名之后的路径
			this.packageName = ClassUtils.getPackageName(type);
		}

		/**
		 * 匹配basePackage
		 *
		 * @param basePackage basePackage包路径
		 * @return true以匹配，false未匹配
		 */
		public boolean match(String basePackage) {
			/*
			 * 如果basePackage存在模式字符，比如? * ** ，那么使用pathMatcher来匹配
			 */
			if (pathMatcher.isPattern(basePackage)) {
				return pathMatcher.match(basePackage, this.packageName);
			}
			/*
			 * 否则，表示basePackage就是一个固定的字符串，那么直接看type是否是以basePackage为起始路径即可
			 */
			else {
				return this.type.startsWith(basePackage);
			}
		}
	}

}
