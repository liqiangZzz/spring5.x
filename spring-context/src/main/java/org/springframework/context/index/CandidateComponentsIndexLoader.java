/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.index;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.core.SpringProperties;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.ConcurrentReferenceHashMap;

/**
 * Candidate components index loading mechanism for internal use within the framework.
 *
 * @author Stephane Nicoll
 * @since 5.0
 */
public final class CandidateComponentsIndexLoader {

	/**
	 * 组件索引文件的默认资源路径。
	 * Spring 会在类路径下的该路径中查找组件索引文件（通常为 `META-INF/spring.components`）。
	 * 该文件用于存储组件类的元数据，以加速组件扫描过程。
	 *
	 * <p>
	 * 组件索引文件可以存在于多个 JAR 文件中，Spring 会加载所有匹配的资源。
	 * </p>
	 *
	 * <p>
	 * 组件索引文件是一个属性文件（Properties），其格式为：
	 * <pre>
	 * com.example.MyService=org.springframework.stereotype.Component
	 * com.example.MyRepository=org.springframework.stereotype.Repository
	 * </pre>
	 * 其中，键为组件类的全限定名，值为组件类的注解类型。
	 * </p>
	 *
	 * <p>
	 * 通过使用组件索引文件，Spring 可以在启动时快速定位组件类，而无需扫描整个类路径。
	 * </p>
	 *
	 * The location to look for components.
	 * <p>Can be present in multiple JAR files.
	 */
	public static final String COMPONENTS_RESOURCE_LOCATION = "META-INF/spring.components";

	/**
	 * 系统属性，指示 Spring 是否忽略组件索引。
	 * 如果设置为 true，则 {@link #loadIndex(ClassLoader)} 方法将始终返回 {@code null}。
	 * <p>
	 * 默认值为 "false"，允许正常使用组件索引。将此标志切换为 {@code true} 适用于以下特殊情况：
	 * 当某些库（或某些用例）部分可用索引，但无法为整个应用程序构建索引时，应用程序上下文将回退到常规的类路径安排
	 * （即，就像根本没有索引一样）。
	 * </p>
	 * 例如，在调试或测试环境中，可能需要跳过索引文件的加载，此时可以将此属性设置为 true。
	 * </p>
	 * System property that instructs Spring to ignore the index, i.e.
	 * to always return {@code null} from {@link #loadIndex(ClassLoader)}.
	 * <p>The default is "false", allowing for regular use of the index. Switching this
	 * flag to {@code true} fulfills a corner case scenario when an index is partially
	 * available for some libraries (or use cases) but couldn't be built for the whole
	 * application. In this case, the application context fallbacks to a regular
	 * classpath arrangement (i.e. as no index was present at all).
	 */
	public static final String IGNORE_INDEX = "spring.index.ignore";

	/**
	 *用于决定是否忽略组件索引文件的加载。如果为 true，则跳过加载索引文件；如果为 false，则正常加载。
	 */
	private static final boolean shouldIgnoreIndex = SpringProperties.getFlag(IGNORE_INDEX);

	private static final Log logger = LogFactory.getLog(CandidateComponentsIndexLoader.class);

	private static final ConcurrentMap<ClassLoader, CandidateComponentsIndex> cache =
			new ConcurrentReferenceHashMap<>();


	private CandidateComponentsIndexLoader() {
	}


	/**
	 * 从默认的文件路径"META-INF/spring.components"加载索引文件成为一个CandidateComponentsIndex实例并返回
	 * 如果没有找到任何文件或者文件中没有定义组件，则返回null
	 *
	 * Load and instantiate the {@link CandidateComponentsIndex} from
	 * {@value #COMPONENTS_RESOURCE_LOCATION}, using the given class loader. If no
	 * index is available, return {@code null}.
	 * @param classLoader the ClassLoader to use for loading (can be {@code null} to use the default)  给定的类加载器
	 * @return the index to use or {@code null} if no index was found  将要使用的CandidateComponentsIndex实例，如果没有找到索引文件则返回null
	 * @throws IllegalArgumentException if any module index cannot
	 * be loaded or if an error occurs while creating {@link CandidateComponentsIndex}
	 */
	@Nullable
	public static CandidateComponentsIndex loadIndex(@Nullable ClassLoader classLoader) {
		// 如果传入的 ClassLoader 为 null，则使用当前类的 ClassLoader
		ClassLoader classLoaderToUse = classLoader;
		if (classLoaderToUse == null) {
			classLoaderToUse = CandidateComponentsIndexLoader.class.getClassLoader();
		}
		// 使用缓存机制，避免重复加载索引文件
		// 如果缓存中已经存在该 ClassLoader 对应的索引，则直接返回；否则调用 doLoadIndex 方法加载索引
		return cache.computeIfAbsent(classLoaderToUse, CandidateComponentsIndexLoader::doLoadIndex);
	}

	@Nullable
	private static CandidateComponentsIndex doLoadIndex(ClassLoader classLoader) {
		// 如果配置为忽略索引文件，则直接返回 null
		if (shouldIgnoreIndex) {
			return null;
		}

		try {
			// 加载所有组件索引文件（默认路径为 META-INF/spring.components）
			Enumeration<URL> urls = classLoader.getResources(COMPONENTS_RESOURCE_LOCATION);
			//如果没有加载到组件索引文件，直接返回null
			if (!urls.hasMoreElements()) {
				return null;
			}
			// 用于存储从索引文件中加载的属性
			List<Properties> result = new ArrayList<>();
			// 遍历所有索引文件
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				// 加载索引文件中的属性
				Properties properties = PropertiesLoaderUtils.loadProperties(new UrlResource(url));
				result.add(properties);
			}
			// 如果启用了调试日志，记录加载的索引文件数量
			if (logger.isDebugEnabled()) {
				logger.debug("Loaded " + result.size() + "] index(es)");
			}
			// 计算所有索引文件中的属性总数
			int totalCount = result.stream().mapToInt(Properties::size).sum();
			// 如果没有从组件索引文件中加载到属性，返回 null；否则创建并返回 CandidateComponentsIndex 实例
			return (totalCount > 0 ? new CandidateComponentsIndex(result) : null);
		}
		catch (IOException ex) {
			// 如果加载过程中发生 IO 异常，抛出 IllegalStateException
			throw new IllegalStateException("Unable to load indexes from location [" +
					COMPONENTS_RESOURCE_LOCATION + "]", ex);
		}
	}

}
